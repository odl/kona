/**
 * \file reduced_kkt_vector.hpp
 * \brief header file for the ReducedKKTVector and related classes
 * \author Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#pragma once

#include "./vectors.hpp"
#include "./vector_operators.hpp"

namespace kona {

// forward declarations
class QuasiNewton;
class LimitedMemoryBFGS;
class ReducedKKTVector;

// ======================================================================
/*!
 * \class ReducedKKTProduct
 * \brief specialization of matrix-vector product for reduced KKT matrix
 */
class ReducedKKTProduct : public MatrixVectorProduct<ReducedKKTVector> {
 public:

  /*!
   * \brief constructor for class
   * \param[in/out] outstream - ostream object to write output to
   */
  ReducedKKTProduct(ostream& outstream = cout);
  
  /*!
   * \brief constructor for class
   * \param[in] eval_at_kkt - (design,dual) vectors to evaluate KKT-matrix at
   * \param[in] eval_at_state - state to evaluate KKT-matrix at
   * \param[in] eval_at_adjoint - adjoint to evaluate KKT-matrix at
   * \param[in] reduced_opt - reduced optimality conditions at eval_at_*
   * \param[in] adjoint_res - the adjoint equation residual at eval_at_*
   * \param[out] design_work1 - design work space
   * \param[out] design_work2 - design work space
   * \param[out] pde_work1 - state or adjoint work space
   * \param[out] pde_work2 - state or adjoint work space
   * \param[out] pde_work3 - state or adjoint work space
   * \param[out] pde_work4 - state or adjoint work space
   * \param[out] pde_work5 - state or adjoint work space
   * \param[out] dual_work1 - dual work space
   * \param[in] product_fac - scales product_tol_ which is used in adjoint tols.
   * \param[in/out] outstream - ostream object to write output to
   * \param[in] lambda - continuation parameter for globalization
   * \param[in] scale - scaling for globalization terms
   */
  ReducedKKTProduct(
      const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
      const PDEVector & eval_at_adjoint, ReducedKKTVector & reduced_opt,
      const PDEVector & adjoint_res, DesignVector & design_work1,
      DesignVector & design_work2, PDEVector & pde_work1,
      PDEVector & pde_work2, PDEVector & pde_work3,
      PDEVector & pde_work4, PDEVector & pde_work5, DualVector & dual_work1,
      const double & product_fac, ostream & outstream,
      const double & lambda = 0.0, const double & scale = 1.0,
      const double & grad_scale = 1.0, const double & ceq_scale = 1.0);

  /*!
   * \brief destructor for class
   */
  ~ReducedKKTProduct();

  /*!
   * \brief returns the user-vector memory requirements
   * \param[in,out] num_required - stores the memory requirements
   *
   * On exit, num_required holds three elements with labels "design", "state",
   * and "dual"; thus, one each for the number of DesignVectors, PDEVectors, and
   * DualVectors, respectively.
   */
  void MemoryRequired(ptree& num_required);
  
  /*!
   * \brief defines the QuasiNewton object that should be updated during products
   * \param[in/out] quasi_newton - quasi-Newtom object that will be updated
   */
  void set_quasi_newton(QuasiNewton * quasi_newton);

  /*!
   * \brief prepares the operator for use
   * \param[in] eval_at_kkt - (primal,dual) state to evaluate KKT matrix at
   * \param[in] eval_at_state - state to evaluate KKT matrix at
   * \param[in] eval_at_adjoint - adjoint to evaluate KKT matrix at
   * \param[in] reduced_opt - current (primal,dual) optimality at eval_at_*
   * \param[in] adjoint_res - the adjoint equation residual at eval_at_*
   * \param[out] design_work - array of DesignVectors for work
   * \param[out] pde_work - array of PDEVectors for work
   * \param[out] dual_work - array of DualVectors for work
   * \param[in] prod_param - input parameters
   * \param[in/out] outstream - ostream object to write output to
   *
   * This is the counterpart to the long version of the constructor above.  This
   * is called when the product is not rebuilt each iteration, yet various
   * quantities must still be redefined.
   */
  void Initialize(
      const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
      const PDEVector & eval_at_adjoint, ReducedKKTVector & reduced_opt, 
      const PDEVector & adjoint_res, std::vector<DesignVector>& design_work,
      std::vector<PDEVector>& pde_work, std::vector<DualVector>& dual_work,
      const ptree& prod_param, ostream & outstream);

  /*!
   * \brief sets product parameters
   * \param[in] prod_param - input parameters
   *
   * This version is primarily used for changing product paramters during a
   * solve.  For example, switching to an augmented system matrix.
   */
  void Initialize(const ptree& prod_param);
  
#if 0
  /*!
   * \brief defines the desired accuracy of the KKT-matrix-vector product
   * \param[in] product_tol - the desired accuracy
   */
  void set_product_tol(const double & product_tol);

  /*!
   * \brief computes the (estimate) of the 2-norm of the sensitivity matrix
   * \returns the estimate of the 2-norm (>0)
   *
   * Here, the sensitivity matrix refers to |du/dx^{T} dpsi/dx^{T}|, where u are
   * the state variables and psi are the adjoint variables.
   * \param[in] approx - if true, approximate primal/dual with precond.
   */
  double compute_sens_norm(const bool & approx = false);

  /*!
   * \brief computes the (estimate) of the 2-norm of the sensitivity matrices
   * \param[out] dudx_norm - the estimate of the 2-norm of dudx
   * \param[out] dpsidx_norm - the estimate of the 2-norm of dpsidx
   * \param[in] approx - if true, approximate primal/dual with precond.
   */
  void compute_sens_norms(double & dudx_norm, double & dpsidx_norm,
                          const bool & approx = false);
      
  /*!
   * \brief defines the (estimate) of the 2-norm of the sensitivity matrix
   * \param[in] sens_norm - the estimate of the 2-norm (>0)
   */
  void set_sens_norm(const double & sens_norm);

  /*!
   * \brief defines the (estimates) of the 2-norm of the sensitivity matrices
   * \param[in] dudx_norm = estimate of the 2-norm of dudx
   * \param[in] dpsidx_norm = estimate of the 2-norm of dpsidx
   */
  void set_sens_norms(const double & dudx_norm, const double & dpsidx_norm);
#endif
  
  /*!
   * \brief operator that defines the Reduced-KKT-matrix product
   * \param[in] u - vector that is being multiplied
   * \param[out] v - result of KKT-matrix-vector product
   *
   * if u.design_ is set to zero, this operator computes only the transposed
   * constraint-Jacobian-vector product with u.dual_
   */
  void operator()(const ReducedKKTVector & u, ReducedKKTVector & v);

 protected:

  /*!
   * \brief operator that defines the reduced augmented product
   * \param[in] u - Reduced KKT vector that is being multiplied
   * \param[out] v - result of augmented-matrix-vector product
   *
   * If u.design_ is set to zero, this operator computes only the transposed
   * constraint-Jacobian-vector product with u.dual_.
   */
  void AugmentedProduct(const ReducedKKTVector & u, ReducedKKTVector & v);

   /*!
   * \brief product with the Hessian approximated by a quasi-Newton method
   * \param[in] u - Reduced KKT Vector that is being multiplied
   * \param[out] v - result of matrix-vector product
   *
   * If u.design_ is set to zero, this operator computes only the transposed
   * constraint-Jacobian-vector product with u.dual_.
   */
  void QuasiNewtonProduct(const ReducedKKTVector & u, ReducedKKTVector & v);

  DesignVector const* eval_at_design_; /// design to evaluate KKT-matrix at
  DualVector const* eval_at_dual_; /// dual (Lagrange multipliers) to evaluate at
  double design_norm_; /// L2 norm of eval_at_design_;
  PDEVector const* eval_at_state_; /// state to evaluate KKT-matrix at
  double state_norm_; /// L2 norm of eval_at_state_;
  PDEVector const* eval_at_adjoint_; /// adjoint to evaluate KKT-matrix at
  DesignVector * reduced_grad_; /// the reduced gradient at eval_at_*
  DualVector * ceq_; /// the equality constraints at eval_at_*
  PDEVector const* adjoint_res_; /// the adjoint equation residual at eval_at_*
  DesignVector * pert_design_; /// perturbation to eval_at_design
  DesignVector * design_work1_; /// design work space
  PDEVector * w_adj_; /// the forward problem adjoint
  PDEVector * lambda_adj_; /// the reverse problem adjoint
  PDEVector * pde_work1_; /// state or adjoint work space
  PDEVector * pde_work2_; /// state or adjoint work space
  PDEVector * pde_work3_; /// state or adjoint work space
  DualVector * dual_work_; /// dual work space
  double product_fac_; /// factor that scales product_tol (see next)
  double product_tol_; // dynamic tolerance to solve linear systems in prod
  ostream * outstream_;
  double lambda_; /// continuation parameter for globalization
  double scale_; // scaling factor for globalization term
  double grad_scale_; // equation scaling for Lagrangian gradient
  double ceq_scale_; // equation scaling for equality constraint
  string bound_type_; // method used to bound KKT-matrix-vector product error
  double sens_norm_; // estimate of 2-norm of the sensitivity matrix
  double dudx_norm_; // estimate of 2-norm of the sens. matrix dudx
  double dpsidx_norm_; // estimate of 2-norm of the sens. matrix dpsidx
  QuasiNewton * quasi_newton_; ///< preconditioner for design
  bool augmented_; ///< evaluate the augmented matrix
  bool approx_hessian_; ///< approximate the Lag. Hessian using Quasi-Newton
};

// ======================================================================
/*!
 * \class LagrangianHessianProduct
 * \brief specialization of matrix-vector product for reduced Hessian of Lag.
 */
class LagrangianHessianProduct : public MatrixVectorProduct<DesignVector> {
 public:

  /*!
   * \brief constructor for class
   * \param[in/out] outstream - ostream object to write output to
   */
  LagrangianHessianProduct(ostream& outstream = cout);
  
  /*!
   * \brief constructor for class
   * \param[in] eval_at_kkt - (design,dual) vectors to evaluate Hessian at
   * \param[in] eval_at_state - state to evaluate KKT-matrix at
   * \param[in] eval_at_adjoint - adjoint to evaluate KKT-matrix at
   * \param[in] reduced_grad - gradient of Lagrangian conditions at eval_at_*
   * \param[in] adjoint_res - the adjoint equation residual at eval_at_*
   * \param[out] design_work1 - design work space
   * \param[out] design_work2 - design work space
   * \param[out] pde_work1 - state or adjoint work space
   * \param[out] pde_work2 - state or adjoint work space
   * \param[out] pde_work3 - state or adjoint work space
   * \param[out] pde_work4 - state or adjoint work space
   * \param[out] pde_work5 - state or adjoint work space
   * \param[in] product_fac - scales product_tol_ which is used in adjoint tols.
   * \param[in/out] outstream - ostream object to write output to
   */
  LagrangianHessianProduct(
      const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
      const PDEVector & eval_at_adjoint, DesignVector & reduced_grad,
      const PDEVector & adjoint_res, DesignVector & design_work1,
      DesignVector & design_work2, PDEVector & pde_work1,
      PDEVector & pde_work2, PDEVector & pde_work3,
      PDEVector & pde_work4, PDEVector & pde_work5,
      const double & product_fac, ostream & outstream);

  /*!
   * \brief destructor for class
   */
  ~LagrangianHessianProduct();

  /*!
   * \brief returns the user-vector memory requirements
   * \param[in,out] num_required - stores the memory requirements
   *
   * On exit, num_required holds three elements with labels "design", "state",
   * and "dual"; thus, one each for the number of DesignVectors, PDEVectors, and
   * DualVectors, respectively.
   */
  void MemoryRequired(ptree& num_required);
  
  /*!
   * \brief defines the QuasiNewton object that should be updated during products
   * \param[in/out] quasi_newton - quasi-Newtom object that will be updated
   */
  void set_quasi_newton(QuasiNewton * quasi_newton);

  /*!
   * \brief prepares the operator for use
   * \param[in] eval_at_kkt - (primal,dual) state to evaluate Hessian at
   * \param[in] eval_at_state - state to evaluate KKT matrix at
   * \param[in] eval_at_adjoint - adjoint to evaluate KKT matrix at
   * \param[in] reduced_grad - current gradient of Lagrangian at eval_at_*
   * \param[in] adjoint_res - the adjoint equation residual at eval_at_*
   * \param[out] design_work - array of DesignVectors for work
   * \param[out] pde_work - array of PDEVectors for work
   * \param[in] prod_param - input parameters
   * \param[in/out] outstream - ostream object to write output to
   *
   * This is the counterpart to the long version of the constructor above.  This
   * is called when the product is not rebuilt each iteration, yet various
   * quantities must still be redefined.
   */
  void Initialize(
      const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
      const PDEVector & eval_at_adjoint, DesignVector & reduced_grad, 
      const PDEVector & adjoint_res, std::vector<DesignVector>& design_work,
      std::vector<PDEVector>& pde_work, const ptree& prod_param,
      ostream & outstream);
  
  /*!
   * \brief operator that defines the (Lagrangian) Hessian-vector product
   * \param[in] u - vector that is being multiplied
   * \param[out] v - result of Hessian-vector product
   */
  void operator()(const DesignVector & u, DesignVector & v);

 protected:
  
  DesignVector const* eval_at_design_; /// design to evaluate KKT-matrix at
  DualVector const* eval_at_dual_; /// dual (Lagrange multipliers) to evaluate at
  double design_norm_; /// L2 norm of eval_at_design_;
  PDEVector const* eval_at_state_; /// state to evaluate KKT-matrix at
  double state_norm_; /// L2 norm of eval_at_state_;
  PDEVector const* eval_at_adjoint_; /// adjoint to evaluate KKT-matrix at
  DesignVector * reduced_grad_; /// the reduced gradient at eval_at_*
  PDEVector const* adjoint_res_; /// the adjoint equation residual at eval_at_*
  DesignVector * pert_design_; /// perturbation to eval_at_design
  DesignVector * design_work1_; /// design work space
  PDEVector * forward_adj_; /// the forward problem adjoint
  PDEVector * reverse_adj_; /// the reverse problem adjoint
  PDEVector * pde_work1_; /// state or adjoint work space
  PDEVector * pde_work2_; /// state or adjoint work space
  PDEVector * pde_work3_; /// state or adjoint work space
  double product_fac_; /// factor that scales product_tol (see next)
  double product_tol_; // dynamic tolerance to solve linear systems in prod
  ostream * outstream_;
  QuasiNewton * quasi_newton_; ///< preconditioner for design
};

// ======================================================================
/*!
 * \class NullSpaceLagrangianProduct
 * \brief specialization of matrix-vector product for reduced Hessian of Lag.
 *
 * This product includes mu*A^T*A, which tends to keep the steps in the null
 * space of the Jacobian
 */
class NullSpaceLagrangianProduct : public MatrixVectorProduct<DesignVector> {
 public:

  /*!
   * \brief constructor for class
   * \param[in/out] outstream - ostream object to write output to
   */
  NullSpaceLagrangianProduct(ostream& outstream = cout);
  
  /*!
   * \brief constructor for class
   * \param[in] eval_at_kkt - (design,dual) vectors to evaluate KKT-matrix at
   * \param[in] eval_at_state - state to evaluate KKT-matrix at
   * \param[in] eval_at_adjoint - adjoint to evaluate KKT-matrix at
   * \param[in] reduced_opt - reduced optimality conditions at eval_at_*
   * \param[in] adjoint_res - the adjoint equation residual at eval_at_*
   * \param[out] design_work1 - design work space
   * \param[out] design_work2 - design work space
   * \param[out] pde_work1 - state or adjoint work space
   * \param[out] pde_work2 - state or adjoint work space
   * \param[out] pde_work3 - state or adjoint work space
   * \param[out] pde_work4 - state or adjoint work space
   * \param[out] pde_work5 - state or adjoint work space
   * \param[out] dual_work1 - dual work space
   * \param[out] dual_work2 - dual work space
   * \param[in] mu - penalty parameter for A^T * A matrix
   * \param[in] product_fac - scales product_tol_ which is used in adjoint tols.
   * \param[in/out] outstream - ostream object to write output to
   */
  NullSpaceLagrangianProduct(
      const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
      const PDEVector & eval_at_adjoint, ReducedKKTVector & reduced_opt,
      const PDEVector & adjoint_res, DesignVector & design_work1,
      DesignVector & design_work2, PDEVector & pde_work1,
      PDEVector & pde_work2, PDEVector & pde_work3,
      PDEVector & pde_work4, PDEVector & pde_work5, DualVector & dual_work1,
      DualVector & dual_work2, const double & mu,
      const double & product_fac, ostream & outstream);

  /*!
   * \brief destructor for class
   */
  ~NullSpaceLagrangianProduct();

  /*!
   * \brief returns the user-vector memory requirements
   * \param[in,out] num_required - stores the memory requirements
   *
   * On exit, num_required holds three elements with labels "design", "state",
   * and "dual"; thus, one each for the number of DesignVectors, PDEVectors, and
   * DualVectors, respectively.
   */
  void MemoryRequired(ptree& num_required);
  
  /*!
   * \brief defines the QuasiNewton object that should be updated during products
   * \param[in/out] quasi_newton - quasi-Newtom object that will be updated
   */
  void set_quasi_newton(QuasiNewton * quasi_newton);

  /*!
   * \brief prepares the operator for use
   * \param[in] eval_at_kkt - (primal,dual) state to evaluate KKT matrix at
   * \param[in] eval_at_state - state to evaluate KKT matrix at
   * \param[in] eval_at_adjoint - adjoint to evaluate KKT matrix at
   * \param[in] reduced_opt - current (primal,dual) optimality at eval_at_*
   * \param[in] adjoint_res - the adjoint equation residual at eval_at_*
   * \param[out] design_work - array of DesignVectors for work
   * \param[out] pde_work - array of PDEVectors for work
   * \param[out] dual_work - array of DualVectors for work
   * \param[in] prod_param - input parameters
   * \param[in/out] outstream - ostream object to write output to
   *
   * This is the counterpart to the long version of the constructor above.  This
   * is called when the product is not rebuilt each iteration, yet various
   * quantities must still be redefined.
   */
  void Initialize(
      const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
      const PDEVector & eval_at_adjoint, ReducedKKTVector & reduced_opt, 
      const PDEVector & adjoint_res, std::vector<DesignVector>& design_work,
      std::vector<PDEVector>& pde_work, std::vector<DualVector>& dual_work,
      const ptree& prod_param, ostream & outstream);
  
  /*!
   * \brief operator that defines the (Lagrangian) Hessian-vector product
   * \param[in] u - vector that is being multiplied
   * \param[out] v - result of Hessian-vector product
   */
  void operator()(const DesignVector & u, DesignVector & v);

 protected:
  
  DesignVector const* eval_at_design_; /// design to evaluate KKT-matrix at
  DualVector const* eval_at_dual_; /// dual (Lagrange multipliers) to evaluate at
  double design_norm_; /// L2 norm of eval_at_design_;
  PDEVector const* eval_at_state_; /// state to evaluate KKT-matrix at
  double state_norm_; /// L2 norm of eval_at_state_;
  PDEVector const* eval_at_adjoint_; /// adjoint to evaluate KKT-matrix at
  DesignVector * reduced_grad_; /// the reduced gradient at eval_at_*
  DualVector * ceq_; /// the equality constraints at eval_at_*
  PDEVector const* adjoint_res_; /// the adjoint equation residual at eval_at_*
  DesignVector * pert_design_; /// perturbation to eval_at_design
  DesignVector * design_work1_; /// design work space
  DualVector * dual_work_; /// dual work space
  DualVector * Au_; /// dual work space
  PDEVector * forward_adj_; /// the forward problem adjoint
  PDEVector * reverse_adj_; /// the reverse problem adjoint
  PDEVector * pde_work1_; /// state or adjoint work space
  PDEVector * pde_work2_; /// state or adjoint work space
  PDEVector * pde_work3_; /// state or adjoint work space
  double mu_; /// penalty parameter for A^T * A matrix
  double product_fac_; /// factor that scales product_tol (see next)
  double product_tol_; // dynamic tolerance to solve linear systems in prod
  ostream * outstream_;
  QuasiNewton * quasi_newton_; ///< preconditioner for design
};

// ======================================================================
/*!
 * \class ReducedAugmentedProduct
 * \brief specialization of matrix-vector product for reduced Augmented system
 */
class ReducedAugmentedProduct : public MatrixVectorProduct<ReducedKKTVector> {
 public:

  /*!
   * \brief constructor for class
   * \param[in/out] outstream - ostream object to write output to
   */
  ReducedAugmentedProduct(ostream& outstream = cout);
  
  /*!
   * \brief constructor for class
   * \param[in] eval_at_kkt - (design,dual) vectors to evaluate augmented-matrix
   * \param[in] eval_at_state - state to evaluate augmented-matrix at
   * \param[out] design_work1 - design work space
   * \param[out] pde_work1 - state or adjoint work space
   * \param[out] pde_work2 - state or adjoint work space
   * \param[out] dual_work1 - dual work space
   * \param[in/out] outstream - ostream object to write output to
   */
  ReducedAugmentedProduct(
      const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
      DesignVector & design_work1, PDEVector & pde_work1, PDEVector & pde_work2,
      DualVector & dual_work1, ostream & outstream);

  /*!
   * \brief destructor for class
   */
  ~ReducedAugmentedProduct();

  /*!
   * \brief returns the user-vector memory requirements
   * \param[in,out] num_required - stores the memory requirements
   *
   * On exit, num_required holds three elements with labels "design", "state",
   * and "dual"; thus, one each for the number of DesignVectors, PDEVectors, and
   * DualVectors, respectively.
   */
  void MemoryRequired(ptree& num_required);
  
  /*!
   * \brief prepares the operator for use
   * \param[in] eval_at_kkt - (primal,dual) state to evaluate augmented matrix at
   * \param[in] eval_at_state - state to evaluate KKT matrix at
   * \param[out] design_work - array of DesignVectors for work
   * \param[out] pde_work - array of PDEVectors for work
   * \param[out] dual_work - array of DualVectors for work
   * \param[in] prod_param - input parameters
   * \param[in/out] outstream - ostream object to write output to
   *
   * This is the counterpart to the long version of the constructor above.  This
   * is called when the product is not rebuilt each iteration, yet various
   * quantities must still be redefined.
   */
  void Initialize(
      const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
      std::vector<DesignVector>& design_work, std::vector<PDEVector>& pde_work,
      std::vector<DualVector>& dual_work, const ptree& prod_param,
      ostream & outstream);

  /*!
   * \brief for use during iterations
   * \param[in] prod_param - input parameters
   */
  void Initialize(const ptree& prod_param);
  
  /*!
   * \brief increases the diagonal of the (1,1) primal block
   * \param[in] diag - value to assign to diagonal
   */
  void set_diagonal(const double & diag);
  
  /*!
   * \brief operator that defines the Reduced-Augmented-matrix product
   * \param[in] u - reduced-KKT-Vector that is being multiplied
   * \param[out] v - result of Augmented-matrix-vector product
   */
  void operator()(const ReducedKKTVector & u, ReducedKKTVector & v);

 protected:
  
  bool solve_ceq_; /// if true, the identity matrix is in the (1,1) block
  bool approx_; /// if true, adjoints are solved using precondtioner operations
  double diag_; /// diagonal matrix coefficient
  DesignVector const* eval_at_design_; /// design to evaluate KKT-matrix at
  DualVector const* eval_at_dual_; /// dual (Lagrange multipliers) to evaluate at
  PDEVector const* eval_at_state_; /// state to evaluate KKT-matrix at
  DesignVector * design_work1_; /// design work space
  PDEVector * adj_; /// the forward problem adjoint
  PDEVector * pde_work1_; /// state or adjoint work space
  DualVector * dual_work_; /// dual work space
  ostream * outstream_;
  boost::scoped_ptr<Preconditioner<PDEVector> >
  primal_precond_; // forward problem preconditioner
  boost::scoped_ptr<Preconditioner<PDEVector> >
  adjoint_precond_; // reverse problem preconditioner  
};

// ======================================================================
/*!
 * \class ApproxReducedKKTProduct
 * \brief specialization of matrix-vector product for reduced KKT matrix
 */
class ApproxReducedKKTProduct : public MatrixVectorProduct<ReducedKKTVector> {
 public:

  /*!
   * \brief constructor for class
   * \param[in/out] outstream - ostream object to write output to
   */
  ApproxReducedKKTProduct(ostream& outstream = cout);
  
  /*!
   * \brief constructor for class
   * \param[in] eval_at_kkt - (primal,dual) state to evaluate approx KKT matrix
   * \param[in] eval_at_state - state to evaluate approx KKT matrix at
   * \param[in] eval_at_adjoint - adjoint to evaluate approx KKT matrix at
   * \param[in] reduced_opt - current (primal,dual) optimality at eval_at_*
   * \param[in] adjoint_res - the adjoint equation residual at eval_at_*
   * \param[out] design_work - array of DesignVectors for work
   * \param[out] pde_work - array of PDEVectors for work
   * \param[out] dual_work - array of DualVectors for work
   * \param[in] prod_param - input parameters
   * \param[in/out] outstream - ostream object to write output to
   */
  ApproxReducedKKTProduct(
      const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
      const PDEVector & eval_at_adjoint, ReducedKKTVector & reduced_opt, 
      const PDEVector & adjoint_res, std::vector<DesignVector>& design_work,
      std::vector<PDEVector>& pde_work, std::vector<DualVector>& dual_work,
      const ptree& prod_param, ostream & outstream);
  
  /*!
   * \brief destructor for class
   */
  ~ApproxReducedKKTProduct();

  /*!
   * \brief returns the user-vector memory requirements
   * \param[in,out] num_required - stores the memory requirements
   *
   * On exit, num_required holds three elements with labels "design", "state",
   * and "dual"; thus, one each for the number of DesignVectors, PDEVectors, and
   * DualVectors, respectively.
   */
  void MemoryRequired(ptree& num_required);
  
  /*!
   * \brief prepares the operator for use
   * \param[in] eval_at_kkt - (primal,dual) state to evaluate approx KKT matrix
   * \param[in] eval_at_state - state to evaluate approx KKT matrix at
   * \param[in] eval_at_adjoint - adjoint to evaluate approx KKT matrix at
   * \param[in] reduced_opt - current (primal,dual) optimality at eval_at_*
   * \param[in] adjoint_res - the adjoint equation residual at eval_at_*
   * \param[out] design_work - array of DesignVectors for work
   * \param[out] pde_work - array of PDEVectors for work
   * \param[out] dual_work - array of DualVectors for work
   * \param[in] prod_param - input parameters
   * \param[in/out] outstream - ostream object to write output to
   *
   * This is the counterpart to the long version of the constructor above.  This
   * is called when the product is not rebuilt each iteration, yet various
   * quantities must still be redefined.
   */
  void Initialize(
      const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
      const PDEVector & eval_at_adjoint, ReducedKKTVector & reduced_opt, 
      const PDEVector & adjoint_res, std::vector<DesignVector>& design_work,
      std::vector<PDEVector>& pde_work, std::vector<DualVector>& dual_work,
      const ptree& prod_param, ostream & outstream);
  
  /*!
   * \brief defines the diagonal matrix added to the Lagrangian Hessian
   * \param[in] lambda - new value for lambda
   */
  void set_lambda(const double & lambda);
  
  /*!
   * \brief operator that defines the approximate reduced-KKT-matrix product
   * \param[in] u - PDEVector that is being multiplied
   * \param[out] v - result of KKT-matrix-vector product
   *
   * if u.design_ is set to zero, this operator computes only the (approx)
   * transposed constraint-Jacobian-vector product with u.dual_
   */
  void operator()(const ReducedKKTVector & u, ReducedKKTVector & v);

 protected:

  DesignVector const* eval_at_design_; /// design to evaluate KKT-matrix at
  DualVector const* eval_at_dual_; /// dual (Lagrange multipliers) to evaluate at
  double design_norm_; /// L2 norm of eval_at_design_;
  PDEVector const* eval_at_state_; /// state to evaluate KKT-matrix at
  double state_norm_; /// L2 norm of eval_at_state_;
  PDEVector const* eval_at_adjoint_; /// adjoint to evaluate KKT-matrix at
  DesignVector * reduced_grad_; /// the reduced gradient at eval_at_*
  DualVector * ceq_; /// the equality constraints at eval_at_*
  PDEVector const* adjoint_res_; /// the adjoint equation residual at eval_at_*
  DesignVector * pert_design_; /// perturbation to eval_at_design
  DesignVector * design_work1_; /// design work space
  PDEVector * w_adj_; /// the forward problem adjoint
  PDEVector * lambda_adj_; /// the reverse problem adjoint
  PDEVector * pde_work1_; /// state or adjoint work space
  PDEVector * pde_work2_; /// state or adjoint work space
  PDEVector * pde_work3_; /// state or adjoint work space
  DualVector * dual_work_; /// dual work space
  double product_fac_; /// factor that scales product_tol (see next)
  double product_tol_; // dynamic tolerance to solve linear systems in prod
  double grad_scale_; // equation scaling for Lagrangian gradient
  double ceq_scale_; // equation scaling for equality constraint
  ostream * outstream_;
  boost::scoped_ptr<Preconditioner<PDEVector> >
  primal_precond_; // forward problem preconditioner
  boost::scoped_ptr<Preconditioner<PDEVector> >
  adjoint_precond_; // reverse problem preconditioner
  double lambda_; /// continuation parameter for globalization
  double scale_; // scaling factor for globalization term
};

// ======================================================================
/*!
 * \class ReducedKKTPreconditioner
 * \brief specialization of preconditioner for reduced-space KKT problem
 */
class ReducedKKTPreconditioner : public Preconditioner<ReducedKKTVector> {
 public:

  /*!
   * \brief class constructor that builds preconditioner
   * \param[in] quasi_newton - Quasi-Newton object to approximate Hess.
   */
  ReducedKKTPreconditioner(QuasiNewton * quasi_newton);

  /*!
   * \brief returns the user-vector memory requirements
   * \param[in,out] num_required - stores the memory requirements
   *
   * On exit, num_required holds three elements with labels "design", "state",
   * and "dual"; thus, one each for the number of DesignVectors, PDEVectors, and
   * DualVectors, respectively.
   */
  void MemoryRequired(ptree& num_required);
  
  /*!
   * \brief operator that defines KKT preconditioner
   * \param[in] u - ReducedKKTVector that is being preconditioned
   * \param[out] v - ReducedKKTVector that is the result of the operation
   */
  void operator()(ReducedKKTVector & u, ReducedKKTVector & v);
  
 protected:
  QuasiNewton * quasi_newton_; ///< preconditioner for design
};

// ======================================================================
/*!
 * \class NestedKKTPreconditioner
 * \brief specialization of preconditioner for reduced-space KKT problem
 */
class NestedKKTPreconditioner : public Preconditioner<ReducedKKTVector> {
 public:

  /*!
   * \brief constructor for class
   * \param[in] krylov_size - number of Krylov iterations used in solver
   * \param[in/out] outstream - ostream object to write output to
   */
  NestedKKTPreconditioner(
      const int& krylov_size, ostream& outstream = std::cout);
  
  /*!
   * \brief class constructor that builds preconditioner
   * \param[in] eval_at_kkt - (primal,dual) state to evaluate approx KKT matrix
   * \param[in] eval_at_state - state to evaluate approx KKT matrix at
   * \param[in] eval_at_adjoint - adjoint to evaluate approx KKT matrix at
   * \param[in] reduced_opt - current (primal,dual) optimality at eval_at_*
   * \param[in] adjoint_res - the adjoint equation residual at eval_at_*
   * \param[out] design_work - array of DesignVectors for work
   * \param[out] pde_work - array of PDEVectors for work
   * \param[out] dual_work - array of DualVectors for work
   * \param[in] prec_param - input parameters
   * \param[in/out] outstream - ostream object to write output to
   */
  NestedKKTPreconditioner(
      const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
      const PDEVector & eval_at_adjoint, ReducedKKTVector & reduced_opt, 
      const PDEVector & adjoint_res, std::vector<DesignVector>& design_work,
      std::vector<PDEVector>& pde_work, std::vector<DualVector>& dual_work,
      const ptree& prec_param, ostream & outstream);
      
  /*!
   * \brief destructor for class
   */
  ~NestedKKTPreconditioner();

  /*!
   * \brief prepares the operator for use
   * \param[in] eval_at_kkt - (primal,dual) state to evaluate approx KKT matrix
   * \param[in] eval_at_state - state to evaluate approx KKT matrix at
   * \param[in] eval_at_adjoint - adjoint to evaluate approx KKT matrix at
   * \param[in] reduced_opt - current (primal,dual) optimality at eval_at_*
   * \param[in] adjoint_res - the adjoint equation residual at eval_at_*
   * \param[out] design_work - array of DesignVectors for work
   * \param[out] pde_work - array of PDEVectors for work
   * \param[out] dual_work - array of DualVectors for work
   * \param[in] prec_param - input parameters
   * \param[in/out] outstream - ostream object to write output to
   *
   * This is the counterpart to the long version of the constructor above.  This
   * is called when the product is not rebuilt each iteration, yet various
   * quantities must still be redefined.
   */
  void Initialize(
      const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
      const PDEVector & eval_at_adjoint, ReducedKKTVector & reduced_opt, 
      const PDEVector & adjoint_res, std::vector<DesignVector>& design_work,
      std::vector<PDEVector>& pde_work, std::vector<DualVector>& dual_work,
      const ptree& prec_param, ostream & outstream);
  
  /*!
   * \brief returns the user-vector memory requirements
   * \param[in,out] num_required - stores the memory requirements
   *
   * On exit, num_required holds three elements with labels "design", "state",
   * and "dual"; thus, one each for the number of DesignVectors, PDEVectors, and
   * DualVectors, respectively.
   */
  void MemoryRequired(ptree& num_required);
  
  /*!
   * \brief updates the diagonal matrix added to the preconditioner matrix
   * \param[in] diag - the value on the diagonal
   */
  void set_diagonal(const double & diag);
  
  /*!
   * \brief operator that defines KKT preconditioner
   * \param[in] u - ReducedKKTVector that is being preconditioned
   * \param[out] v - ReducedKKTVector that is the result of the operation
   */
  void operator()(ReducedKKTVector & u, ReducedKKTVector & v);
  
 protected:
  boost::shared_ptr<ApproxReducedKKTProduct>
  mat_vec_; /// approximate primal-dual (KKT) matrix vector product
  boost::scoped_ptr<IterativeSolver<ReducedKKTVector> >
  solver_; /// iterative solver for discipline block solves
  int krylov_size_; /// maximum number of MINRES iterations
  double diag_; /// value of diagonal matrix added to preconditioner
  ostream * outstream_;
};

// ======================================================================
/*!
 * \class ReducedSchurPreconditioner
 * \brief specialization of preconditioner for reduced-space KKT problem
 */
class ReducedSchurPreconditioner : public Preconditioner<ReducedKKTVector> {
 public:

  /*!
   * \brief constructor for class
   * \param[in] krylov_size - number of Krylov iterations used in Jacobian prod
   * \param[in/out] outstream - ostream object to write output to
   */
  ReducedSchurPreconditioner(
      const int& krylov_size, ostream& outstream = std::cout);
  
  /*!
   * \brief class constructor that builds preconditioner
   * \param[in] eval_at_kkt - (design,dual) vectors to evaluate KKT-matrix at
   * \param[in] eval_at_state - state to evaluate KKT-matrix at
   * \param[out] design_work1 - design work space
   * \param[out] design_work2 - design work space
   * \param[out] design_work3 - design work space
   * \param[out] pde_work1 - state or adjoint work space
   * \param[out] pde_work2 - state or adjoint work space
   * \param[out] dual_work1 - dual work space
   * \param[in] krylov_size - number of Krylov iterations used in prod
   * \param[in/out] outstream - ostream object to write output to
   */
  ReducedSchurPreconditioner(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    DesignVector & design_work1, DesignVector & design_work2,
    DesignVector & design_work3, PDEVector & pde_work1,
    PDEVector & pde_work2, DualVector & dual_work1, const int & krylov_size,
    ostream & outstream);

  /*!
   * \brief destructor for class
   */
  ~ReducedSchurPreconditioner();

  /*!
   * \brief returns the user-vector memory requirements
   * \param[in,out] num_required - stores the memory requirements
   *
   * On exit, num_required holds three elements with labels "design", "state",
   * and "dual"; thus, one each for the number of DesignVectors, PDEVectors, and
   * DualVectors, respectively.
   */
  void MemoryRequired(ptree& num_required);

  /*!
   * \brief prepares the operator for use
   * \param[in] eval_at_kkt - (primal,dual) state to evaluate preconditioner at
   * \param[in] eval_at_state - state to evaluate preconditioner at
   * \param[out] design_work - array of DesignVectors for work
   * \param[out] pde_work - array of PDEVectors for work
   * \param[out] dual_work - array of DualVectors for work
   * \param[in] prec_param - input parameters
   * \param[in/out] outstream - ostream object to write output to
   *
   * This is the counterpart to the long version of the constructor above.  This
   * is called when the operator is not rebuilt each iteration, yet various
   * quantities must still be redefined.
   */  
  void Initialize(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    std::vector<DesignVector>& design_work, std::vector<PDEVector>& pde_work,
    std::vector<DualVector>& dual_work, const ptree& prec_param,
    ostream & outstream = std::cout);
  
  /*!
   * \brief updates the diagonal matrix added to the preconditioner matrix
   * \param[in] diag - the value on the diagonal
   */
  void set_diagonal(const double & diag);
  
  /*!
   * \brief operator that defines KKT preconditioner
   * \param[in] u - ReducedKKTVector that is being preconditioned
   * \param[out] v - ReducedKKTVector that is the result of the operation
   */
  void operator()(ReducedKKTVector & u, ReducedKKTVector & v);
  
 protected:
  DesignVector * design_work1_; ///< design work space
  DesignVector * design_work2_; ///< design work space
  int krylov_size_; ///< number of Krylov iterations used in Jacobian prod
  double diag_; /// entries in diagonal matrix
  //MatrixVectorProduct<DesignVector> * jac_prod_; /// Constraint Jacobian Prod.
  boost::scoped_ptr<ConstraintJacobianProduct>
  jac_prod_; /// Constraint Jacobian Prod.
  boost::scoped_ptr<IterativeSolver<DesignVector> >
  solver_; /// iterative solver for discipline block solves
  ostream * outstream_;
};

// ======================================================================
/*!
 * \class ProjectionPreconditioner
 * \brief specialization of preconditioner that projects onto the null space
 */
class ProjectionPreconditioner : public Preconditioner<DesignVector> {
 public:

  /*!
   * \brief constructor for class
   * \param[in] krylov_size - number of Krylov iterations used for projection
   * \param[in/out] outstream - ostream object to write output to
   */
  ProjectionPreconditioner(
      const int& krylov_size, ostream& outstream = std::cout);
  
  /*!
   * \brief class constructor that builds preconditioner
   * \param[in] eval_at_kkt - (design,dual) vectors to evaluate aug. mat at
   * \param[in] eval_at_state - state to evaluate augmented matrix at
   * \param[out] design_work1 - design work space
   * \param[out] design_work2 - design work space
   * \param[out] design_work3 - design work space
   * \param[out] pde_work1 - state or adjoint work space
   * \param[out] pde_work2 - state or adjoint work space
   * \param[out] dual_work1 - dual work space
   * \param[in] krylov_size - number of Krylov iterations used in prod
   * \param[in/out] outstream - ostream object to write output to
   */
  ProjectionPreconditioner(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    DesignVector & design_work1, DesignVector & design_work2,
    DesignVector & design_work3, PDEVector & pde_work1,
    PDEVector & pde_work2, DualVector & dual_work1, const int & krylov_size,
    ostream & outstream);

  /*!
   * \brief destructor for class
   */
  ~ProjectionPreconditioner();

  /*!
   * \brief returns the user-vector memory requirements
   * \param[in,out] num_required - stores the memory requirements
   *
   * On exit, num_required holds three elements with labels "design", "state",
   * and "dual"; thus, one each for the number of DesignVectors, PDEVectors, and
   * DualVectors, respectively.
   */
  void MemoryRequired(ptree& num_required);

  /*!
   * \brief prepares the operator for use
   * \param[in] eval_at_kkt - (primal,dual) state to evaluate preconditioner at
   * \param[in] eval_at_state - state to evaluate preconditioner at
   * \param[out] design_work - array of DesignVectors for work
   * \param[out] pde_work - array of PDEVectors for work
   * \param[out] dual_work - array of DualVectors for work
   * \param[in] prec_param - input parameters
   * \param[in/out] outstream - ostream object to write output to
   *
   * This is the counterpart to the long version of the constructor above.  This
   * is called when the operator is not rebuilt each iteration, yet various
   * quantities must still be redefined.
   */  
  void Initialize(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    std::vector<DesignVector>& design_work, std::vector<PDEVector>& pde_work,
    std::vector<DualVector>& dual_work, const ptree& prec_param,
    ostream & outstream = std::cout);

  /*!
   * \brief updates the diagonal matrix added to the preconditioner matrix
   * \param[in] diag - the value on the diagonal
   */
  void set_diagonal(const double & diag);

  /*!
   * \brief defines the preconditioner for the augmented system
   * \param[in] aug_precond - a preconditioner that can be used
   */
  void set_aug_precond(const boost::shared_ptr<Preconditioner<ReducedKKTVector> >
                       & aug_precond);

  /*!
   * \brief operator that defines KKT preconditioner
   * \param[in] u - DesignVector that is being preconditioned
   * \param[out] v - DesignVector that is the result of the operation
   */
  void operator()(DesignVector & u, DesignVector & v);
  
 protected:
  boost::scoped_ptr<ReducedKKTVector>
  rhs_; /// used to store the projection problem rhs
  boost::scoped_ptr<ReducedKKTVector>
  sol_; /// used to store the projection problem solution
  boost::shared_ptr<ReducedAugmentedProduct>
  aug_prod_; /// augmented matrix-vector Prod.
  boost::shared_ptr<Preconditioner<ReducedKKTVector> >
  aug_precond_; ///< preconditioner for systems involving aug. matrix
  boost::scoped_ptr<IterativeSolver<ReducedKKTVector> >
  solver_; /// iterative solver that computes the projection
  ostream * outstream_; /// stream for output
};

// ======================================================================
/*!
 * \class IDFProjectionPreconditioner
 * \brief specialization of preconditioner that projects onto the null space
 */
class IDFProjectionPreconditioner : public Preconditioner<DesignVector> {
 public:

  /*!
   * \brief constructor for class
   * \param[in] krylov_size - number of Krylov iterations used for projection
   * \param[in/out] outstream - ostream object to write output to
   */
  IDFProjectionPreconditioner(
      const int& krylov_size, ostream& outstream = std::cout);
  
  /*!
   * \brief class constructor that builds preconditioner
   * \param[in] eval_at_kkt - (design,dual) vectors to evaluate aug. mat at
   * \param[in] eval_at_state - state to evaluate augmented matrix at
   * \param[out] design_work1 - design work space
   * \param[out] design_work2 - design work space
   * \param[out] design_work3 - design work space
   * \param[out] pde_work1 - state or adjoint work space
   * \param[out] pde_work2 - state or adjoint work space
   * \param[out] dual_work1 - dual work space
   * \param[in] krylov_size - number of Krylov iterations used in prod
   * \param[in/out] outstream - ostream object to write output to
   */
  IDFProjectionPreconditioner(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    DesignVector & design_work1, DesignVector & design_work2,
    DesignVector & design_work3, PDEVector & pde_work1,
    PDEVector & pde_work2, DualVector & dual_work1, const int & krylov_size,
    ostream & outstream);

  /*!
   * \brief destructor for class
   */
  ~IDFProjectionPreconditioner();

  /*!
   * \brief returns the user-vector memory requirements
   * \param[in,out] num_required - stores the memory requirements
   *
   * On exit, num_required holds three elements with labels "design", "state",
   * and "dual"; thus, one each for the number of DesignVectors, PDEVectors, and
   * DualVectors, respectively.
   */
  void MemoryRequired(ptree& num_required);

  /*!
   * \brief prepares the operator for use
   * \param[in] eval_at_kkt - (primal,dual) state to evaluate preconditioner at
   * \param[in] eval_at_state - state to evaluate preconditioner at
   * \param[out] design_work - array of DesignVectors for work
   * \param[out] pde_work - array of PDEVectors for work
   * \param[out] dual_work - array of DualVectors for work
   * \param[in] prec_param - input parameters
   * \param[in/out] outstream - ostream object to write output to
   *
   * This is the counterpart to the long version of the constructor above.  This
   * is called when the operator is not rebuilt each iteration, yet various
   * quantities must still be redefined.
   */  
  void Initialize(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    std::vector<DesignVector>& design_work, std::vector<PDEVector>& pde_work,
    std::vector<DualVector>& dual_work, const ptree& prec_param,
    ostream & outstream = std::cout);
  
  void set_idf_precond(const boost::shared_ptr<Preconditioner<ReducedKKTVector> >
                       & idf_precond);

  /*!
   * \brief operator that defines KKT preconditioner
   * \param[in] u - DesignVector that is being preconditioned
   * \param[out] v - DesignVector that is the result of the operation
   */
  void operator()(DesignVector & u, DesignVector & v);
  
 protected:
  double tol_; /// relative tolerance used for solving projection
  //DesignVector* rhs_; /// used to store the projection problem rhs
  //DesignVector* wrk_; /// used for work space
  boost::scoped_ptr<ReducedKKTVector> rhs_; /// based to the idf_precond_
  boost::scoped_ptr<ReducedKKTVector> sol_; /// based back from idf_precond_
  boost::scoped_ptr<ConstraintJacobianProduct>
  jac_prod_; /// Constraint Jacobian product
  boost::shared_ptr<Preconditioner<ReducedKKTVector> >
  idf_precond_; /// pointer to the IDF preconditioner
  boost::scoped_ptr<IterativeSolver<DesignVector> >
  solver_; /// iterative solver that computes the projection
  ostream * outstream_; /// stream for output
};

// ======================================================================
/*!
 * \class ReducedKKTVector
 * \brief represents the vector of unknowns in the reduced KKT system
 *
 * A ReducedKKTVector object represents a column vector of the unknowns in the
 * reduced KKT system: the design/control variables, slacks (if any), and
 * Lagrange multipliers.  It is implemented as a container of separate vectors.
 * The "reduced" terminology refers to the removal of the state constraint from
 * the optimization problem.
 */
class ReducedKKTVector {
 public:

  /*!
   * \brief default constructor
   */
  explicit ReducedKKTVector() {}

  /*!
   * \brief default destructor
   */
  ~ReducedKKTVector() {}

  /*!
   * \brief copy constructor with deep copy
   * \param[in] x - ReducedKKTVector being copied to calling object
   */
  ReducedKKTVector(const ReducedKKTVector & x) :
      design_(x.design_), dual_(x.dual_) {}

  /*!
   * \brief constructor based on assembling individual pieces
   * \param[in] design - vector assigned to design component
   * \param[in] dual - vector assigned to dual component
   */
  explicit ReducedKKTVector(const DesignVector & design,
                            const DualVector & dual) :
      design_(design), dual_(dual) {}
  
  /*!
   * \brief returns a const reference to the design_ member
   */
  const DesignVector & primal() const;

  /*!
   * \brief returns a reference to the design_ member
   */
  DesignVector & primal();

  /*!
   * \brief returns a const reference to the dual_ member
   */
  const DualVector & dual() const;

  /*!
   * \brief returns a reference to the dual_ member
   */
  DualVector & dual();
  
  /*!
   * \brief returns a const reference to the design_ member
   * deprecated by primal() above, which is needed for TRISQP
   */
  const DesignVector & get_design() const;

  /*!
   * \brief returns a const reference to the dual_ member
   * deprecated by dual() above, which is needed for TRISQP
   */
  const DualVector & get_dual() const;

  /*!
   * \brief sets the design_ component of ReducedKKTVector
   * deprecated by primal() above, which is needed for TRISQP
   */
  void set_design(const DesignVector & design);

  /*!
   * \brief sets the dual_ component of ReducedKKTVector
   * deprecated by dual() above, which is needed for TRISQP
   */
  void set_dual(const DualVector & dual);
  
  /*!
   * \brief ReducedKKTVector=ReducedKKTVector assignment operator with deep copy
   * \param[in] x - ReducedKKTVector value being assigned
   */
  ReducedKKTVector & operator=(const ReducedKKTVector & x);

  /*!
   * \brief ReducedKKTVector=double assignment operator
   * \param[in] val - value assigned to each member of ReducedKKTVector
   */
  ReducedKKTVector & operator=(const double & val);

  /*!
   * \brief compound addition-assignment operator
   * \param[in] x - ReducedKKTVector being added to calling object
   */
  ReducedKKTVector & operator+=(const ReducedKKTVector & x);

  /*!
   * \brief compound subtraction-assignment operator
   * \param[in] x - ReducedKKTVector being subtracted from calling object
   */
  ReducedKKTVector & operator-=(const ReducedKKTVector & x);

  /*!
   * \brief compound scalar multiplication-assignment operator
   * \param[in] val - value to multiply calling object by
   */
  ReducedKKTVector & operator*=(const double & val);

  /*!
   * \brief compound scalar division-assignment operator
   * \param[in] val - value to divide calling object by
   */
  ReducedKKTVector & operator/=(const double & val);

  /*!
   * \brief set to a general linear combination of two ReducedKKTVectors
   * \param[in] a - scalar factor for x
   * \param[in] x - first ReducedKKTVector in linear combination
   * \param[in] b - scalar factor for y
   * \param[in] y - second ReducedKKTVector in linear combination
   */
  void EqualsAXPlusBY(const double & a, const ReducedKKTVector & x,
                      const double & b, const ReducedKKTVector & y);

  /*!
   * \brief the L2 norm of the ReducedKKTVector
   */
  double Norm2() const;

  /*!
   * \brief inner product between two ReducedKKTVectors
   * \param[in] x - first ReducedKKTVector in inner product
   * \param[in] y - second ReducedKKTVector in inner product
   */
  friend double InnerProd(const ReducedKKTVector & x,
                          const ReducedKKTVector & y);

  /*!
   * \brief determines appropriate values for the initial KKT solution
   */
  void EqualsInitialGuess();
  
  /*!
   * \brief sets the calling ReducedKKTVector to the reduced KKT conditions
   * \param[in] x - where the KKT equations are evaluated
   * \param[in] state - value of the state variables at x.design
   * \param[in] adjoint - value of the adjoint variables at x.design, x.dual_
   * \param[in,out] design_wrk - design work vector
   *
   * Note, the adjoint here is *NOT* the adjoint for the objective, but for the
   * Lagrangian.
   */
  void EqualsKKTConditions(const ReducedKKTVector & x,
                           const PDEVector & state,
                           const PDEVector & adjoint,
                           DesignVector & design_wrk);

  /*!
   * \brief solves for the state and (1st-order) adjoint variables
   * \param[out] state - value of the state variable at design_
   * \param[out] adjoint - value of the adjoint variable at (design_, dual_)
   * \param[out] pde_work - work space vector for PDEVectors
   */
  void SolveStateAndAdjoint(PDEVector & state, PDEVector & adjoint,
                            PDEVector & pde_work) const;

  /*!
   * \brief solves for the 1st-order adjoint variables
   * \param[in] state - value of the state variable at design_
   * \param[out] adjoint - value of the adjoint variable at (design_, dual_)
   * \param[out] pde_work - work space vector for PDEVectors
   */
  void SolveAdjoint(const PDEVector & state, PDEVector & adjoint,
                    PDEVector & pde_work) const;
  
  /*!
   * \brief calculates the Lagrangian at the given reduced KKT vector
   * \param[in] eval_at_kkt - KKT vector where Lagrangian is evaluated
   * \param[in] eval_at_state - state varibles where Lagrangian is evaluated
   * \param[in] ceq - constraints evaluated at eval_at_kkt and eval_at_state
   */
  friend double Lagrangian(const ReducedKKTVector & eval_at_kkt,
                           const PDEVector & eval_at_state,
                           const DualVector & ceq);

  /*!
   * \brief calculates the Augmented Lagrangian at the given reduced KKT vector
   * \param[in] eval_at_kkt - KKT vector where Augmented Lagrangian is evaluated
   * \param[in] eval_at_state - state varibles where Lagrangian is evaluated
   * \param[in] ceq - constraints evaluated at eval_at_kkt and eval_at_state
   * \param[in] mu - penalty parameter
   */
  friend double AugmentedLagrangian(const ReducedKKTVector & eval_at_kkt,
                                    const PDEVector & eval_at_state,
                                    const DualVector & ceq, const double & mu);

  /*!
   * \brief calculates the L2 merit function at the given reduced KKT vector
   * \param[in] eval_at_kkt - KKT vector where L2 merit function is evaluated
   * \param[in] eval_at_state - state varibles where Lagrangian is evaluated
   * \param[in] ceq - constraints evaluated at eval_at_kkt and eval_at_state
   * \param[in] mu - penalty parameter
   */
  friend double EvaluateL2Merit(const ReducedKKTVector & eval_at_kkt,
                                const PDEVector & eval_at_state,
                                const DualVector & ceq, const double & mu);
  
 private:

  /// elements correpsonding to the design/control variables
  DesignVector design_;

  /// elements corresponding to the dual variables
  DualVector dual_;

  friend class ReducedKKTProduct;
  friend class LagrangianHessianProduct;
  friend class NullSpaceLagrangianProduct;
  friend class ReducedAugmentedProduct;
  friend class ApproxReducedKKTProduct;
  friend class ReducedKKTPreconditioner;
  friend class ReducedSchurPreconditioner;
  friend class IDFProjectionPreconditioner;
#if 0
  friend class GlobalizedKKTProduct;
  friend class RegularizedKKTProduct;
  friend class KKTPreconditioner;
#endif
};

} // namespace kona
