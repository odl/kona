/**
 * \file optimizer.hpp
 * \brief header file for the optimizer class
 * \author Jason Hicken <jason.hicken@gmail.com>
 */

#pragma once

#include <math.h>
#include <ostream>
#include <iostream>
#include <boost/scoped_ptr.hpp>
#include <boost/program_options.hpp>

namespace kona {

using std::cerr;
using std::cout;
using std::endl;
using std::ostringstream;
using std::ostream;
using std::ofstream;
using std::vector;
using std::string;
using std::map;
using boost::program_options::variables_map;

// TODO: delete this when OldOptimizer is eventually removed
namespace po = boost::program_options;

class DesignVector;
class PDEVector;
class DualVector;
class QuasiNewton;

/*!
 * \class Optimizer
 * \brief abstract base class for different optimization algorithms
 */
class Optimizer {
 public:

  /*!
   * \brief class destructor
   */
  virtual ~Optimizer() {}

  /*!
   * \brief defines the options needed by the algorithm
   * \param[in] optns - set of options
   */
  virtual void SetOptions(const variables_map& optns) = 0;
  
  /*!
   * \brief determines how many user stored vectors are needed
   * \param[out] num_design - number of design-sized vectors needed
   * \param[out] num_state - number of state-sized vectors needed
   * \param[out] num_dual - number of dual-sized vectors needed
   */
  virtual void MemoryRequirements(int & num_design, int & num_state,
                          int & num_dual) = 0;

  /*!
   * \brief attempts to solve the optimization problem
   */
  virtual void Solve() = 0;
};
// ==============================================================================
/*!
 * \class OldOptimizer
 * \brief directs and controls the optimization process
 * \warning This class is deprecated
 *
 * The OldOptimizer class implements several PDE-constrained optimization
 * algorithms including 1) reduced gradient Quasi-Newton 2) reduced-gradient
 * Newton-Krylov, and 3) the one-shote Lagrange-Newton-Krylov-Schur algorithm of
 * Biros and Ghattas [SISC Vol. 27, No. 2].  The class runs the algorithm based
 * on the user options read in from a configuration file.
 */
class OldOptimizer {
 public:
  
  /*!
   * \brief default class constructor
   */
  OldOptimizer() {
    opt_method = NULL;
  }

  /*!
   * \brief class destructor
   */
  ~OldOptimizer() {}

  void SetOptions(po::variables_map& optns) {
    options = optns;
  }

  /*!
   * \brief determines how many user stored vectors are needed
   * \param[out] num_design - number of design-sized vectors needed
   * \param[out] num_state - number of state-sized vectors needed
   * \param[out] num_dual - number of dual-sized vectors needed
   */
  void MemoryRequirements(int & num_design, int & num_state,
                          int & num_dual);

  /*!
   * \brief calls the appropriate optimization algorithm
   */
  void Optimize();

 private:

  /*!
   * \brief compute the reduced gradient at the given design
   * \param[in] x - design to evaluate gradient at
   * \param[out] state - solution of primal PDE at x
   * \param[out] adjoint - solution of adjoint PDE at x
   * \param[out] design_work - vector for work space
   * \param[out] djdx - reduced gradient of problem
   */
  void CalcReducedGradient(const DesignVector & x,
                           const PDEVector & state,
                           const PDEVector & adjoint,
                           DesignVector & design_work,
                           DesignVector & djdx) const;

  /*!
   * \brief trust-region reduced-space inexact-Newton optimization
   */
  void TrustRegionReducedSpaceOptimize();
  
  /*!
   * \brief perform the optimiztion using LNKS algorithm
   */
  void LNKSOptimize();

  /*!
   * \brief use the Inexact-Newton algorithm of Byrd et al.
   */
  void InexactNewtonOptimize();

  /*!
   * \brief reduced-space INK optimization with equality constraints
   */
  void ReducedSpaceConstrainedOptimize();

  /*!
   * \brief reduced-space augmented-Lagrangian optimization
   */
  void ReducedSpaceAugmentedLagrangianOptimize();
  
  /*!
   * \brief solves the augmented Lagrangian subproblem (fixed psi and mu)
   * \param[in/out] X - current value of the design variables
   * \param[in] Psi - current estimate of the multipliers
   * \param[in/out] state - value of the state at X
   * \param[in/out] adjoint - value of the adjoint at X (for AugLag gradient)
   * \param[in/out] dLdX - gradient of the augmented Lagrangian
   * \param[in/out] dLdX_old - value of gradient at previous iteration
   * \param[in/out] dual_work - dual vector work space
   * \param[in] mu - penalty parameter
   * \param[in] tol - tolerance with which to solve the subproblem
   * \param[in] outer_grad_tol - grad tolerance to which outer problem is solved
   * \param[in] outer_feas_tol - feas tolerance to which outer problem is solved
   * \param[in/out] radius - trust-region radius
   * \param[in] outer_iter - current outer iteration
   * \param[in/out] nonlinear_sum - total number of nonlinear iterations
   * \param[in/out] quasi_newton - quasi-Newton object for approximating Hessian
   * \param[in/out] hist_out - file handle for convergence history
   */
  void AugmentedLagrangianSubproblem(
      DesignVector & X, const DualVector & Psi, PDEVector & state,
      PDEVector & adjoint, DesignVector & dLdX, DesignVector & dLdX_old,
      DualVector & dual_work, const double & mu, const double & tol,
      const double & outer_grad_tol, const double & outer_feas_tol, 
      double & radius, const int & outer_iter, int & nonlinear_sum,
      QuasiNewton * quasi_newton, ofstream & hist_out);
  
  /*!
   * \brief write a TR reduced INK iteration history to a stream
   * \param[in] iter - the current optimization iteration
   * \param[in] precond - a cummulative sum of preconditioner calls
   * \param[in] norm - L2 norm of the design gradient
   * \param[in] rho - ratio of actual reduction to predicted reduction
   * \param[in] radius - current trust-region radius
   * \param[out] hist - an ostream for writing the convergence
   */
  void WriteTrustReducedHistory(const int & iter, const int & precond, 
                                const double & norm, const double & rho,
                                const double & radius, ostream & hist);
  
  /*!
   * \brief write a complete LNKS inner iteration history to a stream
   * \param[in] outer_iter - the current outer iteration
   * \param[in] inner_iter - the total inner iterations in outer_iter
   * \param[in] hist_string - an ostringstream object containing history
   * \param[out] hist_out - an ostream for writing the convergence
   */
  void WriteLNKSHistory(const int & outer_iter,
                        const int & inner_iter,
                        ostringstream & hist_string,
                        ostream & hist_out);
#if 0
  /*!
   * \brief write a complete LNKS outer iteration history to a stream
   * \param[in] outer_iter - the total outer iterations
   * \param[in] hist_string - an ostringstream object containing history
   * \param[out] hist_out - an ostream for writing the convergence
   */
  void WriteLNKSOuterHistory(const int & outer_iter, 
                             ostringstream & hist_string,
                             ostream & hist_out);
#endif
  /*!
   * \brief write a quasi-Newton outer-iteration history to a stream
   * \param[in] outer_iter - the current outer iteration
   * \param[in] inner_iter - the total inner iterations in outer_iter
   * \param[in] hist_string - an ostringstream object containing history
   * \param[out] hist_out - an ostream for writing the convergence
   */
  void WriteQuasiNewtonHistory(const int & outer_iter,
                               const int & inner_iter,
                               ostringstream & hist_string,
                               ostream & hist_out);
  
  /*!
   * \brief write a single line to a history string-stream
   * \param[in] outer_iter - the current outer iteration
   * \param[in] inner_iter - the current inner iteration
   * \param[in] total_inner - a cummulative sum of inner iterations
   * \param[in] precond - a cummulative sum of preconditioner calls
   * \param[in] outer_norms - set of KKT system norms 
   * \param[in] inner_norms - set of globalized KKT system norms
   * \param[in,out] hist - output-stream object for saving history
   */
  void WriteLNKSIter(const int & outer_iter, const int & inner_iter,
                     const int & total_inner, const int & precond, 
                     const vector<double> & outer_norms,
                     const vector<double> & inner_norms,
                     ostream & hist);
#if 0
  /*!
   * \brief write a single line to a outer history string-stream
   * \param[in] outer_iter - the current outer iteration
   * \param[in] inner_iter - the total inner iterations
   * \param[in] precond - a cummulative sum of preconditioner calls
   * \param[in] outer_norms - set of KKT system norms from outer loop
   * \param[in,out] hist - output-stream object for saving history
   */
  void WriteLNKSOuterIter(const int & outer_iter, const int & inner_iter,
                          const int & precond, 
                          const vector<double> & outer_norms,
                          ostream & hist);
#endif

  /*!
   * \brief write a single line to a history string-stream
   * \param[in] outer_iter - the current outer iteration
   * \param[in] inner_iter - the current inner iteration
   * \param[in] total_inner - a cummulative sum of inner iterations
   * \param[in] precond - a cummulative sum of preconditioner calls
   * \param[in] outer_norms - set of KKT system norms from outer loop
   * \param[in] inner_norms - set of KKT system norms from inner loop
   * \param[in,out] hist - output-stream object for saving history
   */
  void WriteQuasiNewtonIter(
      const int & outer_iter, const int & inner_iter,
      const int & total_inner, const int & precond, 
      const double & outer_norms, const double & inner_norms,
      ostream & hist);
  
  /// collection of user specified and default options
  //boost::shared_ptr<po::variables_map> options;
  po::variables_map options;

  void (OldOptimizer::*opt_method)();
};

} // namespace kona
