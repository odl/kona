/**
 * \file filter.hpp
 * \brief header file for various types of filters
 * \author Jason Hicken <jason.hicken@gmail.com>
 */

#pragma once

#include <math.h>
#include <ostream>
#include <iostream>
#include <list>
#include <utility>

namespace kona {

using std::cerr;
using std::cout;
using std::endl;
using std::ostream;
using std::list;
using std::pair;

// ==============================================================================
/*!
 * \class Filter
 * \brief base class for a (very) simple filter
 */
class Filter {

 public:

  /*!
   * \brief default constructor
   */
  Filter() {}
  
  /*!
   * \brief class destructor
   */
  virtual ~Filter() {}

  /*!
   * \brief determines if the pair (obj, cnstrnt) is acceptable to the filter
   * \param[in] obj - the value of the objective at the point of interest
   * \param[in] cnstrnt - the value of the constraint norm at the point
   * \returns true if (obj, cnstrnt) are dominated by filter, false otherwise
   *
   * If this function returns false, (obj, cnstrnt) is inserted into the filter
   * and all points dominated by this new pair are deleted.
   */
  virtual bool Dominates(double obj, double cnstrnt);

  /*!
   * \brief displays the contents of the filter (for debugging)
   * \param[in,out] fout - object for writing the contents to
   */
  void Print(ostream & fout = cout) const;
      
 protected:

  list<pair<double, double> >
  filter_; ///< stores the (obj, constraint) values that define the filter
};
// ==============================================================================
} // namespace kona
