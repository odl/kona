/**
 * \file user_vector.cpp
 * \brief definitions of UserVector and derived class member functions
 * \author Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#include <vector>
#include <string>
#include <stdlib.h>
#include "./user_memory.hpp"

using std::string;

#if 0
/*!
 * \brief user function declaration
 * \param[in] request - indicates what action the user should take
 * \param[in] leniwrk - the length of the array iwrk
 * \param[in] iwrk - array of indices for UserVector memory
 * \param[in] lendwrk - the length of the array dwrk
 * \param[in,out] dwrk - array to pass and return scalar values
 */
int userFunc(kona::tasks request, int leniwrk, int *iwrk, int lendwrk,
	     double *dwrk);
#endif

namespace kona {

// static data members have to be "redeclared"
int UserMemory::leniwrk_;
int* UserMemory::iwrk_ = NULL;
int UserMemory::lendwrk_;
double* UserMemory::dwrk_ = NULL;
vector<bool> UserMemory::design_assigned_;
vector<bool> UserMemory::state_assigned_;
vector<bool> UserMemory::dual_assigned_;
UserMemory *UserMemory::memory_ = NULL;
int UserMemory::precond_count_ = 0;
int (*UserMemory::user_function_)(int, int, int*, int, double*) = NULL;

// ======================================================================

UserMemory * UserMemory::Allocate(
    int (*func_ptr)(int, int, int*, int, double*),
    const int & num_design_size,
    const int & num_state_size,
    const int & num_dual_size) {
  // check if memory already allocated; Not allowed to call this again!
  if (UserMemory::memory_ != NULL) {
    cerr << "UserMemory(Allocate): "
	 << "attempting to reallocate user memory" << endl;
    throw(-1);
  }    

  // assign user function
  user_function_ = func_ptr;

  // create the singleton memory class and clear vector assignments
  memory_ = new UserMemory();
  design_assigned_.resize(num_design_size);
  for (int i = 0; i < num_design_size; i++)
    design_assigned_[i] = false;
  state_assigned_.resize(num_state_size);
  for (int i = 0; i < num_state_size; i++)
    state_assigned_[i] = false;
  dual_assigned_.resize(num_dual_size);
  for (int i = 0; i < num_dual_size; i++)
    dual_assigned_[i] = false;
  
  // set the size of the work arrays, allocate, and initialize them
  leniwrk_ = 5;
  lendwrk_ = 2;
  iwrk_ = new int[leniwrk_];
  dwrk_ = new double[lendwrk_];
  for (int i = 0; i < leniwrk_; i++)
    iwrk_[i] = -1;
  for (int i = 0; i < lendwrk_; i++)
    dwrk_[i] = 0.0;

  // send request to user to allocate num_design_size design vectors
  // and num_state_size primal/adjoint sized vectors
  iwrk_[0] = num_design_size;
  iwrk_[1] = num_state_size;
  iwrk_[2] = num_dual_size;
  int ierr = user_function_(allocmem, leniwrk_, iwrk_, lendwrk_, dwrk_);
  if (ierr != 0) {
    cerr << "UserMemory(Allocate): "
	 << "user_function_ returned with error code " << ierr << endl;
    throw(-1);
  }

  return memory_;
}

// ======================================================================

void UserMemory::Deallocate() {
  user_function_ = NULL;
  delete [] iwrk_;
  delete [] dwrk_;
  design_assigned_.clear();
  state_assigned_.clear();
  dual_assigned_.clear();
  memory_ = NULL;
  precond_count_ = 0;
  cout << "UserMemory::Deallocate()" << endl;
}

// ======================================================================

void UserMemory::CheckIndex(const vector_type & vec,
                            const bool & negative_one_ok, 
                            const int & index) {
  string err_msg ("UserMemory(CheckIndex): ");
  switch (vec) {
    case design: {// check a design-sized vector index
      if (index >= static_cast<int>(design_assigned_.size())) {
        err_msg.append("design index larger than number of vectors.");
        throw(err_msg);
      }
      if (index < 0) {
        if ( (!negative_one_ok) || 
             ( (negative_one_ok) && (index != -1) ) ) {
          err_msg.append("invalid negative design index.");
          throw(err_msg);
        }
      }
      break;
    }
    case pde: { // check a state/adjoint-sized vector index
      if (index >= static_cast<int>(state_assigned_.size())) {
        err_msg.append("state index larger than number of vectors.");
        throw(err_msg);
      }
      if (index < 0) {
        if ( (!negative_one_ok) || 
             ( (negative_one_ok) && (index != -1) ) ) {
          err_msg.append("invalid negative state index.");
          throw(err_msg);
        }
      }
      break;
    }
    case dual: { // check a dual/constraint-sized vector index
      if (index >= static_cast<int>(dual_assigned_.size())) {
        err_msg.append("dual index larger than number of vectors.");
        throw(err_msg);
      }
      if (index < 0) {
        if ( (!negative_one_ok) || 
             ( (negative_one_ok) && (index != -1) ) ) {
          err_msg.append("invalid negative dual index.");
          throw(err_msg);
        }
      }
      break;
    } 
    default: {
      err_msg.append("invalid vector-type value");
      throw(err_msg);
      break;
    }
  }
}

// ======================================================================

void UserMemory::AssignVector(const vector_type & vec, int & index) {
  switch (vec) {
    case design: {// assign a design-sized vector
      for (int i = 0; i < design_assigned_.size(); i++) {
        if (!design_assigned_[i]) {
          index = i;
#ifdef VERBOSE_DEBUG
          cout << "UserMemory::AssignVector(): "
               << "assigning design index " << i << " of "
               << static_cast<int>(design_assigned_.size())-1 << endl;
#endif
          design_assigned_[i] = true;
          return;
        }
      }
      // if we get here, all memory has been assigned
      cerr << "UserMemory(AssignVector): "
           << "available design-vector memory has been assigned" << endl;
      throw(-1);
      break;
    }
    case pde: {// assign a state/adjoint-sized vector
      for (int i = 0; i < state_assigned_.size(); i++) {
        if (!state_assigned_[i]) {
          index = i;
#ifdef VERBOSE_DEBUG
          cout << "UserMemory::AssignVector(): "
               << "assigning state index " << i << " of "
               << static_cast<int>(state_assigned_.size())-1 << endl;
#endif
          state_assigned_[i] = true;
          return;
        }
      }
      // if we get here, all memory has been assigned
      cerr << "UserMemory(AssignVector): "
           << "available state-vector memory has been assigned" << endl;
      throw(-1);
      break;
    }
    case dual: {// assign a dual/constraint-sized vector
      for (int i = 0; i < dual_assigned_.size(); i++) {
        if (!dual_assigned_[i]) {
          index = i;
#ifdef VERBOSE_DEBUG
          cout << "UserMemory::AssignVector(): "
               << "assigning dual index " << i << " of "
               << static_cast<int>(dual_assigned_.size())-1 << endl;
#endif
          dual_assigned_[i] = true;
          return;
        }
      }
      // if we get here, all memory has been assigned
      cerr << "UserMemory(AssignVector): "
           << "available dual-vector memory has been assigned" << endl;
      throw(-1);
      break;
    }
    default: {
      cerr << "UserMemory(AssignVector): "
           << "invalid vector-type value: vec = " << vec << endl;
      throw(-1);
      break;
    }
  }
}

// ======================================================================

void UserMemory::UnassignVector(const vector_type & vec,
                                const int & index) {
  try { 
    CheckIndex(vec, false, index);
  } catch (string err_msg) {
    cerr << "UserMemory(UnassignVector): index = " << index
         << ": " << err_msg << endl;
    throw(-1);
  }
  switch (vec) {
    case design: {// unassign a design-sized vector
      if (!design_assigned_[index]) {
        cerr << "UserMemory(UnassignVector): "
             << "design-vector memory has been previously "
             << "unassigned" << endl;
        throw(-1);
      }
      design_assigned_[index] = false;
#ifdef VERBOSE_DEBUG
      cout << "UserMemory::UnassignVector(): "
           << "unassigning design-vector index " << index << endl;
#endif
      break;
    }
    case pde: {// unassign a state-sized vector
      if (!state_assigned_[index]) {
        cerr << "UserMemory(UnassignVector): "
             << "state-vector memory has been previously "
             << "unassigned" << endl;
        throw(-1);
      }
      state_assigned_[index] = false;
#ifdef VERBOSE_DEBUG
      cout << "UserMemory::UnassignVector(): "
           << "unassigning state-vector index " << index << endl;
#endif
      break;
    }
    case dual: {// unassign a dual-sized vector
      if (!dual_assigned_[index]) {
        cerr << "UserMemory(UnassignVector): "
             << "dual-vector memory has been previously "
             << "unassigned" << endl;
        throw(-1);
      }
      dual_assigned_[index] = false;
#ifdef VERBOSE_DEBUG
      cout << "UserMemory::UnassignVector(): "
           << "unassigning dual-vector index " << index << endl;
#endif
      break;
    }      
    default: {
      cerr << "UserMemory(UnassignVector): "
           << "invalid vector-type value: vec = " << vec << endl;
      throw(-1);
      break;
    }
  }
}

// ======================================================================

void UserMemory::AXPlusBY(
    const vector_type & vec, const int & target_index,
    const double & a, const int & x_index,
    const double & b, const int & y_index) {
  // check for invalid indices
  try {
    CheckIndex(vec, false, target_index);
  } catch (string err_msg) {
    cerr << "UserMemory(AXPlusBY): target_index = " << target_index
         << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(vec, true, x_index);
  } catch (string err_msg) {
    cerr << "UserMemory(AXPlusBY): x_index = " << x_index 
         << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(vec, true, y_index);
  } catch (string err_msg) {
    cerr << "UserMemory(AXPlusBY): y_index = " << y_index
         << ": " << err_msg << endl;
    throw(-1);
  }

  iwrk_[0] = target_index;
  iwrk_[1] = x_index; 
  iwrk_[2] = y_index;
  dwrk_[0] = a;
  dwrk_[1] = b;
  int ierr;
  switch (vec) {
    case design:
      ierr = user_function_(axpby_d, leniwrk_, iwrk_, lendwrk_, dwrk_);
      break;
    case pde: 
      ierr = user_function_(axpby_s, leniwrk_, iwrk_, lendwrk_, dwrk_);
      break;
    case dual:
      ierr = user_function_(axpby_c, leniwrk_, iwrk_, lendwrk_, dwrk_);
      break;
    default: {
      cerr << "UserMemory(AXPlusBY): "
           << "invalid vector-type value: vec = " << vec << endl;
      throw(-1);
      break;
    }
  }
  if (ierr != 0) {
    cerr << "UserMemory(AXPlusBY): "
         << "user_function_ returned with error code " << ierr << endl;
    throw(-1);
  }
}

// ======================================================================

double UserMemory::InnerProd(
    const vector_type & vec, const int & x_index, const int & y_index) {
  // check for invalid indices
  try {
    CheckIndex(vec, false, x_index);
  } catch (string err_msg) {
    cerr << "UserMemory(InnerProd): x_index = " << x_index
         << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(vec, false, y_index);
  } catch (string err_msg) {
    cerr << "UserMemory(InnerProd): y_index = " << y_index
         << ": " << err_msg << endl;
    throw(-1);
  }
  iwrk_[0] = x_index;
  iwrk_[1] = y_index;
  int ierr;
  switch (vec) {
    case design:
      ierr = user_function_(innerprod_d, leniwrk_, iwrk_,
                            lendwrk_, dwrk_);
      break;
    case pde:
      ierr = user_function_(innerprod_s, leniwrk_, iwrk_,
                            lendwrk_, dwrk_);
      break;
    case dual:
      ierr = user_function_(innerprod_c, leniwrk_, iwrk_,
                            lendwrk_, dwrk_);
      break;      
    default: {
      cerr << "UserMemory(InnerProd): "
           << "invalid vector-type value: vec = " << vec << endl;
      throw(-1);
      break;
    }
  }
  if (ierr != 0) {
    cerr << "UserMemory(InnerProd): "
	 << "user_function_ returned with error code " << ierr << endl;
    throw(-1);
  }
  return dwrk_[0];
}

// ======================================================================

void UserMemory::Restrict(const int & type, const int & design_index) {
  try {
    CheckIndex(design, false, design_index);
  } catch (string err_msg) {
    cerr << "UserMemory(Project): design_index = "
         << design_index << ": " << err_msg << endl;
    throw(-1);
  }
  if ( (type < 0) || (type > 1) ) {
    cerr << "UserMemory(Project): invalid restrict type = "
         << type << endl;
    throw(-1);
  }
  iwrk_[0] = design_index;
  iwrk_[1] = type;
  int ierr = user_function_(restrict_d, leniwrk_, iwrk_, lendwrk_, dwrk_);
  if (ierr != 0) {
    cerr << "UserMemory(Restrict): "
	 << "user_function_ returned with error code " << ierr << endl;
    throw(-1);
  }
}

// ======================================================================

void UserMemory::ConvertVec(const vector_type & vec,
                            const int & source_index,
                            const int & target_index) {
  switch (vec) {
    case design: {
      try {
        CheckIndex(design, false, target_index);
      } catch (string err_msg) {
        cerr << "UserMemory(ConvertVec): design_index = "
             << target_index << ": " << err_msg << endl;
        throw(-1);
      }
      try {
        CheckIndex(dual, false, source_index);
      } catch (string err_msg) {
        cerr << "UserMemory(ConvertVec): dual_index = "
             << source_index << ": " << err_msg << endl;
        throw(-1);
      }
      iwrk_[0] = target_index;
      iwrk_[1] = source_index;
      int ierr = user_function_(convert_d, leniwrk_, iwrk_, lendwrk_, dwrk_);
      if (ierr != 0) {
        cerr << "UserMemory(ConvertVec): "
             << "user_function_ returned with error code " << ierr << endl;
        throw(-1);
      }
      break;
    }
    case dual: {
      try {
        CheckIndex(dual, false, target_index);
      } catch (string err_msg) {
        cerr << "UserMemory(ConvertVec): dual_index = "
             << target_index << ": " << err_msg << endl;
        throw(-1);
      }
      try {
        CheckIndex(design, false, source_index);
      } catch (string err_msg) {
        cerr << "UserMemory(ConvertVec): design_index = "
             << source_index << ": " << err_msg << endl;
        throw(-1);
      }
      iwrk_[0] = target_index;
      iwrk_[1] = source_index;
      int ierr = user_function_(convert_c, leniwrk_, iwrk_, lendwrk_, dwrk_);
      if (ierr != 0) {
        cerr << "UserMemory(ConvertVec): "
             << "user_function_ returned with error code " << ierr << endl;
        throw(-1);
      }
      break;
    }
    default: {
      cerr << "UserMemory(ConvertVec): "
           << "invalid conversion vector: vec = " << vec << endl;
      throw(-1);
      break;
    }
  }
}

// ======================================================================

void UserMemory::SetInitialDesign(const int & target_index) {
  try {
    CheckIndex(design, false, target_index);
  } catch (string err_msg) {
    cerr << "UserMemory(SetInitialDesign): target_index = "
         << target_index << ": " << err_msg << endl;
    throw(-1);
  }
  iwrk_[0] = target_index;
  int ierr = user_function_(initdesign, leniwrk_, iwrk_, lendwrk_, dwrk_);
  if (ierr != 0) {
    cerr << "UserMemory(SetInitialDesign): "
	 << "user_function_ returned with error code " << ierr << endl;
    throw(-1);
  }
  //precond_count_ += iwrk_[0];
}

// ======================================================================

double UserMemory::EvaluateObjective(const int & design_index, 
                                     const int & state_index) {
  try {
    CheckIndex(design, false, design_index);
  } catch (string err_msg) {
    cerr << "UserMemory(EvaluateObjective): design_index = " 
         << design_index << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(pde, true, state_index);
  } catch (string err_msg) {
    cerr << "UserMemory(EvaluateObjective): state_index = "
         << state_index << ": " << err_msg << endl;
    throw(-1);
  }
  iwrk_[0] = design_index;
  iwrk_[1] = state_index;
  int ierr = user_function_(eval_obj, leniwrk_, iwrk_, lendwrk_, dwrk_);
  if (ierr != 0) {
    cerr << "UserMemory(EvaluateObjective): "
	 << "user_function_ returned with error code " << ierr << endl;
    throw(-1);
  }
  precond_count_ += iwrk_[0];
  return dwrk_[0];
}

// ======================================================================

void UserMemory::EvaluatePDE(const int & target_index,
                             const int & design_index, 
                             const int & state_index) {
  try {
    CheckIndex(pde, false, target_index);
  } catch (string err_msg) {
    cerr << "UserMemory(EvaluatePDE): target_index = " << target_index
         << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(design, false, design_index);
  } catch (string err_msg) {
    cerr << "UserMemory(EvaluatePDE): design_index = " << design_index 
         << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(pde, false, state_index);
  } catch (string err_msg) {
    cerr << "UserMemory(EvaluatePDE): state_index = " << state_index
         << ": " << err_msg << endl;
    throw(-1);
  }
  iwrk_[0] = design_index;
  iwrk_[1] = state_index;
  iwrk_[2] = target_index;
  int ierr = user_function_(eval_pde, leniwrk_, iwrk_, lendwrk_, dwrk_);
  if (ierr != 0) {
    cerr << "UserMemory(EvaluatePDE): "
	 << "user_function_ returned with error code " << ierr << endl;
    throw(-1);
  }  
}

// ======================================================================

void UserMemory::EvaluateConstraints(const int & target_index,
                                     const int & design_index, 
                                     const int & state_index) {
  try {
    CheckIndex(dual, false, target_index);
  } catch (string err_msg) {
    cerr << "UserMemory(EvaluateConstraints): target_index = " << target_index
         << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(design, false, design_index);
  } catch (string err_msg) {
    cerr << "UserMemory(EvaluateConstraints): design_index = " << design_index 
         << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(pde, false, state_index);
  } catch (string err_msg) {
    cerr << "UserMemory(EvaluateConstraints): state_index = " << state_index
         << ": " << err_msg << endl;
    throw(-1);
  }
  iwrk_[0] = design_index;
  iwrk_[1] = state_index;
  iwrk_[2] = target_index;
  int ierr = user_function_(eval_ceq, leniwrk_, iwrk_, lendwrk_, dwrk_);
  if (ierr != 0) {
    cerr << "UserMemory(EvaluateConstraints): "
	 << "user_function_ returned with error code " << ierr << endl;
    throw(-1);
  }  
}

// ======================================================================

bool UserMemory::SolvePDE(const int & target_index,
                          const int & design_index) {
  try {
    CheckIndex(pde, false, target_index);
  } catch (string err_msg) {
    cerr << "UserMemory(SolvePDE): target_index = " << target_index
         << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(design, false, design_index);
  } catch (string err_msg) {
    cerr << "UserMemory(SolvePDE): design_index = " << design_index 
         << ": " << err_msg << endl;
    throw(-1);
  }
  iwrk_[0] = design_index;
  iwrk_[1] = target_index;
  int ierr = user_function_(solve, leniwrk_, iwrk_, lendwrk_, dwrk_);
  if (ierr != 0) {
    cerr << "UserMemory(SolvePDE): "
	 << "user_function_ returned with error code " << ierr << endl;
    throw(-1);
  }  
  precond_count_ += abs(iwrk_[0]);
  if (iwrk_[0] >= 0)
    return true; // successful convergence
  else
    return false; // failed to converge
}

// ======================================================================

bool UserMemory::SolveLinearized(const int & target_index,
                                 const int & design_index,
                                 const int & rhs_index,
                                 const int & state_index,
                                 const double & rel_tol) {
  try {
    CheckIndex(pde, false, target_index);
  } catch (string err_msg) {
    cerr << "UserMemory(SolveLinearized): target_index = " << target_index
         << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(design, false, design_index);
  } catch (string err_msg) {
    cerr << "UserMemory(SolveLinearized): design_index = " << design_index 
         << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(pde, false, state_index);
  } catch (string err_msg) {
    cerr << "UserMemory(SolveLinearized): state_index = " << state_index
         << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(pde, false, rhs_index);
  } catch (string err_msg) {
    cerr << "UserMemory(SolveLinearized): rhs_index = " << rhs_index
         << ": " << err_msg << endl;
    throw(-1);
  }
  if (rel_tol < 0.0) {
    cerr << "UserMemory(SolveLinearized): problem with rel_tol = " << rel_tol
         << endl;
    throw(-1);
  }
  iwrk_[0] = design_index;
  iwrk_[1] = state_index;
  iwrk_[2] = rhs_index;
  iwrk_[3] = target_index;
  dwrk_[0] = rel_tol;
  int ierr = user_function_(linsolve, leniwrk_, iwrk_, lendwrk_, dwrk_);
  if (ierr != 0) {
    cerr << "UserMemory(SolveLinearized): "
	 << "user_function_ returned with error code " << ierr << endl;
    throw(-1);
  }
  precond_count_ += abs(iwrk_[0]);
  if (iwrk_[0] >= 0)
    return true; // successful convergence
  else
    return false; // failed to converge
}

// ======================================================================

bool UserMemory::SolveAdjoint(const int & target_index,
                              const int & design_index,
                              const int & rhs_index,
                              const int & state_index,
                              const double & rel_tol) {
  try {
    CheckIndex(pde, false, target_index);
  } catch (string err_msg) {
    cerr << "UserMemory(SolveAdjoint): target_index = " << target_index
         << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(design, false, design_index);
  } catch (string err_msg) {
    cerr << "UserMemory(SolveAdjoint): design_index = " << design_index 
         << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(pde, false, state_index);
  } catch (string err_msg) {
    cerr << "UserMemory(SolveAdjoint): state_index = " << state_index
         << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(pde, true, rhs_index);
  } catch (string err_msg) {
    cerr << "UserMemory(SolveAdjoint): rhs_index = " << rhs_index
         << ": " << err_msg << endl;
    throw(-1);
  }
  if (rel_tol < 0.0) {
    cerr << "UserMemory(SolveAdjoint): problem with rel_tol = " << rel_tol
         << endl;
    throw(-1);
  }
  iwrk_[0] = design_index;
  iwrk_[1] = state_index;
  iwrk_[2] = rhs_index;
  iwrk_[3] = target_index;
  dwrk_[0] = rel_tol;
  int ierr = user_function_(adjsolve, leniwrk_, iwrk_, lendwrk_, dwrk_);
  if (ierr != 0) {
    cerr << "UserMemory(SolveAdjoint): "
	 << "user_function_ returned with error code " << ierr << endl;
    throw(-1);
  }
  precond_count_ += abs(iwrk_[0]);

  // Following is temporary
  if ( (rhs_index != -1) && (iwrk_[1] != -10) ) {
    cerr << "UserMemory(SolveAdjoint): "
         << "did you update kona::adjsolve in userfunc?" << endl;
    throw(-1);
  }
  
  if (iwrk_[0] >= 0)
    return true; // successful convergence
  else
    return false; // failed to converge
}

// ======================================================================

void UserMemory::ObjectiveGradient(const vector_type & vec, 
                                   const int & target_index,
                                   const int & design_index,
                                   const int & state_index) {
  try {
    CheckIndex(vec, false, target_index);
  } catch (string err_msg) {
    cerr << "UserMemory(ObjectiveGradient): target_index = " 
         << target_index << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(design, false, design_index);
  } catch (string err_msg) {
    cerr << "UserMemory(ObjectiveGradient): design_index = "
         << design_index << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(pde, false, state_index);
  } catch (string err_msg) {
    cerr << "UserMemory(ObjectiveGradient): state_index = "
         << state_index << ": " << err_msg << endl;
    throw(-1);
  }
  iwrk_[0] = design_index;
  iwrk_[1] = state_index;
  iwrk_[2] = target_index;
  int ierr;
  switch (vec) {
    case design:
      ierr = user_function_(grad_d, leniwrk_, iwrk_, lendwrk_, dwrk_);
      break;
    case pde:
      ierr = user_function_(grad_s, leniwrk_, iwrk_, lendwrk_, dwrk_);
      break;
    default: {
      cerr << "UserMemory(ObjectiveGradient): "
           << "invalid vector-type value: vec = " << vec << endl;
      throw(-1);
      break;
    }
  }
  if (ierr != 0) {
    cerr << "UserMemory(ObjectiveGradient): "
	 << "user_function_ returned with error code " << ierr << endl;
    throw(-1);
  }
}

// ======================================================================

void UserMemory::JacobianOperation(
    const tasks & request, const int & design_index, 
    const int & state_index, const int & u_index, const int & v_index) {
  if ( (request != jacvec_d) &&
       (request != jacvec_s) &&
       (request != tjacvec_d) &&
       (request != tjacvec_s) ) {
    cerr << "UserMemory(JacobianOperation): "
         << "invalid tasks value: request = " << request << endl;
    throw(-1);
  }
  try {
    CheckIndex(design, true, design_index);
  } catch (string err_msg) {
    cerr << "UserMemory(JacobianOperation): jacvec_d design_index = "
         << design_index << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(pde, true, state_index);
  } catch (string err_msg) {
    cerr << "UserMemory(JacobianOperation): jacvec_d state_index = "
         << state_index << ": " << err_msg << endl;
    throw(-1);
  }
  if (request == jacvec_d) {
    try {
      CheckIndex(design, false, u_index);
    } catch (string err_msg) {
      cerr << "UserMemory(JacobianOperation): jacvec_d u_index = "
           << u_index << ": " << err_msg << endl;
      throw(-1);
    }
    try {
      CheckIndex(pde, false, v_index);
    } catch (string err_msg) {
      cerr << "UserMemory(JacobianOperation): jacvec_d v_index = "
           << v_index << ": " << err_msg << endl;
      throw(-1);
    }
  } else if (request == tjacvec_d) {
    try {
      CheckIndex(pde, false, u_index);
    } catch (string err_msg) {
      cerr << "UserMemory(JacobianOperation): tjacvec_d u_index = "
           << u_index << ": " << err_msg << endl;
      throw(-1);
    }
    try {
      CheckIndex(design, false, v_index);
    } catch (string err_msg) {
      cerr << "UserMemory(JacobianOperation): tjacvec_d v_index = "
           << v_index << ": " << err_msg << endl;
      throw(-1);
    }
  } else {
    try {
      CheckIndex(pde, false, u_index);
    } catch (string err_msg) {
      cerr << "UserMemory(JacobianOperation): u_index = " 
           << u_index << ": " << err_msg << endl;
      throw(-1);
    }
    try {
      CheckIndex(pde, false, v_index);
    } catch (string err_msg) {
      cerr << "UserMemory(JacobianOperation): v_index = "
           << v_index << ": " << err_msg << endl;
      throw(-1);
    }
  }
  iwrk_[0] = design_index;
  iwrk_[1] = state_index;
  iwrk_[2] = u_index;
  iwrk_[3] = v_index;
  int ierr = user_function_(request, leniwrk_, iwrk_, lendwrk_, dwrk_);
  if (ierr != 0) {
    cerr << "UserMemory(JacobianOperation): "
	 << "user_function_ returned with error code " << ierr << endl;
    throw(-1);
  }
}

// ======================================================================

void UserMemory::ConstraintJacobianOperation(
    const tasks & request, const int & design_index, 
    const int & state_index, const int & u_index, const int & v_index) {
  if ( (request != ceqjac_d) &&
       (request != ceqjac_s) &&
       (request != tceqjac_d) &&
       (request != tceqjac_s) ) {
    cerr << "UserMemory(ConstraintJacobianOperation): "
         << "invalid tasks value: request = " << request << endl;
    throw(-1);
  }
  try {
    CheckIndex(design, true, design_index);
  } catch (string err_msg) {
    cerr << "UserMemory(ConstraintJacobianOperation): jacvec_d design_index = "
         << design_index << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(pde, true, state_index);
  } catch (string err_msg) {
    cerr << "UserMemory(ConstraintJacobianOperation): jacvec_d state_index = "
         << state_index << ": " << err_msg << endl;
    throw(-1);
  }
  if (request == ceqjac_d) {
    try {
      CheckIndex(design, false, u_index);
    } catch (string err_msg) {
      cerr << "UserMemory(ConstraintJacobianOperation): ceqjac_d u_index = "
           << u_index << ": " << err_msg << endl;
      throw(-1);
    }
    try {
      CheckIndex(dual, false, v_index);
    } catch (string err_msg) {
      cerr << "UserMemory(ConstraintJacobianOperation): ceqjac_d v_index = "
           << v_index << ": " << err_msg << endl;
      throw(-1);
    }
  } else if (request == ceqjac_s) {
    try {
      CheckIndex(pde, false, u_index);
    } catch (string err_msg) {
      cerr << "UserMemory(ConstraintJacobianOperation): ceqjac_s u_index = "
           << u_index << ": " << err_msg << endl;
      throw(-1);
    }
    try {
      CheckIndex(dual, false, v_index);
    } catch (string err_msg) {
      cerr << "UserMemory(ConstraintJacobianOperation): ceqjac_s v_index = "
           << v_index << ": " << err_msg << endl;
      throw(-1);
    }    
  } else if (request == tceqjac_d) {
    try {
      CheckIndex(dual, false, u_index);
    } catch (string err_msg) {
      cerr << "UserMemory(ConstraintJacobianOperation): tceqjac_d u_index = "
           << u_index << ": " << err_msg << endl;
      throw(-1);
    }    
    try {
      CheckIndex(design, false, v_index);
    } catch (string err_msg) {
      cerr << "UserMemory(ConstraintJacobianOperation): tceqjac_d v_index = "
           << v_index << ": " << err_msg << endl;
      throw(-1);
    }    
  } else if (request == tceqjac_s) {
    try {
      CheckIndex(dual, false, u_index);
    } catch (string err_msg) {
      cerr << "UserMemory(ConstraintJacobianOperation): tceqjac_s u_index = "
           << u_index << ": " << err_msg << endl;
      throw(-1);
    }    
    try {
      CheckIndex(pde, false, v_index);
    } catch (string err_msg) {
      cerr << "UserMemory(ConstraintJacobianOperation): tceqjac_s v_index = "
           << v_index << ": " << err_msg << endl;
      throw(-1);
    }
  }
  iwrk_[0] = design_index;
  iwrk_[1] = state_index;
  iwrk_[2] = u_index;
  iwrk_[3] = v_index;
  int ierr = user_function_(request, leniwrk_, iwrk_, lendwrk_, dwrk_);
  if (ierr != 0) {
    cerr << "UserMemory(ConstraintJacobianOperation): "
	 << "user_function_ returned with error code " << ierr << endl;
    throw(-1);
  } 
}

// ======================================================================

void UserMemory::BuildPreconditioner(const int & design_index,
                                     const int & state_index) {
  try {
    CheckIndex(design, true, design_index);
  } catch (string err_msg) {
    cerr << "UserMemory(BuildPreconditioner): design_index = "
         << design_index << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(pde, true, state_index);
  } catch (string err_msg) {
    cerr << "UserMemory(BuildPreconditioner): state_index = "
         << state_index << ": " << err_msg << endl;
    throw(-1);
  }
  iwrk_[0] = design_index;
  iwrk_[1] = state_index;
  int ierr = user_function_(eval_precond, leniwrk_, iwrk_, lendwrk_,
                            dwrk_);
  if (ierr != 0) {
    cerr << "UserMemory(BuildPreconditioner): "
	 << "user_function_ returned with error code " << ierr << endl;
    throw(-1);
  }  
}

// ======================================================================

void UserMemory::PreconditionOperation(
    const tasks & request, const int & design_index, 
    const int & state_index, const int & u_index, const int & v_index,
    const double & diag) {
  if ( (request != precond_s) &&
       (request != tprecond_s) ) {
    cerr << "UserMemory(PreconditionOperation): "
         << "invalid tasks value: request = " << request << endl;
    throw(-1);
  }
  try {
    CheckIndex(design, true, design_index);
  } catch (string err_msg) {
    cerr << "UserMemory(PreconditionOperation): design_index = "
         << design_index << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(pde, true, state_index);
  } catch (string err_msg) {
    cerr << "UserMemory(PreconditionOperation): state_index = "
         << state_index << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(pde, false, u_index);
  } catch (string err_msg) {
    cerr << "UserMemory(PreconditionOperation): u_index = " 
         << u_index << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(pde, false, v_index);
  } catch (string err_msg) {
    cerr << "UserMemory(PreconditionOperation): v_index = "
         << v_index << ": " << err_msg << endl;
    throw(-1);
  }
  iwrk_[0] = design_index;
  iwrk_[1] = state_index;
  iwrk_[2] = u_index;
  iwrk_[3] = v_index;
  dwrk_[0] = diag;
  int ierr = user_function_(request, leniwrk_, iwrk_, lendwrk_, dwrk_);
  if (ierr != 0) {
    cerr << "UserMemory(PreconditionOperation): "
	 << "user_function_ returned with error code " << ierr << endl;
    throw(-1);
  }
  precond_count_ += iwrk_[0];
}

// ======================================================================

void UserMemory::Debug(const vector_type & vec, const int & index,
                       const int & int_work, const double & dbl_work) {
  try {
    CheckIndex(vec, false, index);
  } catch (string err_msg) {
    cerr << "UserMemory(Debug): index = " << index
         << ": " << err_msg << endl;
    throw(-1);
  }  
  iwrk_[0] = index;
  iwrk_[1] = int_work;
  dwrk_[0] = dbl_work;
  int ierr = user_function_(debug, leniwrk_, iwrk_, lendwrk_, dwrk_);
  if (ierr != 0) {
    cerr << "UserMemory(Debug): "
	 << "user_function_ returned with error code " << ierr << endl;
    throw(-1);
  }
}
  
// ======================================================================

int UserMemory::GetRank() {
  int ierr = user_function_(rank, leniwrk_, iwrk_, lendwrk_, dwrk_);
  if (ierr != 0) {
    cerr << "UserMemory(GetRank): "
	 << "user_function_ returned with error code " << ierr << endl;
    throw(-1);
  }
  return iwrk_[0];
}
  
// ======================================================================

void UserMemory::CurrentSolution(const int & iter,
                                 const int & design_index,
                                 const int & state_index,
                                 const int & adjoint_index,
                                 const int & dual_index,
                                 ostream& out) {
  try {
    CheckIndex(design, true, design_index);
  } catch (string err_msg) {
    cerr << "UserMemory(CurrentSolution): design_index = "
         << design_index << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(pde, true, state_index);
  } catch (string err_msg) {
    cerr << "UserMemory(CurrentSolution): state_index = "
         << state_index << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(pde, true, adjoint_index);
  } catch (string err_msg) {
    cerr << "UserMemory(CurrentSolution): adjoint_index = "
         << adjoint_index << ": " << err_msg << endl;
    throw(-1);
  }
  try {
    CheckIndex(dual, true, dual_index);
  } catch (string err_msg) {
    cerr << "UserMemory(CurrentSolution): dual_index = "
         << dual_index << ": " << err_msg << endl;
    throw(-1);
  }
  iwrk_[0] = design_index;
  iwrk_[1] = state_index;
  iwrk_[2] = adjoint_index;
  iwrk_[3] = dual_index;
  iwrk_[4] = iter;
  int ierr = user_function_(info, leniwrk_, iwrk_, lendwrk_, dwrk_);
  if (ierr != 0) {
    cerr << "UserMemory(CurrentSolution): "
	 << "user_function_ returned with error code " << ierr << endl;
    throw(-1);
  }
  out << "total preconditioner calls (user_memory says) = " 
      << precond_count_  << endl;
}

} // namespace kona
