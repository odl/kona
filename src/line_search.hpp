/**
 * \file line_search.hpp
 * \brief header file for various types of line search algorithms
 * \author Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#pragma once

#include <math.h>
#include <ostream>
#include <iostream>
#include "./merit.hpp"

namespace kona {

using std::cerr;
using std::cout;
using std::endl;

// ==============================================================================
/*!
 * \class LineSearch
 * \brief abstract base class for defining line search algorithms
 */
class LineSearch {
 public:
  
  /*!
   * \brief class destructor
   */
  virtual ~LineSearch() {}

  /*!
   * \brief returns the memory requirements of the object
   * \param[out] design_memory - number of design-space vectors
   * \param[out] pde_memory - number of state-space vectors
   * \param[out] dual_memory - number of dual-space vectors
   */
  virtual void MemoryRequired(int& design_memory, int& pde_memory,
                              int& dual_memory) const = 0;

  /*!
   * \brief defines the output stream used for diagnostics
   * \param[in,out] out - a valid stream for output
   */
  void SetOutputStream(ostream& out);
  
  /*!
   * \brief set the initial step length value
   * \param[in] alpha_init - initial step length
   */
  void SetInitialStepLength(const double & alpha_init);
  
  /*!
   * \brief set the inner product of the search direction and gradient
   * \param[in] p_dot_grad - inner product (P, dJdX)
   */
  void SetSearchDotGrad(const double & p_dot_grad);

  /*!
   * \brief sets a pointer to the scalar merit function definition
   * \param[in] merit - a merit function derived class
   */
  void SetMeritFunction(MeritFunction& merit);

  /*!
   * \brief performs the line search algorithm
   */
  virtual double FindStepLength() = 0;

 protected:
  ostream* out_; ///< output stream for diagnostics
  double alpha_init_; ///< initial step length
  double p_dot_dfdx_; ///< inner product of search direction and gradient  
  MeritFunction* merit_; ///< pointer to a scalar merit function

  /*!
   * \brief constructor that sets output stream
   * \param[in,out] out - a valid stream for output
   */
  LineSearch(ostream& out);
};
// ==============================================================================  
/*!
 * \class BackTracking
 * \brief defines a backtracking line search for sufficient decrease
 */
class BackTracking: public LineSearch {
 public:

  /*!
   * \brief class constructor
   * \param[in] alpha_init - initial step length
   * \param[in] alpha_min - minimum allowable step length
   * \param[in] sufficient_cond - sufficient decrease condition parameter
   * \param[in] reduct_factor - reduction factor applied to step
   * \param[in,out] out - a valid stream for output
   */
  BackTracking(const double & alpha_init, const double & alpha_min,
               const double & sufficient_cond,
               const double & reduct_factor, ostream& out = std::cout);

  /*!
   * \brief returns the memory requirements of the object
   * \param[out] design_memory - number of design-space vectors
   * \param[out] pde_memory - number of state-space vectors
   * \param[out] dual_memory - number of dual-space vectors
   */
  void MemoryRequired(int& design_memory, int& pde_memory,
                      int& dual_memory) const;
  
  /*!
   * \brief class destructor
   */
  ~BackTracking() {}

  /*!
   * \brief performs the line search algorithm
   */
  double FindStepLength();

 protected:
  double alpha_min_; ///< minimum allowable step
  double suff_; ///< sufficient decrease parameter
  double rho_; ///< decrease factor    
};
// ==============================================================================
/*!
 * \class StrongWolfe
 * \brief defines a search that satisfies the strong-Wolfe conditions
 */
class StrongWolfe: public LineSearch {
 public:

  /*!
   * \brief class constructor
   * \param[in] alpha_init - initial step length
   * \param[in] alpha_max - maximum allowable step length
   * \param[in] sufficient_cond - sufficient decrease condition parameter
   * \param[in] curv_cond - curvature condition parameter
   * \param[in] max_iter - maximum number of iterations in search (or zoom)
   * \param[in,out] out - a valid stream for output
   */
  StrongWolfe(const double & alpha_init, const double & alpha_max,
              const double & sufficient_cond, const double & curv_cond,
              const double & max_iter, ostream& out = std::cout);

  /*!
   * \brief returns the memory requirements of the object
   * \param[out] design_memory - number of design-space vectors
   * \param[out] pde_memory - number of state-space vectors
   * \param[out] dual_memory - number of dual-space vectors
   */
  void MemoryRequired(int& design_memory, int& pde_memory,
                      int& dual_memory) const;
  
  /*!
   * \brief class destructor
   */
  ~StrongWolfe() {}

  /*!
   * \brief performs the line search algorithm
   */
  double FindStepLength();

 protected:

  /*!
   * \brief interpolates to find step that satisfies the Wolfe conditions
   * \param[in,out] alpha_low - interval end giving lowest function value
   * \param[in,out] alpha_hi - interval end giving highest function value
   * \param[in,out] phi_low - merit function value at alpha_low
   * \param[in,out] phi_hi - merti function value at alpha_hi
   * \param[in,out] dphi_low - merit function derivative at alpha_low
   * \param[in,out] dphi_hi - merit function derivative at alpha_hi
   * \param[in,out] deriv_hi - indicates if dphi_hi is available or not
   * \returns a step that satisfies the strong Wolfe conditions
   * 
   * Given an interval over which a local minimum must exist, this
   * routine finds a step value that satisfies the strong Wolfe
   * conditions.
   */
  double Zoom(double & alpha_low, double & alpha_hi, 
              double & phi_low, double & phi_hi,
              double & dphi_low, double & dphi_hi,
              bool & deriv_hi);

  int max_iter_; ///< maximum allowable iterations
  double alpha_max_; ///< maximum allowable step
  double phi_init_; ///< initial value of merit function
  double dphi_init_; ///< initial value of merit function derivative
  double suff_; ///< sufficient decrease parameter
  double curv_; ///< curvature condition parameter
};
// ==============================================================================
/*!
 * \brief finds a step in between alpha_low, alpha_hi using interpolation
 * \param[in] alpha_low - evaluation point where merit func is less
 * \param[in] alpha_hi - evaluation point where merit func is greater
 * \param[in] f_low - function value at alpha_low
 * \param[in] f_hi - function value at alpha_hi
 * \param[in] df_low - function derivative at alpha_low
 * \param[in] df_hi - function derivative at alpha_hi (if available)
 * \param[in] deriv_hi - true if derivative is available at alpha_hi
 * \param[in,out] out - a valid stream for output
 */
double InterpStep(const double & alpha_low, const double & alpha_hi,
                  const double & f_low, const double & f_hi,
                  const double & df_low, const double & df_hi,
                  const bool & deriv_hi, ostream& out = cout);

/*!
 * \brief finds a step between alpha_low, alpha_hi using quad interp
 * \param[in] alpha_low - evaluation point where merit func is less
 * \param[in] alpha_hi - evaluation point where merit func is greater
 * \param[in] f_low - function value at alpha_low
 * \param[in] f_hi - function value at alpha_hi
 * \param[in] df_low - function derivative at alpha_low
 * \param[in,out] out - a valid stream for output
 */
double QuadraticStep(const double & alpha_low, const double & alpha_hi,
                     const double & f_low, const double & f_hi,
                     const double & df_low, ostream& out = cout);

} // namespace kona
