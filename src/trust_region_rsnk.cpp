/**
 * \file trust_region_rsnk.cpp
 * \brief definitions of TrustRegionRSNK class member functions
 * \author Jason Hicken <jason.hicken@gmail.com>
 */

#include <fstream>
#include <boost/program_options.hpp>
#include <boost/format.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/shared_array.hpp>
#include <boost/pointer_cast.hpp>
#include "trust_region_rsnk.hpp"
#include "krylov.hpp"

namespace kona {

using std::string;
using std::ostream;
using std::ifstream;
using std::ofstream;
using std::ostringstream;
using std::ios;
using std::max;
using std::vector;
using std::map;

// ==============================================================================
void TrustRegionRSNK::SetOptions(const po::variables_map& optns) {
  options_ = optns;
}
// ==============================================================================
void TrustRegionRSNK::MemoryRequirements(int & num_design,
                                         int & num_state,
                                         int & num_dual) {
  // vectors required in Solve() method
  num_design = 9;
  num_state = 13;
  num_dual = 0;
  int design_req, state_req, dual_req;
  // Set the Krylov method and find its requirements
  if (options_["krylov.solver"].as<string>() == "cg")
    krylov_.reset(new STCGSolver<DesignVector>());
  else if (options_["krylov.solver"].as<string>() == "fitr")
    krylov_.reset(new FITRSolver<DesignVector>());
  else {
    cerr << "TrustRegionRSNK::MemoryRequirements(): "
         << "invalid krylov solver for this optimizer: "
         << options_["krylov.solver"].as<string>() << endl;
    throw(-1);
  }
  krylov_->SubspaceSize(options_["krylov.space_size"].as<int>());
  ptree num_required;
  krylov_->MemoryRequired(num_required);
  num_design += num_required.get<int>("num_vec");
  // Set the quasi-Newton method and find its requirements
  if (options_["quasi_newton.type"].as<string>() == "lbfgs") {
    quasi_newton_.reset(new GlobalizedBFGS
                        (options_["quasi_newton.max_stored"].as<int>()));
  } else {
    quasi_newton_.reset(new GlobalizedSR1
                        (options_["quasi_newton.max_stored"].as<int>()));
  }
  quasi_newton_->MemoryRequired(design_req);
  num_design += design_req;
  // Set the matrix-vector product and find its requirements
  mat_vec_.reset(new ReducedHessianProduct
                 (options_["reduced.krylov_size"].as<int>()));
  num_required.clear();
  mat_vec_->MemoryRequired(num_required);
  num_design += num_required.get<int>("design");
  num_state += num_required.get<int>("state");
  num_dual += num_required.get<int>("dual");
  // Set the preconditioner and find its requirements
  if (options_["reduced.precond"].as<string>() == "none")
    precond_.reset(new IdentityPreconditioner<DesignVector>());
  else if (options_["reduced.precond"].as<string>() == "quasi_newton")
    precond_.reset(new ReducedSpacePreconditioner(quasi_newton_.get()));
  else if (options_["reduced.precond"].as<string>() == "nested_krylov")
    precond_.reset(new NestedReducedSpacePreconditioner());
  else {
    cerr << "ReducedSpaceNewtonKrylov::MemoryRequirements(): "
         << "invalid preconditioner for this optimizer: "
         << options_["reduced.precond"].as<string>() << endl;
    throw(-1);
  }
  precond_->MemoryRequired(num_required);
  num_design += num_required.get<int>("design");
  num_state += num_required.get<int>("state");
  num_dual += num_required.get<int>("dual");  
}
// ==============================================================================
void TrustRegionRSNK::Solve() {
  // open output files to save information and convergence histories
  ostream* info;
  ofstream info_file;
  if (options_["send_info_to_cout"].as<bool>())
    info = &std::cout;
  else {
    info_file.open(options_["info_file"].as<string>().c_str());
    info = &info_file;
  }
  quasi_newton_->SetOutputStream(*info);
  ofstream hist_out(options_["hist_file"].as<string>().c_str());
  WriteHistoryHeader(hist_out);
  ofstream krylov_out(options_["krylov.file"].as<string>().c_str());
  ofstream reduced_out(options_["reduced.krylov_file"].as<string>().c_str());  
  
  DesignVector X; // the current solution of the reduced system
  DesignVector P; // the search direction
  DesignVector dJdX; // current reduced gradient
  DesignVector dJdX_old; // work vector
  PDEVector state; // the current solution of the primal PDE 
  PDEVector adjoint; // the current solution of the adjoint PDE
  PDEVector adjoint_res; // residual of adjoint equation

  // work arrays
  std::vector<DesignVector> design_work1(2, X);
  std::vector<PDEVector> pde_work1(5, state);
  
  // set initial guess and solve for state and adjoint
  X.EqualsInitialDesign();
  state.EqualsPrimalSolution(X);
  adjoint.EqualsAdjointSolution(X, state);
  CurrentSolution(0, X, state, adjoint);
  double obj = ObjectiveValue(X, state);

  // initialize the Quasi-Newton approximation
  quasi_newton_->SetInvHessianToIdentity();
  
  int iter = 0;
  int nonlinear_sum = 0;
  bool converged = false;
  double grad_norm0, grad_norm, grad_tol;
  double obj_old = obj;
  double rho = 0.0;
  double radius = options_["trust.init_radius"].as<double>();
  double krylov_tol;
  double sens_norm = 1.0;
  double dudx_norm = 1.0;
  double dpsidx_norm = 1.0;
  while (iter < options_["max_iter"].as<int>()) {
    iter++;

    *info << endl;
    *info << "==================================================" << endl;
    *info << "Beginning Trust-Region iteration " << iter << endl;
    *info << endl;

    // check for convergence and update Quasi-Newton method if necessary
    dJdX.EqualsReducedGradient(X, state, adjoint, design_work1[0]);
    if (iter == 1) {
      grad_norm0 = dJdX.Norm2();
      grad_norm = grad_norm0;
      quasi_newton_->set_norm_init(1.0);
      *info << "grad_norm0 = " << grad_norm0 << endl;
      grad_tol = options_["des_tol"].as<double>()*grad_norm0; //max(grad_norm0, 1.e-6);
      dJdX_old = dJdX;
    } else {
      grad_norm = dJdX.Norm2();
      *info << "grad_norm : grad_tol = "
           << grad_norm << " : " << grad_tol << endl;
      dJdX_old -= dJdX;
      dJdX_old *= -1.0;
      quasi_newton_->AddCorrection(P, dJdX_old);
      dJdX_old = dJdX;
    }
    CurrentSolution(nonlinear_sum, X, state, adjoint);    
    WriteHistoryIteration(iter, UserMemory::get_precond_count(), grad_norm,
                          rho, radius, hist_out);
    if (grad_norm < grad_tol) {             
      converged = true;
      break;
    }
    // compute Krylov tolerance to achieve superlinear convergence, but to
    // avoid oversolving near the desired nonlinear tolerance
    krylov_tol = options_["krylov.tolerance"].as<double>()*
        std::min(1.0, sqrt(grad_norm/grad_norm0)); // superlinear
    krylov_tol = std::max(krylov_tol, grad_tol/grad_norm); // avoid oversolve
    krylov_tol *= options_["reduced.nu"].as<double>();    
    *info << "krylov_tol = " << krylov_tol << endl;
    
    // need adjoint residual for reduced-Hessian product
    adjoint_res.EqualsObjectiveGradient(X, state);
    pde_work1[0].EqualsTransJacobianTimesVector(X, state, adjoint);
    adjoint_res += pde_work1[0];

    // set Hessian-vector product tolerance parameters
    // Note: we must divide by nu in product_fac, because nu appears in the
    // krylov_tol, which is fed back to the Hessian-vector product; we do not
    // want this nu factor in product_fac (we only want 1.0 - nu), so we
    // "remove" it here
    double product_fac = options_["reduced.product_fac"].as<double>() *
        (1.0 - options_["reduced.nu"].as<double>()) /
        options_["reduced.nu"].as<double>();
#if 0
    double product_fac = (1.0 - options_["reduced.nu"].as<double>()) /
        (*sig*options_["reduced.nu"].as<double>());
#endif
    if (!options_["reduced.dynamic_tol"].as<bool>())
      product_fac *= (krylov_tol/static_cast<double>(
          options_["krylov.space_size"].as<int>()));

    // Initialize the reduced-Hessian-vector product
    ptree prod_param;
    prod_param.put("product_fac", product_fac);
    // set parameters for sensitivity norm bounds (determine the 2nd-order
    // adjoint tolerances)
    if (!(options_["reduced.bound_frozen"].as<bool>()) || (iter == 1) ) {
      prod_param.put("bound_frozen", false);
      prod_param.put("bound_type",
                     options_["reduced.bound_type"].as<string>());
      prod_param.put("bound_approx",
                     options_["reduced.bound_approx"].as<bool>());
    } else {
      prod_param.put("bound_frozen", true);
    }
    boost::static_pointer_cast<ReducedHessianProduct>(mat_vec_)->Initialize(
        X, state, adjoint, dJdX_old, adjoint_res, design_work1, pde_work1,
        prod_param, reduced_out);
    if (options_["quasi_newton.matvec_update"].as<bool>()) {
      // update the quasi-Newton method during Hessian-vector products
      boost::static_pointer_cast<ReducedHessianProduct>(mat_vec_)
          ->set_quasi_newton(quasi_newton_.get());
    }

    // ...and initialize the preconditioner, if necessary
    ptree prec_param;
    if (options_["reduced.precond"].as<string>() == "nested_krylov") {
      boost::static_pointer_cast<NestedReducedSpacePreconditioner>(precond_)->
          Initialize(X, state, adjoint, dJdX_old, adjoint_res, design_work1,
                     pde_work1, prec_param, reduced_out);
    }
    
    // inexactly solve the trust-region problem
    P = 0.0;
    dJdX *= -1.0;
    ptree ptin, ptout;
    ptin.put<double>("tol", krylov_tol);
    ptin.put<double>("radius", radius);
    ptin.put<bool>("check", options_["krylov.check_res"].as<bool>());
    ptin.put<bool>("dynamic", options_["reduced.dynamic_tol"].as<bool>());
    krylov_->Solve(ptin, dJdX, P, *mat_vec_, *precond_, ptout, krylov_out);
    bool active = ptout.get<bool>("active");
    double pred = ptout.get<double>("pred");
    dJdX *= -1.0;
    X += P;
    
    // compute the actual reduction and the trust parameter rho
    obj_old = obj;
    adjoint_res = state; // save current state
    if (state.EqualsPrimalSolution(X)) {
      obj = ObjectiveValue(X, state);
      rho = (obj_old - obj)/pred;
    } else {
      // failed to converge 
      rho = -1E-16;
    }      

    // update the radius if necessary
    if (rho < 0.25) {
      radius *= 0.25;
      // attempt to resolve
      if (options_["krylov.solver"].as<string>() == "fitr") {
        ptree ptin, ptout;
        ptin.put<double>("radius", radius);
        X -= P;
        dJdX *= -1.0;
        krylov_->ReSolve(ptin, dJdX, P, ptout, krylov_out);
        dJdX *= 1.0;
        X += P;
        pred = ptout.get<double>("pred");
        if (state.EqualsPrimalSolution(X)) {
          obj = ObjectiveValue(X, state);
          rho = (obj_old - obj)/pred;
        } else {
          // failed to converge 
          rho = -1E-16;
        }
      }
    } else {
      if ( (rho > 0.75) && (active) )
        radius = std::min(2.0*radius, options_["trust.max_radius"].as<double>());
    }

    // revert the solution if necessary
    if (rho < options_["trust.tol"].as<double>()) {
      *info << "reverting solution..." << endl;
      X -= P;
      state = adjoint_res;
    } else {
      // adjoint is not up-to-date
      adjoint.EqualsAdjointSolution(X, state);
    }
    nonlinear_sum++;
  }
  *info << "Total number of nonlinear iterations: " << nonlinear_sum << endl;
}
// ==============================================================================
void TrustRegionRSNK::WriteHistoryHeader(ostream& out) {
  if (!(out.good())) {
    cerr << "TrustRegionRSNK::WriteHistoryHeader(): "
	 << "ostream is not good for i/o operations." << endl;
    throw(-1);
  }
  out << "# Kona trust-region RSNK convergence history file\n";
  out << boost::format(
      string("%|7| %|9t| %|14| %|25t| %|-12| %|37t| %|-12| %|49t| %|-12|\n"))
      % "# iter" % "precond. calls" % "grad norm" % "ratio" % "radius";
  out.flush();
}
// ==============================================================================
void TrustRegionRSNK::WriteHistoryIteration(
    const int& iter, const int& precond_calls, const double& norm,
    const double& rho, const double& radius, ostream& out) {
  if (!(out.good())) {
    cerr << "TrustRegionRSNK::WriteHistoryIteration(): "
	 << "ostream is not good for i/o operations." << endl;
    throw(-1);
  }
  out << boost::format(string("%|7| %|9t| %|14| %|25t| %|-12.6| %|37t|")+
                       string("%|-12.6| %|49t| %|-12.6|\n"))
      % iter % precond_calls % norm % rho % radius;
  out.flush();
}
// ==============================================================================
} // namespace kona
