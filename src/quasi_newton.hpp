/**
 * \file quasi_newton.h
 * \brief header file for the QuasiNewton and derived class
 * \author Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#pragma once

#include <math.h>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include <ostream>
#include <iostream>
#include <deque>
#include <vector>
#include "./vectors.hpp"

namespace kona {

using std::ostream;
using std::cerr;
using std::cout;
using std::endl;
using std::deque;
using std::vector;
namespace ublas = boost::numeric::ublas;

// ==============================================================================
/*!
 * \class QuasiNewton
 * \brief abstract base class for Broyden-type Quasi-Newton methods
 *
 * This class can be used to derive Quasi-Newton methods of Broyden
 * type, in particular BFGS and SR1.
 % 
 * \todo This should be templated on a generic vector type
 */
class QuasiNewton {
 public:

  /*!
   * \brief default constructor
   * \param[in] max_stored - the maximum number of corrections
   * \param[in,out] out - a valid stream for output diagnostics
   */
  QuasiNewton(const int & max_stored, ostream& out = cout);

  /*!
   *  \brief class destructor
   */
  virtual ~QuasiNewton() {}

  /*!
   * \brief returns the memory requirements of the object
   * \param[out] num_vector - number of vectors required
   */
  virtual void MemoryRequired(int& num_vector) const = 0;

  /*!
   * \brief defines the output stream used for diagnostics
   * \param[in,out] out - a valid stream for output
   */
  void SetOutputStream(ostream& out);
  
  /*!
   * \brief set initial norm of the design component of the gradient
   * \param[in] norm - initial norm of design component of the gradient
   */
  void set_norm_init(const double & norm);

  /*!
   * \brief redefined for globalized versions of BFGS and SR1
   * \param[in] lambda - value on diagonal of a matrix added to approx. Hessian
   */
  virtual void set_lambda(const double & lambda) {};

  /*!
   * \brief set the initial inverse Hessian to the identity matrix
   */
  void SetInvHessianToIdentity();

  /*!
   * \brief add new corrections and discard old if necessary
   * \param[in] s_new - difference between new and old variable
   * \param[in] y_new - difference between new and old gradient
   */
  virtual void AddCorrection(const DesignVector & s_new,
                             const DesignVector & y_new);

  /*!
   * \brief applies the inverse Hessian approximation to a DesignVector
   * \param[in] u - DesignVector being multiplied by approximate H^{-1}
   * \param[out] v - DesignVector that is the result of the product
   */
  virtual void ApplyInvHessianApprox(DesignVector & u,
                                     DesignVector & v) const = 0;

  /*!
   * \brief applies the Hessian approximation to a DesignVector
   * \param[in] u - DesignVector being multiplied by approximate H
   * \param[out] v - DesignVector that is the result of the product
   */
  virtual void ApplyHessianApprox(const DesignVector & u,
                                  DesignVector & v) const = 0;
#if 0
  /*!
   * \brief checks if inv Hessian is the inverse of a given matrix
   * \param[in] hess - matrix stored as columns of DesignVectors
   * \returns - true if hess * (inv Hessian) = identity (to machine tol.)
   */
  bool equalsInvHessian(const vector<DesignVector> & hess);
#endif
 protected:

  ostream* out_; ///< output stream for diagnostics
  int max_stored_; ///< maximum number of corrections stored
  double norm_init_; ///< initial norm of design component of gradient
  boost::scoped_ptr<DesignVector>
  init_Hessian_; ///< initial (diagonal) Hessian approx. (stored as a vector)
  deque<DesignVector>
  s_; ///< difference between subsequent solutions: s_[k] = x_[k+1] - x[k]
  deque<DesignVector>
  y_; ///< difference between subsequent gradients: y_[k] = g[k+1] - g[k]
};
// ==============================================================================
/*!
 * \class LimitedMemoryBFGS
 * \brief limited memory Broyden–Fletcher–Goldfarb–Shanno method
 *
 * This class implements the limited memory variation of the
 * Broyden–Fletcher–Goldfarb–Shanno Quasi-Newton method due to J. Nocedal
 * (Mathematics of Compuation, 35, pp 773-782, 1980).  Permits the inclusion of
 * a diagonal matrix for Trust-region methods.
 */
class LimitedMemoryBFGS : public QuasiNewton {
 public:

  /*!
   * \brief default constructor
   * \param[in] max_stored - the maximum number of corrections
   * \param[in,out] out - a valid stream for output diagnostics
   */
  LimitedMemoryBFGS(const int & max_stored, ostream& out = cout);

  /*!
   * \brief returns the memory requirements of the object
   * \param[out] num_vector - number of vectors required
   */
  void MemoryRequired(int& num_vector) const;
  
  /*!
   * \brief set the value of the diagonal matrix entries
   * \param[in] lambda - the desired value for the diagonal
   * \pre lambda > 0.0
   */
  void set_lambda(const double & lambda);
  
  /*!
   * \brief add new corrections only if curvature condition is satisfied
   * \param[in] s_new - difference between new and old variable
   * \param[in] y_new - difference between new and old gradient
   */
  void AddCorrection(const DesignVector & s_new,
                     const DesignVector & y_new);

  /*!
   * \brief applies the BFGS inv Hessian approximation to a DesignVector
   * \param[in] u - DesignVector being multiplied by approximate H^{-1}
   * \param[out] v - DesignVector that is the result of the product
   */
  void ApplyInvHessianApprox(DesignVector & u,
                             DesignVector & v) const;

  /*!
   * \brief applies the BFGS Hessian approximation to a DesignVector
   * \param[in] u - DesignVector being multiplied by approximate H
   * \param[out] v - DesignVector that is the result of the product
   */
  void ApplyHessianApprox(const DesignVector & u,
                          DesignVector & v) const;

 private:

  double lambda_; ///< value on diagonal matrix (if needed)
  deque<double> s_dot_y_; ///< stores the inner product between s and y
  deque<double> s_dot_s_; ///< stores the norm squared of the s
};
// ==============================================================================
/*!
 * \class GlobalizedBFGS
 * \brief LimitedMemoryBFGS for homotopy-based optimization
 *
 * This class implements the limited memory variation of the
 * Broyden–Fletcher–Goldfarb–Shanno Quasi-Newton method due to
 * J. Nocedal (Mathematics of Compuation, 35, pp 773-782, 1980).  The
 * class if very similar to LimitedMemoryBFGS, but is implemented for
 * homotopy/continuation-based optimization.
 */
class GlobalizedBFGS : public QuasiNewton {
 public:

  /*!
   * \brief class constructor
   * \param[in] max_stored - the maximum number of corrections
   * \param[in,out] out - a valid stream for output diagnostics
   */
  GlobalizedBFGS(const int & max_stored, ostream& out = cout);

  /*!
   * \brief returns the memory requirements of the object
   * \param[out] num_vector - number of vectors required
   */
  void MemoryRequired(int& num_vector) const;
  
  /*!
   * \brief set the value of the continuation parameter
   * \param[in] lambda - the desired value for the continuation parameter
   */
  void set_lambda(const double & lambda);

  /*!
   * \brief add new corrections only if curvature condition is satisfied
   * \param[in] s_new - difference between new and old variable
   * \param[in] y_new - difference between new and old gradient
   */
  void AddCorrection(const DesignVector & s_new,
                     const DesignVector & y_new);

  /*!
   * \brief applies the BFGS inv Hessian approximation to a DesignVector
   * \param[in] u - DesignVector being multiplied by approximate H^{-1}
   * \param[out] v - DesignVector that is the result of the product
   */
  void ApplyInvHessianApprox(DesignVector & u,
                             DesignVector & v) const;

  /*!
   * \brief applies the BFGS Hessian approximation to a DesignVector
   * \param[in] u - DesignVector being multiplied by approximate H
   * \param[out] v - DesignVector that is the result of the product
   */
  void ApplyHessianApprox(const DesignVector & u,
                          DesignVector & v) const;

 private:
  double lambda_; ///< continuation paramter
  deque<double> s_dot_y_; ///< stores the inner product between s and y
  deque<double> s_dot_s_; ///< stores the norm squared of the s
};
// ==============================================================================
/*!
 * \class GlobalizedSR1
 * \brief LimitedMemorySR1 for homotopy-based optimization
 *
 * This class implements a limited memory version of the symmetric rank-1
 * quasi-Newton method (see Nocedal and Wright).  The class accommodates a
 * homotopy/continuation-based optimization.  Note that the implementation is
 * not presently efficient (quadratic in the number of saved vectors).
 */
class GlobalizedSR1 : public QuasiNewton {
 public:

  /*!
   * \brief class constructor
   * \param[in] max_stored - the maximum number of corrections
   * \param[in,out] out - a valid stream for output diagnostics
   */
  GlobalizedSR1(const int & max_stored, ostream& out = cout);

  /*!
   * \brief returns the memory requirements of the object
   * \param[out] num_vector - number of vectors required
   */
  void MemoryRequired(int& num_vector) const;
  
  /*!
   * \brief set the value of the continuation parameter
   * \param[in] lambda - the desired value for the continuation parameter
   */
  void set_lambda(const double & lambda);

  /*!
   * \brief add new corrections only if curvature condition is satisfied
   * \param[in] s_new - difference between new and old variable
   * \param[in] y_new - difference between new and old gradient
   */
  void AddCorrection(const DesignVector & s_new,
                     const DesignVector & y_new);

  /*!
   * \brief applies the SR1 inv Hessian approximation to a DesignVector
   * \param[in] u - DesignVector being multiplied by approximate H^{-1}
   * \param[out] v - DesignVector that is the result of the product
   */
  void ApplyInvHessianApprox(DesignVector & u,
                             DesignVector & v) const;

  /*!
   * \brief applies the SR1 Hessian approximation to a DesignVector
   * \param[in] u - DesignVector being multiplied by approximate H
   * \param[out] v - DesignVector that is the result of the product
   */
  void ApplyHessianApprox(const DesignVector & u,
                          DesignVector & v) const;

 private:
  double lambda_; ///< continuation paramter
  //vector<DesignVector> z_; /// z_[k] = s_[k] - H_k * y_[k]
};
// ==============================================================================
} // namespace kona
