/**
 * \file optimizer.cpp
 * \brief definitions of Optimizer class member functions
 * \author Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#include <math.h>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
//#include <iostream>
#include <fstream>

#include "./optimizer.hpp"
#include "./kkt_vector.hpp"
#include "./reduced_kkt_vector.hpp"
#include "./merit.hpp"
#include "./line_search.hpp"
#include "./quasi_newton.hpp"
#include "./krylov.hpp"

#include <boost/program_options.hpp>
#include <boost/format.hpp>

using std::string;
using std::ostream;
using std::ifstream;
using std::ofstream;
using std::ostringstream;
using std::ios;
using std::max;
using std::vector;
using std::map;

namespace kona {

// The following is deprecated
// ======================================================================

void OldOptimizer::MemoryRequirements(int & num_design, int & num_state,
                                   int & num_dual) {
  num_dual = 0.0;
  if (options["opt_method"].as<string>() == "aug_lag") {
    opt_method = &OldOptimizer::ReducedSpaceAugmentedLagrangianOptimize;

    // number of pde vectors required
    num_state = 8; // state + adjoint + adjoint_res + (5 for product)
    // number of pde vectors for nested preconditioner (should be conditional)
    num_state += 5;
    // work vectors needed by Krylov solver inside reduced-hessian product
    num_state += 2*(options["reduced.krylov_size"].as<int>() + 1);

    // number of desgin vectors needed
    num_design = 5; // X, dLdX, dLdX_old, initial_design, P
    num_design += 1; // for KKT vector
    num_design += 2; // for product
    num_design += 2; // for nested preconditioner
    num_design += 7; // for MINRES part of nested preconditioner
    num_design += 3; // for estimation of sensitivity matrix norm
    num_design += 1; // for CheckProduct !!!! Temporary
    // number needed for quasi-Newton approximation (s_new, y_new)
    num_design += 2*options["quasi_newton.max_stored"].as<int>();
    if (options["quasi_newton.type"].as<string>() == "sr1") // z_new
      num_design += 2 + options["quasi_newton.max_stored"].as<int>();
    // vector needed for the initial (diagonal) Hessian approximation
    num_design += 1;
    if (options["krylov.solver"].as<string>() == "fitr") {
      // work vectors needed by FITR iterative solver
      num_design += 2*(options["krylov.space_size"].as<int>() + 1);
    } else if (options["krylov.solver"].as<string>() == "cg") {
      // work vectors need by Steihaug CG
      num_design += 4;
    } else if (options["krylov.solver"].as<string>() == "ffom") {
      // work vectors need by Steihaug FFOM
      num_design += 2*options["krylov.space_size"].as<int>() + 1;      
    }

    // dual vectors needed
    num_dual = 2;
    num_dual += 1 + 1; // for dual_work1 and KKT vector
        
  } else if (options["opt_method"].as<string>() == "lnks") {
    // use the LNKS optimizer algorithm
    opt_method = &OldOptimizer::LNKSOptimize;
    
    // first, determine the number of KKT vectors needed, which are built
    // of both design and state variables
    int num_kkt = 0;
    
    // kkt vectors needed during the Krylov iterative solution process
    if ( (options["krylov.solver"].as<string>() == "fgmres") ||
         (options["krylov.solver"].as<string>() == "ffom") ) {
      num_kkt += 2*options["krylov.space_size"].as<int>() + 1;
    } else if (options["krylov.solver"].as<string>() == "gmres") {
      num_design += options["krylov.space_size"].as<int>() + 2;
    } else if (options["krylov.solver"].as<string>() == "minres") {
      num_kkt += 7;
    }
    // kkt vectors needed in the nonlinear portion of algorithm
    num_kkt += 4; // X, Res, dX, KKTProduct::operator()->perturb_kkt

    // determine the number of design-sized vectors required
    num_design = num_kkt;
    // number needed for quasi-Newton approximation (s_new, y_new)
    num_design += 2*options["quasi_newton.max_stored"].as<int>();
    // vector needed for the initial (diagonal) Hessian approximation
    num_design += 1;
    // work vectors needed
    num_design += 2;
    // +1 for initial_design
    num_design += 1;
    // work needed in LNKSOptimize()
    num_design += 1;

    // determine the number of state-sized vectors required
    num_state = 2*num_kkt;
    // work vectors needed
    num_state += 1;
    // +1 for KKTPreconditioner::operator()
    num_state += 1;

  } else if (options["opt_method"].as<string>() == "inexact") {
    // use the inexact-Newton algorithm of Byrd et al.
    opt_method = &OldOptimizer::InexactNewtonOptimize;

    // first, determine the number of KKT vectors needed, which are built
    // of both design and state variables
    int num_kkt = 0;
    
    // kkt vectors needed during the Krylov iterative solution process
    if (options["krylov.solver"].as<string>() == "fgmres") {
      num_kkt += 2*(options["krylov.space_size"].as<int>() + 1);
    } else if (options["krylov.solver"].as<string>() == "minres") {
      num_kkt += 7;
    }
    // kkt vectors needed in the nonlinear portion of algorithm
    num_kkt += 4; // X, Res, dX, KKTProduct::operator()->perturb_kkt
    num_kkt += 2; // kktwork1, kktwork2

    // determine the number of design-sized vectors required
    num_design = num_kkt;
    // number needed for quasi-Newton approximation (s_new, y_new)
    num_design += 2*options["quasi_newton.max_stored"].as<int>();
    // vector needed for the initial (diagonal) Hessian approximation
    num_design += 1;
    // work vectors needed (EqualskKTConditions(), 
    num_design += 1;
    // work needed in InexactNewtonOptimize()
    num_design += 1;
    
    // determine the number of state-sized vectors required
    num_state = 2*num_kkt;
    // work vectors needed in InexactNewtonOptimize()
    num_state += 2;
    // +1 for KKTPreconditioner::operator()
    num_state += 1;

  }
    
  cout << "num_design = " << num_design << endl;
  cout << "num_state  = " << num_state << endl;
  cout << "num_dual   = " << num_dual << endl;
}

// ======================================================================

void OldOptimizer::CalcReducedGradient(const DesignVector & x,
                                       const PDEVector & state,
                                       const PDEVector & adjoint,
                                       DesignVector & design_work,
                                       DesignVector & djdx) const {
  // set gradient value
  design_work.EqualsObjectiveGradient(x, state);
  djdx.EqualsTransJacobianTimesVector(x, state, adjoint);
  djdx += design_work;
}

// ======================================================================

void OldOptimizer::Optimize() {
  (*this.*opt_method)();
}

// ======================================================================

void OldOptimizer::LNKSOptimize() {

  KKTVector X; // the current solution of the KKT system
  KKTVector dX;// linear update; solution of the linearized system
  KKTVector Res; // the right-hand-side of the linearized KKT system

  // set initial guess and store initial design
  X.EqualsInitialGuess();
  DesignVector initial_design, design_work;
  initial_design = X.get_design();

  cout << "Initial values:" << endl << "\t";
  X.UpdateUser(0);

  // open some output files to save histories
  ofstream krylov_out(options["krylov.file"].as<string>().c_str());
  ofstream hist_out(options["hist_file"].as<string>().c_str());  

  // initialize the Quasi-Newton approximation
  QuasiNewton* quasi_newton = new GlobalizedBFGS
      (options["quasi_newton.max_stored"].as<int>());
  quasi_newton->SetInvHessianToIdentity();

  int iter = 0;
  int nonlinear_sum = 0;
  bool converged = false;
  double design_tol, pde_tol;
  double design_norm0, pde_norm0, adjoint_norm0;
  double lambda = options["inner.lambda_init"].as<double>();
  vector<double> outer_norms(4, 0.0);
  vector<double> inner_norms(4, 0.0);
  //ostringstream outer_hist_string(ostringstream::out);
  while (iter < options["max_iter"].as<int>()) {
    iter++;

    cout << endl;
    cout << "==================================================" << endl;
    cout << "Beginning Homotopy loop number " << iter << endl;
    cout << "lambda = " << lambda << endl;
    cout << endl;

    // check for convergence
    Res.EqualsKKTConditions(X);
    if (iter == 1) {
      Res.CalcNorms(design_norm0, pde_norm0, adjoint_norm0);
      outer_norms[1] = design_norm0;
      outer_norms[2] = pde_norm0;
      outer_norms[3] = adjoint_norm0;
      outer_norms[0] = sqrt(outer_norms[1]*outer_norms[1] +
                            outer_norms[2]*outer_norms[2] +
                            outer_norms[3]*outer_norms[3]);

      static_cast<GlobalizedBFGS*>(quasi_newton)
          ->set_norm_init(design_norm0);

#if 0
      static_cast<GlobalizedBFGS*>(quasi_newton)
          ->set_norm_init(1.0); 
#endif
      cout << "design_norm0  = " << design_norm0 << endl;
      cout << "pde_norm0     = " << pde_norm0 << endl;
      cout << "adjoint_norm0 = " << adjoint_norm0 << endl;
      design_tol = options["des_tol"].as<double>()*design_norm0; //*max(design_norm0, 1.e-6);
      //pde_tol = options["pde_tol"].as<double>()*max(pde_norm0, 1.e-6);
      pde_tol = options["pde_tol"].as<double>()*max(pde_norm0, 1.0);
    } else if (Res.InexactConverged(outer_norms, design_tol, pde_tol)) {
      converged = true;
    }
#if 0
(Res.OuterConverged(outer_norms, 
        design_norm0, options["des_tol"].as<double>(),
        pde_norm0, options["pde_tol"].as<double>(),
        adjoint_norm0, options["adj_tol"].as<double>())) {
#endif
#if 0
    WriteLNKSOuterIter(iter, nonlinear_sum, 
                       UserMemory::get_precond_count(), outer_norms,
                       outer_hist_string);
#endif
    if (converged) break;

    // solve the globalized KKT system in the following inner loop    
    static_cast<GlobalizedBFGS*>(quasi_newton)->set_lambda(lambda);
    int inner_iter = 0;
    bool inner_converged = false;
    double inner_design_norm0, inner_pde_norm0, inner_adjoint_norm0,
        inner_kkt_norm0;
    ostringstream hist_string(ostringstream::out);
    double krylov_tol;
    while (inner_iter < options["inner.max_iter"].as<int>()) {
      inner_iter++;

      cout << "-------------------------" << endl;
      cout << "inner iteration = " << inner_iter << endl;

      // evaluate the right-hand-side of the linearized KKT system
      Res.EqualsKKTConditions(X); // this is duplicated on first iter
      
      // update the quasi-Newton method
      if (inner_iter == 1) {
        design_work = Res.get_design();
      } else {
        design_work -= Res.get_design();
        design_work *= -1.0;
        dX *= -1.0;
        quasi_newton->AddCorrection(dX.get_design(), design_work);
        design_work = Res.get_design();
      }
      //cout << "Res value before globalization: " << endl << "\t";
      //Res.UpdateUser();

      // get the norms of the non-globalized problem (for output)
      if (Res.InexactConverged(outer_norms, design_tol, pde_tol)) {
        inner_converged = true;
        converged = true;
      }
#if 0
      if (Res.OuterConverged(
              outer_norms, design_norm0, options["des_tol"].as<double>(),
              pde_norm0, options["pde_tol"].as<double>(),
              adjoint_norm0, options["adj_tol"].as<double>())) {
        inner_converged = true;
        converged = true;
      }
#endif
      // add globalization terms and check for convergence
      //Res.AddGlobalization(X, lambda, initial_design, 1.0);
      Res.AddGlobalization(X, lambda, initial_design, design_norm0);
      //cout << "Res value after globalization: " << endl << "\t";
      //Res.UpdateUser();

      if (fabs(lambda) < kEpsilon) {
        if (Res.InexactConverged(outer_norms, design_tol, pde_tol)) {
          // use outer convergence criteria
          inner_converged = true;
        }
        inner_norms[0] = outer_norms[0];
        inner_norms[1] = outer_norms[1];
        inner_norms[2] = outer_norms[2];
        inner_norms[3] = outer_norms[3];
      } else {
        if (inner_iter == 1) {
          Res.CalcNorms(inner_design_norm0, inner_pde_norm0,
                        inner_adjoint_norm0);
          inner_norms[1] = inner_design_norm0;
          inner_norms[2] = inner_pde_norm0;
          inner_norms[3] = inner_adjoint_norm0;
          inner_kkt_norm0 = sqrt(inner_norms[1]*inner_norms[1] +
                                inner_norms[2]*inner_norms[2] +
                                inner_norms[3]*inner_norms[3]);
          inner_norms[0] = inner_kkt_norm0;
        } else if (Res.InnerConverged(
            inner_norms, inner_design_norm0, 
            options["inner.des_tol"].as<double>(),
            inner_pde_norm0, options["inner.pde_tol"].as<double>(),
            inner_adjoint_norm0, options["inner.adj_tol"].as<double>())) {
          inner_converged = true;
        }
      }
      WriteLNKSIter(iter, inner_iter, nonlinear_sum+1, 
                    UserMemory::get_precond_count(), outer_norms,
                    inner_norms, hist_string);
      if (inner_converged) break;

#if 0
      // compute Krylov tolerance to achieve superlinear convergence, but to
      // avoid oversolving near the desired nonlinear tolerance: no difference
      // with fixed tol = 0.01...scaling issue between primal, adjoint and
      // control equations?
      krylov_tol = 0.1*std::min(0.1, sqrt(
          inner_norms[0]/inner_kkt_norm0)); // superlinear
      //krylov_tol = std::max(krylov_tol, grad_tol/grad_norm); // avoid oversolve
      cout << "krylov_tol (inner) = " << krylov_tol << endl;
#endif

      // set equation scaling
      double design_scale = 1.0; //std::max(inner_norms[1], design_tol);
      double state_scale = 1.0; //std::max(inner_norms[3], pde_tol); //*100;
      double adjoint_scale = 1.0; //std::max(inner_norms[2], pde_tol); //*50;

      cout << "design_scale = " << design_scale << endl;
      cout << "state_scale = " << state_scale << endl;
      cout << "adjoint_scale = " << adjoint_scale << endl;

      // define the matrix-vector product ...
      // WARNING: this scales Res
#if 0
      MatrixVectorProduct<KKTVector>* mat_vec =
          new GlobalizedKKTProduct(&X, &Res, lambda, &initial_design,
                                   1.0);
#endif
      MatrixVectorProduct<KKTVector>* mat_vec =
          new GlobalizedKKTProduct(&X, &Res, lambda, &initial_design,
                                   design_norm0, design_scale, state_scale,
                                   adjoint_scale);
      // ...and define the preconditioner
#if 1
      Preconditioner<KKTVector>* precond =
          new KKTPreconditioner(&X, quasi_newton);
#else
      Preconditioner<KKTVector>* precond =
          new IdentityPreconditioner<KKTVector>();
#endif
      
      // solve the linearized KKT equation, and update solution X
      dX = 0.0;
      int iters;
      if (options["krylov.solver"].as<string>() == "fgmres")
        FGMRES<KKTVector>(options["krylov.space_size"].as<int>(),
                          options["krylov.tolerance"].as<double>(), 
                          Res, dX, *mat_vec, *precond, iters, krylov_out,
                          options["krylov.check_res"].as<bool>());
      X -= dX;
      nonlinear_sum++;
      X.UpdateUser(nonlinear_sum);

      //if (inner_iter == 5) throw(-1);
      
    } // while inner_iter < max_iter

    //if (iter == 1) throw(-1);]

    //X.UpdateUser();

    // update the continuation parameter
    //lambda = max(0.0, lambda - 0.1);
    //lambda = max(0.0, lambda - 0.05);
    //lambda = max(0.0, lambda - 0.01);
    //lambda = 0.1*lambda;
    double rel_res = outer_norms[0]/sqrt(design_norm0*design_norm0
                                         + pde_norm0*pde_norm0
                                         + adjoint_norm0*adjoint_norm0);
    cout << "rel_res = " << rel_res << endl;
    //lambda = std::min(0.5*lambda, rel_res*rel_res);
    lambda = std::min(0.9*lambda, rel_res*rel_res);
    
    // write the convergence history from the preceeding inner iteration
    WriteLNKSHistory(iter, inner_iter, hist_string, hist_out);

  }
  cout << "Total number of nonlinear iterations: " 
       << nonlinear_sum << endl;
  // write the convergence history from the outer iterations
  //WriteLNKSOuterHistory(iter, outer_hist_string, hist_out);
  krylov_out.close();
  hist_out.close();
}

// ======================================================================

void OldOptimizer::InexactNewtonOptimize() {

  KKTVector X; // the current solution of the KKT system
  KKTVector dX;// linear update; solution of the linearized system
  KKTVector Res; // the right-hand-side of the linearized KKT system

  // work vectors (equivalent to 3 KKT vectors; this could be reduced)
  KKTVector kktwork1, kktwork2;
  DesignVector design_work;
  PDEVector state_work;
  PDEVector adjoint_work;

  // set initial guess and store initial design
  X.EqualsInitialGuess();

  cout << "Initial values:" << endl << "\t";
  X.UpdateUser(0);

  // open some output files to save histories
  ofstream krylov_out(options["krylov.file"].as<string>().c_str());
  ofstream hist_out(options["hist_file"].as<string>().c_str());  

  // initialize the line-search  
  const double alpha_init = 1.0;
  const double alpha_min = kEpsilon;
  const double sufficient = 1e-4;
  const double reduct_fac = 0.1;
  LineSearch* line_search = new 
      BackTracking(alpha_init, alpha_min, options["inexact.mu"].as<double>(),
                   reduct_fac);

  // initialize the Quasi-Newton approximation
  QuasiNewton* quasi_newton = new GlobalizedBFGS
      (options["quasi_newton.max_stored"].as<int>());
  quasi_newton->SetInvHessianToIdentity();

  int iter = 0;
  int nonlinear_sum = 0;
  bool converged = false;
  double kkt_norm0, design_norm0, pde_norm0, adjoint_norm0;
  double lambda = options["inner.lambda_init"].as<double>();
  double mu = 0.1; //0.0001;
  double mu_old = mu;
  const double lambda_factor = 10.0;
  vector<double> outer_norms(4, 0.0);
  //vector<double> inner_norms(4, 0.0);
  //ostringstream outer_hist_string(ostringstream::out);
  double design_tol, pde_tol;
  double alpha = 1.0;
  while (iter < options["max_iter"].as<int>()) {
    iter++;
    
    cout << endl;
    cout << "==================================================" << endl;
    cout << "Beginning Inexact-Newton loop number " << iter << endl;
    cout << endl;
    
    // check for convergence
    Res.EqualsKKTConditions(X);
    if (iter == 1) {
      Res.CalcNorms(design_norm0, pde_norm0, adjoint_norm0);
      outer_norms[1] = design_norm0;
      outer_norms[2] = pde_norm0;
      outer_norms[3] = adjoint_norm0;
      outer_norms[0] = sqrt(outer_norms[1]*outer_norms[1] +
                            outer_norms[2]*outer_norms[2] +
                            outer_norms[3]*outer_norms[3]);
      kkt_norm0 = outer_norms[0];
#if 0
      static_cast<GlobalizedBFGS*>(quasi_newton)
          ->set_norm_init(design_norm0);
#endif

      static_cast<GlobalizedBFGS*>(quasi_newton)
          ->set_norm_init(1.0); 

      cout << "design_norm0  = " << design_norm0 << endl;
      cout << "pde_norm0     = " << pde_norm0 << endl;
      cout << "adjoint_norm0 = " << adjoint_norm0 << endl;
      design_tol = options["des_tol"].as<double>()*design_norm0; //max(design_norm0, 1.0);
      pde_tol = options["pde_tol"].as<double>()*max(pde_norm0, 1.0);
    } else if (Res.InexactConverged(outer_norms, design_tol, pde_tol)) {
      converged = true;
    }
    
#if 0
    WriteLNKSOuterIter(iter, nonlinear_sum, 
                       UserMemory::get_precond_count(), outer_norms,
                       outer_hist_string);
#endif
    if (converged) break;

    // update the quasi-Newton method
    if (iter > 1) {
      design_work -= Res.get_design();
      design_work *= -1.0;
      quasi_newton->AddCorrection(dX.get_design(), design_work);
    }

    //double rel_res = outer_norms[1]/design_norm0;
    double rel_res = outer_norms[0]/kkt_norm0;
#if 0
    if (alpha == 1.0) {
      lambda *= 0.1;
    } else {
      lambda *= 10.0;
    }
    if (iter == 1) lambda = options["inner.lambda_init"].as<double>();
#endif
#if 0
    lambda = max((rel_res*rel_res)*options["inner.lambda_init"].as<double>(),
                 0.5*lambda);
#endif
    //lambda = 0.0;
    lambda *= rel_res;
    //lambda_ = (rel_res)*(1.0 - lambda_targ_) + lambda_targ_;
    //lambda_ = lambda_targ_;
    //lambda = max(options["inner.lambda_init"].as<double>() - iter*0.1, 0.0);
    //if (rel_res < 1.e-4) lambda = 0.0;
    cout << "regularization parameter lambda = " << lambda << endl;    

    // compute krylov tolerance
    double krylov_tol = options["inexact.kappa_tol"].as<double>();
    //double krylov_tol = 0.01*std::min(1.0, sqrt(rel_res)); // superlinear
    //krylov_tol = std::max(krylov_tol, inner_grad_tol/inner_grad_norm);
    
    // solve the linear KKT system in the following inner loop
    // Note: the hessian may be regularized    
    static_cast<GlobalizedBFGS*>(quasi_newton)->set_lambda(lambda);
    int inner_iter = 0;
    int reg_iter = 0;
    bool inner_converged = false;
    //double inner_design_norm0, inner_pde_norm0, inner_adjoint_norm0;
    ostringstream hist_string(ostringstream::out);
    dX = 0.0;
    bool term1, term2;
    double primal_norm = 0.0, adjoint_norm = 0.0;
    double mu_trial = mu;
    while (inner_iter < options["inner.max_iter"].as<int>()) {
      inner_iter++;
      reg_iter++;

      cout << "----------------------------------" << endl;
      cout << "KKT linear system iteration = " << inner_iter << endl;

      // define the matrix-vector product...
      MatrixVectorProduct<KKTVector>* mat_vec =
          new RegularizedKKTProduct(&X, &Res, lambda);
      // ...and define the preconditioner
      Preconditioner<KKTVector>* precond =
          new KKTPreconditioner(&X, quasi_newton);

      // solve the linearized KKT equation, and update solution X
      int iters;
      if (options["krylov.solver"].as<string>() == "fgmres") 
        FGMRES<KKTVector>(options["krylov.space_size"].as<int>(),
                          options["krylov.tolerance"].as<double>(), 
                          Res, dX, *mat_vec, *precond, iters, krylov_out);
      dX *= -1.0; // recall, Res has not been moved to right-hand side
      nonlinear_sum++;
      
      // apply the SMART tests of Byrd et al.
      kktwork1.CalcLinearPrimalDualNorms(dX, Res, *mat_vec, primal_norm,
                                         adjoint_norm);
      double dT_W_d = dX.HessianProductCheck(*mat_vec, kktwork1, kktwork2);
      bool modify_hess = false;
      dX.SMARTTests(options, X, Res, mu, mu_old, primal_norm, adjoint_norm, dT_W_d,
                    kktwork1, term1, term2, modify_hess, mu_trial);

      if (rel_res < 1e-1) term1 = true;
      
      if (term1 || term2) break;
      
      // we may need to modify the Hessian
      if (reg_iter % 1 == 0) modify_hess = true;
      if (modify_hess) {
        reg_iter = 0;
        //lambda *= lambda_factor;
        lambda = max(lambda*lambda_factor, 1.e-3);
        dX = 0.0;
        cout << "Modifying Hessian: increasing lambda to " << lambda << endl;
        static_cast<GlobalizedBFGS*>(quasi_newton)->set_lambda(lambda);
      } else {
        // need this to reuse dX as initial guesss in FGMRES
        dX *= -1.0;
      }
    }
    cout << "mu = " << mu << ": mu_trial = " << mu_trial << endl;
    if ( (term2) && (mu < mu_trial) ) {
      // termination 2 holds and the penalty does not satisfy (4.5) in paper, so
      // the merit function penalty needs to be increased
      cout << "Increasing penalty parameter from " << mu;
      mu_old = mu;
      mu = mu_trial + 1.e-4;
      cout << " to " << mu << endl;
    }

    if (rel_res > 1e-1) {
    // Finally, perform a line search along dX
    MeritFunction* merit = new L2Merit(mu, X.get_design(), X.get_state(),
                                       dX.get_design(), dX.get_state(),
                                       design_work, state_work, adjoint_work);
    line_search->SetMeritFunction(*merit);
    double Dmerit = merit->EstimateDirDerivative(Res.get_adjoint().Norm2(),
                                                 adjoint_norm);
    cout << "Dmerit = " << Dmerit << endl;
    line_search->SetSearchDotGrad(Dmerit);
    alpha = line_search->FindStepLength();
    } else {
      alpha = 1.0;
    }
    cout << "alpha = " << alpha << endl;
    X.EqualsAXPlusBY(1.0, X, alpha, dX);
    X.UpdateUser(nonlinear_sum);    

    // save design residual for quasi-Newton update later
    design_work = Res.get_design();

    // write the convergence history from the preceeding inner iteration
    //WriteLNKSHistory(iter, inner_iter, hist_string, hist_out);

  } // outer loop
  cout << "Total number of nonlinear iterations: " 
       << nonlinear_sum << endl;
  // write the convergence history from the outer iterations
  //WriteLNKSOuterHistory(iter, outer_hist_string, hist_out);
  krylov_out.close();
  hist_out.close();
}

// ======================================================================
#if 0
void OldOptimizer::ReducedSpaceConstrainedOptimize() {

  ReducedKKTVector X; // the current (design,dual) solution of reduced system
  ReducedKKTVector P; // the search direction
  ReducedKKTVector dLdX; // current reduced gradient of the Lagrangian
  ReducedKKTVector dLdX_old; // work vector
  PDEVector state; // the current solution of the primal PDE 
  PDEVector adjoint; // the current solution of the adjoint PDE
  PDEVector adjoint_res; // residual of adjoint equation

  // work vectors
  DesignVector design_work1, design_work2;
  PDEVector pde_work1, pde_work2, pde_work3, pde_work4, pde_work5;
  DualVector dual_work1;
  
  // set initial guess for design and dual (the latter is set to zero)
  X.EqualsInitialGuess();
  X.SolveStateAndAdjoint(state, adjoint, pde_work1);
  DesignVector initial_design;
  initial_design = X.get_design();
  CurrentSolution(0, X.get_design(), state, adjoint, X.get_dual());
  
  // open output files to save histories
  ofstream hist_out(options["hist_file"].as<string>().c_str());
  ofstream krylov_out(options["krylov.file"].as<string>().c_str());
  ofstream reduced_out(options["reduced.krylov_file"].as<string>().c_str());
  
  // initialize the line-search  
  const double alpha_init = options["line_search.alpha_init"].as<double>();
  const double alpha_min = options["line_search.alpha_min"].as<double>();
  const double alpha_max = options["line_search.alpha_max"].as<double>();
  const double sufficient = options["line_search.sufficient"].as<double>();
  const double reduct_fac = options["line_search.reduct_fac"].as<double>();
  const double curv_cond = options["line_search.curv_cond"].as<double>();
  const int max_iter = options["line_search.max_iter"].as<double>();
  LineSearch* line_search;
  if (options["line_search.type"].as<string>() == "wolfe") {
    line_search = new StrongWolfe
        (alpha_init, alpha_max, sufficient, curv_cond, max_iter);
  } else {
    line_search = new BackTracking
        (alpha_init, alpha_min, sufficient, reduct_fac);
  }
  
  // initialize the Quasi-Newton approximation
  QuasiNewton* quasi_newton;
  if (options["quasi_newton.type"].as<string>() == "lbfgs") {
    quasi_newton = new GlobalizedBFGS
        (options["quasi_newton.max_stored"].as<int>());
  } else {
    quasi_newton = new GlobalizedSR1
        (options["quasi_newton.max_stored"].as<int>());
  }
  quasi_newton->SetInvHessianToIdentity();

  int iter = 0;
  int nonlinear_sum = 0;
  bool converged = false;
  double grad_norm0, grad_norm, grad_tol;
  double ceq_norm0, ceq_norm, ceq_tol;
  double lambda = options["inner.lambda_init"].as<double>();
  double krylov_tol;
  while (iter < options["max_iter"].as<int>()) {
    iter++;

    cout << endl;
    cout << "==================================================" << endl;
    cout << "Beginning Homotopy loop number " << iter << endl;
    cout << "lambda = " << lambda << endl;
    cout << endl;

    // check for convergence
    dLdX.EqualsKKTConditions(X, state, adjoint, design_work1);
    if (iter == 1) {
      grad_norm0 = dLdX.get_design().Norm2();
      grad_norm = grad_norm0;
      ceq_norm0 = dLdX.get_dual().Norm2();
      ceq_norm = ceq_norm0;
      quasi_newton->set_norm_init(grad_norm0);
      cout << "grad_norm0  = " << grad_norm0 << endl;
      cout << "ceq_norm0   = " << ceq_norm0 << endl;
      grad_tol = options["des_tol"].as<double>()*grad_norm0; //max(grad_norm0, 1.e-6);
      ceq_tol = options["ceq_tol"].as<double>()*max(ceq_norm0, 1.e-8);
    } else {
      grad_norm = dLdX.get_design().Norm2();
      ceq_norm = dLdX.get_dual().Norm2();
      cout << "grad_norm : grad_tol = "
           << grad_norm << " : " << grad_tol << endl;
      cout << "ceq_norm : ceq_tol = "
           << ceq_norm << " : " << ceq_tol << endl;
      // check for convergence
      if ( (grad_norm < grad_tol) && (ceq_norm < ceq_tol) ) { 
        converged = true;
        break;
      }
    }

    // update lambda for quasi-Newton method
    if (options["quasi_newton.type"].as<string>() == "lbfgs") {
      static_cast<GlobalizedBFGS*>(quasi_newton)->set_lambda(lambda);
    } else {
      static_cast<GlobalizedSR1*>(quasi_newton)->set_lambda(lambda);
    }

    int inner_iter = 0;
    bool inner_converged = false;
    double inner_grad_norm0, inner_grad_norm, inner_grad_tol;
    double inner_ceq_norm0, inner_ceq_norm, inner_ceq_tol;
    double sens_norm, dudx_norm, dpsidx_norm;
    ostringstream hist_string(ostringstream::out);
    while (inner_iter < options["inner.max_iter"].as<int>()) {
      inner_iter++;

      cout << "-------------------------" << endl;
      cout << "inner iteration = " << inner_iter << endl;

      // update the Quasi-Newton method if necessary
      if (inner_iter == 1) {
        dLdX_old = dLdX;
      } else {
        CurrentSolution(nonlinear_sum, X.get_design(), state, adjoint,
                        X.get_dual());
        dLdX.EqualsKKTConditions(X, state, adjoint, design_work1);
        dLdX_old -= dLdX;
        dLdX_old *= -1.0;
        quasi_newton->AddCorrection(P.get_design(), dLdX_old.get_design());
        dLdX_old = dLdX;
      }
      
      // check for convergence of the un-globalized problem
      grad_norm = dLdX.get_design().Norm2();
      ceq_norm = dLdX.get_dual().Norm2();
      if ( (grad_norm < grad_tol) && (ceq_norm < ceq_tol) ) {
        inner_converged = true;
      }

#if 0
      // evaluate the gradient of the globalized problem
      dLdX.EqualsAXPlusBY((1.0 - lambda), dLdX, lambda*grad_norm0, X);
      dLdX.EqualsAXPlusBY(1.0, dLdX, -lambda*grad_norm0, initial_design);
#endif
#if 0
      dLdX.EqualsAXPlusBY((1.0 - lambda), dLdX, lambda, X);
      dLdX.EqualsAXPlusBY(1.0, dLdX, -lambda, initial_design);
#endif
      // check for convergence of inner problem
      if (fabs(lambda) < kEpsilon) {
        // use outer convergence tolerance
        grad_norm = dLdX.get_design().Norm2();
        ceq_norm = dLdX.get_dual().Norm2();
        inner_grad_norm = grad_norm;
        inner_grad_tol = grad_tol;
        inner_ceq_norm = ceq_norm;
        inner_ceq_tol = ceq_tol;
        cout << "grad_norm : grad_tol = "
             << grad_norm << " : " << grad_tol << endl;
        cout << "ceq_norm : ceq_tol = "
             << ceq_norm << " : " << ceq_tol << endl;
        if (inner_iter == 1) inner_grad_norm0 = grad_norm;
        if (inner_iter == 1) inner_ceq_norm0 = ceq_norm;
        if ( (inner_grad_norm < inner_grad_tol) && 
             (inner_ceq_norm < inner_ceq_tol) ) inner_converged = true;
      } else {
        if (inner_iter == 1) {
          inner_grad_norm0 = dLdX.get_design().Norm2();
          inner_grad_norm = inner_grad_norm0;
          inner_ceq_norm0 = dLdX.get_dual().Norm2();
          inner_ceq_norm = inner_ceq_norm0;
          inner_grad_tol = options["inner.des_tol"].as<double>()
              *inner_grad_norm0; //*max(inner_grad_norm0, 1.0);          
          cout << "inner_grad_norm0  = " << inner_grad_norm0 << endl;
          inner_ceq_tol = options["inner.ceq_tol"].as<double>()
              *inner_ceq_norm0; //*max(inner_ceq_norm0, 1.0);          
          cout << "inner_ceq_norm0  = " << inner_ceq_norm0 << endl;
        } else {
          inner_grad_norm = dLdX.get_design().Norm2();
          inner_ceq_norm = dLdX.get_dual().Norm2();
          cout << "inner_grad_norm : inner_grad_tol = "
               << inner_grad_norm << " : " << inner_grad_tol << endl;
          cout << "inner_ceq_norm : inner_ceq_tol = "
               << inner_ceq_norm << " : " << inner_ceq_tol << endl;
          if ( (inner_grad_norm < inner_grad_tol) &&
               (inner_ceq_norm < inner_ceq_tol) ) {
            inner_converged = true;
          }
        }
      }
      WriteQuasiNewtonIter(iter, inner_iter, nonlinear_sum+1, 
                           UserMemory::get_precond_count(), grad_norm,
                           inner_grad_norm, hist_string);
      if (inner_converged) break;

      // compute Krylov tolerance to achieve superlinear convergence, but to
      // avoid oversolving near the desired nonlinear tolerance
      double inner_kkt_norm0 = sqrt(inner_grad_norm0*inner_grad_norm0 +
                                    inner_ceq_norm0*inner_ceq_norm0);
      double inner_kkt_norm = sqrt(inner_grad_norm*inner_grad_norm +
                                   inner_ceq_norm*inner_ceq_norm);
#if 0
      krylov_tol = options["krylov.tolerance"].as<double>()*std::min(
          1.0, sqrt(inner_kkt_norm/inner_kkt_norm0)); // superlinear
      //krylov_tol *= options["reduced.nu"].as<double>();
#endif 
      krylov_tol = options["krylov.tolerance"].as<double>();
      cout << "krylov_tol (inner) = " << krylov_tol << endl;
      
      // need adjoint residual for reduced-Hessian product
      adjoint_res.EqualsObjectiveGradient(X.get_design(), state);
      pde_work1.EqualsTransJacobianTimesVector(X.get_design(), state, adjoint);
      adjoint_res += pde_work1;
      pde_work1.EqualsTransCnstrJacTimesVector(X.get_design(), state,
                                               X.get_dual());
      adjoint_res += pde_work1;
      
      // set Hessian-vector product tolerance parameters
      // Note: we must divide by nu in product_fac, because nu appears in the
      // krylov_tol, which is fed back to the Hessian-vector product; we do not
      // want this nu factor in product_fac (we only want 1.0 - nu), so we
      // "remove" it here
      double product_fac = options["reduced.product_fac"].as<double>(); // *
      //          (1.0 - options["reduced.nu"].as<double>()) /
      //          options["reduced.nu"].as<double>();
      if (!options["reduced.dynamic_tol"].as<bool>())
        product_fac *= (krylov_tol/static_cast<double>(
            options["krylov.space_size"].as<int>()));

      // set equation scaling
#if 0
      double safe_scale = std::max(inner_kkt_norm*krylov_tol, kEpsilon);
      double grad_scale = std::max(inner_grad_norm, safe_scale); //inner_grad_tol);
      double ceq_scale = std::max(inner_ceq_norm, safe_scale); //inner_ceq_tol);
#endif

      double grad_scale = std::max(inner_grad_norm, inner_grad_tol);

      // TEMP: the fac parameter is for studying IDF applied to chaotic systems
      double fac = 1.0;// + 1000.0/static_cast<double>(inner_iter*inner_iter);
      
      double ceq_scale = fac*std::max(inner_ceq_norm, inner_ceq_tol);

      cout << "grad_scale = " << grad_scale << endl;
      cout << "ceq_scale = " << ceq_scale << endl;
      
      // build reduced-KKT-matrix-vector product...
      // NOTE: this scales dLdX
      MatrixVectorProduct<ReducedKKTVector>* mat_vec = new ReducedKKTProduct(
          X, state, adjoint, dLdX, adjoint_res, design_work1, design_work2,
          pde_work1, pde_work2, pde_work3, pde_work4, pde_work5, dual_work1,
          options["reduced.krylov_size"].as<int>(), product_fac,
          reduced_out, lambda, grad_norm0, grad_scale, ceq_scale);
      
#if 0      
      // determine the paramters required for the Hessian-vector error bound
      if ( (options["reduced.bound_frozen"].as<bool>()) && (inner_iter > 1) ) {
        // the bound is frozen after the first iteration
        if (options["reduced.bound_type"].as<string>() == "common")
          static_cast<ReducedHessianProduct*>
              (mat_vec)->set_sens_norm(sens_norm);
        else if (options["reduced.bound_type"].as<string>() == "independent")
          static_cast<ReducedHessianProduct*>
              (mat_vec)->set_sens_norms(dudx_norm, dpsidx_norm);
      } else {
        // the bound is recomputed each iteration or this is the first iter
        if (options["reduced.bound_type"].as<string>() == "common")
          sens_norm = static_cast<ReducedHessianProduct*>(mat_vec)
              ->compute_sens_norm(options["reduced.bound_approx"].as<bool>());
        else if (options["reduced.bound_type"].as<string>() == "independent")
          static_cast<ReducedHessianProduct*>(mat_vec)
              ->compute_sens_norms(dudx_norm, dpsidx_norm,
                                   options["reduced.bound_approx"].as<bool>());
      }
#endif
      if (options["quasi_newton.matvec_update"].as<bool>()) {
        // update the quasi-Newton method during Hessian-vector products
        static_cast<ReducedKKTProduct*>(mat_vec)
            ->set_quasi_newton(quasi_newton);
      }

      // ...and define the preconditioner

      Preconditioner<ReducedKKTVector>* precond =
          new IdentityPreconditioner<ReducedKKTVector>();
#if 0
      Preconditioner<ReducedKKTVector>* precond =
          new ReducedKKTPreconditioner(quasi_newton);
#endif
      // solve for the reduced-space search direction
      P = 0.0;
      int iters;
      if (options["krylov.solver"].as<string>() == "gmres") {
        GMRES<ReducedKKTVector>(options["krylov.space_size"].as<int>(),
                            krylov_tol, //options["krylov.tolerance"].as<double>(), 
                            dLdX, P, *mat_vec, *precond, iters, krylov_out, 
                            options["krylov.check_res"].as<bool>(),
                            options["reduced.dynamic_tol"].as<bool>());
      } else if (options["krylov.solver"].as<string>() == "fgmres") {
        FGMRES<ReducedKKTVector>(options["krylov.space_size"].as<int>(),
                             krylov_tol, //options["krylov.tolerance"].as<double>(), 
                             dLdX, P, *mat_vec, *precond, iters, krylov_out, 
                             options["krylov.check_res"].as<bool>(),
                             options["reduced.dynamic_tol"].as<bool>());
      } else if (options["krylov.solver"].as<string>() == "ffom") {
        FFOM<ReducedKKTVector>(options["krylov.space_size"].as<int>(),
                           krylov_tol, //options["krylov.tolerance"].as<double>(), 
                           dLdX, P, *mat_vec, *precond, iters, krylov_out, 
                           options["krylov.check_res"].as<bool>(),
                           options["reduced.dynamic_tol"].as<bool>());        
      } else if (options["krylov.solver"].as<string>() == "minres") {
        MINRES<ReducedKKTVector>(options["krylov.space_size"].as<int>(),
                             krylov_tol, //options["krylov.tolerance"].as<double>(), 
                             dLdX, P, *mat_vec, *precond, iters, krylov_out,
                             options["krylov.check_res"].as<bool>(),
                             options["reduced.dynamic_tol"].as<bool>());
      }
      P *= -1.0;
      delete mat_vec;
      delete precond;

      cout << "after Krylov solve of reduced-space direction P" << endl;
      

#if 0      
      // check that P is a descent direction
      double p_dot_grad = InnerProd(P, dJdX);
      if (p_dot_grad >= 0.0) {
        cout << "P is not a descent direction: "
             << "switching to quasi-Newton direction." << endl;
        quasi_newton->ApplyInvHessianApprox(dJdX, P);
        P *= -1.0;
        p_dot_grad = InnerProd(P, dJdX);
        if (p_dot_grad >= 0.0) {        
          // if using SR1, P may still fail to be a descent direction
          cout << "P is not a descent direction: "
               << "switching to steepest descent direction." << endl;
          P = dJdX;
          P *= -1.0;
          p_dot_grad = InnerProd(P, dJdX);
        }
      }

      // set-up the merit function and line search
      MeritFunction* merit = new GlobalizedMerit
          (initial_design, lambda, grad_norm0, P, X, p_dot_grad, state, adjoint);
      line_search->SetMeritFunction(*merit);
      line_search->SetSearchDotGrad(p_dot_grad);

      double alpha = line_search->FindStepLength();
      X.EqualsAXPlusBY(1.0, X, alpha, P);
#endif

      // cross your fingers
      double alpha = 1.0;
      X.EqualsAXPlusBY(1.0, X, alpha, P);

      // update state and adjoint
      X.SolveStateAndAdjoint(state, adjoint, pde_work1);

      // S = delta X = alpha * P is needed later
      P *= alpha;

      // free memory
      //delete merit;
      
      //X += P;
      
      nonlinear_sum++;
      //if (inner_iter == 5) throw(-1);
      
    } // inner iterations
    
    //X.UpdateUser();
    // update the continuation parameter
    lambda = max(0.0, lambda - 0.1);
    //lambda = max(0.0, lambda - 0.05);
    //lambda = max(0.0, lambda - 0.01);
    //lambda = 0.1*lambda;
    
    // write the convergence history from the preceeding inner iteration
    WriteQuasiNewtonHistory(iter, inner_iter, hist_string, hist_out);
    
  } // outer iterations

  // TEMP
  CurrentSolution(nonlinear_sum, X.get_design(), state, adjoint,
                  X.get_dual());
  
  cout << "Total number of nonlinear iterations: " << nonlinear_sum << endl;
  hist_out.close();
  krylov_out.close();
  reduced_out.close();
}
#endif
// ======================================================================

void OldOptimizer::ReducedSpaceAugmentedLagrangianOptimize() {
  
  DesignVector X; // the current solution of the reduced system
  DualVector Psi; // the current Lagragian multiplier estimate
  DualVector C; // the current value of the constraints
  DesignVector dLdX; // current augmented Lagrangian reduced gradient
  DesignVector dLdX_old; // work vector
  PDEVector state; // the current solution of the primal PDE 
  PDEVector adjoint; // the current solution of the adjoint PDE
  
  // set initial guess and store initial design; for Psi = 0.0 guess, the
  // adjoint is simply the adjoint for the objective gradient
  X.EqualsInitialDesign();
  Psi = 0.0;
  state.EqualsPrimalSolution(X);
  adjoint.EqualsAdjointSolution(X, state);
  //adjoint = 0.0;
  CurrentSolution(0, X, state, adjoint, Psi);  
  
  DesignVector initial_design;
  initial_design = X;
  //double obj = ObjectiveValue(X, state);
  
  //cout << "Initial values:" << endl << "\t";
  //X.UpdateUser();
  
  // open output files to save histories
  ofstream hist_out(options["hist_file"].as<string>().c_str());
  hist_out << "# Kona augmented-Lagrangian convergence history" << endl;
  hist_out << boost::format(
      string("%|-7| %|8t| %|-5| %|14t| %|-7| %|23t| %|-10| %|35t| ")+
      string("%|-12| %|47t| %|-12| %|59t| %|-12| %|71t| %|-12| ")+
      string("%|83t| %|-9| %|92t| %|-8|\n"))
      % "# outer" % "inner" % "precond" % "mu" % "AugLag" % "optimality"
      % "feasibility" % "trust rho" % "radius" % "info";
  
  // initialize the Quasi-Newton approximation
  QuasiNewton* quasi_newton;
  if (options["quasi_newton.type"].as<string>() == "lbfgs") {
    quasi_newton = new LimitedMemoryBFGS
        (options["quasi_newton.max_stored"].as<int>());
  }

  int iter = 0;
  int nonlinear_sum = 0;
  bool converged = false;
  double mu = options["aug_lag.mu_init"].as<double>();
  double feas_targ = 1.e-1; //0.5; //1.0/pow(mu, 0.1);
  double grad_targ = 1.e-1; //0.5; //1.0/mu;
  double grad_norm0, grad_norm, grad_tol;
  double feas_norm0, feas_norm, feas_tol;
  double rho = 0.0;
  double radius = options["trust.init_radius"].as<double>();
  while (iter < options["max_iter"].as<int>()) {
    iter++;

    cout << endl;
    cout << "==================================================" << endl;
    cout << "Beginning Augmented Lagrangian Major iteration " << iter << endl;
    cout << endl;

    // check for convergence
    C.EqualsConstraints(X, state);
    feas_norm = C.Norm2();
    C.EqualsAXPlusBY(1.0, Psi, mu, C);    
    dLdX.EqualsLagrangianReducedGradient(X, C, state, adjoint, dLdX_old);
    grad_norm = dLdX.Norm2();
    if (iter == 1) {
      grad_norm0 = grad_norm;
      feas_norm0 = feas_norm;
      quasi_newton->set_norm_init(1.0);
      cout << "grad_norm0  = " << grad_norm0 << endl;
      cout << "feas_norm0  = " << feas_norm0 << endl;
      grad_tol = options["des_tol"].as<double>()*grad_norm0; //max(grad_norm0, 1.e-6);
      feas_tol = options["ceq_tol"].as<double>()*max(feas_norm0, 1.e-8);
    }
    cout << "grad_norm : grad_tol = " << grad_norm << " : " << grad_tol << endl;
    cout << "feas_norm : feas_tol = " << feas_norm << " : " << feas_tol << endl;

    if (feas_norm < feas_targ*feas_norm0) {
      // making good progress on the constraints

      // check for convergence of the global problem
      if ( (grad_norm < grad_tol) && (feas_norm < feas_tol) ) { 
        converged = true;
        break;
      }
      // update multipliers and tighten tolerances      
      Psi = C; // recall, C was set to Psi + mu*Ceq above
      feas_targ /= 2; //pow(mu, 0.9);
      grad_targ /= 2; //mu;
      
    } else if (iter > 1) {
      // we are not making good progress on the constraints; increase penalty
      // parameter and tighten tolerances
      mu *= 100.0;
      feas_targ /= 2; //pow(mu, 0.1);
      grad_targ /= 2; //mu;      
    }
    //grad_targ = std::max(grad_targ, 0.5*options["des_tol"].as<double>());

    // solve the subproblem
    AugmentedLagrangianSubproblem(X, Psi, state, adjoint, dLdX, dLdX_old,
                                  C, mu, grad_targ, grad_tol,
                                  feas_tol, radius, iter, nonlinear_sum,
                                  quasi_newton, hist_out);
    
  }
  cout << "Total number of nonlinear iterations: " << nonlinear_sum << endl;
  hist_out.close();
}
// ======================================================================

void OldOptimizer::AugmentedLagrangianSubproblem(
    DesignVector & X, const DualVector & Psi, PDEVector & state,
    PDEVector & adjoint, DesignVector & dLdX, DesignVector & dLdX_old,
    DualVector & dual_work, const double & mu, const double & tol,
    const double & outer_grad_tol, const double & outer_feas_tol, 
    double & radius, const int & outer_iter, int & nonlinear_sum,
    QuasiNewton * quasi_newton, ofstream & hist_out) {

  DesignVector P; // solution step
  PDEVector adjoint_res; // residual of adjoint equation

  DualVector dual_work1; // needed for IDF preconditioner
  ReducedKKTVector KKT;
  
  // work vectors for Hessian-vector product
  DesignVector design_work1, design_work2;
  PDEVector pde_work1, pde_work2, pde_work3, pde_work4, pde_work5;
  
  // work vectors for nested Krylov preconditioner
  DesignVector design_work3, design_work4;
  PDEVector pde_work6, pde_work7, pde_work8, pde_work9, pde_work10;

  ofstream krylov_out(options["krylov.file"].as<string>().c_str(), ios::app);
  ofstream reduced_out(options["reduced.krylov_file"].as<string>().c_str(),
                       ios::app);
  
  double grad_norm0;
  double grad_tol;
  double feas_norm0;
  double rho = 0.0;
  double sens_norm, dudx_norm, dpsidx_norm;
  string info;
  bool converged = false;
  int iter = 0;
  while (iter < options["inner.max_iter"].as<int>()) {
    iter++;

    cout << endl;
    cout << "--------------------------------------------------" << endl;
    cout << "\tBeginning Trust-Region iteration " << iter << endl;
    cout << endl;

    // check for convergence
    dual_work.EqualsConstraints(X, state);
    double feas_norm = dual_work.Norm2();
    double AugLag = AugmentedLagrangian(X, Psi, state, dual_work, mu);
    dual_work.EqualsAXPlusBY(1.0, Psi, mu, dual_work);
    dLdX.EqualsLagrangianReducedGradient(X, dual_work, state, adjoint,
                                         design_work1);   
    double grad_norm = dLdX.Norm2();
    if (iter == 1) {
      grad_norm0 = grad_norm;
      feas_norm0 = feas_norm;
      quasi_newton->set_norm_init(1.0);
      cout << "\tinner grad_norm0  = " << grad_norm0 << endl;
      cout << "\tinner feas_norm0  = " << feas_norm0 << endl;
      grad_tol = tol*grad_norm0; //max(grad_norm0, 1.e-6);
      info += "i";
    } 
    cout << "\tgrad_norm : grad_tol = "
         << grad_norm << " : " << grad_tol << endl;
    cout << "\tfeas_norm/feas_norm0 = " << feas_norm/feas_norm0 << endl;
    
    // write output and check for convergence
    hist_out << boost::format(
        string("%|7| %|8t| %|5| %|14t| %|7| %|23t| %|-10.4| %|35t| ")+
        string("%|-+12.5e| %|47t| %|-12.6| %|59t| %|-12.6| %|71t| %|-+12.6f| ")+
        string("%|83t| %|-9.5e| %|92t| %|-8|\n"))
        % outer_iter % iter % UserMemory::get_precond_count() % mu % AugLag
        % grad_norm % feas_norm % rho % radius % info;
    hist_out.flush();
    if ( (grad_norm < grad_tol) ||
         ( (grad_norm < outer_grad_tol) && (feas_norm < outer_feas_tol) ) ) {
      converged = true;
      break;
    }
    info = "";
    
    // compute Krylov tolerance to achieve superlinear convergence, but to
    // avoid oversolving near the desired nonlinear tolerance
    double krylov_tol = std::min(options["krylov.tolerance"].as<double>(),
                                 0.5*sqrt(grad_norm/grad_norm0));
    krylov_tol = std::max(krylov_tol, grad_tol/grad_norm); // avoid oversolve
    krylov_tol *= options["reduced.nu"].as<double>();
    cout << "\tkrylov_tol = " << krylov_tol << endl;
    
    // update the Quasi-Newton method
    if (iter == 1) {
      dLdX_old = dLdX;
    } else {
      CurrentSolution(nonlinear_sum, X, state, adjoint, Psi);
      dLdX_old -= dLdX;
      dLdX_old *= -1.0;
      quasi_newton->AddCorrection(P, dLdX_old);
      dLdX_old = dLdX;
    }
    
    // need adjoint residual for reduced-Hessian product
    adjoint_res.EqualsObjectiveGradient(X, state);
    pde_work1.EqualsTransJacobianTimesVector(X, state, adjoint);
    adjoint_res += pde_work1;
    pde_work1.EqualsTransCnstrJacTimesVector(X, state, dual_work);
    adjoint_res += pde_work1;

    // set Hessian-vector product tolerance parameters
    // Note: we must divide by nu in product_fac, because nu appears in the
    // krylov_tol, which is fed back to the Hessian-vector product; we do not
    // want this nu factor in product_fac (we only want 1.0 - nu), so we
    // "remove" it here
    double product_fac = options["reduced.product_fac"].as<double>() *
        (1.0 - options["reduced.nu"].as<double>()) /
        options["reduced.nu"].as<double>();
    if (!options["reduced.dynamic_tol"].as<bool>())
      product_fac *= (grad_norm*krylov_tol/static_cast<double>(
          options["krylov.space_size"].as<int>()));

    // build reduced-Hessian product...
    MatrixVectorProduct<DesignVector>* mat_vec = new AugLagrangianHessianProduct(
        X, Psi, state, adjoint, dLdX_old, adjoint_res, design_work1,
        design_work2, pde_work1, pde_work2, pde_work3, pde_work4, pde_work5,
        dual_work, mu, options["reduced.krylov_size"].as<int>(),
        product_fac, reduced_out);

    // determine the parameters required for the Hessian-vector error bound
    if ( (options["reduced.bound_frozen"].as<bool>()) && (iter > 1) ) {
      // the bound is frozen after the first iteration
      if (options["reduced.bound_type"].as<string>() == "common")
        static_cast<AugLagrangianHessianProduct*>
            (mat_vec)->set_sens_norm(sens_norm);
      else if (options["reduced.bound_type"].as<string>() == "independent")
        static_cast<AugLagrangianHessianProduct*>
            (mat_vec)->set_sens_norms(dudx_norm, dpsidx_norm);
    } else {
      // the bound is recomputed each iteration or this is the first iter
      if (options["reduced.bound_type"].as<string>() == "common")
        sens_norm = static_cast<AugLagrangianHessianProduct*>(mat_vec)
            ->compute_sens_norm(options["reduced.bound_approx"].as<bool>());
      else if (options["reduced.bound_type"].as<string>() == "independent")
        static_cast<AugLagrangianHessianProduct*>(mat_vec)
            ->compute_sens_norms(dudx_norm, dpsidx_norm,
                                 options["reduced.bound_approx"].as<bool>());
    }
    
    if (options["quasi_newton.matvec_update"].as<bool>()) {
      // update the quasi-Newton method during Hessian-vector products
      static_cast<AugLagrangianHessianProduct*>(mat_vec)
          ->set_quasi_newton(quasi_newton);
    }
    // ...and define the preconditioner
    Preconditioner<DesignVector>* precond;
    if (options["reduced.precond"].as<string>() == "quasi_newton") {
      precond = new ReducedSpacePreconditioner(quasi_newton);
    } else if (options["reduced.precond"].as<string>() == "nested_krylov") {   
      precond = new NestedReducedSpacePreconditioner(
          X, state, adjoint, dLdX_old, adjoint_res, design_work3,
          design_work4, pde_work6, pde_work7, pde_work8,
          pde_work9, pde_work10, reduced_out, 0.0, 1.0);
    } else if (options["reduced.precond"].as<string>() == "idf_schur") {
      KKT.primal() = X;
      KKT.dual() = Psi;
      precond = new IDFProjectionPreconditioner(
          KKT, state, design_work1, design_work2, design_work3,
          pde_work1, pde_work2, dual_work1, 10, reduced_out);
    } else {
      precond = new IdentityPreconditioner<DesignVector>();
    }

    // inexactly solve the trust-region problem
    P = 0.0;
    int iters;
    double pred;
    bool active;
    dLdX *= -1.0;
    
    if (options["krylov.solver"].as<string>() == "fitr") {
#if 1
      ptree ptin, ptout;
      FITRSolver<DesignVector> solver;
      solver.SubspaceSize(options["krylov.space_size"].as<int>());
      ptin.put<double>("tol", krylov_tol);
      ptin.put<double>("radius", radius);
      ptin.put<bool>("check", options["krylov.check_res"].as<bool>());
      ptin.put<bool>("dynamic", options["reduced.dynamic_tol"].as<bool>());
      
      solver.Solve(ptin, dLdX, P, *mat_vec, *precond, ptout, krylov_out);
      
      active = ptout.get<bool>("active");
      pred = ptout.get<double>("pred");
      iters = ptout.get<int>("iters");
#else
      FITR_old<DesignVector>(options["krylov.space_size"].as<int>(),
                             krylov_tol, //options["krylov.tolerance"].as<double>(),
                             radius, dLdX, P, *mat_vec, *precond, iters, pred,
                             active, krylov_out,
                             options["krylov.check_res"].as<bool>(),
                             options["reduced.dynamic_tol"].as<bool>());
#endif
    } else if (options["krylov.solver"].as<string>() == "cg") {
      SteihaugCG<DesignVector>(options["krylov.space_size"].as<int>(),
                               krylov_tol, //options["krylov.tolerance"].as<double>(),
                               radius, dLdX, P, *mat_vec, *precond, iters, pred,
                               active, krylov_out,
                               options["krylov.check_res"].as<bool>(),
                               options["reduced.dynamic_tol"].as<bool>());
    } else if (options["krylov.solver"].as<string>() == "fom") {
      SteihaugFOM<DesignVector>(options["krylov.space_size"].as<int>(),
                                krylov_tol, //options["krylov.tolerance"].as<double>(),
                                radius, dLdX, P, *mat_vec, *precond, iters, pred,
                                active, krylov_out,
                                options["krylov.check_res"].as<bool>(),
                                options["reduced.dynamic_tol"].as<bool>());
    }
        
    dLdX *= -1.0;
    double alpha = 1.0;
    X.EqualsAXPlusBY(1.0, X, alpha, P);

    // compute the actual reduction and the trust parameter rho
    double AugLag_old = AugLag;
    adjoint_res = state; // save current state
    if (state.EqualsPrimalSolution(X)) {
      dual_work.EqualsConstraints(X, state);
      AugLag = AugmentedLagrangian(X, Psi, state, dual_work, mu);
      rho = (AugLag_old - AugLag)/pred;
      if (pred < kEpsilon) rho = 1.0;
      cout << "AugLag_old = " << AugLag_old << endl;
      cout << "AugLag     = " << AugLag << endl;
      cout << "pred       = " << pred << endl;
      cout << "rho        = " << rho << endl;
    } else {
      // failed to converge
      info += "f";
      rho = -1E-16;
    }

    // update the radius if necessary
    if (rho < 0.25) {
      info += "-";
      radius *= 0.25;
    } else {
      if ( (rho < 2.0) && (rho > 0.75) && (active) ) {
        info += "+";
        radius = std::min(2.0*radius, options["trust.max_radius"].as<double>());
      }
    }

    // revert the solution if necessary
    if (rho < options["trust.tol"].as<double>()) {
      info += "r";
      cout << "\treverting solution..." << endl;
      X.EqualsAXPlusBY(1.0, X, -alpha, P);
      state = adjoint_res;
    } else {
      // adjoint is not up-to-date
      if (active) info += "a";
      dual_work.EqualsAXPlusBY(1.0, Psi, mu, dual_work);
      adjoint.EqualsAugLagrangianAdjointSolution(
          X, state, dual_work, pde_work1,
          options["reduced.krylov_size"].as<int>());
    }
    nonlinear_sum++;        
  }
  krylov_out.close();
  reduced_out.close();
}

// ======================================================================

void OldOptimizer::WriteTrustReducedHistory(const int & iter, const int & precond, 
                                         const double & norm, const double & rho,
                                         const double & radius, ostream & hist) {
  if (!(hist.good())) {
    cerr << "OldOptimizer(WriteTrustReducedHistory): "
	 << "ostream is not good for i/o operations." << endl;
    throw(-1);
  }
  if (iter == 1) {
    // need to specify the variables on the first iteration
    hist << "TITLE = \"Kona trust-region reduced INK convergence history\""
         << endl;
    hist << "VARIABLES=\"iter\",\"preconditioner calls\",\","
         << "\"design norm\",\"rho\",\"radius\"" << endl;
  }
  std::ios_base::fmtflags old_float =
      hist.setf(ios::scientific, ios::floatfield);
  std::ios_base::fmtflags old_adjust = 
      hist.setf(ios::left, ios::adjustfield);
  hist << std::setw(5) << iter
       << std::setw(7) << precond
       << std::setw(16) << std::setprecision(6) << norm
       << std::setw(16) << std::setprecision(6) << rho
       << std::setw(16) << std::setprecision(6) << radius
       << endl;
  hist.setf(old_float, ios::floatfield);
  hist.setf(old_adjust, ios::adjustfield);
}

// ======================================================================
#if 0
void OldOptimizer::WriteConstrainedTrustHistory(
    const int & iter, const int & precond, const double & grad_norm,
    const double & feas_norm, const double & rho, const double & radius,
    ostream & hist) {
  hist << boost::format(string("%|5| %|7t| %|7| %|16t| %|-12.6| %|28t| ")+
                        string("%|-12.6| %|40t| %|-12.6| %|52t| %|-12.6|\n"))
      % iter % precond % grad_norm % feas_norm % rho % radius;
}
#endif
// ======================================================================

void OldOptimizer::WriteLNKSHistory(const int & outer_iter,
                                 const int & inner_iter,
                                 ostringstream & hist_string,
                                 ostream & hist_out) {
  if (!(hist_string.good())) {
    cerr << "OldOptimizer(WriteLNKSHistory): "
	 << "stringstream is not good for i/o operations." << endl;
    throw(-1);
  }
  if (!(hist_out.good())) {
    cerr << "OldOptimizer(WriteLNKSHistory): "
	 << "ostream is not good for i/o operations." << endl;
    throw(-1);
  }
  if (outer_iter == 1) {
    // need to specify the variables on the first iteration
    hist_out << "TITLE = \"Kona LNKS convergence history\"" << endl;
    hist_out << "VARIABLES=\"outer\",\"inner\",\"total inner\","
             << "\"preconditioner calls\",\"KKT norm\","
             << "\"design norm\",\"primal norm\","
             << "\"dual norm\",\"global. KKT norm\","
             << "\"global. design norm\",\"global. primal norm\","
             << "\"global. adjoint norm\"" << endl;
  }
  hist_out << "ZONE T=\"Outer Iteration " << outer_iter << "\", "
           << "I=" << inner_iter << ", DATAPACKING=POINT" << endl;
  hist_out << hist_string.str();
  //cout << hist_string.str();
}

// ======================================================================
#if 0
void OldOptimizer::WriteLNKSOuterHistory(const int & outer_iter, 
                                      ostringstream & hist_string,
                                      ostream & hist_out) {
  if (!(hist_string.good())) {
    cerr << "OldOptimizer(WriteLNKSOuterHistory): "
	 << "stringstream is not good for i/o operations." << endl;
    throw(-1);
  }
  if (!(hist_out.good())) {
    cerr << "OldOptimizer(WriteLNKSOuterHistory): "
	 << "ostream is not good for i/o operations." << endl;
    throw(-1);
  }
  hist_out << "TITLE = \"Kona LNKS outer convergence history\"" << endl;
  hist_out << "VARIABLES=\"outer\",\"total inner\","
           << "\"preconditioner calls\",\"KKT norm\","
           << "\"design norm\",\"primal norm\","
           << "\"dual norm\"" << endl;
  hist_out << "ZONE I=" << outer_iter << ", DATAPACKING=POINT" << endl;
  hist_out << hist_string.str();
  //cout << hist_string.str();
}
#endif
// ======================================================================

void OldOptimizer::WriteQuasiNewtonHistory(const int & outer_iter,
                                        const int & inner_iter,
                                        ostringstream & hist_string,
                                        ostream & hist_out) {
  if (!(hist_string.good())) {
    cerr << "OldOptimizer(WriteQuasiNewtonHistory): "
	 << "stringstream is not good for i/o operations." << endl;
    throw(-1);
  }
  if (!(hist_out.good())) {
    cerr << "OldOptimizer(WriteQuasiNewtonHistory): "
	 << "ostream is not good for i/o operations." << endl;
    throw(-1);
  }
  if (outer_iter == 1) {
    // need to specify the variables on the first iteration
    hist_out << "TITLE = \"Kona Quasi-Newton convergence history\""
             << endl;
    hist_out << "VARIABLES=\"outer\",\"inner\",\"total inner\","
             << "\"preconditioner calls\",\"design norm\","
             << "\"global. design norm\"" << endl;
  }
  hist_out << "ZONE T=\"Outer Iteration " << outer_iter << "\", "
           << "I=" << inner_iter << ", DATAPACKING=POINT" << endl;
  hist_out << hist_string.str();
}

// ======================================================================

void OldOptimizer::WriteLNKSIter(
    const int & outer_iter, const int & inner_iter,
    const int & total_inner, const int & precond,
    const vector<double> & outer_norms,
    const vector<double> & inner_norms,
    ostream & hist) {
  if (outer_norms.size() != 4) {
    cerr << "OldOptimizer(WriteLNKSIter): invalid inner_norms.size()"
         << endl;
    throw(-1);
  }
  if (inner_norms.size() != 4) {
    cerr << "OldOptimizer(WriteLNKSIter): invalid inner_norms.size()"
         << endl;
    throw(-1);
  }
  if (!(hist.good())) {
    cerr << "OldOptimizer(WriteLNKSIter): "
	 << "ostream is not good for i/o operations." << endl;
    throw(-1);
  }

  std::ios_base::fmtflags old_float =
      hist.setf(ios::scientific, ios::floatfield);
  std::ios_base::fmtflags old_adjust = 
      hist.setf(ios::left, ios::adjustfield);
  hist << std::setw(5) << outer_iter
       << std::setw(5) << inner_iter
       << std::setw(6) << total_inner
       << std::setw(7) << precond
       << std::setw(16) << std::setprecision(6) << outer_norms[0]
       << std::setw(16) << std::setprecision(6) << outer_norms[1]
       << std::setw(16) << std::setprecision(6) << outer_norms[2]
       << std::setw(16) << std::setprecision(6) << outer_norms[3]
       << std::setw(16) << std::setprecision(6) << inner_norms[0]
       << std::setw(16) << std::setprecision(6) << inner_norms[1]
       << std::setw(16) << std::setprecision(6) << inner_norms[2]
       << std::setw(16) << std::setprecision(6) << inner_norms[3]
       << endl;
  hist.setf(old_float, ios::floatfield);
  hist.setf(old_adjust, ios::adjustfield);
}

// ======================================================================
#if 0
void OldOptimizer::WriteLNKSOuterIter(const int & outer_iter,
                                   const int & inner_iter,
                                   const int & precond, 
                                   const vector<double> & outer_norms,
                                   ostream & hist) {
  if (outer_norms.size() != 4) {
    cerr << "OldOptimizer(WriteLNKSOuterIter): invalid inner_norms.size()"
         << endl;
    throw(-1);
  }
  if (!(hist.good())) {
    cerr << "OldOptimizer(WriteLNKSOuterIter): "
	 << "ostream is not good for i/o operations." << endl;
    throw(-1);
  }
  std::ios_base::fmtflags old_float =
      hist.setf(ios::scientific, ios::floatfield);
  std::ios_base::fmtflags old_adjust = 
      hist.setf(ios::left, ios::adjustfield);
  hist << std::setw(5) << outer_iter
       << std::setw(6) << inner_iter
       << std::setw(7) << precond
       << std::setw(16) << std::setprecision(6) << outer_norms[0]
       << std::setw(16) << std::setprecision(6) << outer_norms[1]
       << std::setw(16) << std::setprecision(6) << outer_norms[2]
       << std::setw(16) << std::setprecision(6) << outer_norms[3]
       << endl;
  hist.setf(old_float, ios::floatfield);
  hist.setf(old_adjust, ios::adjustfield);
}
#endif
// ======================================================================

void OldOptimizer::WriteQuasiNewtonIter(
    const int & outer_iter, const int & inner_iter,
    const int & total_inner, const int & precond, 
    const double & outer_norm, const double & inner_norm,
    ostream & hist) {
  if (!(hist.good())) {
    cerr << "OldOptimizer(WriteQuasiNewtonIter): "
	 << "ostream is not good for i/o operations." << endl;
    throw(-1);
  }

  std::ios_base::fmtflags old_float =
      hist.setf(ios::scientific, ios::floatfield);
  std::ios_base::fmtflags old_adjust = 
      hist.setf(ios::left, ios::adjustfield);
  hist << std::setw(5) << outer_iter
       << std::setw(5) << inner_iter
       << std::setw(6) << total_inner
       << std::setw(7) << precond
       << std::setw(16) << std::setprecision(6) << outer_norm
       << std::setw(16) << std::setprecision(6) << inner_norm
       << endl;
  hist.setf(old_float, ios::floatfield);
  hist.setf(old_adjust, ios::adjustfield);
}

} // namespace
