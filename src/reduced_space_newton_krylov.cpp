/**
 * \file reduced_space_newton_krylov.cpp
 * \brief definitions of ReducedSpaceNewtonKrylov class member functions
 * \author Jason Hicken <jason.hicken@gmail.com>
 */

#include <fstream>
#include <boost/program_options.hpp>
#include <boost/format.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/shared_array.hpp>
#include <boost/pointer_cast.hpp>
#include "reduced_space_newton_krylov.hpp"
#include "krylov.hpp"

namespace kona {

using std::string;
using std::ostream;
using std::ifstream;
using std::ofstream;
using std::ostringstream;
using std::ios;
using std::max;
using std::vector;
using std::map;

// ==============================================================================
void ReducedSpaceNewtonKrylov::SetOptions(const po::variables_map& optns) {
  options_ = optns;
}
// ==============================================================================
void ReducedSpaceNewtonKrylov::MemoryRequirements(int & num_design,
                                                  int & num_state,
                                                  int & num_dual) {
  // vectors required in Solve() method
  num_design = 7;
  num_state = 8;
  num_dual = 0;
  int design_req, state_req, dual_req;
  // Set the Krylov method and find its requirements
  if (options_["krylov.solver"].as<string>() == "fgmres")
    krylov_.reset(new FGMRESSolver<DesignVector>());
  else if (options_["krylov.solver"].as<string>() == "ffom")
    krylov_.reset(new FFOMSolver<DesignVector>());
  else if (options_["krylov.solver"].as<string>() == "minres")
    krylov_.reset(new MINRESSolver<DesignVector>());
  else {
    cerr << "ReducedSpaceNewtonKrylov::MemoryRequirements(): "
         << "invalid krylov solver for this optimizer: "
         << options_["krylov.solver"].as<string>() << endl;
    throw(-1);
  }
  krylov_->SubspaceSize(options_["krylov.space_size"].as<int>());
  ptree num_required;
  krylov_->MemoryRequired(num_required);
  num_design += num_required.get<int>("num_vec");
  // Set the quasi-Newton method and find its requirements
  if (options_["quasi_newton.type"].as<string>() == "lbfgs") {
    quasi_newton_.reset(new GlobalizedBFGS
                        (options_["quasi_newton.max_stored"].as<int>()));
  } else {
    quasi_newton_.reset(new GlobalizedSR1
                       (options_["quasi_newton.max_stored"].as<int>()));
  }
  quasi_newton_->MemoryRequired(design_req);
  num_design += design_req;
  // Set the line search method and find its requirements
  const double alpha_init = options_["line_search.alpha_init"].as<double>();
  const double alpha_min = options_["line_search.alpha_min"].as<double>();
  const double alpha_max = options_["line_search.alpha_max"].as<double>();
  const double sufficient = options_["line_search.sufficient"].as<double>();
  const double reduct_fac = options_["line_search.reduct_fac"].as<double>();
  const double curv_cond = options_["line_search.curv_cond"].as<double>();
  const int max_iter = options_["line_search.max_iter"].as<double>();
  if (options_["line_search.type"].as<string>() == "wolfe") {
    line_search_.reset(new StrongWolfe
                       (alpha_init, alpha_max, sufficient, curv_cond, max_iter));
  } else {
    line_search_.reset(new BackTracking
                       (alpha_init, alpha_min, sufficient, reduct_fac));
  }
  line_search_->MemoryRequired(design_req, state_req, dual_req);
  num_design += design_req;
  num_state += state_req;
  num_dual += dual_req;
  // Find memory requirements of merit function (it is not allocated here)
  GlobalizedMerit::MemoryRequired(design_req, state_req, dual_req);
  num_design += design_req;
  num_state += state_req;
  num_dual += dual_req;
  // Set the matrix-vector product and find its requirements
  mat_vec_.reset(new ReducedHessianProduct
                 (options_["reduced.krylov_size"].as<int>()));
  num_required.clear();
  mat_vec_->MemoryRequired(num_required);
  num_design += num_required.get<int>("design");
  num_state += num_required.get<int>("state");
  num_dual += num_required.get<int>("dual");
  // Set the preconditioner and find its requirements
  if (options_["reduced.precond"].as<string>() == "none")
    precond_.reset(new IdentityPreconditioner<DesignVector>());
  else if (options_["reduced.precond"].as<string>() == "quasi_newton")
    precond_.reset(new ReducedSpacePreconditioner(quasi_newton_.get()));
  else {
    cerr << "ReducedSpaceNewtonKrylov::MemoryRequirements(): "
         << "invalid preconditioner for this optimizer: "
         << options_["reduced.precond"].as<string>() << endl;
    throw(-1);
  }
  precond_->MemoryRequired(num_required);
  num_design += num_required.get<int>("design");
  num_state += num_required.get<int>("state");
  num_dual += num_required.get<int>("dual");
}
// ==============================================================================
void ReducedSpaceNewtonKrylov::Solve() {
  // open output files to save information and convergence histories
  ostream* info;
  ofstream info_file;
  if (options_["send_info_to_cout"].as<bool>())
    info = &std::cout;
  else {
    info_file.open(options_["info_file"].as<string>().c_str());
    info = &info_file;
  }
  quasi_newton_->SetOutputStream(*info);
  line_search_->SetOutputStream(*info);
  ofstream hist_out(options_["hist_file"].as<string>().c_str());
  WriteHistoryHeader(hist_out);
  ofstream krylov_out(options_["krylov.file"].as<string>().c_str());
  ofstream reduced_out(options_["reduced.krylov_file"].as<string>().c_str());

  DesignVector X; // the current solution of the reduced system
  DesignVector P; // the search direction
  DesignVector dJdX; // current reduced gradient
  DesignVector dJdX_old; // work vector
  PDEVector state; // the current solution of the primal PDE 
  PDEVector adjoint; // the current solution of the adjoint PDE
  PDEVector adjoint_res; // residual of adjoint equation

  // work arrays
  std::vector<DesignVector> design_work1(2, X);
  std::vector<PDEVector> pde_work1(5, state);

  // set initial guess and store initial design
  X.EqualsInitialDesign();
  state.EqualsPrimalSolution(X);
  adjoint.EqualsAdjointSolution(X, state);
  DesignVector initial_design;
  initial_design = X;
  CurrentSolution(0, X, state, adjoint);

  // initialize the Quasi-Newton approximation
  quasi_newton_->SetInvHessianToIdentity();

  int iter = 0;
  int nonlinear_sum = 0;
  bool converged = false;
  double grad_norm0, grad_norm, grad_tol;
  double lambda = options_["inner.lambda_init"].as<double>();
  double krylov_tol;
  boost::scoped_ptr<double> sig(new double);
  *sig = 1.0/options_["reduced.product_fac"].as<double>();
  while (iter < options_["max_iter"].as<int>()) {
    iter++;

    *info << endl;
    *info << "==================================================" << endl;
    *info << "Beginning Homotopy loop number " << iter << endl;
    *info << "lambda = " << lambda << endl;
    *info << endl;

    // check for convergence
    dJdX.EqualsReducedGradient(X, state, adjoint, design_work1[0]);
    if (iter == 1) {
      grad_norm0 = dJdX.Norm2();
      grad_norm = grad_norm0;
      quasi_newton_->set_norm_init(grad_norm0);
      *info << "grad_norm0  = " << grad_norm0 << endl;
      grad_tol = options_["des_tol"].as<double>()*grad_norm0; //*max(grad_norm0, 1.e-6);
    } else {
      grad_norm = dJdX.Norm2();
      *info << "grad_norm : grad_tol = "
            << grad_norm << " : " << grad_tol << endl;
      // check for convergence
      if (grad_norm < grad_tol) {                               
        converged = true;
        break;
      }
    }

    // update lambda for quasi-Newton method
    quasi_newton_->set_lambda(lambda);

    int inner_iter = 0;
    bool inner_converged = false;
    double inner_grad_norm0, inner_grad_norm, inner_grad_tol;
    double sens_norm, dudx_norm, dpsidx_norm;
    ostringstream hist_string(ostringstream::out);
    while (inner_iter < options_["inner.max_iter"].as<int>()) {
      inner_iter++;

      *info << "-------------------------" << endl;
      *info << "inner iteration = " << inner_iter << endl;

      // update the Quasi-Newton method if necessary
      if (inner_iter == 1) {
        dJdX_old = dJdX;
      } else {
        //state.EqualsPrimalSolution(X);
        //adjoint.EqualsAdjointSolution(X, state);
        dJdX.EqualsReducedGradient(X, state, adjoint, design_work1[0]);
        CurrentSolution(nonlinear_sum, X, state, adjoint);
        dJdX_old -= dJdX;
        dJdX_old *= -1.0;
        quasi_newton_->AddCorrection(P, dJdX_old);
        dJdX_old = dJdX;
      }
      
      // check for convergence of the un-globalized problem
      grad_norm = dJdX.Norm2();
      if (grad_norm < grad_tol) {
        inner_converged = true;
      }

      // evaluate the gradient of the globalized problem
      dJdX.EqualsAXPlusBY((1.0 - lambda), dJdX, lambda*grad_norm0, X);
      dJdX.EqualsAXPlusBY(1.0, dJdX, -lambda*grad_norm0, initial_design);
#if 0
      dJdX.EqualsAXPlusBY((1.0 - lambda), dJdX, lambda, X);
      dJdX.EqualsAXPlusBY(1.0, dJdX, -lambda, initial_design);
#endif
      // check for convergence of inner problem
      if (fabs(lambda) < kEpsilon) {
        // use outer convergence tolerance
        grad_norm = dJdX.Norm2();
        inner_grad_norm = grad_norm;
        inner_grad_tol = grad_tol;
        *info << "grad_norm : grad_tol = "
              << grad_norm << " : " << grad_tol << endl;
        if (inner_iter == 1) inner_grad_norm0 = grad_norm;
        if (inner_grad_norm < inner_grad_tol) inner_converged = true;
      } else {
        if (inner_iter == 1) {
          inner_grad_norm0 = dJdX.Norm2();
          inner_grad_norm = inner_grad_norm0;
          inner_grad_tol = options_["inner.des_tol"].as<double>()
              *inner_grad_norm0; //*max(inner_grad_norm0, 1.0);
          *info << "inner_grad_norm0  = " << inner_grad_norm0 << endl;
        } else {
          inner_grad_norm = dJdX.Norm2();
          *info << "inner_grad_norm : inner_grad_tol = "
                << inner_grad_norm << " : " << inner_grad_tol << endl;
          if (inner_grad_norm < inner_grad_tol) {
            inner_converged = true;
          }
        }
      }
      WriteHistoryIteration(iter, inner_iter, nonlinear_sum+1,
                            UserMemory::get_precond_count(), inner_grad_norm,
                            grad_norm, hist_out);
      if (inner_converged) break;

      // compute Krylov tolerance to achieve superlinear convergence, but to
      // avoid oversolving near the desired nonlinear tolerance
      krylov_tol = 0.5*std::min(
          1.0, sqrt(inner_grad_norm/inner_grad_norm0)); // superlinear
      krylov_tol = std::max(krylov_tol, inner_grad_tol/inner_grad_norm);
      krylov_tol *= options_["reduced.nu"].as<double>();
      *info << "krylov_tol (inner) = " << krylov_tol << endl;
      
      // need adjoint residual for reduced-Hessian product
      adjoint_res.EqualsObjectiveGradient(X, state);
      pde_work1[0].EqualsTransJacobianTimesVector(X, state, adjoint);
      adjoint_res += pde_work1[0];
      
      // set Hessian-vector product tolerance parameters
      // Note: we must divide by nu in product_fac, because nu appears in the
      // krylov_tol, which is fed back to the Hessian-vector product; we do not
      // want this nu factor in product_fac (we only want 1.0 - nu), so we
      // "remove" it here
      double product_fac = options_["reduced.product_fac"].as<double>() *
          (1.0 - options_["reduced.nu"].as<double>()) /
          options_["reduced.nu"].as<double>();

#if 0
      double product_fac = (1.0 - options_["reduced.nu"].as<double>()) /
          (*sig*options_["reduced.nu"].as<double>());
#endif
      if (!options_["reduced.dynamic_tol"].as<bool>())
        product_fac *= (krylov_tol/static_cast<double>(
            options_["krylov.space_size"].as<int>()));

      // Initialize the reduced-Hessian-vector product
      ptree prod_param;
      prod_param.put("product_fac", product_fac);
      prod_param.put("lambda", lambda);
      prod_param.put("scale", grad_norm0);
      // set parameters for sensitivity norm bounds (determine the 2nd-order
      // adjoint tolerances)
      if (!(options_["reduced.bound_frozen"].as<bool>()) || (inner_iter == 1) ) {
        prod_param.put("bound_frozen", false);
        prod_param.put("bound_type",
                       options_["reduced.bound_type"].as<string>());
        prod_param.put("bound_approx",
                       options_["reduced.bound_approx"].as<bool>());
      } else {
        prod_param.put("bound_frozen", true);
      }
      // if (options_["quasi_newton.matvec_update"].as<bool>()) {
      //   // update the quasi-Newton method during Hessian-vector products
      //   prod_param.put("quasi_newton", quasi_newton_.get());
      // }
      boost::static_pointer_cast<ReducedHessianProduct>(mat_vec_)->Initialize(
          X, state, adjoint, dJdX_old, adjoint_res, design_work1, pde_work1,
          prod_param, reduced_out);
      if (options_["quasi_newton.matvec_update"].as<bool>()) {
        // update the quasi-Newton method during Hessian-vector products
        boost::static_pointer_cast<ReducedHessianProduct>(mat_vec_)
            ->set_quasi_newton(quasi_newton_.get());
      }
      
#if 0
      // ...and define the preconditioner
      Preconditioner<DesignVector>* precond =
          new ReducedSpacePreconditioner(quasi_newton_);
#endif
      
      // solve for the reduced-space search direction
      P = 0.0;
      ptree ptin, ptout;
      ptin.put<double>("tol", krylov_tol);
      ptin.put<bool>("check", options_["krylov.check_res"].as<bool>());
      ptin.put<bool>("dynamic", options_["reduced.dynamic_tol"].as<bool>());
      krylov_->Solve(ptin, dJdX, P, *mat_vec_, *precond_, ptout, krylov_out);
      int iters = ptout.get<int>("iters");
      P *= -1.0;
      *info << "after Krylov solve of reduced-space direction P" << endl;
      
      // check that P is a descent direction
      double p_dot_grad = InnerProd(P, dJdX);
      if (p_dot_grad >= 0.0) {
        *info << "P is not a descent direction: "
              << "switching to quasi-Newton direction." << endl;
        quasi_newton_->ApplyInvHessianApprox(dJdX, P);
        P *= -1.0;
        p_dot_grad = InnerProd(P, dJdX);
        if (p_dot_grad >= 0.0) {        
          // if using SR1, P may still fail to be a descent direction
          *info << "P is not a descent direction: "
                << "switching to steepest descent direction." << endl;
          P = dJdX;
          P *= -1.0;
          p_dot_grad = InnerProd(P, dJdX);
        }
      }

      // set-up the merit function and line search
      merit_.reset(); // needs to come first to free user memory
      merit_.reset(new GlobalizedMerit(initial_design, lambda, grad_norm0, P, X,
                                       p_dot_grad, state, adjoint));
      line_search_->SetMeritFunction(*merit_);
      line_search_->SetSearchDotGrad(p_dot_grad);

      double alpha = line_search_->FindStepLength();
      X.EqualsAXPlusBY(1.0, X, alpha, P);

#if 0
      double alpha = 1.0;
      X.EqualsAXPlusBY(1.0, X, alpha, P);
#endif

      // state should be up-to-date, adjoint not
      //state.EqualsPrimalSolution(X);
      adjoint.EqualsAdjointSolution(X, state);

      // S = delta X = alpha * P is needed later
      P *= alpha;
      
      nonlinear_sum++;
    } // inner iterations
    
    // update the continuation parameter
    lambda = max(0.0, lambda - 0.1);
    //lambda = max(0.0, lambda - 0.05);
    //lambda = max(0.0, lambda - 0.01);
    //lambda = 0.1*lambda;
  } // outer iterations
  *info << "Total number of nonlinear iterations: " << nonlinear_sum << endl;
}
// ==============================================================================
void ReducedSpaceNewtonKrylov::WriteHistoryHeader(ostream& out) {
  if (!(out.good())) {
    cerr << "ReducedSpaceNewtonKrylov::WriteHistoryHeader(): "
	 << "ostream is not good for i/o operations." << endl;
    throw(-1);
  }
  out << "# Kona Newton-Krylov convergence history file\n";
  boost::format col_head(string("%|7| %|9t| %|5| %|16t| %|11| %|29t|")+
                         string("%|14| %|45t| %|-17| %|64t| %|-12|\n"));
  col_head % "# outer" % "inner" % "total inner" % "precond. calls"
      % "global. grad norm" % "grad norm";
  out << col_head;
  out.flush();
}
// ==============================================================================
void ReducedSpaceNewtonKrylov::WriteHistoryIteration(
    const int& outer, const int& inner, const int& total_inner,
    const int& precond_calls, const double& globalized_norm, const double& norm,
    ostream& out) {
  if (!(out.good())) {
    cerr << "ReducedSpaceNewtonKrylov::WriteHistoryIteration(): "
	 << "ostream is not good for i/o operations." << endl;
    throw(-1);
  }
  out << boost::format(string("%|7| %|9t| %|5| %|16t| %|11| %|29t|")+
                         string("%|14| %|45t| %|-17.6| %|64t| %|-12.6|\n"))
      % outer % inner % total_inner % precond_calls % globalized_norm % norm;
  out.flush();
}
// ==============================================================================
} // namespace
