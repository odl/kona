/**
 * \file filter.cpp
 * \brief definitions of members of Filter
 * \author Jason Hicken <jason.hicken@gmail.com>
 */

#include "filter.hpp"

namespace kona {
// ==============================================================================
/*!
 * \class compare_points
 * \brief a predicate for remove_if used in Filter::Dominated
 */
class compare_points {
 public:
  /*!
   * \brief constructor
   * \param[in] new_point - point that old points are compared with
   */
  compare_points(const pair<double, double>& new_point) :
      new_point_(new_point) {}
  /*!
   * \brief comparison operator
   * \param[in] old_point - a point in the filter that we are comparing
   * \returns true if old_point is dominated by new point
   */
  bool operator()(const pair<double, double>& old_point) {
    return ( (old_point.first >= new_point_.first) &&
             (old_point.second >= new_point_.second) );
  }
 private:
  const pair<double, double> new_point_; ///< stores the new point for comparison
};
// ==============================================================================
bool Filter::Dominates(double obj, double cnstrnt) {
  // create new point and check if it is dominated by elements of filter
  pair<double, double> new_point(obj, cnstrnt);
  list<pair<double, double> >::iterator it;
  for (it = filter_.begin(); it != filter_.end(); ++it)
    if ( (new_point.first >= it->first) &&
         (new_point.second >= it->second) ) return true;
  // if we get here, the new pair is acceptable to the filter; remove old pairs
  // that are dominated by new pair
  compare_points dominated_by(new_point);
  filter_.remove_if(dominated_by);
  // finally, add new pair
  filter_.push_front(new_point);
  return false;
}
// ==============================================================================
void Filter::Print(ostream & fout) const {
  fout << "Filter contents:" << endl;
  int i = 1;
  list<pair<double, double> >::const_iterator it;
  for (it = filter_.begin(); it != filter_.end(); ++it, ++i)
    fout << i << ": (" << it->first << "," << it->second << ")" << endl;
}
// ==============================================================================
} // namespace kona
