/**
 * \file verifier.hpp
 * \brief header file for the Verifier class
 * \author Jason Hicken <jason.hicken@gmail.com>
 */

#pragma once

#include <boost/program_options.hpp>
#include "optimizer.hpp"

namespace kona {

using boost::program_options::variables_map;

/*!
 * \class Verifier
 * \brief checks the accuracy and correctness of user supplied routines
 */
class Verifier : public Optimizer {
 public:

  /*!
   * \brief default constructor
   */
  Verifier() {}
  
  /*!
   * \brief class destructor
   */
  ~Verifier() {}

  /*!
   * \brief defines the options needed by the algorithm
   * \param[in] optns - set of options
   */
  void SetOptions(const po::variables_map& optns);
  
  /*!
   * \brief determines how many user stored vectors are needed
   * \param[out] num_design - number of design-sized vectors needed
   * \param[out] num_state - number of state-sized vectors needed
   * \param[out] num_dual - number of dual-sized vectors needed
   */
  void MemoryRequirements(int & num_design, int & num_state,
                          int & num_dual);

  /*!
   * \brief attempts to solve the optimization problem
   */
  void Solve();

 protected:
  variables_map options_; ///< options for the algorithm
};

} // namespace kona
