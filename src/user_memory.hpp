/**
 * \file user_memory.hpp
 * \brief header file for user memory management classes
 * \author Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#pragma once

#include <math.h>
#include <ostream>
#include <iostream>
#include <vector>

namespace kona {

using std::cerr;
using std::cout;
using std::endl;
using std::vector;
using std::ostream;

/*!
 * \brief set of possible requests sent to userfunc
 */
enum tasks {
  allocmem = 0,   ///< allocate memory for user vectors
  axpby_d = 1,    ///< user design-sized vector version of a*x +b*y
  axpby_s = 2,    ///< user state-sized vector version of a*x + b*y
  axpby_c = 20,   ///< user dual-sized vector version of a*x + b*y
  innerprod_d = 3,///< dot product of two user design-sized vectors
  innerprod_s = 4,///< dot product of two user state-sized vectors
  innerprod_c =21,///< dot product of two user dual-sized vectors
  restrict_d = 27,///< restrict design vector to particular subspace
  convert_d = 28, ///< convert dual to design-subspace
  convert_c = 29, ///< convert design-subspace to dual
  eval_obj = 19,  ///< evaluate the objective function
  eval_pde = 5,   ///< evaluate the state equations (discretized PDE)
  eval_ceq = 22,  ///< evaluate the equality constraints
  jacvec_d = 6,   ///< compute (design) Jacobian-vector product
  jacvec_s = 7,   ///< compute (state) Jacobian-vector product
  tjacvec_d = 8,  ///< compute (design) transposed-Jacobian-vector product  
  tjacvec_s = 9,  ///< compute (state) transposed-Jacobian-vector product
  ceqjac_d = 23,  ///< compute (design) constraint-Jacobian-vector product
  ceqjac_s = 24,  ///< compute (state) constraint-Jacobian-vector product
  tceqjac_d = 25, ///< compute (design) trans-constraint-Jacobian-vector product
  tceqjac_s = 26, ///< compute (state) trans-constraint-Jacobian-vector product
  eval_precond=10,///< prepare, factorize (etc) the preconditioner
  precond_s = 11, ///< apply state problem preconditioner
  tprecond_s = 12,///< apply adjoint problem preconditioner
  grad_d = 13,    ///< compute design component of objective gradient
  grad_s = 14,    ///< compute state component of objective gradient
  initdesign = 15,///< set initial design vector
  solve = 16,     ///< solve primal problem
  adjsolve = 17,  ///< solve adjoint problem
  linsolve = 30,  ///< solve linearized pde problem
  debug = 31,     ///< used for development
  rank = 32,      ///< used for parallel development
  info = 18       ///< provide information to user
};

/*!
 * \brief different types of vectors, or vector spaces
 */
enum vector_type {
  design = 0,     ///< design space vector
  pde = 1,        ///< state or adjoint space vector
  dual = 2        ///< constraint or Lagrange multiplier vector
};

// ======================================================================
/*!
 * \class UserMemory
 * \brief manages user allocated memory for vectors
 * 
 * The PDE constraints may be implemented by the user in various
 * paradigms (serial, MPI, CUDA, etc).  Consequently, the vectors in
 * the design and state spaces may also be implemented in different
 * ways.  Rather than implementing a version of Kona for each of these
 * paradigms, this class and UserVector are used to associate and
 * manipulate user memory vectors allocated by the user without
 * needing to know how those vectors are actually implemented.
 *
 * UserMemory is implemented as a singleton class, although similar
 * functionality could be achieved using global memory and functions.
 */
class UserMemory {
 public:

  /*!
   * \brief allocates user memory and provides singleton to manage it
   * \param[in] func_ptr - user function that allows memory management
   * \param[in] num_design_size - number of vectors in design space
   * \param[in] num_state_size - number of vectors in state space
   * \param[in] num_dual_size - number of constraints
   */
  static UserMemory * Allocate(
      int (*func_ptr)(int, int, int*, int, double*),
      const int & num_design_size,
      const int & num_state_size,
      const int & num_dual_size = 0);

  /*!
   * \brief frees allocated arrays and singleton
   */
  void Deallocate();

  /*!
   * \brief determines if a given index is valid 
   * \param[in] vec - enum indicating the type of user vector to check
   * \param[in] negative_one_ok - indicates if index = -1 is valid
   * \param[in] index - index to check
   */
  static void CheckIndex(const vector_type & vec, 
                         const bool & negative_one_ok, 
                         const int & index);

  /*!
   * \brief assigns user memory of a particular type
   * \param[in] vec - enum indicating the type of user vector to assign
   * \param[out] index - a unique index associated with user memory
   */
  static void AssignVector(const vector_type & vec, int & index);

  /*!
   * \brief reverse operation of AssignVector; frees user memory
   * \param[in] vec - enum indicating the type of user vector to unassign
   * \param[in] index - unique index associated with user memory to free
   */
  static void UnassignVector(const vector_type & vec, const int & index);
  
  /*!
   * \brief interface for requesting operation a*x + b*y
   * \param[in] vec - enum indicating the type of user vector
   * \param[in] target_index - index of vector being assigned the value
   * \param[in] a - scaling coefficient for x
   * \param[in] x_index - index of first vector in operation
   * \param[in] b - scaling coefficient for y
   * \param[in] y_index - index of second vector in operation
   */
  static void AXPlusBY(const vector_type & vec, const int & target_index,
                       const double & a, const int & x_index,
                       const double & b, const int & y_index);

  /*!
   * \brief interface for requesting inner product between to vectors
   * \param[in] vec - enum indicating the type of user vector
   * \param[in] x_index - index of first vector in inner product
   * \param[in] y_index - index of second vector in inner product
   */
  static double InnerProd(const vector_type & vec, const int & x_index,
                          const int & y_index);

  /*!
   * \breif restricts the design vector to a subspace (by zeroing entries)
   * \param[in] type - indicates which subspace is retained
   * \param[in] design_index - index of the vector being restricted
   *
   * This is used primarily for IDF-type problems, where there is a natural
   * partition of the design space into "design" and "target" variables.
   */
  static void Restrict(const int & type, const int & design_index);

  /*!
   * \brief transform a dual vector to design-subspace, or vice-versa
   * \param[in] vec - enum indicating the type of target vector
   * \param[in] source_index - index of the vector being transformed
   * \param[in] target_index - index of the vector being set
   *
   * This is used primarily for IDF-type problems, where there is a natural
   * partition of the design space into "design" and "target" variables.
   */
  static void ConvertVec(const vector_type & vec, const int & source_index,
                         const int & target_index);
  
  /*!
   * \brief set given design vector to the initial design
   * \param[in] target_index - design index where initial design stored
   */
  static void SetInitialDesign(const int & target_index);
  
  /*!
   * \brief request user to evaluate the objective function value
   * \param[in] design_index - design index where objective is evaluated
   * \param[in] state_index - state index where objective is evaluated
   */
  static double EvaluateObjective(const int & design_index,
                                const int & state_index);

  /*!
   * \brief request user to evaluate state equations (PDEs)
   * \param[in] target_index - index where the equations are to be stored
   * \param[in] design_index - design index where equations are evaluated
   * \param[in] state_index - state index where equations are evaluated
   */
  static void EvaluatePDE(const int & target_index,
                          const int & design_index,
                          const int & state_index);

  /*!
   * \brief request user to evaluate the constraints
   * \param[in] target_index - dual index where the constraints are to be stored
   * \param[in] design_index - design index where constraints are evaluated
   * \param[in] state_index - state index where constraints are evaluated
   */
  static void EvaluateConstraints(const int & target_index,
                                  const int & design_index,
                                  const int & state_index);
  
  /*!
   * \brief request user to solve the discretized PDE at given design
   * \param[in] target_index - index where the solution is to be stored
   * \param[in] design_index - design index where equations are evaluated
   * \returns true if solver is successful, false otherwise
   */
  static bool SolvePDE(const int & target_index,
                       const int & design_index);

  /*!
   * \brief request user to solve the linearized PDE at given (design,state)
   * \param[in] target_index - index where the solution is to be stored
   * \param[in] design_index - design index where equations are evaluated
   * \param[in] state_index - state index where equations are evaluated
   * \param[in] rhs_index - state index where rhs is stored
   * \param[in] rel_tol - requested relative tolerance for final residual norm
   * \returns true if solver is successful, false otherwise
   */
  static bool SolveLinearized(const int & target_index,
                              const int & design_index,
                              const int & state_index,
                              const int & rhs_index,
                              const double & rel_tol);
  
  /*!
   * \brief request user to solve the adjoint PDE at given (design,state)
   * \param[in] target_index - index where the solution is to be stored
   * \param[in] design_index - design index where equations are evaluated
   * \param[in] state_index - state index where equations are evaluated
   * \param[in] rhs_index - state index where rhs is stored
   * \param[in] rel_tol - requested relative tolerance for final residual norm
   * \returns true if solver is successful, false otherwise
   */
  static bool SolveAdjoint(const int & target_index,
                           const int & design_index,
                           const int & state_index,
                           const int & rhs_index,
                           const double & rel_tol);

  /*!
   * \brief request user to evaluate component of objective gradient
   * \param[in] vec - component of gradient to evaluate
   * \param[in] target_index - index where the equations are to be stored
   * \param[in] design_index - design index where gradient is evaluated
   * \param[in] state_index - state index where gradient is evaluated
   */
  static void ObjectiveGradient(const vector_type & vec, 
                                const int & target_index,
                                const int & design_index,
                                const int & state_index);
  
  /*!
   * \brief interface for requesting various types of PDE Jacobian products
   * \param[in] request - one of several types of products, e.g. jacvec_s
   * \param[in] design_index - index of design to evaluate Jacobian at
   * \param[in] state_index - index of state to evaluate Jacobian at
   * \param[in] u_index - index of the vector being operated on
   * \param[in] v_index - index of the vector for result of operation
   *
   * both design_index and state_index can be set to -1 to indicate
   * that the system matrix is linear in these variables
   */
  static void JacobianOperation(
      const tasks & request, const int & design_index, 
      const int & state_index, const int & u_index, const int & v_index);

  /*!
   * \brief interface for requesting various constraint Jacobian products
   * \param[in] request - one of several types of products, e.g. diffceq_d
   * \param[in] design_index - index of design to evaluate Jacobian at
   * \param[in] state_index - index of state to evaluate Jacobian at
   * \param[in] u_index - index of the vector being operated on
   * \param[in] v_index - index of the vector for result of operation
   *
   * both design_index and state_index can be set to -1 to indicate
   * that the constraint Jacobian is linear in these variables
   */
  static void ConstraintJacobianOperation(
      const tasks & request, const int & design_index, 
      const int & state_index, const int & u_index, const int & v_index);
  
  /*!
   * \brief request that the user build the preconditioner(s)
   * \param[in] design_index - index of design to build preconditioner at
   * \param[in] state_index - index of state to build preconditioner at
   *
   * The user is asked to build preconditioners for both the state and
   * adjoint if they are distinct.  The user may also ignore this 
   * request if the preconditioner is constructed "on the fly".
   */
  static void BuildPreconditioner(const int & design_index,
                                  const int & state_index);

  /*!
   * \brief interface for requesting a preconditioning operation
   * \param[in] request - type of preconditioning (primal or adjoint)
   * \param[in] design_index - index of design to evaluate Jacobian at
   * \param[in] state_index - index of state to evaluate Jacobian at
   * \param[in] u_index - index of the vector being preconditioned
   * \param[in] v_index - index of vector to store result
   * \param[in] diag - (optional) defines diagonal matrix added to preconditioner
   *
   * as with JacobianOperation(), both design_index and state_index
   * can be set to -1 to indicate that the preconditioner is
   * independent of the state and/or design.  Also, these indices may
   * be ignored by the user in cases where the preconditioner has
   * already been built using a call to BuildPreconditioner().
   */
  static void PreconditionOperation(
      const tasks & request, const int & design_index, 
      const int & state_index, const int & u_index, const int & v_index,
      const double & diag = 0.0);

  /*
   * \brief used to intrude into the user vectors for debugging
   * \param[in] vec - enum indicating the type of user vector
   * \param[in] index - index of first vector in inner product
   * \param[in] int_work - used to pass integer data
   * \param[in] dbl_work - used to pass double data
   */
  static void Debug(const vector_type & vec, const int & index,
                    const int & int_work,
                    const double & dbl_work);      

  /*
   * \brief asks the user to return the rank of the process (for debugging)
   * \returns the rank of the calling process
   */
  static int GetRank();
  
  /*!
   * \brief lets user know where the current solution is stored
   * \param[in] iter - current nonlinear iteration
   * \param[in] design_index - index of current design 
   * \param[in] state_index - index of current solution of PDE
   * \param[in] adjoint_index - index of current solution of adjoint
   * \param[in] dual_index - index of current solution of Lagrange multipliers
   * \param[in,out] out - a valid output stream
   */
  static void CurrentSolution(const int & iter,
                              const int & design_index,
                              const int & state_index,
                              const int & adjoint_index,
                              const int & dual_index = -1,
                              ostream& out = std::cout);

  /*!
   * \brief returns the current cummulative sum of preconditioner calls
   * 
   * This is only as good as the information supplied by the user
   */
  static int get_precond_count() { return precond_count_; }

 private:
  
  /// private constructor to avoid creating more than one instance
  UserMemory() {}

  /// private copy constructor, again, to avoid more than one instance
  UserMemory(const UserMemory &) {}

  /// private destructor
  ~UserMemory() {}

  /// private assignment operator, to avoid more than one instance
  UserMemory & operator=(const UserMemory &) {}

  /// size of iwrk integer array
  static int leniwrk_;

  /// integer work array used to pass information to userfunc
  static int *iwrk_;

  /// size of dwrk double array
  static int lendwrk_;
  
  /// double work array used to pass information to userfunc
  static double *dwrk_;
  
  /// indicates which design-sized user vectors are assigned
  static vector<bool> design_assigned_;
  
  /// indicates which state-sized user vectors are assigned
  static vector<bool> state_assigned_;

  /// indicates which dual-sized user vectors are assigned
  static vector<bool> dual_assigned_;
  
  /// pointer that provides access to single instance
  static UserMemory * memory_;

  // number of preconditioner calls 
  static int precond_count_;

  // pointer to the user function
  static int (*user_function_)(int, int, int*, int, double*); 
};

// ======================================================================
/*!
 * \class UserVector
 * \brief templated class for manipulating user vectors
 */
template <vector_type space>
class UserVector {
 public:

  /*!
   * \brief default constructor associates user memory to UserVector
   */
  UserVector() { UserMemory::AssignVector(space, index_); }

  /*!
   * \brief default destructor removes association to user memory
   */
  virtual ~UserVector() { UserMemory::UnassignVector(space, index_); }

  /*!
   * \brief constructor to set values to a constant
   * \param[in] val - value assigned to each element of UserVector
   */
  explicit UserVector(const double & val) {
    UserMemory::AssignVector(space, index_);
    UserMemory::AXPlusBY(space, index_, val, -1, 0.0, -1);
  }
  
  /*!
   * \brief copy constructor with deep copy (potentially expensive)
   * \param[in] u - UserVector being copied to calling object
   */
  explicit UserVector(const UserVector & u) {
    UserMemory::AssignVector(space, index_);
    UserMemory::AXPlusBY(space, index_, 0.0, -1, 1.0, u.index_);
  }

  /*!
   * \brief UserVector=UserVector assignment operator with deep copy
   * \param[in] u - UserVector value being assigned
   */
  UserVector & operator=(const UserVector & u) {
    if (this == &u) return *this;
    UserMemory::AXPlusBY(space, index_, 0.0, -1, 1.0, u.index_);
    return *this;
  }

  /*!
   * \brief UserVector=double assignment operator
   * \param[in] val - value assigned to each element of UserVector
   */
  UserVector & operator=(const double & val) {
    UserMemory::AXPlusBY(space, index_, val, -1, 0.0, -1);
    return *this;
  }

  /*!
   * \brief compound addition-assignment operator
   * \param[in] u - UserVector being added to calling object
   */
  UserVector & operator+=(const UserVector & u) {
    UserMemory::AXPlusBY(space, index_, 1.0, u.index_, 1.0, index_);
    return *this;
  }

  /*!
   * \brief compound subtraction-assignment operator
   * \param[in] u - UserVector being subtracted from calling object
   */
  UserVector & operator-=(const UserVector & u) {
    UserMemory::AXPlusBY(space, index_, -1.0, u.index_, 1.0, index_);
    return *this;
  }

  /*!
   * \brief compound scalar multiplication-assignment operator
   * \param[in] val - value to multiply calling object by
   */
  UserVector & operator*=(const double & val) {
    UserMemory::AXPlusBY(space, index_, val, index_, 0.0, -1);
    return *this;
  }

  /*!
   * \brief compound scalar division-assignment operator
   * \param[in] val - value to divide elements of calling object by
   */
  UserVector & operator/=(const double & val) {
    *this *= (1.0/val);
    return *this;
  }

  /*!
   * \brief general linear combination of two UserVectors
   * \param[in] a - scalar factor for x
   * \param[in] x - first UserVector in linear combination
   * \param[in] b - scalar factor for y
   * \param[in] y - second UserVector in linear combination
   */
  void EqualsAXPlusBY(const double & a, const UserVector & x,
                      const double & b, const UserVector & y) {
    UserMemory::AXPlusBY(space, index_, a, x.index_, b, y.index_);
  }

  /*!
   * \brief the L2 norm of the UserVector
   */
  double Norm2() const {
    double val = InnerProd(*this, *this);
#ifdef DEBUG
    if (val < 0.0) {
      cerr << "UserVector(Norm2): "
           << "inner product of UserVector is negative" << endl;
      throw(-1);
    }
#endif
    return sqrt(val);
  }

  /*!
   * \brief inner product between two UserVectors
   * \param[in] u - first UserVector in inner product
   * \param[in] v - second UserVector in inner product
   */
  friend double InnerProd(const UserVector & u, const UserVector & v) {
    return UserMemory::InnerProd(space, u.index_, v.index_);
  }

 protected:
  
  /// index associating UserVector to a particular user memory
  int index_;
};

} // namespace kona
