/**
 * \file reduced_kkt_vector.cpp
 * \brief definitions of the ReducedKKTVector class member and friend functions
 * \author Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

//#include <algorithm>
#include "./reduced_kkt_vector.hpp"
#include "./quasi_newton.hpp"
//#include "./user_memory.hpp"
#include "./vectors.hpp"
#include "./krylov.hpp"

namespace kona {
// ==============================================================================
ReducedKKTProduct::ReducedKKTProduct(ostream& outstream) {
  outstream_ = &outstream;
  bound_type_ = "fixed";
  sens_norm_ = 1.0;
  dudx_norm_ = 1.0;
  dpsidx_norm_ = 1.0;
}
// ==============================================================================
ReducedKKTProduct::ReducedKKTProduct(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    const PDEVector & eval_at_adjoint, ReducedKKTVector & reduced_opt,
    const PDEVector & adjoint_res, DesignVector & design_work1,
    DesignVector & design_work2, PDEVector & pde_work1,
    PDEVector & pde_work2, PDEVector & pde_work3,
    PDEVector & pde_work4, PDEVector & pde_work5, DualVector & dual_work1,
    const double & product_fac, ostream & outstream, const double & lambda,
    const double & scale, const double & grad_scale, const double & ceq_scale) {
  eval_at_design_ = &(eval_at_kkt.design_);
  eval_at_dual_ = &(eval_at_kkt.dual_);
  design_norm_ = eval_at_design_->Norm2();
  eval_at_state_ = &eval_at_state;
  state_norm_ = eval_at_state_->Norm2();
  eval_at_adjoint_ = &eval_at_adjoint;
  adjoint_res_ = &adjoint_res;
  reduced_grad_ = &(reduced_opt.design_);
  ceq_ = &(reduced_opt.dual_);
  pde_work1_ = &pde_work1;
  pde_work2_ = &pde_work2;
  pde_work3_ = &pde_work3;
  w_adj_ = &pde_work4;
  lambda_adj_ = &pde_work5;
  design_work1_ = &design_work1;
  pert_design_  = &design_work2;
  dual_work_ = &dual_work1;
  product_fac_ = product_fac;
  product_tol_ = 1.0;
  outstream_ = &outstream;
  lambda_ = lambda;
  scale_ = scale;
  grad_scale_ = 1.0/grad_scale;
  ceq_scale_ = 1.0/ceq_scale;
  *reduced_grad_ *= grad_scale_;
  *ceq_ *= ceq_scale_;
  bound_type_ = "fixed";
  sens_norm_ = 1.0; // this is defined by set_sens_norm (if necessary)
  dudx_norm_ = 1.0; // defined by compute/set_sens_norms
  dpsidx_norm_ = 1.0; // defined by compute/set_sens_norms
  quasi_newton_ = NULL; // this is defined by a set_quasi_newton (if necessary)
  augmented_ = false;
  approx_hessian_ = false;
}
// ==============================================================================
void ReducedKKTProduct::Initialize(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    const PDEVector & eval_at_adjoint, ReducedKKTVector & reduced_opt, 
    const PDEVector & adjoint_res, std::vector<DesignVector>& design_work,
    std::vector<PDEVector>& pde_work, std::vector<DualVector>& dual_work,
    const ptree& prod_param, ostream & outstream) {
  if ( (design_work.size() < 2) || (pde_work.size() < 5) ||
       (dual_work.size() < 1) ) {
    cerr << "ReducedKKTProduct::Initialize():"
         << "one or more of the work arrays has too few elements" << endl;
    throw(-1);
  }
  eval_at_design_ = &(eval_at_kkt.design_);
  eval_at_dual_ = &(eval_at_kkt.dual_);
  design_norm_ = eval_at_design_->Norm2();
  eval_at_state_ = &eval_at_state;
  state_norm_ = eval_at_state_->Norm2();
  eval_at_adjoint_ = &eval_at_adjoint;
  adjoint_res_ = &adjoint_res;
  reduced_grad_ = &(reduced_opt.design_);
  ceq_ = &(reduced_opt.dual_);
  pde_work1_ = &pde_work[0];
  pde_work2_ = &pde_work[1];
  pde_work3_ = &pde_work[2];
  w_adj_ = &pde_work[3];
  lambda_adj_ = &pde_work[4];
  design_work1_ = &design_work[0];
  pert_design_  = &design_work[1];
  dual_work_ = &dual_work[0];

  product_fac_ = prod_param.get<double>("product_fac");
  product_tol_ = 1.0;
  outstream_ = &outstream;
  lambda_ = prod_param.get<double>("lambda", 0.0);
  scale_ = prod_param.get<double>("scale", 1.0);
  grad_scale_ = 1.0/prod_param.get<double>("grad_scale", 1.0);
  ceq_scale_ = 1.0/prod_param.get<double>("ceq_scale", 1.0);
  *reduced_grad_ *= grad_scale_;
  *ceq_ *= ceq_scale_;
  
  quasi_newton_ = NULL; // this is defined by set_quasi_newton (if necessary)
  //augmented_ = false; // this is defined during solve, if necessary
  augmented_ = prod_param.get<bool>("augmented", false);
  approx_hessian_ = prod_param.get<bool>("approx_hessian", false);

  // second-order adjoint adaptive tolerance selection will go here...
}
// ==============================================================================
void ReducedKKTProduct::Initialize(const ptree& prod_param) {
  augmented_ = prod_param.get<bool>("augmented", false);
  approx_hessian_ = prod_param.get<bool>("approx_hessian", false);
}
// ==============================================================================
ReducedKKTProduct::~ReducedKKTProduct() {
}
// ==============================================================================
void ReducedKKTProduct::MemoryRequired(ptree& num_required) {
  num_required.put("design", 0);
  num_required.put("state", 0);
  num_required.put("dual", 0);
}
// ==============================================================================
void ReducedKKTProduct::set_quasi_newton(QuasiNewton * quasi_newton) {
  quasi_newton_ = quasi_newton;
}
// ==============================================================================
void ReducedKKTProduct::operator()(const ReducedKKTVector & u,
                                   ReducedKKTVector & v) {
  if (augmented_) {
    AugmentedProduct(u, v);
    return;
  }

  if (approx_hessian_) {
    QuasiNewtonProduct(u, v);
    return;
  }

  //-----------------------------------------------------------------------------
  // Find the FD step size; this is used for all perturbations
  double epsilon_fd = CalcEpsilon(design_norm_, u.design_.Norm2());
  //double epsilon_fd = 0.1*std::max(design_norm_, u.design_.Norm2());  
  *outstream_ << "ReducedKKTProduct: eps for design = " << epsilon_fd << endl;
  double eps_r = 1.0/epsilon_fd;  
  
  //-----------------------------------------------------------------------------
  // Find the state perturbation by solving the linearized state equation

  // RHS for first adjoint system
  pde_work1_->EqualsJacobianTimesVector(*eval_at_design_, *eval_at_state_,
                                        u.design_);
  *pde_work1_ *= -1.0;
  *w_adj_ = 0.0;
  int iters;
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# Forward adjoint problem in reduced-KKT product" << endl;
  // In the notation of the inexact-Hessian paper, product_fac = (1.0 - nu)/ C
  // where C is the unknown bound
  double rel_tol = product_tol_*product_fac_/pde_work1_->Norm2();
#if 0
  if (bound_type_ == "independent")
    rel_tol /= (2.0*dpsidx_norm_);
  else if (bound_type_ == "common")
    rel_tol /= (sqrt(2.0)*sens_norm_);
#endif
  rel_tol = 1e-8;//1e-6; //
  w_adj_->EqualsLinearizedSolution(*eval_at_design_, *eval_at_state_,
                                   *pde_work1_, rel_tol);

  //-----------------------------------------------------------------------------
  // Find the adjoint perturbation by solving the linearized dual equation

  pert_design_->EqualsAXPlusBY(1.0, *eval_at_design_, epsilon_fd, u.design_);
  pde_work3_->EqualsAXPlusBY(1.0, *eval_at_state_, epsilon_fd, *w_adj_);

  // first part of LHS: evaluate the adjoint-equation residual at the perturbed
  // design and state
  pde_work1_->EqualsObjectiveGradient(*pert_design_, *pde_work3_);
  pde_work2_->EqualsTransJacobianTimesVector(*pert_design_, *pde_work3_,
                                             *eval_at_adjoint_);
  *pde_work1_ += *pde_work2_;
  pde_work2_->EqualsTransCnstrJacTimesVector(*pert_design_, *pde_work3_,
                                             *eval_at_dual_);
  *pde_work1_ += *pde_work2_;

  // at this point *pde_work1 should contain the perturbed adjoint
  // residual, so take difference with unperturbed adjoint residual
  // NOTE: signs on eps_r move vector to RHS for subsequent solve
  pde_work1_->EqualsAXPlusBY(-eps_r, *pde_work1_, eps_r, *adjoint_res_);

  // second part of LHS: (dc/du)*u.dual_
  pde_work2_->EqualsTransCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                             u.dual_);
  *pde_work1_ -= *pde_work2_;

  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# Reverse adjoint problem in reduced-KKT product" << endl;
  *lambda_adj_ = 0.0;
  
  //cout << "ReducedHessianProduct::operator(): "
  //     << "norm of reverse adjoint rhs vector = " << pde_work1_->Norm2() << endl;
  rel_tol = product_tol_*product_fac_/pde_work1_->Norm2();
#if 0
  if (bound_type_ == "independent")
    rel_tol /= (2.0*dudx_norm_);
  else if (bound_type_ == "common")
    rel_tol /= (sqrt(2.0)*sens_norm_);
#endif
  rel_tol = 1e-8;//1e-6; //
  lambda_adj_->EqualsAdjointSolution(*eval_at_design_, *eval_at_state_,
                                     *pde_work1_, rel_tol);  

  //-----------------------------------------------------------------------------
  // Evaluate the first-order optimality conditions at perturbed
  // design, state, and adjoint; then take difference with unperturbed conditions

  pde_work2_->EqualsAXPlusBY(1.0, *eval_at_adjoint_, epsilon_fd, *lambda_adj_);
  
  v.design_.EqualsObjectiveGradient(*pert_design_, *pde_work3_);
  design_work1_->EqualsTransJacobianTimesVector(*pert_design_, *pde_work3_,
                                                *pde_work2_);
  v.design_ += *design_work1_;
  design_work1_->EqualsTransCnstrJacTimesVector(*pert_design_, *pde_work3_,
                                                *eval_at_dual_);
  v.design_ += *design_work1_;

  // the dual perturbation does not need a FD
  design_work1_->EqualsTransCnstrJacTimesVector(*eval_at_design_,
                                                *eval_at_state_,
                                                u.dual_);
  *design_work1_ *= epsilon_fd;
  v.design_ += *design_work1_;

  // perform difference
  v.design_ *= grad_scale_;
  v.design_.EqualsAXPlusBY(eps_r, v.design_, -eps_r, *reduced_grad_);

  //-----------------------------------------------------------------------------
  // evaluate dual part of product
  v.dual_.EqualsCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                    u.design_);
  dual_work_->EqualsCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                        *w_adj_);
  v.dual_ += *dual_work_;
  v.dual_ *= ceq_scale_;
  
  
#if 0
  if (u.design_.Norm2() < kEpsilon*u.dual_.Norm2()) {
    // only the transposed constraint-Jacobian-vector product is required
    
    // Compute (dc/dx)^T * u and store in v
    v.design_.EqualsTransCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                             u.dual_);  
    // build RHS for adjoint problem
    pde_work1_->EqualsTransCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                               u.dual_);
    *pde_work1_ *= -1.0;
    // Solve adjoint variable
    *lambda_adj_ = 0.0;
    double rel_tol = 1e-8; //1e-6; //
    lambda_adj_->EqualsAdjointSolution(*eval_at_design_, *eval_at_state_,
                                       *pde_work1_, rel_tol);
    // Apply lambda_adj_ to design-part of the Jacobian
    design_work1_->EqualsTransJacobianTimesVector(*eval_at_design_,
                                                  *eval_at_state_,
                                                  *lambda_adj_);
    v.design_ += *design_work1_;
    v.dual_ = 0.0;

    return;
  }

  // Full KKT-matrix-vector product...
  
  // First part: state-independent part of the KKT-matrix-vector product
  // NOTE: we can do this at the very beginning, because this
  // part of the product does not depend on w_adj_ or lambda_adj_
  
  // Compute (d^2 L/dx^2)*u.design_ and store in v.design_
  double epsilon_fd = CalcEpsilon(design_norm_, u.design_.Norm2());
 //double epsilon_fd = 0.1*std::max(design_norm_, u.design_.Norm2());
  *outstream_ << "ReducedKKTProduct: eps for design = " << epsilon_fd
              << endl;
  double eps_r = 1.0/epsilon_fd;
  pert_design_->EqualsAXPlusBY(1.0, *eval_at_design_, epsilon_fd, u.design_);

  v.design_.EqualsObjectiveGradient(*pert_design_, *eval_at_state_);
  design_work1_->EqualsTransJacobianTimesVector(*pert_design_, 
                                                *eval_at_state_,
                                                *eval_at_adjoint_);
  v.design_ += *design_work1_;
  design_work1_->EqualsTransCnstrJacTimesVector(*pert_design_, *eval_at_state_,
                                                *eval_at_dual_);
  v.design_ += *design_work1_;
  v.design_ *= grad_scale_;
  v.design_.EqualsAXPlusBY(eps_r, v.design_, -eps_r, *reduced_grad_);

  // Compute (dc/dx)^T * u.dual_ and add to v.design_
  design_work1_->EqualsTransCnstrJacTimesVector(*eval_at_design_,
                                                *eval_at_state_, u.dual_);
  *design_work1_ *= grad_scale_;
  v.design_ += *design_work1_;

  // Compute (dc/dx) * u.design_ and store in v.dual_
  v.dual_.EqualsCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                    u.design_);  

  // build RHS for first adjoint system, and solve for adjoint w (stored
  // in *w_adj)
  pde_work1_->EqualsJacobianTimesVector(*eval_at_design_, *eval_at_state_,
                                        u.design_);
  *pde_work1_ *= -1.0;
  *w_adj_ = 0.0;
  int iters;
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# Forward adjoint problem in reduced-Hessian product" << endl;
  // In the notation of the inexact-Hessian paper, product_fac = (1.0 - nu)/ C
  // where C is the unknown bound
  double rel_tol = product_tol_*product_fac_/pde_work1_->Norm2();
#if 0
  if (bound_type_ == "independent")
    rel_tol /= (2.0*dpsidx_norm_);
  else if (bound_type_ == "common")
    rel_tol /= (sqrt(2.0)*sens_norm_);
#endif
  rel_tol = 1e-8;//1e-6; //
  w_adj_->EqualsLinearizedSolution(*eval_at_design_, *eval_at_state_,
                                   *pde_work1_, rel_tol);
  
#if 0
  // tried using just preconditioner to define an approximate Hessian; works
  // well for early outer iterations, but slow to converge later on.
  primal_precond_->operator()(*pde_work1_, *w_adj_);
#endif
  //cout << "\tforward adjoint (w) = " << w_adj_->Norm2() << endl;
  
  // finish the dual part of the KKT-matrix-vector product; add (dc/du)*w_adj to
  // v.dual_
  dual_work_->EqualsCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                        *w_adj_);
  v.dual_ += *dual_work_;
  v.dual_ *= ceq_scale_;

  // build RHS for second adjoint system (lambda_adj_)...
  
  // first part of LHS: perturb eval_at_design_, evaluate the adjoint
  // equation residual, finally take difference
  pde_work1_->EqualsObjectiveGradient(*pert_design_, *eval_at_state_);
  pde_work2_->EqualsTransJacobianTimesVector(*pert_design_, *eval_at_state_,
                                             *eval_at_adjoint_);
  *pde_work1_ += *pde_work2_;
  pde_work2_->EqualsTransCnstrJacTimesVector(*pert_design_, *eval_at_state_,
                                             *eval_at_dual_);
  *pde_work1_ += *pde_work2_;
  // at this point *pde_work1 should contain the perturbed adjoint
  // residual, so take difference with unperturbed adjoint residual
  // NOTE: signs on eps_r move vector to RHS for subsequent solve
  pde_work1_->EqualsAXPlusBY(-eps_r, *pde_work1_, eps_r, *adjoint_res_);

  // second part of LHS: perturb eval_at_state_, evaluate the adjoint
  // equation residual, finally take difference
  epsilon_fd = CalcEpsilon(state_norm_, w_adj_->Norm2());
  //epsilon_fd = 0.0001*std::max(state_norm_, w_adj_->Norm2()); //CalcEpsilon(state_norm_, w_adj_->Norm2());
  *outstream_ << "ReducedHessianProduct: eps for state = " << epsilon_fd
              << endl;
  eps_r = 1.0/epsilon_fd;
  pde_work2_->EqualsAXPlusBY(1.0, *eval_at_state_, epsilon_fd, *w_adj_);
  pde_work3_->EqualsObjectiveGradient(*eval_at_design_, *pde_work2_);
  //lambda_adj_->EqualsAXPlusBY(-eps_r, *pde_work3_, eps_r, *adjoint_res_);
  *lambda_adj_ = *pde_work3_;
  pde_work3_->EqualsTransJacobianTimesVector(*eval_at_design_, *pde_work2_,
                                             *eval_at_adjoint_);
  //lambda_adj_->EqualsAXPlusBY(1.0, *lambda_adj_, -eps_r, *pde_work3_);
  *lambda_adj_ += *pde_work3_;
  pde_work3_->EqualsTransCnstrJacTimesVector(*eval_at_design_, *pde_work2_,
                                             *eval_at_dual_);
  //lambda_adj_->EqualsAXPlusBY(1.0, *lambda_adj_, -eps_r, *pde_work3_);
  *lambda_adj_ += *pde_work3_;

  // TEMP...(why?)
  lambda_adj_->EqualsAXPlusBY(-eps_r, *lambda_adj_, eps_r, *adjoint_res_);
  
  *pde_work1_ += *lambda_adj_;

  // third part of LHS: (dc/du)*u.dual_
  lambda_adj_->EqualsTransCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                              u.dual_);
  *pde_work1_ -= *lambda_adj_;  
  *lambda_adj_ = 0.0;
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# Reverse adjoint problem in reduced-Hessian product" << endl;

  //cout << "ReducedHessianProduct::operator(): "
  //     << "norm of reverse adjoint rhs vector = " << pde_work1_->Norm2() << endl;
  rel_tol = product_tol_*product_fac_/pde_work1_->Norm2();
#if 0
  if (bound_type_ == "independent")
    rel_tol /= (2.0*dudx_norm_);
  else if (bound_type_ == "common")
    rel_tol /= (sqrt(2.0)*sens_norm_);
#endif
  rel_tol = 1e-8;//1e-6; //
  lambda_adj_->EqualsAdjointSolution(*eval_at_design_, *eval_at_state_,
                                     *pde_work1_, rel_tol);
  
#if 0
  adjoint_precond_->operator()(*pde_work1_, *lambda_adj_);
#endif
  //cout << "\treverse adjoint (lambda) = " << lambda_adj_->Norm2() << endl;
  
  // Assemble remaining pieces of the KKT-matrix-vector product
  
  // Apply lambda_adj_ to design-part of the Jacobian
  design_work1_->EqualsTransJacobianTimesVector(*eval_at_design_,
                                                *eval_at_state_,
                                                *lambda_adj_);
  *design_work1_ *= grad_scale_;
  v.design_ += *design_work1_;

  // Apply w_adj_ to the cross-derivative part of Hessian
  // NOTE: pde_work2_ still holds the perturbed state
  design_work1_->EqualsObjectiveGradient(*eval_at_design_, *pde_work2_);
  pert_design_->EqualsTransJacobianTimesVector(*eval_at_design_,
                                               *pde_work2_,
                                               *eval_at_adjoint_);
  *design_work1_ += *pert_design_;
  pert_design_->EqualsTransCnstrJacTimesVector(*eval_at_design_, *pde_work2_,
                                               *eval_at_dual_);
  *design_work1_ += *pert_design_;
  *design_work1_ *= grad_scale_;
  design_work1_->EqualsAXPlusBY(eps_r, *design_work1_, -eps_r,
                                *reduced_grad_);
  v.design_ += *design_work1_;
#endif
  
  // update the quasi-Newton method if necessary
  if (quasi_newton_ != NULL) {
    quasi_newton_->AddCorrection(u.design_, v.design_);
  }

  // add globalization if necessary
  if (lambda_ > kEpsilon) {
    v.design_.EqualsAXPlusBY(1.0, v.design_, lambda_*scale_, u.design_);
  }
  
#if 0
  cout << "End of ReducedKKTProduct::operator(): " << endl;
  cout << "\tNorm of u = " << u.Norm2() << endl;
  cout << "\tNorm of v = " << v.Norm2() << endl;
#endif
}
// ==============================================================================
void ReducedKKTProduct::AugmentedProduct(const ReducedKKTVector & u,
                                         ReducedKKTVector & v) {
  v = 0.0;
  if (1 > 0) { //u.dual_.Norm2() > kEpsilon*u.design_.Norm2()) {
    // Compute v.design_ = A^{T} u.dual_
    
    // Compute (dc/dx)^T * u.dual_ and add to v.design_
    design_work1_->EqualsTransCnstrJacTimesVector(*eval_at_design_,
                                                  *eval_at_state_, u.dual_);
    v.design_ += *design_work1_;
    // build RHS for adjoint system and solve (adjoint stored in *adj_)
    pde_work1_->EqualsTransCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                               u.dual_);
    *pde_work1_ *= -1.0;    
    *lambda_adj_ = 0.0;
    int iters;
    *outstream_ << "#---------------------------------------------------"
                << endl;
    *outstream_ << "# Reverse adjoint problem in Augmented product" << endl;
    double rel_tol = 1e-8;
    lambda_adj_->EqualsAdjointSolution(*eval_at_design_, *eval_at_state_,
                                       *pde_work1_, rel_tol);
    // Apply lambda_adj_ to design-part of the Jacobian
    design_work1_->EqualsTransJacobianTimesVector(*eval_at_design_,
                                                  *eval_at_state_,
                                                  *lambda_adj_);
    v.design_ += *design_work1_;
  }
  if (1 > 0) { //u.design_.Norm2() > kEpsilon*u.dual_.Norm2()) {
    // Compute v.dual_ = A* u.design_
    
    // Compute (dc/dx) * u.design_ and store in v.dual_
    v.dual_.EqualsCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                      u.design_);
    // build RHS for adjoint system and solve (adjoint stored in *adj_)
    pde_work1_->EqualsJacobianTimesVector(*eval_at_design_, *eval_at_state_,
                                          u.design_);
    *pde_work1_ *= -1.0;
    *w_adj_ = 0.0;
    int iters;
    *outstream_ << "#---------------------------------------------------"
                << endl;
    *outstream_ << "# Forward adjoint problem in Augmented product" << endl;
    double rel_tol = 1e-8;
    w_adj_->EqualsLinearizedSolution(*eval_at_design_, *eval_at_state_,
                                     *pde_work1_, rel_tol);
    // finish the dual part of the KKT-matrix-vector product; add (dc/du)*adj_ to
    // v.dual_
    dual_work_->EqualsCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                          *w_adj_);
    v.dual_ += *dual_work_;
  }
  v.design_ += u.design_;
}
// ==============================================================================
#if 1
void ReducedKKTProduct::QuasiNewtonProduct(const ReducedKKTVector & u,
                                           ReducedKKTVector & v) {
  v = 0.0;
  // add the Quasi-Newton contribution
  quasi_newton_->ApplyHessianApprox(u.design_, v.design_);
  //v.design_ = u.design_;

  if (u.dual_.Norm2() > kEpsilon*u.design_.Norm2()) {
    // Compute v.design_ = A^{T} u.dual_
    
    // Compute (dc/dx)^T * u.dual_ and add to v.design_
    design_work1_->EqualsTransCnstrJacTimesVector(*eval_at_design_,
                                                  *eval_at_state_, u.dual_);
    v.design_ += *design_work1_;
    // build RHS for adjoint system and solve (adjoint stored in *adj_)
    pde_work1_->EqualsTransCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                               u.dual_);
    *pde_work1_ *= -1.0;    
    *lambda_adj_ = 0.0;
    int iters;
    *outstream_ << "#---------------------------------------------------"
                << endl;
    *outstream_ << "# Reverse adjoint problem in ReducedKKTProduct::QuasiNewtonProduct" << endl;
    double rel_tol = 1e-8;
    lambda_adj_->EqualsAdjointSolution(*eval_at_design_, *eval_at_state_,
                                       *pde_work1_, rel_tol);
    // Apply lambda_adj_ to design-part of the Jacobian
    design_work1_->EqualsTransJacobianTimesVector(*eval_at_design_,
                                                  *eval_at_state_,
                                                  *lambda_adj_);
    v.design_ += *design_work1_;
  }
  if (u.design_.Norm2() > kEpsilon*u.dual_.Norm2()) {
    // Compute v.dual_ = A* u.design_
    
    // Compute (dc/dx) * u.design_ and store in v.dual_
    v.dual_.EqualsCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                      u.design_);
    // build RHS for adjoint system and solve (adjoint stored in *adj_)
    pde_work1_->EqualsJacobianTimesVector(*eval_at_design_, *eval_at_state_,
                                          u.design_);
    *pde_work1_ *= -1.0;
    *w_adj_ = 0.0;
    int iters;
    *outstream_ << "#---------------------------------------------------"
                << endl;
    *outstream_ << "# Forward adjoint problem in ReducedKKTProduct::QuasiNewtonProduct" << endl;
    double rel_tol = 1e-8;
    w_adj_->EqualsLinearizedSolution(*eval_at_design_, *eval_at_state_,
                                     *pde_work1_, rel_tol);
    // finish the dual part of the KKT-matrix-vector product; add (dc/du)*adj_ to
    // v.dual_
    dual_work_->EqualsCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                          *w_adj_);
    v.dual_ += *dual_work_;
  }
}
#endif
// ==============================================================================
LagrangianHessianProduct::LagrangianHessianProduct(ostream& outstream) {
  outstream_ = &outstream;
}
// ==============================================================================
LagrangianHessianProduct::LagrangianHessianProduct(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    const PDEVector & eval_at_adjoint, DesignVector & reduced_grad,
    const PDEVector & adjoint_res, DesignVector & design_work1,
    DesignVector & design_work2, PDEVector & pde_work1,
    PDEVector & pde_work2, PDEVector & pde_work3,
    PDEVector & pde_work4, PDEVector & pde_work5,
    const double & product_fac, ostream & outstream) {
  eval_at_design_ = &(eval_at_kkt.design_);
  eval_at_dual_ = &(eval_at_kkt.dual_);
  design_norm_ = eval_at_design_->Norm2();
  eval_at_state_ = &eval_at_state;
  state_norm_ = eval_at_state_->Norm2();
  eval_at_adjoint_ = &eval_at_adjoint;
  adjoint_res_ = &adjoint_res;
  reduced_grad_ = &(reduced_grad);
  pde_work1_ = &pde_work1;
  pde_work2_ = &pde_work2;
  pde_work3_ = &pde_work3;
  forward_adj_ = &pde_work4;
  reverse_adj_ = &pde_work5;
  design_work1_ = &design_work1;
  pert_design_  = &design_work2;
  product_fac_ = product_fac;
  product_tol_ = 1.0;
  outstream_ = &outstream;
  quasi_newton_ = NULL; // this is defined by a set_quasi_newton (if necessary)
}
// ==============================================================================
void LagrangianHessianProduct::Initialize(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    const PDEVector & eval_at_adjoint, DesignVector & reduced_grad, 
    const PDEVector & adjoint_res, std::vector<DesignVector>& design_work,
    std::vector<PDEVector>& pde_work, const ptree& prod_param,
    ostream & outstream) {
  if ( (design_work.size() < 2) || (pde_work.size() < 5) ) {
    cerr << "ReducedHessianProduct::Initialize():"
         << "one or more of the work arrays has too few elements" << endl;
    throw(-1);
  }
  eval_at_design_ = &(eval_at_kkt.design_);
  eval_at_dual_ = &(eval_at_kkt.dual_);
  design_norm_ = eval_at_design_->Norm2();
  eval_at_state_ = &eval_at_state;
  state_norm_ = eval_at_state_->Norm2();
  eval_at_adjoint_ = &eval_at_adjoint;
  adjoint_res_ = &adjoint_res;
  reduced_grad_ = &reduced_grad;
  pde_work1_ = &pde_work[0];
  pde_work2_ = &pde_work[1];
  pde_work3_ = &pde_work[2];
  forward_adj_ = &pde_work[3];
  reverse_adj_ = &pde_work[4];
  design_work1_ = &design_work[0];
  pert_design_  = &design_work[1];

  product_fac_ = prod_param.get<double>("product_fac");
  product_tol_ = 1.0;
  outstream_ = &outstream;
  
  quasi_newton_ = NULL; // this is defined by set_quasi_newton (if necessary)

  // second-order adjoint adaptive tolerance selection would go here...
}
// ==============================================================================
LagrangianHessianProduct::~LagrangianHessianProduct() {
}
// ==============================================================================
void LagrangianHessianProduct::MemoryRequired(ptree& num_required) {
  num_required.put("design", 0);
  num_required.put("state", 0);
  num_required.put("dual", 0);
}
// ==============================================================================
void LagrangianHessianProduct::set_quasi_newton(QuasiNewton * quasi_newton) {
  quasi_newton_ = quasi_newton;
}
// ==============================================================================
void LagrangianHessianProduct::operator()(const DesignVector & u,
                                          DesignVector & v) {
  // First part: state-independent part of the Hessian-vector product
  // NOTE: we can do this at the very beginning, because this
  // part of the product does not depend on w_adj_ or lambda_adj_
  
  // Compute (d^2 L/dx^2)*u and store in v
  double epsilon_fd = CalcEpsilon(design_norm_, u.Norm2());
  *outstream_ << "LagrangianHessianProduct: eps for design = " << epsilon_fd
              << endl;
  double eps_r = 1.0/epsilon_fd;
  pert_design_->EqualsAXPlusBY(1.0, *eval_at_design_, epsilon_fd, u);

  v.EqualsObjectiveGradient(*pert_design_, *eval_at_state_);
  design_work1_->EqualsTransJacobianTimesVector(*pert_design_, 
                                                *eval_at_state_,
                                                *eval_at_adjoint_);
  v += *design_work1_;
  design_work1_->EqualsTransCnstrJacTimesVector(*pert_design_, *eval_at_state_,
                                                *eval_at_dual_);
  v += *design_work1_;
  v.EqualsAXPlusBY(eps_r, v, -eps_r, *reduced_grad_);
  
  // build RHS for first adjoint system, and solve for forward_adj_
  pde_work1_->EqualsJacobianTimesVector(*eval_at_design_, *eval_at_state_, u);
  *pde_work1_ *= -1.0;
  *forward_adj_ = 0.0;
  int iters;
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# Forward adjoint problem in reduced-Hessian product" << endl;
  double rel_tol = 1e-8;//1e-6; //
  forward_adj_->EqualsLinearizedSolution(*eval_at_design_, *eval_at_state_,
                                         *pde_work1_, rel_tol);

  // build RHS for second adjoint system (reverse_adj_)...
  
  // first part of LHS: perturb eval_at_design_, evaluate the adjoint
  // equation residual, finally take difference
  pde_work1_->EqualsObjectiveGradient(*pert_design_, *eval_at_state_);
  pde_work2_->EqualsTransJacobianTimesVector(*pert_design_, *eval_at_state_,
                                             *eval_at_adjoint_);
  *pde_work1_ += *pde_work2_;
  pde_work2_->EqualsTransCnstrJacTimesVector(*pert_design_, *eval_at_state_,
                                             *eval_at_dual_);
  *pde_work1_ += *pde_work2_;
  // at this point *pde_work1 should contain the perturbed adjoint
  // residual, so take difference with unperturbed adjoint residual
  // NOTE: signs on eps_r move vector to RHS for subsequent solve
  pde_work1_->EqualsAXPlusBY(-eps_r, *pde_work1_, eps_r, *adjoint_res_);

  // second part of LHS: perturb eval_at_state_, evaluate the adjoint
  // equation residual, finally take difference
  epsilon_fd = CalcEpsilon(state_norm_, forward_adj_->Norm2());
  *outstream_ << "ReducedHessianProduct: eps for state = " << epsilon_fd
              << endl;
  eps_r = 1.0/epsilon_fd;
  pde_work2_->EqualsAXPlusBY(1.0, *eval_at_state_, epsilon_fd, *forward_adj_);
  pde_work3_->EqualsObjectiveGradient(*eval_at_design_, *pde_work2_);
  *reverse_adj_ = *pde_work3_;
  pde_work3_->EqualsTransJacobianTimesVector(*eval_at_design_, *pde_work2_,
                                             *eval_at_adjoint_);
  *reverse_adj_ += *pde_work3_;
  pde_work3_->EqualsTransCnstrJacTimesVector(*eval_at_design_, *pde_work2_,
                                             *eval_at_dual_);
  *reverse_adj_ += *pde_work3_;

  // TEMP...(why?)
  reverse_adj_->EqualsAXPlusBY(-eps_r, *reverse_adj_, eps_r, *adjoint_res_);  
  *pde_work1_ += *reverse_adj_;

  *reverse_adj_ = 0.0;
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# Reverse adjoint problem in reduced-Hessian product" << endl;
  rel_tol = 1e-8;//1e-6; //
  reverse_adj_->EqualsAdjointSolution(*eval_at_design_, *eval_at_state_,
                                     *pde_work1_, rel_tol);
  
  // Assemble remaining pieces of the Hessian-vector product
  
  // Apply reverse_adj_ to design-part of the Jacobian
  design_work1_->EqualsTransJacobianTimesVector(*eval_at_design_,
                                                *eval_at_state_,
                                                *reverse_adj_);
  v += *design_work1_;

  // Apply w_adj_ to the cross-derivative part of Hessian
  // NOTE: pde_work2_ still holds the perturbed state
  design_work1_->EqualsObjectiveGradient(*eval_at_design_, *pde_work2_);
  pert_design_->EqualsTransJacobianTimesVector(*eval_at_design_,
                                               *pde_work2_,
                                               *eval_at_adjoint_);
  *design_work1_ += *pert_design_;
  pert_design_->EqualsTransCnstrJacTimesVector(*eval_at_design_, *pde_work2_,
                                               *eval_at_dual_);
  *design_work1_ += *pert_design_;
  design_work1_->EqualsAXPlusBY(eps_r, *design_work1_, -eps_r,
                                *reduced_grad_);
  v += *design_work1_;

  // update the quasi-Newton method if necessary
  if (quasi_newton_ != NULL) {
    quasi_newton_->AddCorrection(u, v);
  }

#if 0
  cout << "End of LagrangianHessianProduct::operator(): " << endl;
  cout << "\tNorm of u = " << u.Norm2() << endl;
  cout << "\tNorm of v = " << v.Norm2() << endl;
#endif
}
// ==============================================================================
NullSpaceLagrangianProduct::NullSpaceLagrangianProduct(ostream& outstream) {
  outstream_ = &outstream;
}
// ==============================================================================
NullSpaceLagrangianProduct::NullSpaceLagrangianProduct(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    const PDEVector & eval_at_adjoint, ReducedKKTVector & reduced_opt,
    const PDEVector & adjoint_res, DesignVector & design_work1,
    DesignVector & design_work2, PDEVector & pde_work1,
    PDEVector & pde_work2, PDEVector & pde_work3,
    PDEVector & pde_work4, PDEVector & pde_work5, DualVector & dual_work1,
    DualVector & dual_work2, const double & mu,
    const double & product_fac, ostream & outstream) {
  eval_at_design_ = &(eval_at_kkt.design_);
  eval_at_dual_ = &(eval_at_kkt.dual_);
  design_norm_ = eval_at_design_->Norm2();
  eval_at_state_ = &eval_at_state;
  state_norm_ = eval_at_state_->Norm2();
  eval_at_adjoint_ = &eval_at_adjoint;
  adjoint_res_ = &adjoint_res;
  reduced_grad_ = &(reduced_opt.design_);
  ceq_ = &(reduced_opt.dual_);
  pde_work1_ = &pde_work1;
  pde_work2_ = &pde_work2;
  pde_work3_ = &pde_work3;
  forward_adj_ = &pde_work4;
  reverse_adj_ = &pde_work5;
  design_work1_ = &design_work1;
  pert_design_  = &design_work2;
  dual_work_ = &dual_work1;
  Au_ = &dual_work2;
  mu_ = mu;
  product_fac_ = product_fac;
  product_tol_ = 1.0;
  outstream_ = &outstream;
  quasi_newton_ = NULL; // this is defined by a set_quasi_newton (if necessary)
}
// ==============================================================================
void NullSpaceLagrangianProduct::Initialize(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    const PDEVector & eval_at_adjoint, ReducedKKTVector & reduced_opt, 
    const PDEVector & adjoint_res, std::vector<DesignVector>& design_work,
    std::vector<PDEVector>& pde_work, std::vector<DualVector>& dual_work,
    const ptree& prod_param, ostream & outstream) {
  if ( (design_work.size() < 2) || (pde_work.size() < 5) ||
       (dual_work.size() < 2) ) {
    cerr << "NullSpaceLagrangianProduct::Initialize():"
         << "one or more of the work arrays has too few elements" << endl;
    throw(-1);
  }
  eval_at_design_ = &(eval_at_kkt.design_);
  eval_at_dual_ = &(eval_at_kkt.dual_);
  design_norm_ = eval_at_design_->Norm2();
  eval_at_state_ = &eval_at_state;
  state_norm_ = eval_at_state_->Norm2();
  eval_at_adjoint_ = &eval_at_adjoint;
  adjoint_res_ = &adjoint_res;
  reduced_grad_ = &(reduced_opt.design_);
  ceq_ = &(reduced_opt.dual_);
  pde_work1_ = &pde_work[0];
  pde_work2_ = &pde_work[1];
  pde_work3_ = &pde_work[2];
  forward_adj_ = &pde_work[3];
  reverse_adj_ = &pde_work[4];
  design_work1_ = &design_work[0];
  pert_design_  = &design_work[1];
  dual_work_ = &dual_work[0];
  Au_ = &dual_work[1];
  mu_ = prod_param.get<double>("mu", 0.0);
  product_fac_ = prod_param.get<double>("product_fac");
  product_tol_ = 1.0;
  outstream_ = &outstream;
  quasi_newton_ = NULL; // this is defined by set_quasi_newton (if necessary)
}
// ==============================================================================
NullSpaceLagrangianProduct::~NullSpaceLagrangianProduct() {
}
// ==============================================================================
void NullSpaceLagrangianProduct::MemoryRequired(ptree& num_required) {
  num_required.put("design", 0);
  num_required.put("state", 0);
  num_required.put("dual", 0);
}
// ==============================================================================
void NullSpaceLagrangianProduct::set_quasi_newton(QuasiNewton * quasi_newton) {
  quasi_newton_ = quasi_newton;
}
// ==============================================================================
void NullSpaceLagrangianProduct::operator()(const DesignVector & u,
                                            DesignVector & v) {  
  // First part: state-independent part of the product
  // NOTE: we can do this at the very beginning, because this
  // part of the product does not depend on forward_adj_ or reverse_adj_
  
  // Compute (d^2 L/dx^2)*u.design_ and store in v.design_
  double epsilon_fd = CalcEpsilon(design_norm_, u.Norm2());
  *outstream_ << "NullSpaceLagrangianProduct: eps for design = " << epsilon_fd
              << endl;
  double eps_r = 1.0/epsilon_fd;
  pert_design_->EqualsAXPlusBY(1.0, *eval_at_design_, epsilon_fd, u);

  v.EqualsObjectiveGradient(*pert_design_, *eval_at_state_);
  design_work1_->EqualsTransJacobianTimesVector(*pert_design_, 
                                                *eval_at_state_,
                                                *eval_at_adjoint_);
  v += *design_work1_;
  design_work1_->EqualsTransCnstrJacTimesVector(*pert_design_, *eval_at_state_,
                                                *eval_at_dual_);
  v += *design_work1_;
  v.EqualsAXPlusBY(eps_r, v, -eps_r, *reduced_grad_);

  // Compute (dc/dx) * u and store in Au_
  Au_->EqualsCnstrJacTimesVector(*eval_at_design_, *eval_at_state_, u);  

  // build RHS for first adjoint system, and solve for adjoint w (stored
  // in *forward_adj)
  pde_work1_->EqualsJacobianTimesVector(*eval_at_design_, *eval_at_state_, u);
  *pde_work1_ *= -1.0;
  *forward_adj_ = 0.0;
  int iters;
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# Forward adjoint problem in Null-Space Aug. Lag. product" << endl;
  double rel_tol = 1e-8;
  forward_adj_->EqualsLinearizedSolution(*eval_at_design_, *eval_at_state_,
                                   *pde_work1_, rel_tol);
  
  // finish the dual part of the KKT-matrix-vector product;
  // add (dc/du)*forward_adj to Au_
  dual_work_->EqualsCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                        *forward_adj_);
  *Au_ += *dual_work_;

  // scale Au_ by penalty parameter
  *Au_ *= mu_;

  // Compute (dc/dx)^T * Au_ and add to v
  design_work1_->EqualsTransCnstrJacTimesVector(*eval_at_design_,
                                                *eval_at_state_, *Au_);
  v += *design_work1_;

  // build RHS for second adjoint system (reverse_adj_)...
  
  // first part of LHS: perturb eval_at_design_, evaluate the adjoint
  // equation residual, finally take difference
  pde_work1_->EqualsObjectiveGradient(*pert_design_, *eval_at_state_);
  pde_work2_->EqualsTransJacobianTimesVector(*pert_design_, *eval_at_state_,
                                             *eval_at_adjoint_);
  *pde_work1_ += *pde_work2_;
  pde_work2_->EqualsTransCnstrJacTimesVector(*pert_design_, *eval_at_state_,
                                             *eval_at_dual_);
  *pde_work1_ += *pde_work2_;
  // at this point *pde_work1 should contain the perturbed adjoint
  // residual, so take difference with unperturbed adjoint residual
  // NOTE: signs on eps_r move vector to RHS for subsequent solve
  pde_work1_->EqualsAXPlusBY(-eps_r, *pde_work1_, eps_r, *adjoint_res_);

  // second part of LHS: perturb eval_at_state_, evaluate the adjoint
  // equation residual, finally take difference
  epsilon_fd = CalcEpsilon(state_norm_, forward_adj_->Norm2());
  *outstream_ << "ReducedHessianProduct: eps for state = " << epsilon_fd
              << endl;
  eps_r = 1.0/epsilon_fd;
  pde_work2_->EqualsAXPlusBY(1.0, *eval_at_state_, epsilon_fd, *forward_adj_);
  pde_work3_->EqualsObjectiveGradient(*eval_at_design_, *pde_work2_);
  //reverse_adj_->EqualsAXPlusBY(-eps_r, *pde_work3_, eps_r, *adjoint_res_);
  *reverse_adj_ = *pde_work3_;
  pde_work3_->EqualsTransJacobianTimesVector(*eval_at_design_, *pde_work2_,
                                             *eval_at_adjoint_);
  //reverse_adj_->EqualsAXPlusBY(1.0, *reverse_adj_, -eps_r, *pde_work3_);
  *reverse_adj_ += *pde_work3_;
  pde_work3_->EqualsTransCnstrJacTimesVector(*eval_at_design_, *pde_work2_,
                                             *eval_at_dual_);
  //reverse_adj_->EqualsAXPlusBY(1.0, *reverse_adj_, -eps_r, *pde_work3_);
  *reverse_adj_ += *pde_work3_;

  // TEMP...(why?)
  reverse_adj_->EqualsAXPlusBY(-eps_r, *reverse_adj_, eps_r, *adjoint_res_);
  
  *pde_work1_ += *reverse_adj_;

  // third part of LHS: (dc/du)^T * Au_
  reverse_adj_->EqualsTransCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                               *Au_);
  *pde_work1_ -= *reverse_adj_;
  *reverse_adj_ = 0.0;
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# Reverse adjoint problem in Null-Space Aug. Lag. product" << endl;
  rel_tol = 1e-8;//1e-6; //
  reverse_adj_->EqualsAdjointSolution(*eval_at_design_, *eval_at_state_,
                                     *pde_work1_, rel_tol);
  
  // Assemble remaining pieces of the product
  
  // Apply reverse_adj_ to design-part of the Jacobian
  design_work1_->EqualsTransJacobianTimesVector(*eval_at_design_,
                                                *eval_at_state_,
                                                *reverse_adj_);
  v += *design_work1_;

  // Apply forward_adj_ to the cross-derivative part of Hessian
  // NOTE: pde_work2_ still holds the perturbed state
  design_work1_->EqualsObjectiveGradient(*eval_at_design_, *pde_work2_);
  pert_design_->EqualsTransJacobianTimesVector(*eval_at_design_,
                                               *pde_work2_,
                                               *eval_at_adjoint_);
  *design_work1_ += *pert_design_;
  pert_design_->EqualsTransCnstrJacTimesVector(*eval_at_design_, *pde_work2_,
                                               *eval_at_dual_);
  *design_work1_ += *pert_design_;
  design_work1_->EqualsAXPlusBY(eps_r, *design_work1_, -eps_r,
                                *reduced_grad_);
  v += *design_work1_;

  // update the quasi-Newton method if necessary
  if (quasi_newton_ != NULL) {
    quasi_newton_->AddCorrection(u, v);
  }
  
#if 0
  cout << "End of NullSpaceLagrangianProduct::operator(): " << endl;
  cout << "\tNorm of u = " << u.Norm2() << endl;
  cout << "\tNorm of v = " << v.Norm2() << endl;
#endif
}
// ==============================================================================
ReducedAugmentedProduct::ReducedAugmentedProduct(ostream& outstream) {
  outstream_ = &outstream;
}
// ==============================================================================
ReducedAugmentedProduct::ReducedAugmentedProduct(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    DesignVector & design_work1, PDEVector & pde_work1, PDEVector & pde_work2,
    DualVector & dual_work1, ostream & outstream) {
  solve_ceq_ = true;
  approx_ = false;
  diag_ = 0.0;
  eval_at_design_ = &(eval_at_kkt.design_);
  eval_at_dual_ = &(eval_at_kkt.dual_);
  eval_at_state_ = &eval_at_state;
  pde_work1_ = &pde_work1;
  adj_ = &pde_work2;
  design_work1_ = &design_work1;
  dual_work_ = &dual_work1;
  outstream_ = &outstream;
  if (approx_) {
    primal_precond_.reset(new StatePreconditioner(*eval_at_design_,
                                                  *eval_at_state_));
    adjoint_precond_.reset(new AdjointPreconditioner(*eval_at_design_,
                                                     *eval_at_state_));
  }
}
// ==============================================================================
ReducedAugmentedProduct::~ReducedAugmentedProduct() {
}
// ==============================================================================
void ReducedAugmentedProduct::MemoryRequired(ptree& num_required) {
  num_required.put("design", 0);
  num_required.put("state", 0);
  num_required.put("dual", 0);
}
// ==============================================================================
void ReducedAugmentedProduct::Initialize(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    std::vector<DesignVector>& design_work, std::vector<PDEVector>& pde_work,
    std::vector<DualVector>& dual_work, const ptree& prod_param,
    ostream & outstream) {
  solve_ceq_ = prod_param.get<bool>("solve_ceq", true);
  approx_ = prod_param.get<bool>("approx", false);
  diag_ = prod_param.get<double>("diag", 0.0);
  eval_at_design_ = &(eval_at_kkt.design_);
  eval_at_dual_ = &(eval_at_kkt.dual_);
  eval_at_state_ = &eval_at_state;
  pde_work1_ = &pde_work[0];
  adj_ = &pde_work[1];
  design_work1_ = &design_work[0];
  dual_work_ = &dual_work[0];
  outstream_ = &outstream;
  if (approx_) {
    primal_precond_.reset(new StatePreconditioner(*eval_at_design_,
                                                  *eval_at_state_));
    adjoint_precond_.reset(new AdjointPreconditioner(*eval_at_design_,
                                                     *eval_at_state_));
  }
}
// ==============================================================================
void ReducedAugmentedProduct::Initialize(const ptree& prod_param) {
  diag_ = prod_param.get<double>("diag", 0.0);
}
// ==============================================================================
void ReducedAugmentedProduct::operator()(const ReducedKKTVector & u,
                                         ReducedKKTVector & v) {
  v = 0.0;  
  if (u.dual_.Norm2() > kEpsilon*u.design_.Norm2()) {
    // Compute v.design_ = A^{T} u.dual_
    
    // Compute (dc/dx)^T * u.dual_ and add to v.design_
    design_work1_->EqualsTransCnstrJacTimesVector(*eval_at_design_,
                                                  *eval_at_state_, u.dual_);
    v.design_ += *design_work1_;
    // build RHS for adjoint system and solve (adjoint stored in *adj_)
    pde_work1_->EqualsTransCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                               u.dual_);
    *pde_work1_ *= -1.0;    
    *adj_ = 0.0;
    //*outstream_ << "#---------------------------------------------------"
    //            << endl;
    //*outstream_ << "# Reverse adjoint problem in Augmented product" << endl;
    if (approx_) {
      adjoint_precond_->operator()(*pde_work1_, *adj_);
    } else {
      double rel_tol = 1e-8;
      adj_->EqualsAdjointSolution(*eval_at_design_, *eval_at_state_,
                                  *pde_work1_, rel_tol);
    }
    // Apply reverse_adj_ to design-part of the Jacobian
    design_work1_->EqualsTransJacobianTimesVector(*eval_at_design_,
                                                  *eval_at_state_,
                                                  *adj_);
    v.design_ += *design_work1_;      
  }
  if (u.design_.Norm2() > kEpsilon*u.dual_.Norm2()) {
    // Compute v.dual_ = A* u.design_
    
    // Compute (dc/dx) * u.design_ and store in v.dual_
    v.dual_.EqualsCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                      u.design_);
    // build RHS for adjoint system and solve (adjoint stored in *adj_)
    pde_work1_->EqualsJacobianTimesVector(*eval_at_design_, *eval_at_state_,
                                          u.design_);
    *pde_work1_ *= -1.0;
    *adj_ = 0.0;
    //*outstream_ << "#---------------------------------------------------"
    //            << endl;
    //*outstream_ << "# Forward adjoint problem in Augmented product" << endl;
    if (approx_) {
      primal_precond_->operator()(*pde_work1_, *adj_);
    } else {
      double rel_tol = 1e-8;
      adj_->EqualsLinearizedSolution(*eval_at_design_, *eval_at_state_,
                                     *pde_work1_, rel_tol);
    }
    // finish the dual part of the KKT-matrix-vector product; add (dc/du)*adj_ to
    // v.dual_
    dual_work_->EqualsCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                          *adj_);
    v.dual_ += *dual_work_;
  }
  if (solve_ceq_) {// solve the constraint problem
    (*design_work1_) = u.design_;
    (*design_work1_) *= (1.0 + diag_);
    v.design_ += *design_work1_;
  } else {// solve the min-norm multiplier problem
    (*dual_work_) = u.dual_;
    (*dual_work_) *= (1.0 + diag_);
    v.dual_ += *dual_work_;    
  }
}
// ==============================================================================
ApproxReducedKKTProduct::ApproxReducedKKTProduct(ostream& outstream) {
  outstream_ = &outstream;
}
// ==============================================================================
ApproxReducedKKTProduct::ApproxReducedKKTProduct(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    const PDEVector & eval_at_adjoint, ReducedKKTVector & reduced_opt, 
    const PDEVector & adjoint_res, std::vector<DesignVector>& design_work,
    std::vector<PDEVector>& pde_work, std::vector<DualVector>& dual_work,
    const ptree& prod_param, ostream & outstream) {
  if ( (design_work.size() < 2) || (pde_work.size() < 5) ||
       (dual_work.size() < 1) ) {
    cerr << "ApproxReducedKKTProduct::Initialize():"
         << "one or more of the work arrays has too few elements" << endl;
    throw(-1);
  }
  eval_at_design_ = &(eval_at_kkt.design_);
  eval_at_dual_ = &(eval_at_kkt.dual_);
  design_norm_ = eval_at_design_->Norm2();
  eval_at_state_ = &eval_at_state;
  state_norm_ = eval_at_state_->Norm2();
  eval_at_adjoint_ = &eval_at_adjoint;
  adjoint_res_ = &adjoint_res;
  reduced_grad_ = &(reduced_opt.design_);
  ceq_ = &(reduced_opt.dual_);
  pde_work1_ = &pde_work[0];
  pde_work2_ = &pde_work[1];
  pde_work3_ = &pde_work[2];
  w_adj_ = &pde_work[3];
  lambda_adj_ = &pde_work[4];
  design_work1_ = &design_work[0];
  pert_design_  = &design_work[1];
  dual_work_ = &dual_work[0];
  
  outstream_ = &outstream;
  lambda_ = prod_param.get<double>("lambda", 0.0);
  scale_ = prod_param.get<double>("scale", 1.0);
  grad_scale_ = 1.0/prod_param.get<double>("grad_scale", 1.0);
  ceq_scale_ = 1.0/prod_param.get<double>("ceq_scale", 1.0);
  *reduced_grad_ *= grad_scale_;
  *ceq_ *= ceq_scale_;

  // define the primal problem preconditioner
  primal_precond_.reset(
      new StatePreconditioner(*eval_at_design_, *eval_at_state_)); 

  // define the adjoint problem preconditioner
  adjoint_precond_.reset(
      new AdjointPreconditioner(*eval_at_design_, *eval_at_state_));
}
// ==============================================================================
ApproxReducedKKTProduct::~ApproxReducedKKTProduct() {
}
// ==============================================================================
void ApproxReducedKKTProduct::MemoryRequired(ptree& num_required) {
  num_required.put("design", 0);
  num_required.put("state", 0);
  num_required.put("dual", 0);
}
// ==============================================================================
void ApproxReducedKKTProduct::Initialize(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    const PDEVector & eval_at_adjoint, ReducedKKTVector & reduced_opt, 
    const PDEVector & adjoint_res, std::vector<DesignVector>& design_work,
    std::vector<PDEVector>& pde_work, std::vector<DualVector>& dual_work,
    const ptree& prod_param, ostream & outstream) {
  if ( (design_work.size() < 2) || (pde_work.size() < 5) ||
       (dual_work.size() < 1) ) {
    cerr << "ApproxReducedKKTProduct::Initialize():"
         << "one or more of the work arrays has too few elements" << endl;
    throw(-1);
  }
  eval_at_design_ = &(eval_at_kkt.design_);
  eval_at_dual_ = &(eval_at_kkt.dual_);
  design_norm_ = eval_at_design_->Norm2();
  eval_at_state_ = &eval_at_state;
  state_norm_ = eval_at_state_->Norm2();
  eval_at_adjoint_ = &eval_at_adjoint;
  adjoint_res_ = &adjoint_res;
  reduced_grad_ = &(reduced_opt.design_);
  ceq_ = &(reduced_opt.dual_);
  pde_work1_ = &pde_work[0];
  pde_work2_ = &pde_work[1];
  pde_work3_ = &pde_work[2];
  w_adj_ = &pde_work[3];
  lambda_adj_ = &pde_work[4];
  design_work1_ = &design_work[0];
  pert_design_  = &design_work[1];
  dual_work_ = &dual_work[0];

  product_fac_ = prod_param.get<double>("product_fac");
  product_tol_ = 1.0;
  outstream_ = &outstream;
  lambda_ = prod_param.get<double>("lambda", 0.0);
  scale_ = prod_param.get<double>("scale", 1.0);
  grad_scale_ = 1.0/prod_param.get<double>("grad_scale", 1.0);
  ceq_scale_ = 1.0/prod_param.get<double>("ceq_scale", 1.0);
  *reduced_grad_ *= grad_scale_;
  *ceq_ *= ceq_scale_;

  // define the primal problem preconditioner
  primal_precond_.reset(
      new StatePreconditioner(*eval_at_design_, *eval_at_state_)); 

  // define the adjoint problem preconditioner
  adjoint_precond_.reset(
      new AdjointPreconditioner(*eval_at_design_, *eval_at_state_));
}
// ==============================================================================
void ApproxReducedKKTProduct::set_lambda(const double & lambda) {
  lambda_ = lambda;
}
// ==============================================================================
void ApproxReducedKKTProduct::operator()(const ReducedKKTVector & u,
                                   ReducedKKTVector & v) {
  //-----------------------------------------------------------------------------
  // Find the FD step size; this is used for all perturbations
  double epsilon_fd = CalcEpsilon(design_norm_, u.design_.Norm2());
  //double epsilon_fd = 0.1*std::max(design_norm_, u.design_.Norm2());  
  *outstream_ << "ApproxReducedKKTProduct: eps for design = " << epsilon_fd
              << endl;
  double eps_r = 1.0/epsilon_fd;  
  
  //-----------------------------------------------------------------------------
  // Find the state perturbation by solving the linearized state equation

  // RHS for first adjoint system
  pde_work1_->EqualsJacobianTimesVector(*eval_at_design_, *eval_at_state_,
                                        u.design_);
  *pde_work1_ *= -1.0;
  *w_adj_ = 0.0;
  int iters;
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# Forward adjoint problem in approx KKT product" << endl;
  primal_precond_->operator()(*pde_work1_, *w_adj_);

  //-----------------------------------------------------------------------------
  // Find the adjoint perturbation by solving the linearized dual equation

  pert_design_->EqualsAXPlusBY(1.0, *eval_at_design_, epsilon_fd, u.design_);
  pde_work3_->EqualsAXPlusBY(1.0, *eval_at_state_, epsilon_fd, *w_adj_);

  // first part of LHS: evaluate the adjoint-equation residual at the perturbed
  // design and state
  pde_work1_->EqualsObjectiveGradient(*pert_design_, *pde_work3_);
  pde_work2_->EqualsTransJacobianTimesVector(*pert_design_, *pde_work3_,
                                             *eval_at_adjoint_);
  *pde_work1_ += *pde_work2_;
  pde_work2_->EqualsTransCnstrJacTimesVector(*pert_design_, *pde_work3_,
                                             *eval_at_dual_);
  *pde_work1_ += *pde_work2_;

  // at this point *pde_work1 should contain the perturbed adjoint
  // residual, so take difference with unperturbed adjoint residual
  // NOTE: signs on eps_r move vector to RHS for subsequent solve
  pde_work1_->EqualsAXPlusBY(-eps_r, *pde_work1_, eps_r, *adjoint_res_);
  //*pde_work1_ *= -eps_r;

  // second part of LHS: (dc/du)*u.dual_
  pde_work2_->EqualsTransCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                             u.dual_);
  *pde_work1_ -= *pde_work2_;

  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# Reverse adjoint problem in reduced-Hessian product" << endl;
  *lambda_adj_ = 0.0;
  adjoint_precond_->operator()(*pde_work1_, *lambda_adj_);

  //-----------------------------------------------------------------------------
  // Evaluate the first-order optimality conditions at perturbed
  // design, state, and adjoint; then take difference with unperturbed conditions

  pde_work2_->EqualsAXPlusBY(1.0, *eval_at_adjoint_, epsilon_fd, *lambda_adj_);
  
  v.design_.EqualsObjectiveGradient(*pert_design_, *pde_work3_);
  design_work1_->EqualsTransJacobianTimesVector(*pert_design_, *pde_work3_,
                                                *pde_work2_);
  v.design_ += *design_work1_;
  design_work1_->EqualsTransCnstrJacTimesVector(*pert_design_, *pde_work3_,
                                                *eval_at_dual_);
  v.design_ += *design_work1_;

  // the dual perturbation does not need a FD
  design_work1_->EqualsTransCnstrJacTimesVector(*eval_at_design_,
                                                *eval_at_state_,
                                                u.dual_);
  *design_work1_ *= epsilon_fd;
  v.design_ += *design_work1_;

  // perform difference
  v.design_ *= grad_scale_;
  v.design_.EqualsAXPlusBY(eps_r, v.design_, -eps_r, *reduced_grad_);

  //-----------------------------------------------------------------------------
  // evaluate dual part of product
  v.dual_.EqualsCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                    u.design_);
  dual_work_->EqualsCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                        *w_adj_);
  v.dual_ += *dual_work_;
  v.dual_ *= ceq_scale_;
}
// ==============================================================================
ReducedKKTPreconditioner::ReducedKKTPreconditioner(
    QuasiNewton * quasi_newton) {
  quasi_newton_ = quasi_newton;
}
// ==============================================================================
void ReducedKKTPreconditioner::MemoryRequired(ptree& num_required) {
  num_required.put("design", 0);
  num_required.put("state", 0);
  num_required.put("dual", 0);
  num_required.put("num_vec", 0);
}
// ==============================================================================
void ReducedKKTPreconditioner::operator()(ReducedKKTVector & u,
                                          ReducedKKTVector & v) {
  quasi_newton_->ApplyInvHessianApprox(u.design_, v.design_);
  v.dual_ = u.dual_;
}
// ==============================================================================
NestedKKTPreconditioner::NestedKKTPreconditioner(
    const int& krylov_size, ostream& outstream) {
  outstream_ = &outstream;
  krylov_size_ = krylov_size;
  mat_vec_.reset(new ApproxReducedKKTProduct(outstream));
  solver_.reset(new MINRESSolver<ReducedKKTVector>());
  solver_->SubspaceSize(krylov_size_);
}
// ==============================================================================
NestedKKTPreconditioner::NestedKKTPreconditioner(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    const PDEVector & eval_at_adjoint, ReducedKKTVector & reduced_opt, 
    const PDEVector & adjoint_res, std::vector<DesignVector>& design_work,
    std::vector<PDEVector>& pde_work, std::vector<DualVector>& dual_work,
    const ptree& prec_param, ostream & outstream) {
  // define the reduced KTT-matrix-vector product
  ptree prod_param;
  mat_vec_.reset(new ApproxReducedKKTProduct(
      eval_at_kkt, eval_at_state, eval_at_adjoint, reduced_opt, adjoint_res,
      design_work, pde_work, dual_work, prod_param, outstream));
      
#if 0  
  mat_vec_ = new ReducedAugmentedProduct(
      eval_at_kkt, eval_at_state, design_work1, pde_work1, pde_work2, dual_work1,
      outstream);
#endif
  outstream_ = &outstream;
}
// ==============================================================================
void NestedKKTPreconditioner::MemoryRequired(ptree& num_required) {
  ptree prod_required, solver_required;
  mat_vec_->MemoryRequired(prod_required);
  solver_->MemoryRequired(solver_required);
  num_required.put("design", prod_required.get<int>("design")
                   + solver_required.get<int>("num_vec"));
  num_required.put("state", prod_required.get<int>("state"));
  num_required.put("dual", prod_required.get<int>("dual")
                   + solver_required.get<int>("num_vec"));
  num_required.put("num_vec", 0);
}
// ==============================================================================
NestedKKTPreconditioner::~NestedKKTPreconditioner() {
}
// ==============================================================================
void NestedKKTPreconditioner::Initialize(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    const PDEVector & eval_at_adjoint, ReducedKKTVector & reduced_opt, 
    const PDEVector & adjoint_res, std::vector<DesignVector>& design_work,
    std::vector<PDEVector>& pde_work, std::vector<DualVector>& dual_work,
    const ptree& prec_param, ostream & outstream) {
  ptree prod_param;
  mat_vec_.reset(new ApproxReducedKKTProduct(
      eval_at_kkt, eval_at_state, eval_at_adjoint, reduced_opt, adjoint_res,
      design_work, pde_work, dual_work, prod_param, outstream));
  outstream_ = &outstream;
}
// ==============================================================================
void NestedKKTPreconditioner::set_diagonal(const double & diag) {
  boost::static_pointer_cast<ApproxReducedKKTProduct>(mat_vec_)->
      set_lambda(diag);
}
// ==============================================================================
void NestedKKTPreconditioner::operator()(ReducedKKTVector & u,
                                         ReducedKKTVector & v) {
  IdentityPreconditioner<ReducedKKTVector> precond; // do-nothing preconditioner
  double tol = 0.1;
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# preconditioner in reduced-KKT-matrix inversion" << endl;
  ptree ptin, ptout;
  ptin.put<double>("tol", tol);
  ptin.put<bool>("check", true); //false);
  solver_->Solve(ptin, u, v, *mat_vec_, precond, ptout, *outstream_);
}
// ==============================================================================
ReducedSchurPreconditioner::ReducedSchurPreconditioner(
    const int& krylov_size, ostream& outstream) {
  outstream_ = &outstream;
  krylov_size_ = krylov_size;
  jac_prod_.reset(new ConstraintJacobianProduct(outstream));
  solver_.reset(new FGMRESSolver<DesignVector>());
  solver_->SubspaceSize(krylov_size_);
}
// ==============================================================================
ReducedSchurPreconditioner::ReducedSchurPreconditioner(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    DesignVector & design_work1, DesignVector & design_work2,
    DesignVector & design_work3, PDEVector & pde_work1,
    PDEVector & pde_work2, DualVector & dual_work1, const int & krylov_size,
    ostream & outstream) {
  design_work1_ = &design_work1;
  design_work2_ = &design_work2;
  diag_ = 0.0;
  outstream_ = &outstream;
  krylov_size_ = krylov_size;
  
  // define the Constraint Jacobian Product
  jac_prod_.reset();
  jac_prod_.reset(new ConstraintJacobianProduct(
      eval_at_kkt.design_, eval_at_state, design_work3, pde_work1, pde_work2,
      dual_work1, outstream));
  // define the Krylov iterative method
  solver_.reset();
  solver_.reset(new FGMRESSolver<DesignVector>());
  solver_->SubspaceSize(krylov_size_);
}
// ==============================================================================
void ReducedSchurPreconditioner::MemoryRequired(ptree& num_required) {
  ptree prod_required, solver_required;
  jac_prod_->MemoryRequired(prod_required);
  solver_->MemoryRequired(solver_required);
  num_required.put("design", prod_required.get<int>("design")
                   + solver_required.get<int>("num_vec"));
  num_required.put("state", prod_required.get<int>("state"));
  num_required.put("dual", prod_required.get<int>("dual"));
  num_required.put("num_vec", 0);
}
// ==============================================================================
void ReducedSchurPreconditioner::Initialize(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    std::vector<DesignVector>& design_work, std::vector<PDEVector>& pde_work,
    std::vector<DualVector>& dual_work, const ptree& prec_param,
    ostream & outstream) {
  design_work1_ = &design_work[0];
  design_work2_ = &design_work[1];
  diag_ = 0.0;
  outstream_ = &outstream;
  // define the Constraint Jacobian Product
  bool approx = true;
  jac_prod_.reset(); // clears vectors used inside jac_prod_
  jac_prod_.reset(new ConstraintJacobianProduct(
      eval_at_kkt.design_, eval_at_state, design_work[2], pde_work[0],
      pde_work[1], dual_work[0], outstream, approx));
}
// ==============================================================================
ReducedSchurPreconditioner::~ReducedSchurPreconditioner() {
}
// ==============================================================================
void ReducedSchurPreconditioner::set_diagonal(const double & diag) {
  diag_ = diag;
}
// ==============================================================================
void ReducedSchurPreconditioner::operator()(ReducedKKTVector & u,
                                            ReducedKKTVector & v) {
  v = 0.0;
  IdentityPreconditioner<DesignVector> precond; // do-nothing preconditioner
  double tol = 0.01;
  *design_work1_ = u.design_;
  design_work1_->RestrictToTarget();
  
  // Step 1: solve (dc/dy)^T v.dual_ = (u.design_)_(target subspace)
  jac_prod_->Transpose();
  jac_prod_->RestrictToTarget();
  
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# Reverse problem in IDF-Schur preconditioner" << endl;
  int iters;
  *design_work2_ = u.design_;
  design_work2_->RestrictToTarget();
  *design_work1_ = 0.0;
  ptree ptin, ptout;
  ptin.put<double>("tol", tol);
  ptin.put<bool>("check", false);
  solver_->Solve(ptin, *design_work2_, *design_work1_, *jac_prod_, precond,
                 ptout, *outstream_);
  v.dual_.Convert(*design_work1_);

  // Step 2: compute (v.design_)_(design subspace) =
  // (u.design_)_(design subspace) - (dc/dx)^T v.dual_
  jac_prod_->RestrictToDesign();
  jac_prod_->operator()(*design_work1_, v.design_);
  double fac = 1.0/(1.0 + diag_);
  v.design_.EqualsAXPlusBY(-fac, v.design_, fac, u.design_);
  v.design_.RestrictToDesign(); // zero-out (u.design_)_(target subspace)

  // Step 3: solve (dc/dy) (v.design_)_(target subspace) = 
  // u.dual_ - (dc/dx) (v.design_)_(design subspace)
  jac_prod_->Untranspose();
  jac_prod_->operator()(v.design_, *design_work1_);
  design_work2_->Convert(u.dual_);
  design_work1_->EqualsAXPlusBY(-1.0, *design_work1_, 1.0, *design_work2_);
  
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# Forward problem in IDF-Schur preconditioner" << endl;
  *design_work2_ = 0.0;
  jac_prod_->RestrictToTarget();
  ptin.clear();
  ptout.clear();
  ptin.put<double>("tol", tol);
  ptin.put<bool>("check", false);
  solver_->Solve(ptin, *design_work1_, *design_work2_, *jac_prod_, precond,
                 ptout, *outstream_);
  
  v.design_ += *design_work2_;

  //cout << "ReducedSchurPreconditioner::operator(): v.Norm2() = "
  //     << v.Norm2() << endl;
}
// ==============================================================================
ProjectionPreconditioner::ProjectionPreconditioner(
    const int& krylov_size, ostream& outstream) {
  outstream_ = &outstream;
  aug_prod_.reset(new ReducedAugmentedProduct(outstream));
  aug_precond_.reset(new IdentityPreconditioner<ReducedKKTVector>());
  solver_.reset(new FGMRESSolver<ReducedKKTVector>());
  solver_->SubspaceSize(krylov_size);
}
// ==============================================================================
ProjectionPreconditioner::ProjectionPreconditioner(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    DesignVector & design_work1, DesignVector & design_work2,
    DesignVector & design_work3, PDEVector & pde_work1,
    PDEVector & pde_work2, DualVector & dual_work1, const int & krylov_size,
    ostream & outstream) {
  outstream_ = &outstream; 
  // define the augmented matrix-vector product
  aug_prod_.reset(new ReducedAugmentedProduct(
      eval_at_kkt, eval_at_state, design_work1, pde_work1, pde_work2, dual_work1,
      outstream));
  aug_precond_.reset(new IdentityPreconditioner<ReducedKKTVector>());
  // define the Krylov iterative method
  solver_.reset(new FGMRESSolver<ReducedKKTVector>());
  solver_->SubspaceSize(krylov_size);
  rhs_.reset(new ReducedKKTVector());
  sol_.reset(new ReducedKKTVector());
}
// ==============================================================================
void ProjectionPreconditioner::MemoryRequired(ptree& num_required) {
  ptree prod_required, solver_required;
  aug_prod_->MemoryRequired(prod_required);
  solver_->MemoryRequired(solver_required);
  num_required.put("design", prod_required.get<int>("design")
                   + solver_required.get<int>("num_vec")
                   + 2 /* for rhs_ and sol_ vectors */);
  num_required.put("state", prod_required.get<int>("state"));
  num_required.put("dual", prod_required.get<int>("dual")
                   + solver_required.get<int>("num_vec")
                   + 2 /* for rhs_ and sol_ vectors */);
  num_required.put("num_vec", 0);
}
// ==============================================================================
void ProjectionPreconditioner::Initialize(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    std::vector<DesignVector>& design_work, std::vector<PDEVector>& pde_work,
    std::vector<DualVector>& dual_work, const ptree& prec_param,
    ostream & outstream) {
  outstream_ = &outstream;
  // initialize the augmented matrix-vector product
  ptree prod_param;
  prod_param.put<bool>("approx", false);
  boost::static_pointer_cast<ReducedAugmentedProduct>(aug_prod_)->Initialize
  (eval_at_kkt, eval_at_state, design_work, pde_work, dual_work, prod_param,
   outstream);
  aug_precond_.reset();
  aug_precond_.reset(new IdentityPreconditioner<ReducedKKTVector>());
  rhs_.reset();
  rhs_.reset(new ReducedKKTVector());
  sol_.reset();
  sol_.reset(new ReducedKKTVector());
}
// ==============================================================================
ProjectionPreconditioner::~ProjectionPreconditioner() {
}
// ==============================================================================
void ProjectionPreconditioner::set_diagonal(const double & diag) {
  ptree prod_param;
  prod_param.put<bool>("diag", diag);
  aug_prod_->Initialize(prod_param);
}
// ==============================================================================
void ProjectionPreconditioner::set_aug_precond(
    const boost::shared_ptr<Preconditioner<ReducedKKTVector> >& aug_precond) {
  aug_precond_.reset();
  aug_precond_ = aug_precond;
}
// ==============================================================================
void ProjectionPreconditioner::operator()(DesignVector & u,
                                          DesignVector & v) {
  rhs_->primal() = u;
  rhs_->dual() = 0.0;
  *sol_ = 0.0;
  ptree ptin, ptout;
  ptin.put<double>("tol", 0.01);
  ptin.put<bool>("check", false);
  *outstream_ << "# ProjectionPreconditioner" << endl;
  solver_->Solve(ptin, *rhs_, *sol_, *aug_prod_, *aug_precond_, ptout,
                 *outstream_);
  v = sol_->primal();
}
// ==============================================================================
IDFProjectionPreconditioner::IDFProjectionPreconditioner(
    const int& krylov_size, ostream& outstream) {
  outstream_ = &outstream;
#if 0
  jac_prod_.reset(new ConstraintJacobianProduct(outstream));
  solver_.reset(new FGMRESSolver<DesignVector>());
  solver_->SubspaceSize(krylov_size);
#endif
}
// ==============================================================================
IDFProjectionPreconditioner::IDFProjectionPreconditioner(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    DesignVector & design_work1, DesignVector & design_work2,
    DesignVector & design_work3, PDEVector & pde_work1,
    PDEVector & pde_work2, DualVector & dual_work1, const int & krylov_size,
    ostream & outstream) {
  outstream_ = &outstream;
#if 0
  tol_ = 0.01;
  // define the constraint-Jacobian product
  jac_prod_.reset();
  bool approx = true;
  jac_prod_.reset(new ConstraintJacobianProduct(
      eval_at_kkt.design_, eval_at_state, design_work1, pde_work1, pde_work2,
      dual_work1, outstream, approx));
  // define the Krylov iterative method
  solver_.reset(new FGMRESSolver<DesignVector>());
  solver_->SubspaceSize(krylov_size);
  rhs_ = &design_work2;
  wrk_ = &design_work3;
#else
  rhs_.reset(new ReducedKKTVector);
  sol_.reset(new ReducedKKTVector);
#endif 
}
// ==============================================================================
void IDFProjectionPreconditioner::MemoryRequired(ptree& num_required) {
#if 0
  ptree prod_required, solver_required;
  jac_prod_->MemoryRequired(prod_required);
  solver_->MemoryRequired(solver_required);
  num_required.put("design", prod_required.get<int>("design")
                   + solver_required.get<int>("num_vec")
                   + 2 /* for rhs_ and sol_ vectors */);
  num_required.put("state", prod_required.get<int>("state"));
  num_required.put("dual", prod_required.get<int>("dual")
                   + solver_required.get<int>("num_vec")
                   + 2 /* for rhs_ and sol_ vectors */);
#endif
  num_required.put("design", 2 /* for rhs_ and sol_ vectors */);
  num_required.put("state", 0);
  num_required.put("dual", 2 /* for rhs_ and sol_ vectors */);                 
  num_required.put("num_vec", 0);
}
// ==============================================================================
void IDFProjectionPreconditioner::Initialize(
    const ReducedKKTVector & eval_at_kkt, const PDEVector & eval_at_state,
    std::vector<DesignVector>& design_work, std::vector<PDEVector>& pde_work,
    std::vector<DualVector>& dual_work, const ptree& prec_param,
    ostream & outstream) {
  outstream_ = &outstream;
#if 0
  tol_ = prec_param.get("tol", 0.1);
  // initialize the constraint Jacobian product
  jac_prod_.reset();
  bool approx = prec_param.get("approx", true);
  jac_prod_.reset(new ConstraintJacobianProduct(
      eval_at_kkt.design_, eval_at_state, design_work[0], pde_work[0],
      pde_work[1], dual_work[0], outstream, approx));
  rhs_ = &design_work[1];
  wrk_ = &design_work[2];
#else
  rhs_.reset();
  rhs_.reset(new ReducedKKTVector);
  sol_.reset();
  sol_.reset(new ReducedKKTVector);
#endif
}
// ==============================================================================
IDFProjectionPreconditioner::~IDFProjectionPreconditioner() {
}
// ==============================================================================
void IDFProjectionPreconditioner::set_idf_precond(
  const boost::shared_ptr<Preconditioner<ReducedKKTVector> >& idf_precond) {
  idf_precond_.reset();
  idf_precond_ = idf_precond;
}
// ==============================================================================
void IDFProjectionPreconditioner::operator()(DesignVector & u,
                                          DesignVector & v) {

#if 0
  // build the rhs: -(dc/dx) (u_(design subspace))
  v = u;
  v.RestrictToDesign();
  jac_prod_->RestrictToDesign();
  jac_prod_->Untranspose();
  jac_prod_->operator()(v, *rhs_);
  //*wrk_ = u;
  //wrk_->RestrictToTarget();
  //rhs_->EqualsAXPlusBY(-1.0, *rhs_, 1.0, *wrk_);
  *rhs_ *= -1.0;
  jac_prod_->RestrictToTarget();
  if (rhs_->Norm2() < kEpsilon) {
    v = 0.0;
    return;
  }

  // solve (dc/dy) (v_(target subspace)) = rhs where
  // rhs = u_(target subspace) -(dc/dx) (u_(design subspace))
  *outstream_ << "# IDFProjectionPreconditioner" << endl;
  ptree ptin, ptout;
  ptin.put<double>("tol", tol_);
  ptin.put<bool>("check", false);
  IdentityPreconditioner<DesignVector> precond; // do-nothing preconditioner
  cout << "rhs_->Norm2() = " << rhs_->Norm2() << endl;
  solver_->Solve(ptin, *rhs_, v, *jac_prod_, precond, ptout, *outstream_);
#if 0
  rhs_->EqualsAXPlusBY(1.0, v, -1.0, u);
  rhs_->RestrictToDesign();
  cout << "rhs_->Norm2() = " << rhs_->Norm2() << endl;
#endif
#else
  rhs_->primal() = u;
  rhs_->dual() = 0.0;
  idf_precond_->operator()(*rhs_, *sol_);
  v = sol_->primal();
#endif
}
// ==============================================================================
const DesignVector & ReducedKKTVector::primal() const {
  return design_;
}
DesignVector & ReducedKKTVector::primal() {
  return design_;
}
// ==============================================================================
const DualVector & ReducedKKTVector::dual() const {
  return dual_;
}
DualVector & ReducedKKTVector::dual() {
  return dual_;
}
// ==============================================================================
const DesignVector & ReducedKKTVector::get_design() const {
  return design_;
}
// ==============================================================================
const DualVector & ReducedKKTVector::get_dual() const {
  return dual_;
}
// ==============================================================================
void ReducedKKTVector::set_design(const DesignVector & design) {
  design_ = design;
}
// ==============================================================================
void ReducedKKTVector::set_dual(const DualVector & dual) {
  dual_ = dual;
}
// ==============================================================================
ReducedKKTVector & ReducedKKTVector::operator=(const ReducedKKTVector & x) {
  if (this == &x) return *this; // ReducedKKTVector being assigned to itself
  design_ = x.design_;
  dual_ = x.dual_;
  return *this;
}
// ==============================================================================
ReducedKKTVector & ReducedKKTVector::operator=(const double & val) {
  design_ = val;
  dual_ = val;
  return *this;
}
// ==============================================================================
ReducedKKTVector & ReducedKKTVector::operator+=(const ReducedKKTVector & x) {
  design_ += x.design_;
  dual_ += x.dual_;
  return *this;
}
// ==============================================================================
ReducedKKTVector & ReducedKKTVector::operator-=(const ReducedKKTVector & x) {
  design_ -= x.design_;
  dual_ -= x.dual_;
  return *this;
}
// ==============================================================================
ReducedKKTVector & ReducedKKTVector::operator*=(const double & val) {
  design_ *= val;
  dual_ *= val;
  return *this;
}
// ==============================================================================
ReducedKKTVector & ReducedKKTVector::operator/=(const double & val) {
  design_ /= val;
  dual_ /= val;
  return *this;
}
// ==============================================================================
void ReducedKKTVector::EqualsAXPlusBY(const double & a,
                                      const ReducedKKTVector & x,
                                      const double & b,
                                      const ReducedKKTVector & y) {
  design_.EqualsAXPlusBY(a, x.design_, b, y.design_);
  dual_.EqualsAXPlusBY(a, x.dual_, b, y.dual_);
}
// ==============================================================================
double ReducedKKTVector::Norm2() const {
  // call an inner product on ReducedKKTVector
  double val = InnerProd(*this, *this);
  if (val < 0.0) {
    cerr << "ReducedKKTVector(Norm2): "
	 << "inner product of ReducedKKTVector is negative" << endl;
    throw(-1);
  }
  return sqrt(val);
}
// ==============================================================================
double InnerProd(const ReducedKKTVector & x, const ReducedKKTVector & y) {
  double prod = 0.0;
  prod += InnerProd(x.design_, y.design_);
  prod += InnerProd(x.dual_, y.dual_);
  return prod;
}
// ==============================================================================
void ReducedKKTVector::EqualsInitialGuess() {
  design_.EqualsInitialDesign();
  dual_ = 0.0; //0.0; // set Lagrange multiplier to zero initially
}
// ==============================================================================
void ReducedKKTVector::EqualsKKTConditions(const ReducedKKTVector & x,
                                           const PDEVector & state,
                                           const PDEVector & adjoint,
                                           DesignVector & design_wrk) {
  dual_.EqualsConstraints(x.design_, state);
  design_wrk.EqualsObjectiveGradient(x.design_, state);
  design_.EqualsTransJacobianTimesVector(x.design_, state, adjoint);
  design_ += design_wrk;
  design_wrk.EqualsTransCnstrJacTimesVector(x.design_, state, x.dual_);
  design_ += design_wrk;
}
// ==============================================================================
void ReducedKKTVector::SolveStateAndAdjoint(PDEVector & state,
                                            PDEVector & adjoint,
                                            PDEVector & pde_work) const {
  state.EqualsPrimalSolution(design_);
  SolveAdjoint(state, adjoint, pde_work);
}
// ==============================================================================
void ReducedKKTVector::SolveAdjoint(const PDEVector & state, PDEVector & adjoint,
                                    PDEVector & pde_work) const {
  // build rhs for adjoint equation
  adjoint.EqualsTransCnstrJacTimesVector(design_, state, dual_);
  pde_work.EqualsObjectiveGradient(design_, state);
  pde_work.EqualsAXPlusBY(-1.0, pde_work, -1.0, adjoint);
  adjoint.EqualsAdjointSolution(design_, state, pde_work);
}
// ==============================================================================
double Lagrangian(const ReducedKKTVector & eval_at_kkt,
                  const PDEVector & eval_at_state,
                  const DualVector & ceq) {
  double Lag = ObjectiveValue(eval_at_kkt.primal(), eval_at_state);      
  Lag += InnerProd(eval_at_kkt.dual(), ceq);
  return Lag;
}
// ==============================================================================
double AugmentedLagrangian(const ReducedKKTVector & eval_at_kkt,
                           const PDEVector & eval_at_state,
                           const DualVector & ceq, const double & mu) {
  double AugLag = ObjectiveValue(eval_at_kkt.primal(), eval_at_state);      
  AugLag += InnerProd(eval_at_kkt.dual(), ceq);
  AugLag += 0.5*mu*InnerProd(ceq, ceq);
  return AugLag;
}
// ==============================================================================
double EvaluateL2Merit(const ReducedKKTVector & eval_at_kkt,
                       const PDEVector & eval_at_state,
                       const DualVector & ceq, const double & mu) {
  double merit = ObjectiveValue(eval_at_kkt.primal(), eval_at_state);      
  //merit += InnerProd(eval_at_kkt.dual(), ceq);
  merit += mu*ceq.Norm2();
  return merit;
}
// ==============================================================================
}
