/**
 * \file merit.hpp
 * \brief header file for various types of merit functions
 * \author Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#pragma once

#include <math.h>
#include <ostream>
#include <iostream>
#include "./vectors.hpp"
#include "./reduced_kkt_vector.hpp"

namespace kona {

using std::cerr;
using std::cout;
using std::endl;

class DesignVector;
class PDEVector;
class DualVector;
class ReducedKKTVector;

// ==============================================================================
/*!
 * \class MeritFunction
 * \brief abstract base class used to define a merit function
 */
class MeritFunction {
 public:

  /*!
   * \brief class destructor
   */
  virtual ~MeritFunction() {}
  
#if 0
  /*!
   * \brief returns the memory requirements of a single object
   * \param[out] design_memory - number of design-space vectors
   * \param[out] pde_memory - number of state-space vectors
   * \param[out] dual_memory - number of dual-space vectors
   *
   * \warning this assumes the memory requirements of the class are static; if
   * this is not the case, this function should not be used.
   */
  virtual static void MemoryRequired(int& design_memory, int& pde_memory,
                                     int& dual_memory) const = 0;
#endif
  
  /*!
   * \brief evaluates the merit function at a given step
   * \param[in] alpha - step length at which to evaluate merit function
   */
  virtual double EvalFunc(double alpha) = 0;

  /*!
   * \brief evaluates the merit function derivative at a given step
   * \param[in] alpha - step length at which to evaluate derivative
   */
  virtual double EvalGrad(double alpha) = 0;

  /*!
   * \brief returns an estimate of the directional derivative at initial design
   * \param[in] eqn_norm - L2 norm of the state equations at initial design
   * \param[in] eqn_norm - L2 norm of the state equation linear residual
   */ 
  virtual double EstimateDirDerivative(const double & eqn_norm,
                                       const double & lin_eqn_norm) {
    return EvalGrad(0.0);
  }

};
// ==============================================================================
/*!
 * \class GlobalizedMerit
 * \brief merit function for homotopy-based globalization
 */
class GlobalizedMerit: public MeritFunction {
 public:

  /*!
   * \brief class constructor
   * \param[in] x_init - the initial design in the homotopy definition
   * \param[in] lambda - the continuation parameter
   * \param[in] scale - a scaling used in the homotopy definition
   * \param[in] search_dir - the search direction 
   * \param[in] x_start - the initial position for the search
   * \param[in] p_dot_grad - inner product of serach direction with gradient
   * \param[in,out] state_work - state at x_start on entry
   * \param[in,out] adjoint_work - adjoint at x_start on entry
   */
  GlobalizedMerit(DesignVector & x_init, const double & lambda, 
                  const double & scale, DesignVector & search_dir,
                  DesignVector & x_start, const double & p_dot_grad,
                  PDEVector & state_work, PDEVector & adjoint_work);

  /*!
   * \brief class destructor
   */
  ~GlobalizedMerit() {}

  /*!
   * \brief returns the memory requirements of the object
   * \param[out] design_memory - number of design-space vectors
   * \param[out] pde_memory - number of state-space vectors
   * \param[out] dual_memory - number of dual-space vectors
   */
  static void MemoryRequired(int& design_memory, int& pde_memory,
                             int& dual_memory);
  
  /*!
   * \brief evaluates the homotopy merit function at a given step
   * \param[in] alpha - step length at which to evaluate merit function
   */
  double EvalFunc(double alpha);

  /*!
   * \brief evaluates the merit function derivative at a given step
   * \param[in] alpha - step length at which to evaluate derivative
   */
  double EvalGrad(double alpha);

 protected:
  
  double lambda_; ///< continuation parameter
  double scale_; ///< scaling used in homotopy function
  double alpha_last_; ///< previous step length used
  double obj_start_; ///< value of objective at x_start_
  double p_dot_grad_; ///< value of gradient^T search at x_start_
  DesignVector* x_init_; ///< initial design for homotopy function
  DesignVector* x_start_; ///< start position for line searches
  DesignVector* search_dir_; ///< search direction
  PDEVector* state_work_; ///< work vector for the state
  PDEVector* adjoint_work_; ///< work vector for the adjoint
  DesignVector design_work_; ///< work vector for design
};
// ==============================================================================
/*!
 * \class L2Merit
 * \brief L2 exact merit function
 */
class L2Merit: public MeritFunction {
 public:
  
  /*!
   * \brief class constructor
   * \param[in] mu - the penalty parameter
   * \param[in] design_start - the initial position of the design vector
   * \param[in] state_start - the initial position of the state vector
   * \param[in] design_dir - the design search direction 
   * \param[in] state_dir - the state search direction
   * \param[in] design_work - a design vector work space
   * \param[in] state_work - a state vector work space
   * \param[in] eqn_work - work space for the state equations
   */
  L2Merit(const double & mu, const DesignVector & design_start,
          const PDEVector & state_start, const DesignVector & design_dir,
          const PDEVector & state_dir, DesignVector & design_work,
          PDEVector & state_work, PDEVector & eqn_work);

  /*!
   * \brief class destructor
   */
  ~L2Merit() {}

  /*!
   * \brief returns the memory requirements of the object
   * \param[out] design_memory - number of design-space vectors
   * \param[out] pde_memory - number of state-space vectors
   * \param[out] dual_memory - number of dual-space vectors
   */
  static void MemoryRequired(int& design_memory, int& pde_memory,
                      int& dual_memory);
  
  /*!
   * \brief evaluates the L2 merit function at a given step
   * \param[in] alpha - step length at which to evaluate merit function
   */
  double EvalFunc(double alpha);

  /*!
   * \brief evaluates the merit function derivative at a given step
   * \param[in] alpha - step length at which to evaluate derivative
   *
   * NOTE: the L2 merit function is not set-up for evaluating its derivative, so
   * this will throw an error if called
   */
  double EvalGrad(double alpha);

  /*!
   * \brief returns an estimate of the directional derivative at initial design
   * \param[in] eqn_norm - L2 norm of the state equations at initial design
   * \param[in] eqn_norm - L2 norm of the state equation linear residual
   */ 
  double EstimateDirDerivative(const double & eqn_norm,
                               const double & lin_eqn_norm);

 protected:

  double mu_; ///< penalty parameter
  DesignVector const * design_start_; ///< start design for line searches
  PDEVector const * state_start_; ///< start state for line searches
  DesignVector const * design_dir_; ///< design vector search direction
  PDEVector const * state_dir_; ///< state vector search direction
  DesignVector* design_work_; ///< work vector for design
  PDEVector* state_work_; ///< work vector for the state
  PDEVector* eqn_work_; ///< work vector for the state equations
};
// =============================================================================
/*!
 * \class L2Merit
 * \brief L2 exact merit function
 */
class ReducedSpaceL2Merit: public MeritFunction {
 public:

  /*!
   * \brief class constructor
   * \param[in] pi - the penalty parameter
   * \param[in] design_start - the initial position of the design vector
   * \param[in] state_start - the state at design_start
   * \param[in] ceq_start - the constraints at design_start
   * \param[in] design_dir - the design search direction 
   * \param[out] design_work - a design vector work space
   * \param[out] state_work - a state vector work space
   * \param[out] dual_work - work space for the constraints
   */
  ReducedSpaceL2Merit(const double& pi, const DesignVector& design_start,
                      const PDEVector& state_start, const DualVector& ceq_start,
                      const DesignVector& design_dir, DesignVector& design_work,
                      PDEVector& state_work, DualVector& dual_work);
  
  /*!
   * \brief class destructor
   */
  ~ReducedSpaceL2Merit() {}

  /*!
   * \brief returns the memory requirements of the object
   * \param[out] design_memory - number of design-space vectors
   * \param[out] pde_memory - number of state-space vectors
   * \param[out] dual_memory - number of dual-space vectors
   */
  static void MemoryRequired(int& design_memory, int& pde_memory,
                      int& dual_memory);
  
  /*!
   * \brief evaluates the reduced-space L2 merit function at a given step
   * \param[in] alpha - step length at which to evaluate merit function
   */
  double EvalFunc(double alpha);

  /*!
   * \brief evaluates the reduced-space merit function derivative at a given step
   * \param[in] alpha - step length at which to evaluate derivative
   *
   * NOTE: the ReducedSpaceL2Merit merit function is not set-up for evaluating
   * its derivative, so this will throw an error if called
   */
  double EvalGrad(double alpha);

 private:
  double pi_; ///< penalty parameter
  double merit_start_; ///< value of merit function at alpha = 0.0
  DesignVector const* design_start_; ///< start design for line searches
  DesignVector const* design_dir_; ///< design vector search direction
  DesignVector* design_work_; ///< work vector for design
  PDEVector* state_work_; ///< work vector for the state
  DualVector* dual_work_; ///< work vector for the constraints
};
  
}// namespace kona
