/**
 * \file equality_constrained_rsnk.hpp
 * \brief header file for the EqualityConstrainedRSNK class
 * \author Jason Hicken <jason.hicken@gmail.com>
 */

#pragma once

#include <boost/scoped_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/program_options.hpp>
#include "merit.hpp"
#include "line_search.hpp"
#include "quasi_newton.hpp"
#include "optimizer.hpp"
#include "krylov.hpp"
#include "filter.hpp"

#define FLECS

namespace kona {

using boost::scoped_ptr;
using boost::shared_ptr;
using boost::program_options::variables_map;

/*!
 * \class EqualityConstrainedRSNK
 * \brief For equality constrained problems using Newton-Krylov in reduced space
 *
 * The underlying algorithm for this class is the inexact-Newton method of Byrd,
 * Curtis, and Nocedal (see, e.g., "An inexact Newton method for nonconvex
 * equality constrained optimization," doi:10.1007/s10107-008-0248-3).
 */
class EqualityConstrainedRSNK : public Optimizer {
 public:

  /*!
   * \brief default constructor
   */
  EqualityConstrainedRSNK() {}
  
  /*!
   * \brief class destructor
   */
  ~EqualityConstrainedRSNK() {}

  /*!
   * \brief defines the options needed by the algorithm
   * \param[in] optns - set of options
   */
  void SetOptions(const po::variables_map& optns);
  
  /*!
   * \brief determines how many user stored vectors are needed
   * \param[out] num_design - number of design-sized vectors needed
   * \param[out] num_state - number of state-sized vectors needed
   * \param[out] num_dual - number of dual-sized vectors needed
   */
  void MemoryRequirements(int & num_design, int & num_state,
                          int & num_dual);

  /*!
   * \brief attempts to solve the optimization problem
   */
  void Solve();

 protected:
  variables_map options_; ///< options for the algorithm
#ifdef FLECS
  shared_ptr<IterativeSolver<ReducedKKTVector> >
  comp_step_solver_; ///< defines the solver for the composite-step problem  
  shared_ptr<MatrixVectorProduct<ReducedKKTVector> >
  mat_vec_; ///< defines the KKT-matrix-vector product
#else  
  scoped_ptr<IterativeSolver<ReducedKKTVector> >
  normal_step_solver_; ///< defines the solver for the normal-step problem
  scoped_ptr<IterativeSolver<DesignVector> >
  tang_step_solver_; ///< defines the solver for the tangential-step problem
  shared_ptr<MatrixVectorProduct<DesignVector> >
  hess_mat_vec_; ///< defines the Hessian-vector product
  shared_ptr<MatrixVectorProduct<ReducedKKTVector> >
  aug_mat_vec_; ///< defines the Augmented-matrix-vector product
  shared_ptr<Preconditioner<DesignVector> >
  tang_precond_; ///< preconditioner for the tangential subproblem
#endif
  shared_ptr<Preconditioner<ReducedKKTVector> >
  aug_precond_; ///< preconditioner for systems involving aug. matrix
  shared_ptr<QuasiNewton> quasi_newton_; ///< defines quasi-Newton method
  scoped_ptr<LineSearch> line_search_; ///< defines line search
  scoped_ptr<MeritFunction> merit_; ///< defines the merit function
  scoped_ptr<Filter> filter_; ///< defines the filter

 private:

  /*!
   * \brief writes header information to the main history file
   * \param[in,out] out - a valid stream to write the history to
   */
  void WriteHistoryHeader(ostream& out);

  /*!
   * \brief writes an history iteration to the main history file
   * \param[in] iter - current iteration
   * \param[in] precond_calls - number of user preconditioner calls
   * \param[in] optimality - L2 norm of the Lagrangian gradient
   * \param[in] feasibility - L2 norm of the constraints
   * \param[in] obj - current value of the objective function
   * \param[in] mu - current value of the penalty parameter
   * \param[in] info - indicates if any actions/events occured during iteration
   * \param[in,out] out - a valid stream to write the history to
   */
  void WriteHistoryIteration(
      const int& iter, const int& precond_calls, const double& optimality,
      const double& feasibility, const double& obj, const double& mu,
      const string& info, ostream& out);
};

} // namespace kona
