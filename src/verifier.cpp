/**
 * \file reduced_space_quasi_newton.cpp
 * \brief definitions of ReducedSpaceQuasiNewton class member functions
 * \author Jason Hicken <jason.hicken@gmail.com>
 */

#include <fstream>
#include <boost/program_options.hpp>
#include "verifier.hpp"
#include "vectors.hpp"
#include "krylov.hpp"
#include "vector_operators.hpp"

namespace kona {

using std::string;
using std::ostream;
using std::ifstream;
using std::ofstream;
using std::ostringstream;
using std::ios;
using std::max;
using std::vector;
using std::map;

// ==============================================================================
void Verifier::SetOptions(const po::variables_map& optns) {
  options_ = optns;
}
// ==============================================================================
void Verifier::MemoryRequirements(int & num_design,
                                  int & num_state,
                                  int & num_dual) {
  num_design = 0;
  num_state = 0;
  num_dual = 0;
  // vectors required in Solve() method
  if (options_["verify.design_vec"].as<bool>()) {
    num_design = max(num_design,6);
    num_state = max(num_state,0);
    num_dual = max(num_dual,0);
  }
  if (options_["verify.pde_vec"].as<bool>()) {
    num_design = max(num_design,0);
    num_state = max(num_state,6);
    num_dual = max(num_dual,0);
  }
  if (options_["verify.dual_vec"].as<bool>()) {
    num_design = max(num_design,0);
    num_state = max(num_state,0);
    num_dual = max(num_dual,6);
  }
  if (options_["verify.gradient"].as<bool>()) {
    num_design = max(num_design,6);
    num_state = max(num_state,6);
    num_dual = max(num_dual,0);
  }
  if (options_["verify.jacobian"].as<bool>()) {
    num_design = max(num_design,6);
    num_state = max(num_state,6);
    num_dual = max(num_dual,0);
  }
  if (options_["verify.cnstr_jac"].as<bool>()) {
    num_design = max(num_design,6);
    num_state = max(num_state,6);
    num_dual = max(num_dual,6);
  }
  if (options_["verify.red_grad"].as<bool>()) {
    num_design = max(num_design,6);
    num_state = max(num_state,6);
    num_dual = max(num_dual,0);
  }
  if (options_["verify.linear_solve"].as<bool>()) {
    num_design = max(num_design,1);
    num_state = max(num_state,5);
    num_dual = max(num_dual,0);
  }
  if (options_["verify.hessian_prod"].as<bool>()) {
    num_design = max(num_design,8);
    num_state = max(num_state,11);
    num_dual = max(num_dual,0);
  }
}
// ==============================================================================
void Verifier::Solve() {
  int num_warn = 0;
  double Huge = 1.0/kEpsilon;

  // open output file, if necessary
  ostream* info;
  ofstream info_file;
#if 1
  int myrank = UserMemory::GetRank();
  if (myrank == 0) {
    if (options_["send_info_to_cout"].as<bool>())
      info = &std::cout;
    else {
      info_file.open(options_["info_file"].as<string>().c_str());
      info = &info_file;
    }
  } else {
    // set stream to null (black-hole) streams
    info_file.open(0);
    info = &info_file;
  }
#else
  if (options_["send_info_to_cout"].as<bool>())
    info = &std::cout;
  else {
    info_file.open(options_["info_file"].as<string>().c_str());
    info = &info_file;
  }
#endif
  
  if (options_["verify.design_vec"].as<bool>()) {
    // verify design vector assignment operators, compound addition-assignment
    // operator, and norm
    DesignVector u_d, v_d, w_d, x_d, y_d, z_d;
    u_d = 1.0;
    v_d = -1.0;
    w_d = u_d;
    w_d += v_d;
    double zero = w_d.Norm2();
    if (fabs(zero) > kEpsilon) {
      cerr << "Verifier::Solve(): problem in UserFunc;" << endl;
      cerr << "\tcheck for errors in axpby_d and/or innerprod_d." << endl;
      throw(-1);
    }
    
    // verify design vector compound scalar mult-assignment operator, compound
    // division-assignment, and EqualsAXPlusBY routine
    u_d *= 2.0;
    v_d /= 3.0;
    w_d.EqualsAXPlusBY(1.0/3.0, u_d, 2.0, v_d);
    zero = w_d.Norm2();
    if (fabs(zero) > kEpsilon) {
      cerr << "Verifier::Solve(): problem in UserFunc;" << endl;
      cerr << "\tcheck for errors in axpby_d and/or innerprod_d." << endl;
      throw(-1);
    }
  }
    
  if (options_["verify.pde_vec"].as<bool>()) {
    // verify the state vector daxpy routines
    PDEVector u_s, v_s, w_s, x_s, y_s, z_s;
    
    // verify design vector assignment operators, compound addition-assignment
    // operator, and norm
    u_s = 1.0;
    v_s = -1.0;
    w_s = u_s;
    w_s += v_s;
    double zero = w_s.Norm2();
    if (fabs(zero) > kEpsilon) {
      cerr << "Verifier::Solve(): problem in UserFunc;" << endl;
      cerr << "\tcheck for errors in axpby_s and/or innerprod_s." << endl;
      throw(-1);
    }
    
    // verify design vector compound scalar mult-assignment operator, compound
    // division-assignment, and EqualsAXPlusBY routine
    u_s *= 2.0;
    v_s /= 3.0;
    w_s.EqualsAXPlusBY(1.0/3.0, u_s, 2.0, v_s);
    zero = w_s.Norm2();
    if (fabs(zero) > kEpsilon) {
      cerr << "Verifier::Solve(): problem in UserFunc;" << endl;
      cerr << "\tcheck for errors in axpby_s and/or innerprod_s." << endl;
      throw(-1);
    }
  }

  if (options_["verify.dual_vec"].as<bool>()) {
    // verify dual vector assignment operators, compound addition-assignment
    // operator, and norm
    DualVector u_c, v_c, w_c, x_c, y_c, z_c;
    u_c = 1.0;
    v_c = -1.0;
    w_c = u_c;
    w_c += v_c;
    double zero = w_c.Norm2();
    if (fabs(zero) > kEpsilon) {
      cerr << "Verifier::Solve(): problem in UserFunc;" << endl;
      cerr << "\tcheck for errors in axpby_c and/or innerprod_c." << endl;
      throw(-1);
    }
    
    // verify dual vector compound scalar mult-assignment operator, compound
    // division-assignment, and EqualsAXPlusBY routine
    u_c *= 2.0;
    v_c /= 3.0;
    w_c.EqualsAXPlusBY(1.0/3.0, u_c, 2.0, v_c);
    zero = w_c.Norm2();
    if (fabs(zero) > kEpsilon) {
      cerr << "Verifier::Solve(): problem in UserFunc;" << endl;
      cerr << "\tcheck for errors in axpby_c and/or innerprod_c." << endl;
      throw(-1);
    }
  }

  if (options_["verify.gradient"].as<bool>()) {      
    // test the objective gradient dJ/dx
    DesignVector u_d, v_d, w_d, x_d, y_d, z_d;
    PDEVector u_s, v_s, w_s, x_s, y_s, z_s;
    u_d.EqualsInitialDesign();
    u_s.EqualsPrimalSolution(u_d);
    double J = ObjectiveValue(u_d, u_s);
    v_d = Huge; // need this in case user failed to define product
    v_d.EqualsObjectiveGradient(u_d, u_s);
    z_d = 1.0;  
    double epsilon_fd = CalcEpsilon(u_d.Norm2(), z_d.Norm2());
    w_d = z_d;
    w_d *= epsilon_fd;
    w_d += u_d;
    double J_p = ObjectiveValue(w_d, u_s);
    double dirDeriv_fd = (J_p - J)/epsilon_fd;
    double dirDeriv = InnerProd(v_d, z_d);
    *info << "=========================================================" << endl;
    *info << "Directional derivative test: dJ/d(design) dot 1" << endl;
    *info << "\tFD perturbation    : " << epsilon_fd << endl;
    *info << "\tanalytical dirDeriv: " << dirDeriv << endl;
    *info << "\tfinite-difference  : " << dirDeriv_fd << endl;
    *info << "\trelative error     : " << fabs(dirDeriv - dirDeriv_fd)
        /(kEpsilon + fabs(dirDeriv)) << endl;
    if (fabs(dirDeriv - dirDeriv_fd) > (fabs(dirDeriv) + kEpsilon)*epsilon_fd) {
      num_warn++;
      *info << "WARNING: dJ/d(design) gradient (or objective) "
            << "may be inaccurate." << endl;
    }
    
    // test the objective gradient dJ/du
    v_s = Huge; // need this in case user failed to define product
    v_s.EqualsObjectiveGradient(u_d, u_s);
    z_s = 1.0;
    epsilon_fd = CalcEpsilon(u_s.Norm2(), z_s.Norm2());
    w_s = z_s;
    w_s *= epsilon_fd;
    w_s += u_s;
    J_p = ObjectiveValue(u_d, w_s);
    dirDeriv_fd = (J_p - J)/epsilon_fd;
    dirDeriv = InnerProd(v_s, z_s);
    *info << "=========================================================" << endl;
    *info << "Directional derivative test: dJ/d(state) dot 1" << endl;
    *info << "\tFD perturbation    : " << epsilon_fd << endl;
    *info << "\tanalytical dirDeriv: " << dirDeriv << endl;
    *info << "\tfinite-difference  : " << dirDeriv_fd << endl;
    *info << "\trelative error     : " << fabs(dirDeriv - dirDeriv_fd)
        /(kEpsilon + fabs(dirDeriv)) << endl;
    if (fabs(dirDeriv - dirDeriv_fd) > (fabs(dirDeriv) + kEpsilon)*epsilon_fd) {
      num_warn++;
      *info << "WARNING: dJ/d(state) gradient (or objective) "
            << "may be inaccurate." << endl;
    }
  }

  if (options_["verify.jacobian"].as<bool>()) {
    // test the Jacobian-design product
    DesignVector u_d, v_d, w_d, x_d, y_d, z_d;
    PDEVector u_s, v_s, w_s, x_s, y_s, z_s;
    u_d.EqualsInitialDesign();
    u_s.EqualsPrimalSolution(u_d);
    z_d = 1.0;
    z_s = 1.0;
    x_s.EqualsStateEquations(u_d, u_s);
    v_s = Huge; // need this in case user failed to define product
    v_s.EqualsJacobianTimesVector(u_d, u_s, z_d);
    double prod_fwd = InnerProd(v_s, z_s);
    double epsilon_fd = CalcEpsilon(u_d.Norm2(), z_d.Norm2());
    w_d = z_d;
    w_d *= epsilon_fd;
    w_d += u_d;
    y_s.EqualsStateEquations(w_d, u_s);
    y_s -= x_s;
    y_s /= epsilon_fd;
    v_s -= y_s;
    double error = v_s.Norm2();
    *info << "=========================================================" << endl;
    *info << "Jacobian-vector-product test: dR/d(design) * 1" << endl;
    *info << "\tFD perturbation    : " << epsilon_fd << endl;
    *info << "\tabsolute error     : " << error << endl;
    *info << "\trelative error     : " << error/(y_s.Norm2() + kEpsilon) << endl;
    if (error > epsilon_fd) {
      num_warn++;
      *info << "WARNING: Jacobian-design-vector product (or state equations) "
            << "may be inaccurate." << endl;
    }

    // test transposed-Jacobian-design product
    v_d = Huge; // need this in case user failed to define product
    v_d.EqualsTransJacobianTimesVector(u_d, u_s, z_s);
    double prod_rev = InnerProd(v_d, z_d);
    //double prod_fd = InnerProd(y_s, z_s);
    *info << "=========================================================" << endl;
    *info << "transposed-Jacobian-vector-product test: "
          << "1^{T} * dR/d(design) * 1" << endl;
    //*info << "\tFD perturbation    : " << epsilon_fd << endl;
    //*info << "\tanalytical product : " << prod << endl;
    //*info << "\tFD product         : " << prod_fd << endl;
    //*info << "\trelative error     : " << fabs(prod - prod_fd)
    //    /(fabs(prod) + kEpsilon) << endl;
    *info << "\tforward product    : " << prod_fwd << endl;
    *info << "\treverse product    : " << prod_rev << endl;
    *info << "\trelative error     : " << fabs(prod_fwd - prod_rev)
        /(fabs(prod_fwd) + kEpsilon) << endl;
    if (fabs(prod_fwd - prod_rev) > fabs(prod_fwd)*kEpsilon) {
      num_warn++;
      *info << "WARNING: tranpsosed-Jacobian-design-vector product " << endl;
      *info << "(or Jacobian-design-vector product) may be inaccurate." << endl;
    }
      
    // test the Jacobian-state product
    v_s = Huge; // need this in case user failed to define product
    v_s.EqualsJacobianTimesVector(u_d, u_s, z_s);
    epsilon_fd = CalcEpsilon(u_s.Norm2(), z_s.Norm2());
    w_s = z_s;
    w_s *= epsilon_fd;
    w_s += u_s;
    y_s.EqualsStateEquations(u_d, w_s);
    y_s -= x_s;
    y_s /= epsilon_fd;
    v_s -= y_s;
    error = v_s.Norm2();
    *info << "=========================================================" << endl;
    *info << "Jacobian-vector-product test: dR/d(state) * 1" << endl;
    *info << "\tFD perturbation    : " << epsilon_fd << endl;
    *info << "\tabsolute error     : " << error << endl;
    *info << "\trelative error     : " << error/(y_s.Norm2() + kEpsilon) << endl;
    if (error > epsilon_fd) {
      num_warn++;
      *info << "WARNING: Jacobian-state-vector product (or state equations) "
            << "may be inaccurate." << endl;
    }    

    // second test of Jacobian-state product
    v_s = Huge; // need this in case user failed to define product
    v_s.EqualsJacobianTimesVector(u_d, u_s, z_s);
    double prod = InnerProd(v_s, z_s);
    double prod_fd = InnerProd(y_s, z_s);
    *info << "=========================================================" << endl;
    *info << "Jacobian-vector-product test: "
          << "1^{T} * dR/d(state) * 1" << endl;
    *info << "\tFD perturbation    : " << epsilon_fd << endl;
    *info << "\tanalytical product : " << prod << endl;
    *info << "\tFD product         : " << prod_fd << endl;
    *info << "\trelative error     : " << fabs(prod - prod_fd)
        /(fabs(prod) + kEpsilon) << endl;
    if (fabs(prod - prod_fd) > (fabs(prod) + kEpsilon)*epsilon_fd) {
      num_warn++;
      *info << "WARNING: Jacobian-state-vector product " << endl;
      *info << "(or state equations) may be inaccurate." << endl;
    }
  
    // test the transposed-Jacobian-state product
    v_s = Huge; // need this in case user failed to define product
    v_s.EqualsTransJacobianTimesVector(u_d, u_s, z_s);
    prod = InnerProd(v_s, z_s);
    prod_fd = InnerProd(y_s, z_s);
    *info << "=========================================================" << endl;
    *info << "transposed-Jacobian-vector-product test: "
          << "1^{T} * dR/d(state) * 1" << endl;
    *info << "\tFD perturbation    : " << epsilon_fd << endl;
    *info << "\tanalytical product : " << prod << endl;
    *info << "\tFD product         : " << prod_fd << endl;
    *info << "\trelative error     : " << fabs(prod - prod_fd)
        /(fabs(prod) + kEpsilon) << endl;
    if (fabs(prod - prod_fd) > (fabs(prod) + kEpsilon)*epsilon_fd) {
      num_warn++;
      *info << "WARNING: tranpsosed-Jacobian-state-vector product " << endl;
      *info << "(or state equations) may be inaccurate." << endl;
    }
  }

  if (options_["verify.cnstr_jac"].as<bool>()) {
    // test the constraint-Jacobian-design product
    DesignVector u_d, v_d, w_d, x_d, y_d, z_d;
    PDEVector u_s, v_s, w_s, x_s, y_s, z_s;
    DualVector u_c, v_c, w_c, x_c, y_c, z_c;
    u_d.EqualsInitialDesign();
    u_s.EqualsPrimalSolution(u_d);
    z_d = 1.0;
    x_c.EqualsConstraints(u_d, u_s);
    v_c = Huge; // need this in case user failed to define product
    v_c.EqualsCnstrJacTimesVector(u_d, u_s, z_d);
    double product_norm = v_c.Norm2();
    double epsilon_fd = CalcEpsilon(u_d.Norm2(), z_d.Norm2());
    w_d = z_d;
    w_d *= epsilon_fd;
    w_d += u_d;
    y_c.EqualsConstraints(w_d, u_s);
    y_c -= x_c;
    y_c /= epsilon_fd;
    v_c -= y_c;
    double error = v_c.Norm2();
    *info << "=========================================================" << endl;
    *info << "Constraint-Jacobian-vector-product test: dC/d(design) * 1" << endl;
    *info << "\tFD perturbation    : " << epsilon_fd << endl;
    *info << "\tabsolute error     : " << error << endl;
    *info << "\trelative error     : " << error/product_norm << endl;
    if (error > epsilon_fd) {
      num_warn++;
      *info << "WARNING: Constraint-Jacobian-design-vector product "
            << "(or equality constraint) may be inaccurate." << endl;
    }
    
    // test transposed-constraint-Jacobian-design product
    z_c = 1.0;
    v_d = Huge; // need this in case user failed to define product
    v_d.EqualsTransCnstrJacTimesVector(u_d, u_s, z_c);
    double prod = InnerProd(v_d, z_d);
    double prod_fd = InnerProd(y_c, z_c);
    *info << "=========================================================" << endl;
    *info << "transposed-Constraint-Jacobian-vector-product test: "
          << "1^{T} * dC/d(design) * 1" << endl;
    *info << "\tFD perturbation    : " << epsilon_fd << endl;
    *info << "\tanalytical product : " << prod << endl;
    *info << "\tFD product         : " << prod_fd << endl;
    *info << "\trelative error     : " << fabs(prod - prod_fd)
        /(fabs(prod) + kEpsilon) << endl;
    if (fabs(prod - prod_fd) > (fabs(prod) + kEpsilon)*epsilon_fd) {
      num_warn++;
      *info << "WARNING: tranpsosed-Constraint-Jacobian-design-vector product "
            << endl << "(or equality constraints) may be inaccurate." << endl;
    }

    // test the constraint-Jacobian-state product
    z_s = 1.0;
    //x_c.EqualsConstraints(u_d, u_s); // x_c still hold this from earlier
    v_c = Huge; // need this in case user failed to define product
    v_c.EqualsCnstrJacTimesVector(u_d, u_s, z_s);
    product_norm = v_c.Norm2();
    epsilon_fd = CalcEpsilon(u_s.Norm2(), z_s.Norm2());
    w_s = z_s;
    w_s *= epsilon_fd;
    w_s += u_s;
    y_c.EqualsConstraints(u_d, w_s);
    y_c -= x_c;
    y_c /= epsilon_fd;
    v_c -= y_c;
    error = v_c.Norm2();
    *info << "=========================================================" << endl;
    *info << "Constraint-Jacobian-vector-product test: dC/d(state) * 1" << endl;
    *info << "\tFD perturbation    : " << epsilon_fd << endl;
    *info << "\tabsolute error     : " << error << endl;
    *info << "\trelative error     : " << error/product_norm << endl;
    if (error > epsilon_fd) {
      num_warn++;
      *info << "WARNING: Constraint-Jacobian-state-vector product "
            << "(or equality constraint) may be inaccurate." << endl;
    }

    // test transposed-constraint-Jacobian-state product
    v_s = Huge; // need this in case user failed to define product
    v_s.EqualsTransCnstrJacTimesVector(u_d, u_s, z_c);
    prod = InnerProd(v_s, z_s);
    prod_fd = InnerProd(y_c, z_c);
    *info << "=========================================================" << endl;
    *info << "transposed-Constraint-Jacobian-vector-product test: "
          << "1^{T} * dC/d(state) * 1" << endl;
    *info << "\tFD perturbation    : " << epsilon_fd << endl;
    *info << "\tanalytical product : " << prod << endl;
    *info << "\tFD product         : " << prod_fd << endl;
    *info << "\trelative error     : " << fabs(prod - prod_fd)
        /(fabs(prod) + kEpsilon) << endl;
    if (fabs(prod - prod_fd) > (fabs(prod) + kEpsilon)*epsilon_fd) {
      num_warn++;
      *info << "WARNING: tranpsosed-Constraint-Jacobian-state-vector product "
            << endl << "(or equality constraints) may be inaccurate." << endl;
    }
  }

  if (options_["verify.red_grad"].as<bool>()) {
    // test the reduced gradient (total derivative)
    DesignVector u_d, v_d, w_d, x_d, y_d, z_d;
    PDEVector u_s, v_s, w_s, x_s, y_s, z_s;
    u_d.EqualsInitialDesign();
    u_s.EqualsPrimalSolution(u_d);
    v_s.EqualsAdjointSolution(u_d, u_s);
    v_d.EqualsReducedGradient(u_d, u_s, v_s, w_d);
    z_d = 1.0;
    double prod = InnerProd(z_d, v_d);
    
    // compute the finite-difference approximation
    double J = ObjectiveValue(u_d, u_s);
    double epsilon_fd = CalcEpsilon(u_d.Norm2(), z_d.Norm2());
    w_d = z_d;
    w_d *= epsilon_fd;
    w_d += u_d;
    w_s.EqualsPrimalSolution(w_d);
    double Jp = ObjectiveValue(w_d, w_s);
    double prod_fd = (Jp - J)/epsilon_fd;
    *info << "=========================================================" << endl;
    *info << "reduced-gradient (total derivative) test: "
          << "dJ/d(design) * 1" << endl;
    *info << "\tFD perturbation    : " << epsilon_fd << endl;
    *info << "\tanalytical product : " << prod << endl;
    *info << "\tFD product         : " << prod_fd << endl;
    *info << "\trelative error     : " << fabs(prod - prod_fd)
        /(fabs(prod) + kEpsilon) << endl;
    if (fabs(prod - prod_fd) > (fabs(prod) + kEpsilon)*epsilon_fd) {
      num_warn++;
      *info << "WARNING: adjoint solve or objective gradients"
            << endl << "(or objective function) may be inaccurate." << endl;
    }
  }

  if (options_["verify.linear_solve"].as<bool>()) {
    // test that the forward and reverse linear solves are self consistent
    DesignVector design;
    PDEVector primal_sol, u, v, w, z;
    design.EqualsInitialDesign();
    primal_sol.EqualsPrimalSolution(design);
    u = 1.0;
    v = 1.0;
    double rel_tol = 1e-8;
    w.EqualsLinearizedSolution(design, primal_sol, u, rel_tol);
    double forward = InnerProd(v, w);
    z.EqualsAdjointSolution(design, primal_sol, v, rel_tol);
    double reverse = InnerProd(u, z);
    *info << "=========================================================" << endl;
    *info << "linear solver test" << endl;
    *info << "\tforward solve product : v^T (A^{-1} u) = " << forward << endl;
    *info << "\treverse solve product : u^T (A^{-T} v) = " << reverse << endl;
    *info << "\tdifference            : v^T (A^{-1}) u - u^T (A^{-T}) v "
          << forward - reverse << endl;
    if (fabs(forward - reverse) > (fabs(forward) + kEpsilon)*10.0*rel_tol) {
      num_warn++;
      *info << "WARNING: adjoint or linearized solve may be inaccurate." << endl;
    }
  }

  if (options_["verify.hessian_prod"].as<bool>()) {
    // compares the matrix-free Hessian-vector product with FD
    DesignVector u_d, v_d, w_d, x_d, y_d, z_d;
    PDEVector u_s, v_s, w_s, x_s, y_s, z_s;
    
    // evaluate the gradient at u_d = initial design
    u_d.EqualsInitialDesign();
    u_s.EqualsPrimalSolution(u_d);
    v_s.EqualsAdjointSolution(u_d, u_s);
    v_d.EqualsReducedGradient(u_d, u_s, v_s, y_d);

    // save the adjoint residual
    w_s.EqualsObjectiveGradient(u_d, u_s);
    x_s.EqualsTransJacobianTimesVector(u_d, u_s, v_s);
    w_s += x_s;

    // compute the matrix-free Hessian-vector product
    std::vector<DesignVector> design_work1(2, u_d);
    std::vector<PDEVector> pde_work1(5, u_s);
    ptree prod_param;
    prod_param.put("product_fac", 1.0);
    prod_param.put("bound_frozen", true);
    ReducedHessianProduct matvec(0);
    matvec.Initialize(u_d, u_s, v_s, v_d, w_s, design_work1, pde_work1,
                      prod_param, cout);
    
    matvec(v_d, z_d);
    double norm_matvec = z_d.Norm2();
    
    // compute the FD approximation to the product
    
    // evaluate the gradient at w_d = u_d + epsilon*v_d
    double epsilon_fd = 0.1; //CalcEpsilon(u_d.Norm2(), v_d.Norm2());
    w_d = v_d;
    w_d *= epsilon_fd;
    w_d += u_d;
    u_s.EqualsPrimalSolution(w_d);
    v_s.EqualsAdjointSolution(w_d, u_s);
    z_d.EqualsReducedGradient(w_d, u_s, v_s, y_d);

    // compute Hessian-vector product, and take norm
    z_d -= v_d;
    z_d /= epsilon_fd;
    double norm_fd = z_d.Norm2();

    *info << "=========================================================" << endl;
    *info << "Hessian-vector product test" << endl;
    *info << "\tUsing adjoints: " << norm_matvec << endl;
    *info << "\tUsing FD      : " << norm_fd << endl;
    *info << "\tdifference    : " << norm_matvec - norm_fd << endl;
    if (fabs(norm_matvec - norm_fd) > (fabs(norm_matvec) + kEpsilon)*0.01) {
      num_warn++;
      *info << "WARNING: Hessian-vector product may be inaccurate." << endl;
    }
  }
    
  *info << "Verifier::Solve(): passed all checks with " << num_warn
        << " warnings." << endl;
}
// ==============================================================================
} // namespace kona
