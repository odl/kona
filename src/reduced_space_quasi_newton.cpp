/**
 * \file reduced_space_quasi_newton.cpp
 * \brief definitions of ReducedSpaceQuasiNewton class member functions
 * \author Jason Hicken <jason.hicken@gmail.com>
 */

#include <fstream>
#include <boost/program_options.hpp>
#include <boost/format.hpp>
#include <boost/shared_ptr.hpp>
#include "reduced_space_quasi_newton.hpp"
#include "krylov.hpp"

namespace kona {

using std::string;
using std::ostream;
using std::ifstream;
using std::ofstream;
using std::ostringstream;
using std::ios;
using std::max;
using std::vector;
using std::map;

// ==============================================================================
void ReducedSpaceQuasiNewton::SetOptions(const po::variables_map& optns) {
  options_ = optns;
}
// ==============================================================================
void ReducedSpaceQuasiNewton::MemoryRequirements(int & num_design,
                                                 int & num_state,
                                                 int & num_dual) {
  // vectors required in Solve() method
  num_design = 6;
  num_state = 2;
  num_dual = 0;
  int design_req, state_req, dual_req;
  // Set the quasi-Newton method and find its requirements
  if (options_["quasi_newton.type"].as<string>() == "lbfgs") {
    quasi_newton_.reset(new GlobalizedBFGS
                        (options_["quasi_newton.max_stored"].as<int>()));
  } else {
    quasi_newton_.reset(new GlobalizedSR1
                       (options_["quasi_newton.max_stored"].as<int>()));
  }
  quasi_newton_->MemoryRequired(design_req);
  num_design += design_req;
  // Set the line search method and find its requirements
  const double alpha_init = options_["line_search.alpha_init"].as<double>();
  const double alpha_min = options_["line_search.alpha_min"].as<double>();
  const double alpha_max = options_["line_search.alpha_max"].as<double>();
  const double sufficient = options_["line_search.sufficient"].as<double>();
  const double reduct_fac = options_["line_search.reduct_fac"].as<double>();
  const double curv_cond = options_["line_search.curv_cond"].as<double>();
  const int max_iter = options_["line_search.max_iter"].as<double>();
  if (options_["line_search.type"].as<string>() == "wolfe") {
    line_search_.reset(new StrongWolfe
                       (alpha_init, alpha_max, sufficient, curv_cond, max_iter));
  } else {
    line_search_.reset(new BackTracking
                       (alpha_init, alpha_min, sufficient, reduct_fac));
  }
  line_search_->MemoryRequired(design_req, state_req, dual_req);
  num_design += design_req;
  num_state += state_req;
  num_dual += dual_req;
  // Find memory requirements of merit function (it is not allocated here)
  GlobalizedMerit::MemoryRequired(design_req, state_req, dual_req);
  num_design += design_req;
  num_state += state_req;
  num_dual += dual_req;
}
// ==============================================================================
void ReducedSpaceQuasiNewton::Solve() {  
  // open output files to save information and convergence histories
  ostream* info;
  ofstream info_file;
  if (options_["send_info_to_cout"].as<bool>())
    info = &std::cout;
  else {
    info_file.open(options_["info_file"].as<string>().c_str());
    info = &info_file;
  }
  ofstream hist_out(options_["hist_file"].as<string>().c_str());
  WriteHistoryHeader(hist_out);
  quasi_newton_->SetOutputStream(*info);
  line_search_->SetOutputStream(*info);

  DesignVector X; // the current solution of the reduced system
  DesignVector P;// the search direction
  DesignVector dJdX; // current reduced gradient
  DesignVector dJdX_old; // work vector
  PDEVector state; // the current solution of the primal PDE 
  PDEVector adjoint; // the current solution of the adjoint PDE

  // set initial guess and store initial design
  X.EqualsInitialDesign();
  DesignVector initial_design, design_work;
  initial_design = X;
  CurrentSolution(0, X, state, adjoint);

  //info << "Initial values:" << endl << "\t";
  //X.UpdateUser();
  
  // initialize the Quasi-Newton approximation
  quasi_newton_->SetInvHessianToIdentity();

  int iter = 0;
  int nonlinear_sum = 0;
  bool converged = false;
  double grad_norm0, grad_norm, grad_tol;
  double lambda = options_["inner.lambda_init"].as<double>();
  while (iter < options_["max_iter"].as<int>()) {
    iter++;

    *info << endl;
    *info << "==================================================" << endl;
    *info << "Beginning Homotopy loop number " << iter << endl;
    *info << "lambda = " << lambda << endl;
    *info << endl;

    // check for convergence
    state.EqualsPrimalSolution(X);
    adjoint.EqualsAdjointSolution(X, state);
    dJdX.EqualsReducedGradient(X, state, adjoint, design_work);
    if (iter == 1) {
      grad_norm0 = dJdX.Norm2();
      grad_norm = grad_norm0;
      quasi_newton_->set_norm_init(grad_norm0);
      *info << "grad_norm0  = " << grad_norm0 << endl;
      grad_tol = options_["des_tol"].as<double>()*grad_norm0; //max(grad_norm0, 1.e-6);
    } else {
      grad_norm = dJdX.Norm2();
      *info << "grad_norm : grad_norm_init = "
           << grad_norm << " : " << grad_norm0 << endl;
      
      // check for convergence
      if (grad_norm < grad_tol) {                               
        converged = true;
        break;
      }
    }

    // update lambda for BFGS method
    quasi_newton_->set_lambda(lambda);
#if 0
    if (options_["quasi_newton.type"].as<string>() == "lbfgs") {
      static_cast<GlobalizedBFGS*>(quasi_newton_)->set_lambda(lambda);
    } else {
      static_cast<GlobalizedSR1*>(quasi_newton_)->set_lambda(lambda);
    }
#endif
    
    int inner_iter = 0;
    bool inner_converged = false;
    double inner_grad_norm0, inner_grad_norm, inner_grad_tol;
    while (inner_iter < options_["inner.max_iter"].as<int>()) {
      inner_iter++;

      *info << "-------------------------" << endl;
      *info << "inner iteration = " << inner_iter << endl;

      // update the Quasi-Newton method if necessary
      if (inner_iter == 1) {
        dJdX_old = dJdX;
      } else {
        //state.EqualsPrimalSolution(X);
        //adjoint.EqualsAdjointSolution(X, state);
        dJdX.EqualsReducedGradient(X, state, adjoint, design_work);
        CurrentSolution(nonlinear_sum, X, state, adjoint);
        dJdX_old -= dJdX;
        dJdX_old *= -1.0;
        quasi_newton_->AddCorrection(P, dJdX_old);
        dJdX_old = dJdX;
      }
      
      // check for convergence of the un-globalized problem
      grad_norm = dJdX.Norm2();
      if (grad_norm < grad_tol) {                               
        inner_converged = true;
      }

      // evaluate the gradient of the globalized problem
      dJdX.EqualsAXPlusBY((1.0 - lambda), dJdX, lambda*grad_norm0, X);
      dJdX.EqualsAXPlusBY(1.0, dJdX, -lambda*grad_norm0, initial_design);
#if 0
      dJdX.EqualsAXPlusBY((1.0 - lambda), dJdX, lambda, X);
      dJdX.EqualsAXPlusBY(1.0, dJdX, -lambda, initial_design);
#endif
      // check for convergence of inner problem
      if (fabs(lambda) < kEpsilon) {
        // use outer convergence tolerance
        grad_norm = dJdX.Norm2();
        inner_grad_norm = grad_norm;
        *info << "grad_norm : grad_norm_init = "
             << grad_norm << " : " << grad_norm0 << endl;
        if (inner_grad_norm < grad_tol) {                               
          inner_converged = true;
        }
      } else {
        if (inner_iter == 1) {
          inner_grad_norm0 = dJdX.Norm2();
          inner_grad_norm = inner_grad_norm0;
          inner_grad_tol = options_["inner.des_tol"].as<double>()
              *inner_grad_norm0; //*max(inner_grad_norm0, 1.0);
          *info << "inner_grad_norm0  = " << inner_grad_norm0 << endl;
        } else {
          inner_grad_norm = dJdX.Norm2();
          *info << "inner_grad_norm : inner_grad_norm_init = "
               << inner_grad_norm << " : " << inner_grad_norm0 << endl;
          if (inner_grad_norm < inner_grad_tol) {
            inner_converged = true;
          }
        }
      }
      WriteHistoryIteration(iter, inner_iter, nonlinear_sum+1,
                            UserMemory::get_precond_count(), inner_grad_norm,
                            grad_norm, hist_out);
      if (inner_converged) break;
      
      // approximately invert the Hessian of the reduced problem
      quasi_newton_->ApplyInvHessianApprox(dJdX, P);
      P *= -1.0;

      // set-up the merit function and line search
      double p_dot_grad = InnerProd(P, dJdX);
      merit_.reset(new GlobalizedMerit(initial_design, lambda, grad_norm0, P, X,
                                       p_dot_grad, state, adjoint));
      line_search_->SetMeritFunction(*merit_);
      line_search_->SetSearchDotGrad(p_dot_grad);

      double alpha = line_search_->FindStepLength();
      X.EqualsAXPlusBY(1.0, X, alpha, P);
#if 0
      double alpha = 1.0;
      X.EqualsAXPlusBY(1.0, X, alpha, P);
#endif

      // state should be up-to-date, adjoint not
      //state.EqualsPrimalSolution(X);
      adjoint.EqualsAdjointSolution(X, state);
      
      // S = delta X = alpha * P is needed later
      P *= alpha;

      //X -= dX;
      nonlinear_sum++;
    } // inner iterations
    
    //X.UpdateUser();
    // update the continuation parameter
    lambda = max(0.0, lambda - 0.1);
    //lambda = max(0.0, lambda - 0.05);
    //lambda = max(0.0, lambda - 0.01);
    //lambda = 0.1*lambda;
    
  } // outer iterations
  *info << "Total number of nonlinear iterations: " << nonlinear_sum << endl;
}
// ==============================================================================
void ReducedSpaceQuasiNewton::WriteHistoryHeader(ostream& out) {
  if (!(out.good())) {
    cerr << "ReducedSpaceQuasiNewton::WriteHistoryHeader: "
	 << "ostream is not good for i/o operations." << endl;
    throw(-1);
  }
  out << "# Kona quasi-Newton convergence history file\n";
  boost::format col_head(string("%|7| %|9t| %|5| %|16t| %|11| %|29t|")+
                         string("%|14| %|45t| %|-17| %|64t| %|-12|\n"));
  col_head % "# outer" % "inner" % "total inner" % "precond. calls"
      % "global. grad norm" % "grad norm";
  out << col_head;
  out.flush();
}
// ==============================================================================
void ReducedSpaceQuasiNewton::WriteHistoryIteration(
    const int& outer, const int& inner, const int& total_inner,
    const int& precond_calls, const double& globalized_norm, const double& norm,
    ostream& out) {
  if (!(out.good())) {
    cerr << "ReducedSpaceQuasiNewton::WriteHistoryIteration: "
	 << "ostream is not good for i/o operations." << endl;
    throw(-1);
  }
  out << boost::format(string("%|7| %|9t| %|5| %|16t| %|11| %|29t|")+
                         string("%|14| %|45t| %|-17.6| %|64t| %|-12.6|\n"))
      % outer % inner % total_inner % precond_calls % globalized_norm % norm;
  out.flush();
}
// ==============================================================================
} // namespace kona
