/**
 * \file trust_region_rsnk.hpp
 * \brief header file for the TrustRegionRSNK class
 * \author Jason Hicken <jason.hicken@gmail.com>
 */

#pragma once

#include <boost/scoped_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/program_options.hpp>
#include "merit.hpp"
#include "quasi_newton.hpp"
#include "optimizer.hpp"
#include "krylov.hpp"

namespace kona {

using boost::scoped_ptr;
using boost::shared_ptr;
using boost::program_options::variables_map;

/*!
 * \class TrustRegionRSNK
 * \brief unconstrained trust-region optimization using RSNK
 */
class TrustRegionRSNK : public Optimizer {
 public:

  /*!
   * \brief default constructor
   */
  TrustRegionRSNK() {}
  
  /*!
   * \brief class destructor
   */
  ~TrustRegionRSNK() {}

  /*!
   * \brief defines the options needed by the algorithm
   * \param[in] optns - set of options
   */
  void SetOptions(const po::variables_map& optns);
  
  /*!
   * \brief determines how many user stored vectors are needed
   * \param[out] num_design - number of design-sized vectors needed
   * \param[out] num_state - number of state-sized vectors needed
   * \param[out] num_dual - number of dual-sized vectors needed
   */
  void MemoryRequirements(int & num_design, int & num_state,
                          int & num_dual);

  /*!
   * \brief attempts to solve the optimization problem
   */
  void Solve();

 protected:
  variables_map options_; ///< options for the algorithm
  scoped_ptr<IterativeSolver<DesignVector> >
  krylov_; ///< defines the Krylov iterative method
  shared_ptr<MatrixVectorProduct<DesignVector> >
  mat_vec_; ///< defines the Hessian-vector product
  shared_ptr<Preconditioner<DesignVector> >
  precond_; ///< defines the preconditioner
  scoped_ptr<QuasiNewton> quasi_newton_; ///< defines quasi-Newton method

 private:

  /*!
   * \brief writes header information to the main history file
   * \param[in,out] out - a valid stream to write the history to
   */
  void WriteHistoryHeader(ostream& out);

  /*!
   * \brief writes an history iteration to the main history file
   * \param[in] iter - current outer iteration
   * \param[in] precond_calls - number of user preconditioner calls
   * \param[in] norm - norm of the gradient
   * \param[in] rho - ratio of the actual to predicted objective reduction
   * \param[in] radius - current trust radius
   * \param[in,out] out - a valid stream to write the history to
   */
  void WriteHistoryIteration(const int& iter, const int& precond_calls,
                             const double& norm, const double& rho,
                             const double& radius, ostream& out);
};

} // namespace kona
