/**
 * \file reduced_space_newton_krylov.hpp
 * \brief header file for the ReducedSpaceNewtonKrylov class
 * \author Jason Hicken <jason.hicken@gmail.com>
 */

#pragma once

#include <boost/scoped_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/program_options.hpp>
#include "merit.hpp"
#include "line_search.hpp"
#include "quasi_newton.hpp"
#include "optimizer.hpp"
#include "krylov.hpp"

namespace kona {

using boost::scoped_ptr;
using boost::shared_ptr;
using boost::program_options::variables_map;

/*!
 * \class ReducedSpaceNewtonKrylov
 * \brief unconstrained optimization using Newton-Krylov in the reduced space
 */
class ReducedSpaceNewtonKrylov : public Optimizer {
 public:

  /*!
   * \brief default constructor
   */
  ReducedSpaceNewtonKrylov() {}
  
  /*!
   * \brief class destructor
   */
  ~ReducedSpaceNewtonKrylov() {}

  /*!
   * \brief defines the options needed by the algorithm
   * \param[in] optns - set of options
   */
  void SetOptions(const po::variables_map& optns);
  
  /*!
   * \brief determines how many user stored vectors are needed
   * \param[out] num_design - number of design-sized vectors needed
   * \param[out] num_state - number of state-sized vectors needed
   * \param[out] num_dual - number of dual-sized vectors needed
   */
  void MemoryRequirements(int & num_design, int & num_state,
                          int & num_dual);

  /*!
   * \brief attempts to solve the optimization problem
   */
  void Solve();

 protected:
  variables_map options_; ///< options for the algorithm
  scoped_ptr<IterativeSolver<DesignVector> >
  krylov_; ///< defines the Krylov iterative method
  shared_ptr<MatrixVectorProduct<DesignVector> >
  mat_vec_; ///< defines the Hessian-vector product
  scoped_ptr<Preconditioner<DesignVector> >
  precond_; ///< defines the preconditioner
  scoped_ptr<QuasiNewton> quasi_newton_; ///< defines quasi-Newton method
  scoped_ptr<LineSearch> line_search_; ///< defines line search
  scoped_ptr<MeritFunction> merit_; ///< defines the merit function

 private:

  /*!
   * \brief writes header information to the main history file
   * \param[in,out] out - a valid stream to write the history to
   */
  void WriteHistoryHeader(ostream& out);

  /*!
   * \brief writes an history iteration to the main history file
   * \param[in] outer - current outer iteration
   * \param[in] inner - current inner iteration within outer
   * \param[in] total_inner - sum of all inner iterations to this point
   * \param[in] precond_calls - number of user preconditioner calls
   * \param[in] globalized_norm - norm of the globalized gradient
   * \param[in] norm - norm of the gradient
   * \param[in,out] out - a valid stream to write the history to
   */
  void WriteHistoryIteration(const int& outer, const int& inner,
                             const int& total_inner, const int& precond_calls,
                             const double& globalized_norm, const double& norm,
                             ostream& out);
};

} // namespace kona
