/**
 * \file kkt_vector.hpp
 * \brief header file for the KKTVector and related classes
 * \author Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#pragma once

#include <boost/program_options.hpp>

#include "./vectors.hpp"
#include "./vector_operators.hpp"

namespace po = boost::program_options;

namespace kona {

// forward declarations
class DesignVector;
class PDEVector;
class KKTVector;
class QuasiNewton;
class LimitedMemoryBFGS;

// ======================================================================
/*!
 * \class KKTProduct
 * \brief specialization of matrix-vector product for KKTVectors
 */
class KKTProduct : public MatrixVectorProduct<KKTVector> {
 public:

  /*!
   * \brief class constructor that builds KKT product
   * \param[in] eval_at_vector - vector where KKT system is constructed
   * \param[in] initial_kkt - initial value of KKT vector
   *
   * The KKT matrix-vector products are formed using finite-differencing
   * here; If \f$x\f$ denotes the current iterative solution to the KKT 
   * system, and \f$h(x)\f$ denotes the current value of the KKT residual
   * then the Jacobian-vector products are approximated by
   \f[
   v \equiv K u \approx \frac{h(x + epsilon u) - h(x)}{\epsilon}.
   \f]
   * where epsilon is the size of the perturbation (depends on u).
   * This approximation may not work well for poorly scaled problems.
   */
  KKTProduct(KKTVector* eval_at_vector, KKTVector* initial_kkt);

  ~KKTProduct() {} ///< class destructor

  /*!
   * \brief operator that defines KKT matrix-vector product
   * \param[in] u - KKTVector that is being multiplied by the Jacobian
   * \param[out] v - KKTVector that is the result of the product
   */
  void operator()(const KKTVector & u, KKTVector & v);

 private:
  KKTVector* eval_at_; ///< KKT vector where system Jacobian is evaluated
  double eval_at_norm_; ///< L2 norm of vector (*eval_at_)
  KKTVector* initial_residual_; ///< initial value of KKT system residual
};

// ======================================================================
/*!
 * \class GlobalizedKKTProduct
 * \brief specialization of matrix-vector product for KKTVectors
 */
class GlobalizedKKTProduct : public MatrixVectorProduct<KKTVector> {
 public:

  /*!
   * \brief class constructor that builds a globalized-KKT product
   * \param[in] eval_at_vector - vector where KKT system is constructed
   * \param[in] initial_kkt - initial value of globalized KKT vector
   * \param[in] lambda - homotopy/continuation parameter
   * \param[in] design - initial design vector
   * \param[in] scale - a scaling for the perturbed problem
   * \param[in] design_scale - row scaling for design equations
   * \param[in] state_scale - row scaling for state equations
   * \param[in] adjoint_scale - row scaling for adjoint equations
   *
   * The globalized-KKT matrix-vector products are formed using
   * finite-differencing here; If \f$x\f$ denotes the current
   * iterative solution to the globalized-KKT system, and \f$h(x)\f$
   * denotes the current value of the globalized-KKT residual then the
   * Jacobian-vector products are approximated by
   \f[
   v \equiv K u \approx \frac{h(x + epsilon u) - h(x)}{\epsilon}.
   \f]
   * where epsilon is the size of the perturbation (depends on u).
   * This approximation may not work well for poorly scaled problems.
   */
  GlobalizedKKTProduct(KKTVector* eval_at_vector, KKTVector* initial_kkt, 
                       const double & lambda, DesignVector* design,
                       const double & scale, const double design_scale = 1.0,
                       const double state_scale = 1.0,
                       const double adjoint_scale = 1.0);

  ~GlobalizedKKTProduct() {} ///< class destructor

  /*!
   * \brief operator that defines a globalized-KKT matrix-vector product
   * \param[in] u - KKTVector that is being multiplied by the Jacobian
   * \param[out] v - KKTVector that is the result of the product
   */
  void operator()(const KKTVector & u, KKTVector & v);

 private:
  KKTVector* eval_at_; ///< KKT vector where system Jacobian is evaluated
  double eval_at_norm_; ///< L2 norm of the vector (*eval_at_)
  KKTVector* initial_residual_; ///< initial globalized system residual
  double lambda_param_; ///< globalization/continuation parameter
  DesignVector* init_design_; ///< initial value of the design
  double scale_; ///< scaling factor for inverse design problem
  double design_scale_; ///< row scaling for design equations
  double state_scale_; ///< row scaling for state equations
  double adjoint_scale_; ///< row scaling for adjoint equations
};

// ======================================================================
/*!
 * \class RegularizedKKTProduct
 * \brief specialization of matrix-vector product for inexact-Newton method
 */
class RegularizedKKTProduct : public MatrixVectorProduct<KKTVector> {
 public:

  /*!
   * \brief class constructor that builds a regularized KKT product
   * \param[in] eval_at_vector - vector where KKT system is constructed
   * \param[in] initial_kkt - initial value of globalized KKT vector
   * \param[in] lambda - homotopy/continuation parameter
   *
   * The regularized KKT matrix-vector products are formed using
   * finite-differencing here; If \f$x\f$ denotes the current iterative solution
   * to the KKT system, and \f$h(x)\f$ denotes the current value of the KKT
   * residual then the Jacobian-vector products are approximated by
   \f[
   v \equiv K u \approx \lambda u + \frac{h(x + epsilon u) - h(x)}{\epsilon}.
   \f]
   * where epsilon is the size of the perturbation (depends on u).
   * This approximation may not work well for poorly scaled problems.
   */
  RegularizedKKTProduct(KKTVector* eval_at_vector, KKTVector* initial_kkt, 
                        const double & lambda);

  ~RegularizedKKTProduct() {} ///< class destructor

  /*!
   * \brief operator that defines a regularized KKT matrix-vector product
   * \param[in] u - KKTVector that is being multiplied by the Jacobian
   * \param[out] v - KKTVector that is the result of the product
   */
  void operator()(const KKTVector & u, KKTVector & v);

 private:
  KKTVector* eval_at_; ///< KKT vector where system Jacobian is evaluated
  double eval_at_norm_; ///< L2 norm of the vector (*eval_at_)
  KKTVector* initial_residual_; ///< initial globalized system residual
  double lambda_param_; ///< globalization/continuation parameter
};

// ======================================================================
/*!
 * \class KKTPreconditioner
 * \brief specialization of preconditioners for KKTVectors
 *
 * Implements the LNKS quasi-Newton-based preconditioner
 */
class KKTPreconditioner : public Preconditioner<KKTVector> {
 public:

  /*!
   * \brief class constructor that builds preconditioner at given vector
   * \param[in] eval_at_vector - vector that defines the preconditioner
   * \param[in] quasi_newton - Quasi-Newton component of preconditioner
   */
  KKTPreconditioner(KKTVector* eval_at_vector,
                    QuasiNewton* quasi_newton);

  ~KKTPreconditioner() {} ///< class destructor

  /*!
   * \brief operator that defines KKT preconditioner
   * \param[in] u - KKTVector that is being preconditioned
   * \param[out] v - KKTVector that is the result of the operation
   */
  void operator()(KKTVector & u, KKTVector & v);

 private:
  KKTVector* eval_at_; ///< KKT vector where preconditioner is evaluated  
  QuasiNewton* precond_design_; ///< preconditioner for design
  StatePreconditioner precond_state_; ///< preconditioner for state
  AdjointPreconditioner precond_adjoint_; ///< preconditioner for adjoint
};

// ======================================================================
/*!
 * \class KKTVector
 * \brief represents the complete vector of unknowns in the KKT system
 *
 * A KKTVector object represents a column vector of all the unknowns
 * in the KKT system: the design/control variables, the state
 * variables, the adjoint/costate variables, slacks, and Lagrange
 * multipliers.  It is implemented as a container of separate vectors.
 */
class KKTVector {
 public:

  /*!
   * \brief default constructor
   */
  explicit KKTVector() {}

  /*!
   * \brief default destructor
   */
  ~KKTVector() {}

  /*!
   * \brief copy constructor with deep copy
   * \param[in] x - KKTVector being copied to calling object
   */
  KKTVector(const KKTVector & x) :
      design_(x.design_), state_(x.state_), adjoint_(x.adjoint_) {}

  /*!
   * \brief constructor based on assembling individual pieces
   * \param[in] design - vector assigned to design component
   * \param[in] state - vector assigned to state component
   * \param[in] adjoint - vector assigned to adjoint component
   */
  explicit KKTVector(const DesignVector & design, const PDEVector & state,
                     const PDEVector & adjoint) :
      design_(design), state_(state), adjoint_(adjoint) {}

  /*!
   * \brief returns a const reference to the design_ component of KKTVector
   */
  const DesignVector & get_design() const;

  /*!
   * \brief returns a const reference to the state_ component of KKTVector
   */
  const PDEVector & get_state() const;

  /*!
   * \brief returns a const reference to the adjoint_ component of KKTVector
   */
  const PDEVector & get_adjoint() const;

  /*!
   * \brief sets the design_ component of KKTVector
   */
  void set_design(const DesignVector & design);

  /*!
   * \brief sets the state_ component of KKTVector
   */
  void set_state(const PDEVector & state);

  /*!
   * \brief sets the adjoint_ component of KKTVector
   */
  void set_adjoint(const PDEVector & adjoint);
  
  /*!
   * \brief KKTVector=KKTVector assignment operator with deep copy
   * \param[in] x - KKTVector value being assigned
   */
  KKTVector & operator=(const KKTVector & x);

  /*!
   * \brief KKTVector=double assignment operator
   * \param[in] val - value assigned to each element of KKTVector
   */
  KKTVector & operator=(const double & val);

  /*!
   * \brief compound addition-assignment operator
   * \param[in] x - KKTVector being added to calling object
   */
  KKTVector & operator+=(const KKTVector & x);

  /*!
   * \brief compound subtraction-assignment operator
   * \param[in] x - KKTVector being subtracted from calling object
   */
  KKTVector & operator-=(const KKTVector & x);

  /*!
   * \brief compound scalar multiplication-assignment operator
   * \param[in] val - value to multiply calling object by
   */
  KKTVector & operator*=(const double & val);

  /*!
   * \brief compound scalar division-assignment operator
   * \param[in] val - value to divide calling object by
   */
  KKTVector & operator/=(const double & val);

  /*!
   * \brief set to a general linear combination of two KKTVectors
   * \param[in] a - scalar factor for x
   * \param[in] x - first KKTVector in linear combination
   * \param[in] b - scalar factor for y
   * \param[in] y - second KKTVector in linear combination
   */
  void EqualsAXPlusBY(const double & a, const KKTVector & x,
                      const double & b, const KKTVector & y);

  /*!
   * \brief the L2 norm of the KKTVector
   */
  double Norm2() const;

  /*!
   * \brief inner product between two KKTVectors
   * \param[in] x - first KKTVector in inner product
   * \param[in] y - second KKTVector in inner product
   */
  friend double InnerProd(const KKTVector & x, const KKTVector & y);

  /*!
   * \brief determines appropriate values for the initial KKT solution
   */
  void EqualsInitialGuess();

  /*!
   * \brief solve for state_ and adjoint_ based on the design_
   */
  void solveForPrimalAndAdjoint();
  
  /*!
   * \brief sets the calling KKTVector to the KKT equations
   * \param[in] x - where the KKT equations are evaluated
   */
  void EqualsKKTConditions(const KKTVector & x);

  /*!
   * \brief computes the KKT linear residual norms
   * \param[in] dX - a KKT vector, typically a search direction
   * \param[in] Res - the KKT equation vector at the current iteration
   * \param[in] mat_vec - a matrix-vector product for the KKT system
   * \param[out] primal_norm - the primal residual (design + state) L2 norm
   * \param[out] adjoint_norm - the adjoint residual L2 norm
   * 
   * On exist, the calling vector is equal to the KKT system linear residual
   */
  void CalcLinearPrimalDualNorms(const KKTVector & dX, const KKTVector & Res, 
                                 MatrixVectorProduct<KKTVector> & mat_vec,
                                 double & primal_norm, double & adjoint_norm);

  /*!
   * \brief computes v^T H v, where H is the Hessian and v is the calling vector
   * \param[in] mat_vec - matrix-vector product operator that defines Hessian
   * \param[out] work1 - KKTVector work space
   * \param[out] work2 - KKTVector work space
   */
  double HessianProductCheck(MatrixVectorProduct<KKTVector> & mat_vec,
                             KKTVector & work1, KKTVector & work2) const;

  /*!
   * \brief Sufficient Merit function Approximation Reduction Termination Tests
   * \param[in] opts - set of options that define parameters for SMART tests
   * \param[in] X - the current solution
   * \param[in] Res - the current nonlinear residual
   * \param[in] mu - the merit function penalty parameter
   * \param[in] mu_old - the previous value of the merit penalty parameter
   * \param[in] primal_norm - the primal part of the linear residual L2 norm
   * \param[in] adjoint_norm - the adjoint part of the linear residual L2 norm
   * \param[in] dT_W_d - the product dX^T*W*dX, where W is the approx. Hessian
   * \param[out] work - work space
   * \param[out] term1 - the result of Termination Test 1
   * \param[out] term2 - the result of Termination Test 2
   * \param[out] modify_hess - true if Hessian Modification Strategy satisfied
   * \param[out] mu_trial - trial value if penalty value must be adjusted
   *
   * The SMART tests are applied to the calling KKTVector, which is assumed to
   * be a search direction computed from a linear (inexact) solve
   */
  void SMARTTests(const po::variables_map & opts, const KKTVector & X,
                  const KKTVector & Res, const double & mu,
                  const double & mu_old, const double & primal_norm,
                  const double & adjoint_norm, const double & dT_W_d,
                  KKTVector & work, bool & term1, bool & term2,
                  bool & modify_hess, double & mu_trial) const;

  /*!
   * \brief returns the L2 norm of the design, state and adjoint vectors
   * \param[out] design_norm - L2 norm of design_ component
   * \param[out] primal_norm - L2 norm of state_ component
   * \param[out] adjoint_norm - L2 norm of adjoint_component
   */
  void CalcNorms(double & design_norm, double & primal_norm,
                 double & adjoint_norm) const;

  /*!
   * \brief check the inner convergence criteria (first-order, etc)
   * \param[out] norms - current value of the norms
   * \param[in] design_init - initial L2 norm of design component
   * \param[in] design_tol - tolerance for the design component
   * \param[in] primal_init - initial L2 norm of PDE residual
   * \param[in] primal_tol - tolerance for the PDE constraints
   * \param[in] adjoint_init - initial L2 norm of the adjoint PDE residual
   * \param[in] adjoint_tol - tolerance for the adjoint equations
   * \result true if all conergence criteria are are met
   */
  bool InnerConverged(vector<double> & norms,
      const double & design_init, const double & design_tol,
      const double & primal_init, const double & primal_tol,
      const double & adjoint_init, const double & adjoint_tol) const;

 /*!
   * \brief check the outer convergence criteria (first-order, etc)
   * \param[out] norms - current value of the norms
   * \param[in] design_init - initial L2 norm of design component
   * \param[in] design_tol - tolerance for the design component
   * \param[in] primal_init - initial L2 norm of PDE residual
   * \param[in] primal_tol - tolerance for the PDE constraints
   * \param[in] adjoint_init - initial L2 norm of the adjoint PDE residual
   * \param[in] adjoint_tol - tolerance for the adjoint equations
   * \result true if all conergence criteria are are met
   */
  bool OuterConverged(vector<double> & norms,
      const double & design_init, const double & design_tol,
      const double & primal_init, const double & primal_tol,
      const double & adjoint_init, const double & adjoint_tol) const;
    
 /*!
   * \brief check the outer convergence criteria for the inexact-Newton method
   * \param[out] norms - current value of the norms
   * \param[in] design_tol - tolerance for the design component
   * \param[in] adjoint_tol - tolerance for the state equations
   * \result true if all conergence criteria are are met
   */
  bool InexactConverged(vector<double> & norms, const double & design_tol,
                        const double & adjoint_tol) const;

  /*!
   * \brief adds the globalization to KKT residual
   * \param[in] x - where the KKT equations are evaluated
   * \param[in] lambda - continuation parameter
   * \param[in] init_design - initial value of the design vector
   * \param[in] init_norm - initial L2 norm of the design gradient
   */
  void AddGlobalization(const KKTVector & x, const double & lambda,
                        const DesignVector & init_design,
                        const double & init_norm);

  /*!
   * \brief let user know current solution
   * \param[in] iter - current nonlinear iteration
   */
  void UpdateUser(const int & iter = -1) const;

 private:

  /// elements correpsonding to the design/control variables
  DesignVector design_;

  /// elements corresponding to the state variables
  PDEVector state_;

  /// elements corresponding to the adjoint variables
  PDEVector adjoint_;

  friend class GlobalizedKKTProduct;
  friend class RegularizedKKTProduct;
  friend class KKTPreconditioner;
};

} // namespace kona
