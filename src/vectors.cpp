/**
 * \file vectors.cpp
 * \brief definitions of members of various vector objects and friends
 * \author Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

//#include <boost/scoped_ptr.hpp>

#include "./vectors.hpp"
#include "./vector_operators.hpp"
#include "./quasi_newton.hpp"

namespace kona {

// ==============================================================================
void DesignVector::Convert(const DualVector & dual) {
  this->RestrictToTarget(); // in case user does not zero design
  UserMemory::ConvertVec(design, dual.index_, index_);
}
// ==============================================================================
void DesignVector::EqualsObjectiveGradient(
    const DesignVector & eval_at_design,
    const PDEVector & eval_at_state) {
  UserMemory::ObjectiveGradient(design, index_, eval_at_design.index_, 
                                eval_at_state.index_);
}
// ==============================================================================
void DesignVector::EqualsReducedGradient(
    const DesignVector & eval_at_design, const PDEVector & eval_at_state,
    const PDEVector & eval_at_adjoint, DesignVector & design_wrk) {
  design_wrk.EqualsObjectiveGradient(eval_at_design, eval_at_state);
  this->EqualsTransJacobianTimesVector(eval_at_design, eval_at_state,
                                       eval_at_adjoint);
  *this += design_wrk;
}
// ==============================================================================
void DesignVector::EqualsLagrangianReducedGradient(
    const DesignVector & eval_at_design, const DualVector & eval_at_dual,
    const PDEVector & eval_at_state, const PDEVector & eval_at_adjoint,
    DesignVector & design_wrk) {
  design_wrk.EqualsObjectiveGradient(eval_at_design, eval_at_state);
  this->EqualsTransJacobianTimesVector(eval_at_design, eval_at_state,
                                       eval_at_adjoint);
  *this += design_wrk;
  design_wrk.EqualsTransCnstrJacTimesVector(eval_at_design, eval_at_state,
                                            eval_at_dual);
  *this += design_wrk;
}
// ==============================================================================
void DesignVector::EqualsTransJacobianTimesVector(
    const DesignVector & eval_at_design, const PDEVector & eval_at_state,
    const PDEVector & vector) {
  UserMemory::JacobianOperation(tjacvec_d, eval_at_design.index_,
                                eval_at_state.index_, vector.index_,
                                index_);
}
// ==============================================================================
void DesignVector::EqualsTransCnstrJacTimesVector(
    const DesignVector & eval_at_design, const PDEVector & eval_at_state,
    const DualVector & vector) {
  UserMemory::ConstraintJacobianOperation(tceqjac_d, eval_at_design.index_,
                                          eval_at_state.index_, vector.index_,
                                          index_);
}
// ==============================================================================
double ObjectiveValue(const DesignVector & eval_at_design) {
  return UserMemory::EvaluateObjective(eval_at_design.index_, -1);
}
// ==============================================================================
void PDEVector::EqualsAugLagrangianAdjointSolution(
    const DesignVector & solve_at_design, const PDEVector & solve_at_state,
    const DualVector & dual_plus_ceq, PDEVector & pde_work,
    const int & krylov_size) {
  // build rhs for adjoint equation
  this->EqualsTransCnstrJacTimesVector(solve_at_design, solve_at_state,
                                       dual_plus_ceq);
  pde_work.EqualsObjectiveGradient(solve_at_design, solve_at_state);
  pde_work.EqualsAXPlusBY(-1.0, pde_work, -1.0, *this);
  
  // JEH TODO: this needs to be given to the user to do
  MatrixVectorProduct<PDEVector>* adjoint_matvec =
      new JacobianTAdjointProduct(solve_at_design, solve_at_state);
  Preconditioner<PDEVector>* adjoint_precond =
      new AdjointPreconditioner(solve_at_design, solve_at_state);
  double rel_tol = 1e-8;
  int iters;
  *this = 0.0;
  FGMRES<PDEVector>(krylov_size, rel_tol, pde_work, *this,
                    *adjoint_matvec, *adjoint_precond, iters);
  delete adjoint_matvec;
  delete adjoint_precond;
}
// ==============================================================================
void PDEVector::EqualsObjectiveGradient(
    const DesignVector & eval_at_design,
    const PDEVector & eval_at_state) {
  UserMemory::ObjectiveGradient(pde, index_, eval_at_design.index_, 
                                eval_at_state.index_);
}
// ==============================================================================
void PDEVector::EqualsJacobianTimesVector(
    const DesignVector & eval_at_design, const PDEVector & eval_at_state,
    const DesignVector & vector) {
  UserMemory::JacobianOperation(jacvec_d, eval_at_design.index_,
                                eval_at_state.index_, vector.index_,
                                index_);
}
// ==============================================================================
void PDEVector::EqualsJacobianTimesVector(
    const DesignVector & eval_at_design, const PDEVector & eval_at_state,
    const PDEVector & vector) {
  UserMemory::JacobianOperation(jacvec_s, eval_at_design.index_,
                                eval_at_state.index_, vector.index_,
                                index_);
}
// ==============================================================================
void PDEVector::EqualsTransJacobianTimesVector(
    const DesignVector & eval_at_design, const PDEVector & eval_at_state,
    const PDEVector & vector) {
  UserMemory::JacobianOperation(tjacvec_s, eval_at_design.index_,
                                eval_at_state.index_, vector.index_,
                                index_);
}
// ==============================================================================
void PDEVector::EqualsTransCnstrJacTimesVector(
    const DesignVector & eval_at_design, const PDEVector & eval_at_state,
    const DualVector & vector) {
  UserMemory::ConstraintJacobianOperation(tceqjac_s, eval_at_design.index_,
                                          eval_at_state.index_, vector.index_,
                                          index_);
}
// ==============================================================================
void DualVector::Convert(const DesignVector & design) {
  UserMemory::ConvertVec(dual, design.index_, index_);
}
// ==============================================================================
void DualVector::EqualsConstraints(const DesignVector & eval_at_design,
                                   const PDEVector & eval_at_state) {
  UserMemory::EvaluateConstraints(index_, eval_at_design.index_,
                                  eval_at_state.index_);
}
// ==============================================================================
void DualVector::EqualsCnstrJacTimesVector(const DesignVector & eval_at_design,
                                           const PDEVector & eval_at_state,
                                           const DesignVector & vector) {
  UserMemory::ConstraintJacobianOperation(ceqjac_d, eval_at_design.index_,
                                          eval_at_state.index_, vector.index_,
                                          index_);
}
// ==============================================================================
void DualVector::EqualsCnstrJacTimesVector(const DesignVector & eval_at_design,
                                           const PDEVector & eval_at_state,
                                           const PDEVector & vector) {
  UserMemory::ConstraintJacobianOperation(ceqjac_s, eval_at_design.index_,
                                          eval_at_state.index_, vector.index_,
                                          index_);
}
// ==============================================================================
double ObjectiveValue(const DesignVector & eval_at_design,
                      const PDEVector & eval_at_state) {
  return UserMemory::EvaluateObjective(eval_at_design.index_,
                                       eval_at_state.index_);
}
// ==============================================================================
double AugmentedLagrangian(const DesignVector & eval_at_design,
                           const DualVector & eval_at_dual,
                           const PDEVector & eval_at_state,
                           const DualVector & ceq, const double & mu) {
  double AugLag = ObjectiveValue(eval_at_design, eval_at_state);      
  AugLag += InnerProd(eval_at_dual, ceq);
  AugLag += 0.5*mu*InnerProd(ceq, ceq);
  return AugLag;
}
// ==============================================================================
void CurrentSolution(const int & iter,
                     const DesignVector & current_design,
                     const PDEVector & current_state,
                     const PDEVector & current_adjoint) {
  UserMemory::CurrentSolution(iter, current_design.index_,
                              current_state.index_,
                              current_adjoint.index_);
}
// ==============================================================================
void CurrentSolution(const int & iter,
                     const DesignVector & current_design,
                     const PDEVector & current_state,
                     const PDEVector & current_adjoint,
                     const DualVector & current_dual) {
  UserMemory::CurrentSolution(iter, current_design.index_, current_state.index_,
                              current_adjoint.index_, current_dual.index_);
}
// ==============================================================================
} // namespace kona
