/**
 * \file equality_constrained_rsnk.cpp
 * \brief definitions of EqualityConstrainedRSNK class member functions
 * \author Jason Hicken <jason.hicken@gmail.com>
 */

#include <fstream>
#include <boost/program_options.hpp>
#include <boost/format.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/shared_array.hpp>
#include <boost/pointer_cast.hpp>
//#include <boost/iostreams/stream.hpp>
//#include <boost/iostreams/null.hpp>
#include "equality_constrained_rsnk.hpp"
#include "krylov.hpp"

namespace kona {

using std::string;
using std::ostream;
using std::ifstream;
using std::ofstream;
using std::ostringstream;
using std::ios;
using std::max;
using std::vector;
using std::map;

// ==============================================================================
void EqualityConstrainedRSNK::SetOptions(const po::variables_map& optns) {
  options_ = optns;
}
// ==============================================================================
void EqualityConstrainedRSNK::MemoryRequirements(int & num_design,
                                                  int & num_state,
                                                  int & num_dual) {
  // vectors required in Solve() method
  int num_kkt = 5;
  num_design = 6;
  num_state = 9;
  num_dual = 1;
  int design_req, state_req, dual_req;

  ptree num_required;
  //comp_step_solver_.reset(new SNAPSSolver<ReducedKKTVector,DesignVector,DualVector>());
  //comp_step_solver_.reset(new FISQPSolver<ReducedKKTVector,DesignVector,DualVector>());
#ifdef FLECS
  // Set the Krylov method for the composite step and find its requirements
  comp_step_solver_.reset(new FLECSSolver<ReducedKKTVector,DesignVector,DualVector>());
  comp_step_solver_->SubspaceSize(options_["krylov.space_size"].as<int>());
  // not all methods define num_primal and num_dual, so initialize them to 0
  num_required.put("num_primal", 0);
  num_required.put("num_dual", 0);
  comp_step_solver_->MemoryRequired(num_required);
  num_design += num_required.get<int>("num_vec");
  num_design += num_required.get<int>("num_primal");
  num_dual += num_required.get<int>("num_vec");
  num_dual += num_required.get<int>("num_dual");
#else
  // Set the Krylov solver for the tangential step and find its requirements
  //tang_step_solver_.reset(new FITRSolver<DesignVector>());
  tang_step_solver_.reset(new STCGSolver<DesignVector>());
  tang_step_solver_->SubspaceSize(options_["krylov.space_size"].as<int>());
  // not all methods define num_primal and num_dual, so initialize them to 0
  num_required.put("num_primal", 0);
  num_required.put("num_dual", 0);
  tang_step_solver_->MemoryRequired(num_required);
  num_design += num_required.get<int>("num_vec");
  num_design += num_required.get<int>("num_primal");
  num_dual += num_required.get<int>("num_dual");
  // Set the Krylov solver for the normal step and find is requirements
  normal_step_solver_.reset(new FGMRESSolver<ReducedKKTVector>());
  //normal_step_solver_.reset(new BPDSolver<ReducedKKTVector,DesignVector,DualVector>());
  //normal_step_solver_.reset(new FISQPSolver<ReducedKKTVector,DesignVector,DualVector>());
  normal_step_solver_->SubspaceSize(options_["krylov.space_size"].as<int>());
  // not all methods define num_primal and num_dual, so initialize them to 0
  num_required.put("num_primal", 0);
  num_required.put("num_dual", 0);
  normal_step_solver_->MemoryRequired(num_required);
  num_kkt += num_required.get<int>("num_vec");
  num_design += num_required.get<int>("num_primal");
  num_dual += num_required.get<int>("num_dual");
#endif
  
  // Set the quasi-Newton method and find its requirements
  if (options_["quasi_newton.type"].as<string>() == "lbfgs") {
    quasi_newton_.reset(new GlobalizedBFGS
                        (options_["quasi_newton.max_stored"].as<int>()));
  } else {
    quasi_newton_.reset(new GlobalizedSR1
                       (options_["quasi_newton.max_stored"].as<int>()));
  }
  quasi_newton_->MemoryRequired(design_req);
  num_design += design_req;

  // Create the filter
  filter_.reset(new Filter);
  
  // Set the line search method and find its requirements
  const double alpha_init = options_["line_search.alpha_init"].as<double>();
  const double alpha_min = options_["line_search.alpha_min"].as<double>();
  const double sufficient = options_["line_search.sufficient"].as<double>();
  const double reduct_fac = options_["line_search.reduct_fac"].as<double>();
  const int max_iter = options_["line_search.max_iter"].as<double>();
  if (options_["line_search.type"].as<string>() == "backtrack") {
    line_search_.reset(new BackTracking
                       (alpha_init, alpha_min, sufficient, reduct_fac));
  } else {
    cerr << "EqualityConstrainedRSNK::MemoryRequirements(): "
         << "invalid line-search method for this optimizer: "
         << options_["line_search.type"].as<string>() << endl;
  }
  line_search_->MemoryRequired(design_req, state_req, dual_req);
  num_design += design_req;
  num_state += state_req;
  num_dual += dual_req;

  // Find memory requirements of merit function (it is not allocated here)
  ReducedSpaceL2Merit::MemoryRequired(design_req, state_req, dual_req);
  num_design += design_req;
  num_state += state_req;
  num_dual += dual_req;

#ifdef FLECS
  // set the KKT-matrix-vector product and find its requirements
  mat_vec_.reset(new ReducedKKTProduct());
  num_required.clear();
  mat_vec_->MemoryRequired(num_required);
  num_design += num_required.get<int>("design");
  num_state += num_required.get<int>("state");
  num_dual += num_required.get<int>("dual");
#else
  // Set the Hessian-vector product and find its requirements
  hess_mat_vec_.reset(new LagrangianHessianProduct());
  num_required.clear();
  hess_mat_vec_->MemoryRequired(num_required);
  num_design += num_required.get<int>("design");
  num_state += num_required.get<int>("state");
  num_dual += num_required.get<int>("dual");
  // Set the augmented-matrix-vector product and find its requirements
  aug_mat_vec_.reset(new ReducedAugmentedProduct());
  num_required.clear();
  aug_mat_vec_->MemoryRequired(num_required);
  num_design += num_required.get<int>("design");
  num_state += num_required.get<int>("state");
  num_dual += num_required.get<int>("dual");
  // Set the tangential system preconditioner and find its requirments
#if 1
  if (options_["reduced.precond"].as<string>() == "idf_schur")
    tang_precond_.reset(new IDFProjectionPreconditioner
                        (options_["reduced.krylov_size"].as<int>()));
  else
#endif
  tang_precond_.reset(new ProjectionPreconditioner
                      (options_["reduced.krylov_size"].as<int>()));  
  num_required.clear();
  num_required.put("design", 0);
  num_required.put("state", 0);
  num_required.put("dual", 0);
  tang_precond_->MemoryRequired(num_required);
  num_design += num_required.get<int>("num_vec");
  num_design += num_required.get<int>("design");
  num_state += num_required.get<int>("state");
  num_dual += num_required.get<int>("dual");
#endif
  
  // Set the augmented system preconditioner and find its requirements
  if (options_["reduced.precond"].as<string>() == "none")
    aug_precond_.reset(new IdentityPreconditioner<ReducedKKTVector>());
  else if (options_["reduced.precond"].as<string>() == "idf_schur")
    aug_precond_.reset(new ReducedSchurPreconditioner
                   (options_["reduced.krylov_size"].as<int>()));
  else if (options_["reduced.precond"].as<string>() == "quasi_newton")
    aug_precond_.reset(new ReducedKKTPreconditioner(quasi_newton_.get()));
  else if (options_["reduced.precond"].as<string>() == "nested")
    aug_precond_.reset(new NestedKKTPreconditioner
                       (options_["reduced.krylov_size"].as<int>()));
  else {
    cerr << "EqualityConstrainedRSNK::MemoryRequirements(): "
         << "invalid preconditioner for augmented system: "
         << options_["reduced.precond"].as<string>() << endl;
    throw(-1);
  }
  num_required.clear();
  num_required.put("design", 0);
  num_required.put("state", 0);
  num_required.put("dual", 0);
  aug_precond_->MemoryRequired(num_required);
  num_kkt += num_required.get<int>("num_vec");
  num_design += num_required.get<int>("design");
  num_state += num_required.get<int>("state");
  num_dual += num_required.get<int>("dual");
  
  num_design += num_kkt;
  num_dual += num_kkt;
}
// ==============================================================================
#ifdef FLECS
void EqualityConstrainedRSNK::Solve() {
  // open output files to save information and convergence histories
  ostream* info;
  ofstream info_file, hist_out, krylov_out, reduced_out;
  int myrank = UserMemory::GetRank();
  if (myrank == 0) {
    if (options_["send_info_to_cout"].as<bool>())
      info = &std::cout;
    else {
      info_file.open(options_["info_file"].as<string>().c_str());
      info = &info_file;
    }
    hist_out.open(options_["hist_file"].as<string>().c_str());
    WriteHistoryHeader(hist_out);
    krylov_out.open(options_["krylov.file"].as<string>().c_str());
    reduced_out.open(options_["reduced.krylov_file"].as<string>().c_str());
    
  } else {
    // set streams to null (black-hole) streams
    info_file.open(0);
    info = &info_file;
    hist_out.open(0);
    krylov_out.open(0);
    reduced_out.open(0);    
  }
  quasi_newton_->SetOutputStream(*info);
  line_search_->SetOutputStream(*info);

  *info << endl;
  *info << "**************************************************" << endl;
  *info << "***        Using FLECS-based Algorithm         ***" << endl;
  *info << "**************************************************" << endl;
  
  ReducedKKTVector X; // the current (design,dual) solution of reduced system
  ReducedKKTVector P; // used for augmented-system solves
  ReducedKKTVector P_corr; // used for 2nd-order correction
  DesignVector P_norm; // the quasi-normal search direction
  DesignVector P_tang; // the step projected onto the null space
  ReducedKKTVector dLdX; // current reduced gradient of the Lagrangian
  ReducedKKTVector dLdX_save; // work vector
  PDEVector state; // the current solution of the primal PDE
  PDEVector state_save;
  PDEVector adjoint; // the current solution of the adjoint PDE
  PDEVector adjoint_res; // residual of adjoint equation
  DesignVector initial_design; // the initial design variables
  
  // work arrays for matrix-vector product
  std::vector<DesignVector> design_work(3, X.primal());
  std::vector<PDEVector> pde_work(5, state);
  std::vector<DualVector> dual_work(1, X.dual());
  
  // set initial guess for design and dual (the latter is set to zero)
  X.EqualsInitialGuess();
  X.SolveStateAndAdjoint(state, adjoint, pde_work[0]);
  initial_design = X.get_design();
  CurrentSolution(0, X.get_design(), state, adjoint, X.get_dual());

  // initialize the Quasi-Newton approximation
  quasi_newton_->SetInvHessianToIdentity();
  
  double mu = options_["aug_lag.mu_init"].as<double>();
  double radius = options_["trust.init_radius"].as<double>();
  int iter = 0;
  double grad_norm, grad_norm0, grad_tol;
  double feas_norm, feas_norm0, feas_tol;
  double kkt_norm, kkt_norm0;
  double obj_value;
  bool converged = false;
  string events; // information string for output
  bool filter_success = false;  
  while (iter < options_["max_iter"].as<int>()) {
    iter++;
    
    *info << endl;
    *info << "==================================================" << endl;
    *info << "Beginning Major iteration " << iter << endl;
    *info << endl;

    // check for convergence
    dLdX.EqualsKKTConditions(X, state, adjoint, design_work[0]);
    state_save = state;
    if (iter == 1) {
      grad_norm0 = dLdX.primal().Norm2();
      grad_norm = grad_norm0;
      feas_norm0 = dLdX.dual().Norm2();
      feas_norm = feas_norm0;
      kkt_norm0 = sqrt(feas_norm0*feas_norm0 + grad_norm0*grad_norm0);
      kkt_norm = kkt_norm0;
      *info << "grad_norm0  = " << grad_norm0 << endl;
      *info << "feas_norm0  = " << feas_norm0 << endl;
      grad_tol = options_["des_tol"].as<double>()*grad_norm0; //max(grad_norm0, 1.e-6);
      feas_tol = options_["ceq_tol"].as<double>()*max(feas_norm0, 1.e-8);
      events += "i";
      quasi_newton_->set_norm_init(1.0); //grad_norm0);
    } else {
      //if (dLdX.dual().Norm2() > feas_norm) {
      //  mu *= 100.0;
      //  events += " mu+";
      //}
      grad_norm = dLdX.primal().Norm2();
      feas_norm = dLdX.dual().Norm2();
      mu = std::max(mu, options_["aug_lag.mu_init"].as<double>()*
                    pow(feas_norm0/feas_norm, options_["aug_lag.mu_pow"].as<double>()));
      kkt_norm = sqrt(feas_norm*feas_norm + grad_norm*grad_norm);
      *info << "grad_norm = " << grad_norm << " (" << grad_tol
            << " <-- tolerance)" << endl;
      *info << "feas_norm = " << feas_norm << " (" << feas_tol
            << " <-- tolerance)" << endl;
      if (filter_success) {
        dLdX_save -= dLdX;
        dLdX_save *= -1.0;
        quasi_newton_->AddCorrection(P.primal(), dLdX_save.primal());
      }
    }
    dLdX_save = dLdX;
    obj_value = ObjectiveValue(X.primal(), state);
    
    // write output and check for convergence
    WriteHistoryIteration(iter, UserMemory::get_precond_count(), grad_norm,
                          feas_norm, obj_value, mu, events, hist_out);
    if ( (grad_norm < grad_tol) && (feas_norm < feas_tol) ) { 
      converged = true;
      break;
    }
    events = "";

    // compute Krylov tolerance to achieve superlinear convergence, but to
    // avoid oversolving near the desired nonlinear tolerance
    double krylov_tol = options_["krylov.tolerance"].as<double>()*std::min(
        1.0, sqrt(kkt_norm/kkt_norm0)); // superlinear
#if 1
    krylov_tol = std::max(krylov_tol,
                          std::min(grad_tol/grad_norm,feas_tol/feas_norm));
#else
    krylov_tol = std::max(krylov_tol, 0.001);
#endif
    krylov_tol *= options_["reduced.nu"].as<double>();
    *info << "krylov_tol = " << krylov_tol << endl;

    // need adjoint residual for reduced-Hessian product
    adjoint_res.EqualsObjectiveGradient(X.primal(), state);
    pde_work[0].EqualsTransJacobianTimesVector(X.primal(), state, adjoint);
    adjoint_res += pde_work[0];
    pde_work[0].EqualsTransCnstrJacTimesVector(X.primal(), state, X.dual());
    adjoint_res += pde_work[0];

    // set Hessian-vector product tolerance parameters
    // Note: we must divide by nu in product_fac, because nu appears in the
    // krylov_tol, which is fed back to the Hessian-vector product; we do not
    // want this nu factor in product_fac (we only want 1.0 - nu), so we
    // "remove" it here
    double product_fac = options_["reduced.product_fac"].as<double>(); // *
    //          (1.0 - options_["reduced.nu"].as<double>()) /
    //          options_["reduced.nu"].as<double>();
    if (!options_["reduced.dynamic_tol"].as<bool>())
      product_fac *= (krylov_tol/static_cast<double>(
          options_["krylov.space_size"].as<int>()));
    if (options_["reduced.dynamic_tol"].as<bool>()) {
      cerr << "EqualityConstrainedRSNK::Solve(): "
           << "not yet set-up to handle dynamic tolerance in product" << endl;
      throw(-1);
    }

    // initialize the reduced KKT-matrix-vector product
    ptree prod_param;
    prod_param.put("product_fac", product_fac);
    prod_param.put<double>("lambda", 0.0);
    //prod_param.put<bool>("augmented", true); // TEMP!!!
    //prod_param.put<bool>("approx_hessian", true); // TEMP!!!
    boost::static_pointer_cast<ReducedKKTProduct>(mat_vec_)->Initialize(
        X, state_save, adjoint, dLdX_save, adjoint_res, design_work, pde_work,
        dual_work, prod_param, reduced_out);

    // TEMP: this should be an option
    //boost::static_pointer_cast<ReducedKKTProduct>(mat_vec_)
    //    ->set_quasi_newton(quasi_newton_.get());
    
    // Initialize the preconditioner
    ptree prec_param;
    if (options_["reduced.precond"].as<string>() == "idf_schur") {
      prec_param.put("krylov_size", options_["reduced.krylov_size"].as<int>());
      boost::static_pointer_cast<ReducedSchurPreconditioner>(aug_precond_)->
          Initialize(X, state_save, design_work, pde_work, dual_work,
                     prec_param, reduced_out);
    }
    if (options_["reduced.precond"].as<string>() == "nested") {
      prec_param.put("krylov_size", options_["reduced.krylov_size"].as<int>());
      boost::static_pointer_cast<NestedKKTPreconditioner>(aug_precond_)->
          Initialize(X, state_save, adjoint, dLdX_save, adjoint_res, design_work,
                     pde_work, dual_work, prec_param, reduced_out);
    }
    if (options_["quasi_newton.matvec_update"].as<bool>()) {
      // update the quasi-Newton method during KKT-vector products
      boost::static_pointer_cast<ReducedKKTProduct>(mat_vec_)
          ->set_quasi_newton(quasi_newton_.get());
    }
    
    krylov_out << "#-------------------------------------------------" << endl;
    krylov_out << "# primal solve" << endl;
    P = 0.0;
    dLdX *= -1.0;
    ptree ptin, ptout;
    ptin.put<double>("tol", krylov_tol);
    ptin.put<double>("radius", radius);
    ptin.put<double>("mu", mu);
    ptin.put<bool>("check", options_["krylov.check_res"].as<bool>());
    ptin.put<bool>("dynamic", false); //options_["reduced.dynamic_tol"].as<bool>());
#if 0
    if (grad_norm > feas_norm) {
      ptin.put("grad_scale", feas_norm/grad_norm);
      ptin.put("feas_scale", 1.0);
    } else {
      ptin.put("grad_scale", 1.0);
      ptin.put("feas_scale", grad_norm/feas_norm);
    }
#endif
    comp_step_solver_->Solve(ptin, dLdX, P, *mat_vec_, *aug_precond_, ptout,
                             krylov_out);
    int iters = ptout.get<int>("iters");
    bool trust_active = ptout.get<bool>("active");
    
#if 1
    X.primal() += P.primal();
    state.EqualsPrimalSolution(X.primal());
    X.dual() += P.dual();  
#else

    // loop until filter is satisfied
    filter_success = false;
    int i;
    int max_filter_iter = 3;
    for (i = 0; i < max_filter_iter; i++) {
      design_work[0] = X.primal(); // save the primal solution, for reverting
      state_save = state;
      X.primal() += P.primal();
      *info << "filter: trust region radius = " << radius << endl;
      if (state.EqualsPrimalSolution(X.primal())) {
        // state equation solution was successful, so try filter
        double obj = ObjectiveValue(X.primal(), state);
        dual_work[0].EqualsConstraints(X.primal(), state);
        double cnstrnt_norm = dual_work[0].Norm2();
        if (!filter_->Dominates(obj, cnstrnt_norm)) {
          if ( (i == 0) && trust_active )
            radius = std::min(2.0*radius,
                              options_["trust.max_radius"].as<double>());
          filter_success = true;
          break;
        }
      }
      
      if (i == 0) {
        // try a second-order correction
        *info << "attempting a second-order correction...";
        P = 0.0;
        //dLdX.dual() = dual_work[0];
        //dLdX.primal() = dLdX_save.primal();
        //dLdX *= -1.0;
        ptree ptin, ptout;
        ptin.put<double>("tol", krylov_tol);
        ptin.put<double>("mu", options_["aug_lag.mu_init"].as<double>());
        ptin.put<double>("grad_tol", 0.9*grad_norm);
        ptin.put<double>("feas_tol", 0.9*feas_norm);
        ptin.put<bool>("check", options_["krylov.check_res"].as<bool>());
        ptin.put<bool>("dynamic", false); //options_["reduced.dynamic_tol"].as<bool>());
        krylov_out << "#-------------------------------------------------" << endl;
        krylov_out << "# Second-order correction (iter = " << iter << ")" << endl;
        dual_work[0] *= -1.0;
        boost::static_pointer_cast<
          FLECSSolver<ReducedKKTVector,DesignVector,DualVector
                      > >(comp_step_solver_)->Correct2ndOrder(
                          ptin, dual_work[0], dLdX, P, ptout, krylov_out); 
        //int iters = ptout.get<int>("iters");
        X.primal() += P.primal();
        if (state.EqualsPrimalSolution(X.primal())) {
          // state equation solution was successful, so try filter
          double obj = ObjectiveValue(X.primal(), state);
          dual_work[0].EqualsConstraints(X.primal(), state);
          double cnstrnt_norm = dual_work[0].Norm2();
          if (!filter_->Dominates(obj, cnstrnt_norm)) {
            filter_success = true;
            *info << "successful" << endl;
            break;
          }
        }
        *info << "unsuccessful" << endl;
      }

      // if we get here, the filter dominated the point; reset and shrink radius
      X.primal() = design_work[0];
      state = state_save;
      radius *= 0.25;
      if (i == max_filter_iter-1) break;

      // resolve with reduced radius
      ptree ptin, ptout;
      ptin.put<double>("tol", krylov_tol);
      ptin.put<double>("radius", radius);
      ptin.put<double>("mu", mu);
      ptin.put<bool>("check", options_["krylov.check_res"].as<bool>());
      ptin.put<bool>("dynamic", false); //options_["reduced.dynamic_tol"].as<bool>());
#if 0
      if (grad_norm > feas_norm) {
        ptin.put("grad_scale", feas_norm/grad_norm);
        ptin.put("feas_scale", 1.0);
      } else {
        ptin.put("grad_scale", 1.0);
        ptin.put("feas_scale", grad_norm/feas_norm);
      }
#endif
      comp_step_solver_->ReSolve(ptin, dLdX, P, ptout, krylov_out);
      trust_active = ptout.get<bool>("active");
    }
    if (filter_success)
      X.dual() += P.dual(); // update multipliers
#endif

    // need to update adjoint
    X.SolveAdjoint(state, adjoint, pde_work[0]);
    
    // for debugging
    //filter_->Print(*info);

    CurrentSolution(iter, X.primal(), state, adjoint, X.dual());
    cout << "Norm of multipliers = " << X.dual().Norm2() << endl;
  }
  *info << "Total number of nonlinear iterations: " << iter << endl;
}
// ==============================================================================
#else
void EqualityConstrainedRSNK::Solve() {
  // open output files to save information and convergence histories
  ostream* info;
  ofstream info_file;
  if (options_["send_info_to_cout"].as<bool>())
    info = &std::cout;
  else {
    info_file.open(options_["info_file"].as<string>().c_str());
    info = &info_file;
  }
  quasi_newton_->SetOutputStream(*info);
  line_search_->SetOutputStream(*info);
  ofstream hist_out(options_["hist_file"].as<string>().c_str());
  WriteHistoryHeader(hist_out);
  ofstream krylov_out(options_["krylov.file"].as<string>().c_str());
  ofstream reduced_out(options_["reduced.krylov_file"].as<string>().c_str());

  *info << endl;
  *info << "**************************************************" << endl;
  *info << "***       Using Composite-Step Algorithm       ***" << endl;
  *info << "**************************************************" << endl;
  
  ReducedKKTVector X; // the current (design,dual) solution of reduced system
  ReducedKKTVector P; // used for augmented-system solves
  ReducedKKTVector P_corr; // used for 2nd-order correction
  DesignVector P_norm; // the quasi-normal search direction
  DesignVector P_tang; // the step projected onto the null space
  ReducedKKTVector dLdX; // current reduced gradient of the Lagrangian
  ReducedKKTVector dLdX_save; // work vector
  PDEVector state; // the current solution of the primal PDE
  PDEVector state_save;
  PDEVector adjoint; // the current solution of the adjoint PDE
  PDEVector adjoint_res; // residual of adjoint equation
  DesignVector initial_design; // the initial design variables
  
  // work arrays for matrix-vector product
  std::vector<DesignVector> design_work(3, X.primal());
  std::vector<PDEVector> pde_work(5, state);
  std::vector<DualVector> dual_work(1, X.dual());
  
  // set initial guess for design and dual (the latter is set to zero)
  X.EqualsInitialGuess();
  X.SolveStateAndAdjoint(state, adjoint, pde_work[0]);
  initial_design = X.get_design();
  CurrentSolution(0, X.get_design(), state, adjoint, X.get_dual());
  
  double radius = options_["trust.init_radius"].as<double>();
  int iter = 0;
  double grad_norm, grad_norm0, grad_tol;
  double feas_norm, feas_norm0, feas_tol;
  double kkt_norm, kkt_norm0;
  double obj_value;
  bool converged = false;
  string events; // information string for output
  
  while (iter < options_["max_iter"].as<int>()) {
    iter++;
    
    *info << endl;
    *info << "==================================================" << endl;
    *info << "Beginning Major iteration " << iter << endl;
    *info << endl;

    // check for convergence
    dLdX.EqualsKKTConditions(X, state, adjoint, design_work[0]);
    dLdX_save = dLdX;
    state_save = state;
    if (iter == 1) {
      grad_norm0 = dLdX.primal().Norm2();
      grad_norm = grad_norm0;
      feas_norm0 = dLdX.dual().Norm2();
      feas_norm = feas_norm0;
      kkt_norm0 = sqrt(feas_norm0*feas_norm0 + grad_norm0*grad_norm0);
      kkt_norm = kkt_norm0;
      *info << "grad_norm0  = " << grad_norm0 << endl;
      *info << "feas_norm0  = " << feas_norm0 << endl;
      grad_tol = options_["des_tol"].as<double>()*grad_norm0; //max(grad_norm0, 1.e-6);
      feas_tol = options_["ceq_tol"].as<double>()*max(feas_norm0, 1.e-8);
      events += "i";
    } else {
      grad_norm = dLdX.primal().Norm2();
      feas_norm = dLdX.dual().Norm2();
      kkt_norm = sqrt(feas_norm*feas_norm + grad_norm*grad_norm);
      *info << "grad_norm = " << grad_norm << " (" << grad_tol
            << " <-- tolerance)" << endl;
      *info << "feas_norm = " << feas_norm << " (" << feas_tol
            << " <-- tolerance)" << endl;
    }
    obj_value = ObjectiveValue(X.primal(), state);
    
    // write output and check for convergence
    WriteHistoryIteration(iter, UserMemory::get_precond_count(), grad_norm,
                          feas_norm, obj_value, 0.0, events, hist_out);
    if ( (grad_norm < grad_tol) && (feas_norm < feas_tol) ) { 
      converged = true;
      break;
    }
    events = "";

    // compute Krylov tolerance to achieve superlinear convergence, but to
    // avoid oversolving near the desired nonlinear tolerance
    double krylov_tol = options_["krylov.tolerance"].as<double>()*std::min(
        1.0, sqrt(kkt_norm/kkt_norm0)); // superlinear
#if 0
    krylov_tol = std::max(krylov_tol,
                          std::min(grad_tol/grad_norm,feas_tol/feas_norm));
#else
    krylov_tol = std::max(krylov_tol, 0.001);
#endif
    krylov_tol *= options_["reduced.nu"].as<double>();
    *info << "krylov_tol = " << krylov_tol << endl;

    // need adjoint residual for reduced-Hessian product
    adjoint_res.EqualsObjectiveGradient(X.primal(), state);
    pde_work[0].EqualsTransJacobianTimesVector(X.primal(), state, adjoint);
    adjoint_res += pde_work[0];
    pde_work[0].EqualsTransCnstrJacTimesVector(X.primal(), state, X.dual());
    adjoint_res += pde_work[0];

    // set Hessian-vector product tolerance parameters
    // Note: we must divide by nu in product_fac, because nu appears in the
    // krylov_tol, which is fed back to the Hessian-vector product; we do not
    // want this nu factor in product_fac (we only want 1.0 - nu), so we
    // "remove" it here
    double product_fac = options_["reduced.product_fac"].as<double>(); // *
    //          (1.0 - options_["reduced.nu"].as<double>()) /
    //          options_["reduced.nu"].as<double>();
    if (!options_["reduced.dynamic_tol"].as<bool>())
      product_fac *= (krylov_tol/static_cast<double>(
          options_["krylov.space_size"].as<int>()));
    if (options_["reduced.dynamic_tol"].as<bool>()) {
      cerr << "EqualityConstrainedRSNK::Solve(): "
           << "not yet set-up to handle dynamic tolerance in product" << endl;
      throw(-1);
    }

    // Initialize the reduced-Hessian-vector product
    ptree prod_param;
    prod_param.put("product_fac", product_fac);
    boost::static_pointer_cast<LagrangianHessianProduct>(hess_mat_vec_)
        ->Initialize(X, state_save, adjoint, dLdX_save.primal(), adjoint_res,
                     design_work, pde_work, prod_param, reduced_out);   
    if (options_["quasi_newton.matvec_update"].as<bool>()) {
      // update the quasi-Newton method during Hessian-vector products
      boost::static_pointer_cast<LagrangianHessianProduct>(hess_mat_vec_)
          ->set_quasi_newton(quasi_newton_.get());
    }

    // Initialize the tangential-step preconditioner
    ptree prec_param;
#if 1
    if (options_["reduced.precond"].as<string>() == "idf_schur") {
      prec_param.put<double>("tol", 0.01*krylov_tol);
      prec_param.put<bool>("approx", false); //false);
      boost::static_pointer_cast<IDFProjectionPreconditioner>(tang_precond_)->
          Initialize(X, state_save, design_work, pde_work, dual_work,
                     prec_param, reduced_out);
    } else {      
      boost::static_pointer_cast<ProjectionPreconditioner>(tang_precond_)->
          Initialize(X, state_save, design_work, pde_work, dual_work,
                     prec_param, reduced_out);
    }
#else
    boost::static_pointer_cast<ProjectionPreconditioner>(tang_precond_)->
        Initialize(X, state_save, design_work, pde_work, dual_work,
                   prec_param, reduced_out);
#endif
        
    // Initialize the reduced augmented-matrix-vector product
    //prod_param.clear();
    //prod_param.put<bool>("approx", true);
    boost::static_pointer_cast<ReducedAugmentedProduct>(aug_mat_vec_)->Initialize(
        X, state_save, design_work, pde_work, dual_work, prod_param, reduced_out);

    // initialize the reduced KKT-matrix-vector product
    prod_param.put<double>("lambda", 0.0);
    boost::static_pointer_cast<ReducedKKTProduct>(mat_vec_)->Initialize(
        X, state_save, adjoint, dLdX_save, adjoint_res, design_work, pde_work,
        dual_work, prod_param, reduced_out);
    
    // Initialize the augmented system preconditioner
    prec_param.clear();
    if (options_["reduced.precond"].as<string>() == "idf_schur") {
      prec_param.put("krylov_size", options_["reduced.krylov_size"].as<int>());
      boost::static_pointer_cast<ReducedSchurPreconditioner>(aug_precond_)->
          Initialize(X, state_save, design_work, pde_work, dual_work,
                     prec_param, reduced_out);
      //boost::static_pointer_cast<ProjectionPreconditioner>(tang_precond_)->
      //    set_aug_precond(aug_precond_);
      boost::static_pointer_cast<IDFProjectionPreconditioner>(tang_precond_)->
          set_idf_precond(aug_precond_);
    }

    // solve for the normal step
    krylov_out << "#-------------------------------------------------" << endl;
    krylov_out << "# normal-step solve" << endl;
    P = 0.0;
    dLdX *= -1.0;
    dLdX.primal() = 0.0;
    ptree ptin, ptout;
    ptin.put<double>("tol", krylov_tol);
    //ptin.put<double>("radius", radius);
    ptin.put<bool>("check", options_["krylov.check_res"].as<bool>());
    ptin.put<bool>("dynamic", false); //options_["reduced.dynamic_tol"].as<bool>());
    normal_step_solver_->Solve(ptin, dLdX, P, *aug_mat_vec_, *aug_precond_, ptout,
                               krylov_out);
    int iters = ptout.get<int>("iters");
    P_norm = P.primal();

    // scale the normal step if necessary
    double normal_len = P_norm.Norm2();
    if (normal_len > 0.8*radius) {
      P_norm *= 0.8*radius/normal_len;
      normal_len = 0.8*radius;
    }

    // solve for the tangential step
    krylov_out << "#-------------------------------------------------" << endl;
    krylov_out << "# tangential-step solve" << endl;
    (*hess_mat_vec_)(P_norm, dLdX.primal());
    dLdX.primal() += dLdX_save.primal();
    dLdX.primal() *= -1.0;
    P.primal() = 0.0;
    ptin.clear();
    ptout.clear();
    ptin.put<double>("tol", krylov_tol);
    ptin.put<bool>("proj_cg", true);
    ptin.put<double>("radius", sqrt(radius*radius - normal_len*normal_len));
    ptin.put<bool>("check", options_["krylov.check_res"].as<bool>());
    ptin.put<bool>("dynamic", false); //options_["reduced.dynamic_tol"].as<bool>());
    IdentityPreconditioner<DesignVector> ident_precond;
    IdentityMatrix<DesignVector> ident_matrix;
    tang_step_solver_->Solve(ptin, dLdX.primal(), P.primal(), *hess_mat_vec_,
                             *tang_precond_, ptout, krylov_out);
    iters = ptout.get<int>("iters");

    // Set P_tang
    //(*tang_precond_)(P.primal(), P_tang);
    P_tang = P.primal();

    // loop until filter is satisfied
    bool filter_success = false;
    for (int i = 0; i < 10; i++) {
      P.primal().EqualsAXPlusBY(1.0, P_norm, 1.0, P_tang);
      X.primal() += P.primal();
      *info << "filter: trust region radius = " << radius << endl;
      if (state.EqualsPrimalSolution(X.primal())) {
        // state equation solution was successful, so try filter
        double obj = ObjectiveValue(X.primal(), state);
        dual_work[0].EqualsConstraints(X.primal(), state);
        double cnstrnt_norm = dual_work[0].Norm2();
        if (!filter_->Dominates(obj, cnstrnt_norm)) {
          if (i == 0)
            radius = std::min(2.0*radius,
                              options_["trust.max_radius"].as<double>());
          filter_success = true;
          break;
        }
      }

      if (i == 0) {
        // try a second-order correction
        *info << "attempting a second-order correction...";
        P = 0.0;
        dLdX.dual() = dual_work[0];
        dLdX *= -1.0;
        dLdX.primal() = 0.0;
        ptree ptin, ptout;
        ptin.put<double>("tol", krylov_tol);
        //ptin.put<double>("radius", radius);
        ptin.put<bool>("check", options_["krylov.check_res"].as<bool>());
        ptin.put<bool>("dynamic", false); //options_["reduced.dynamic_tol"].as<bool>());
        normal_step_solver_->Solve(ptin, dLdX, P, *aug_mat_vec_, *aug_precond_, ptout,
                                   krylov_out);
        int iters = ptout.get<int>("iters");
        X.primal() += P.primal();
        if (state.EqualsPrimalSolution(X.primal())) {
          // state equation solution was successful, so try filter
          double obj = ObjectiveValue(X.primal(), state);
          dual_work[0].EqualsConstraints(X.primal(), state);
          double cnstrnt_norm = dual_work[0].Norm2();
          if (!filter_->Dominates(obj, cnstrnt_norm)) {
            radius = std::min(2.0*radius,
                              options_["trust.max_radius"].as<double>());
            filter_success = true;
            *info << "successful" << endl;
            break;
          }
        }
        *info << "unsuccessful" << endl;
        // remove contribution due to P_norm and P_tang (correction will be
        // removed below
        X.primal() -= P_norm;
        X.primal() -= P_tang;
      }
      
      // if we get here, the filter dominated the point; shink trust radius
      X.primal() -= P.primal();
      radius *= 0.25;
      state = state_save;

      if (i == 9) break;
      
      // shrink the normal step
      normal_len = P_norm.Norm2();
      if (normal_len > 0.8*radius) {
        P_norm *= 0.8*radius/normal_len;
        normal_len = 0.8*radius;
      }
      // resolve the tangential step
      (*hess_mat_vec_)(P_norm, dLdX.primal());
      dLdX.primal() += dLdX_save.primal();
      dLdX.primal() *= -1.0;
      P_tang = 0.0;
      ptout.clear();      
      ptin.put<double>("radius", sqrt(radius*radius - normal_len*normal_len));
      tang_step_solver_->Solve(ptin, dLdX.primal(), P_tang, *hess_mat_vec_,
                               *tang_precond_, ptout, krylov_out);
    }

    if (filter_success) {
      krylov_out << "#-------------------------------------------------" << endl;
      krylov_out << "# multipliers solve" << endl;
      // solve for the multipliers
      P = 0.0;
      //dLdX.primal() = dLdX_save.primal();
      //dLdX.primal() *= -1.0;
      //(*hess_mat_vec_)(P.primal(), dLdX.primal());
      dLdX.primal() = dLdX_save.primal();
      dLdX.primal() *= -1.0;
      dLdX.dual() = 0.0;
      ptin.put<double>("tol", krylov_tol);
      //ptin.put<double>("radius", radius);
      ptin.put<bool>("check", options_["krylov.check_res"].as<bool>());
      ptin.put<bool>("dynamic", false); //options_["reduced.dynamic_tol"].as<bool>());
      normal_step_solver_->Solve(ptin, dLdX, P, *aug_mat_vec_, *aug_precond_, ptout,
                                 krylov_out);
      X.dual() += P.dual();      
    }
    
    // need to update adjoint
    X.SolveAdjoint(state, adjoint, pde_work[0]);
    
    CurrentSolution(iter, X.primal(), state, adjoint, X.dual());
    cout << "Norm of multipliers = " << X.dual().Norm2() << endl;
  }
  *info << "Total number of nonlinear iterations: " << iter << endl;

#if 0
  adjoint_res.EqualsObjectiveGradient(X.primal(), state);
  pde_work[0].EqualsTransJacobianTimesVector(X.primal(), state, adjoint);
  adjoint_res += pde_work[0];
  pde_work[0].EqualsTransCnstrJacTimesVector(X.primal(), state, X.dual());
  adjoint_res += pde_work[0];
  // Initialize the KKT product
  *info << "Starting Explicit build of KKT-matrix" << endl; 
  ptree prod_param;
  prod_param.put("product_fac", 1.0);
  boost::static_pointer_cast<ReducedKKTProduct>(mat_vec_)->Initialize(
      X, state, adjoint, dLdX_save, adjoint_res, design_work, pde_work,
      dual_work, prod_param, reduced_out);
  for (int i = 0; i < 142; i++) {
    P.primal().EqualsBasisVector(i);
    P.dual() = 0.0;
    mat_vec_->operator()(P, dLdX);
    CurrentSolution(iter, dLdX.primal(), state, adjoint, dLdX.dual());
  }

  // Initialize the reduced-Hessian-vector product
  *info << "Starting Explicit build of Hessian" << endl; 
  prod_param.put("product_fac", 1.0);
  boost::static_pointer_cast<LagrangianHessianProduct>(hess_mat_vec_)
      ->Initialize(X, state, adjoint, dLdX_save.primal(), adjoint_res,
                   design_work, pde_work, prod_param, reduced_out);
  for (int i = 0; i < 142; i++) {
    P.primal().EqualsBasisVector(i);
    P.dual() = 0.0;
    dLdX.dual() = 0.0;
    hess_mat_vec_->operator()(P.primal(), dLdX.primal());
    CurrentSolution(iter, dLdX.primal(), state, adjoint, dLdX.dual());
  }
  
  // Initialize the projection preconditioner
  *info << "Starting check of projection preconditioner" << endl; 
  ptree prec_param;
  boost::static_pointer_cast<ProjectionPreconditioner>(tang_precond_)->
        Initialize(X, state, design_work, pde_work, dual_work,
                   prec_param, reduced_out);
  for (int i = 0; i < 142; i++) {
    P.primal().EqualsBasisVector(i);
    P.dual() = 0.0;
    dLdX.dual() = 0.0;
    tang_precond_->operator()(P.primal(), dLdX.primal());
    CurrentSolution(iter, dLdX.primal(), state, adjoint, dLdX.dual());
  }
#endif
}
#endif
// ==============================================================================
void EqualityConstrainedRSNK::WriteHistoryHeader(ostream& out) {
#if 0
  if (!(out.good())) {
    cerr << "EqualityConstrainedRSNK::WriteHistoryHeader(): "
	 << "ostream is not good for i/o operations." << endl;
    throw(-1);
  }
#endif
  out << "# Kona equality-constrained RSNK convergence history" << endl;
  out << boost::format(
      string("%|5| %|7t| %|7| %|16t| %|-12| %|28t| ")+
      string("%|-12| %|40t| %|-12| %|52t| %|-12| %|64t| %|-8|\n"))
      % "# iter" % "precond" % "optimality" % "feasibility" % "objective"
      % "mu param." % "info";
  out.flush();
}
// ==============================================================================
void EqualityConstrainedRSNK::WriteHistoryIteration(
    const int& iter, const int& precond_calls, const double& optimality,
    const double& feasibility, const double& obj, const double& mu,
    const string& info, ostream& out) {
#if 0
  if (!(out.good())) {
    cerr << "EqualityConstrainedRSNK::WriteHistoryIteration(): "
	 << "ostream is not good for i/o operations." << endl;
    throw(-1);
  }
#endif
  out << boost::format(
      string("%|5| %|7t| %|7| %|16t| %|-12.6| %|28t| ")+
      string("%|-12.6| %|40t| %|-12.6| %|52t| %|-12.6| ")+
      string("%|64t| %|-8|\n"))
      % iter % precond_calls % optimality % feasibility % obj % mu % info;
  out.flush();
}
// ==============================================================================
} // namespace
