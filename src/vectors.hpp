/**
 * \file vectors.hpp
 * \brief header file for various types of vector spaces
 * \author Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#pragma once

#include <math.h>
#include <ostream>
#include <iostream>
#include <vector>
#include <string>

#include "./user_memory.hpp"
//#include "./krylov.hpp"

using std::cerr;
using std::cout;
using std::endl;
using std::vector;
using std::string;

namespace kona {

// forward declarations
class PDEVector;
class DualVector;
class JacobianStateProduct;
class JacobianTAdjointProduct;
class StatePreconditioner;
class AdjointPreconditioner;

// ======================================================================
/*!
 * \class DesignVector
 * \brief storage and operations for vectors in the design space
 *
 * The DesignVector class represents vectors that lie in the design
 * space. The class inherits from UserVector, so that data is stored
 * and manipulated by the user in the implementation of their
 * choice.
 */
class DesignVector : public UserVector<design> {
 public:
  
  /*!
   * \brief inlined default constructor
   */
  DesignVector() {}
  
  /*!
   * \brief inlined object destructor
   */
  ~DesignVector() {}

  /*!
   * \brief constructor to set values to constant
   * \param[in] val - value assigned to each element of DesignVector
   */
  explicit DesignVector(const double & val) : 
      UserVector<design>(val) {}
  
  /*!
   * \brief copy constructor with deep copy (potentially expensive)
   * \param[in] u - DesignVector being copied to calling object
   */
  DesignVector(const DesignVector & u) : UserVector<design>(u) {}

  /*!
   * \brief DesignVector=DesignVector assignment operator with deep copy
   * \param[in] u - DesignVector value being assigned
   */
  DesignVector & operator=(const DesignVector & u) {
    UserVector<design>::operator=(u);
  };
  
  /*!
   * \brief DesignVector=double assignment operator
   * \param[in] val - value assigned to each element of DesignVector
   */
  DesignVector & operator=(const double & val) {
    UserVector<design>::operator=(val);
  };

  DesignVector & EqualsBasisVector(const int& i) {
    UserMemory::Debug(design, index_, i, 1.0);
  }
  
  /*!
   * \brief restrict calling object to design subspace (zero-out target space)
   */
  void RestrictToDesign() {
    UserMemory::Restrict(0, index_);
  };

  /*!
   * \brief restrict calling object to target subspace (zero-out design space)
   */
  void RestrictToTarget() {
    UserMemory::Restrict(1, index_);
  };

  /*!
   * \brief copy the given DualVector to "target" subspace of given DesignVector
   * \param[in] dual - dual vector being copied
   */
  void Convert(const DualVector & dual);
  
  /*!
   * \brief sets calling object to value of initial design
   */
  void EqualsInitialDesign() {
    UserMemory::SetInitialDesign(index_);
  }
  
  /*!
   * \brief calling object is set to design component of gradient
   * \param[in] eval_at_design - design where gradient is evaluated at
   * \param[in] eval_at_state - state where gradient is evaluated at
   */
  void EqualsObjectiveGradient(const DesignVector & eval_at_design,
                               const PDEVector & eval_at_state);

  /*!
   * \brief calling object is set to (reduced) gradient of objective
   * \param[in] eval_at_design - design where gradient is evaluated
   * \param[in] eval_at_state - state where gradient is evaluated
   * \param[in] eval_at_adjoint - adjoint where gradient is evaluated
   * \param[out] design_wrk - work vector
   */
  void EqualsReducedGradient(const DesignVector & eval_at_design,
                             const PDEVector & eval_at_state,
                             const PDEVector & eval_at_adjoint,
                             DesignVector & design_wrk);
  
  /*!
   * \brief calling object is set to (reduced) gradient of Lagrangian
   * \param[in] eval_at_design - design where gradient is evaluated
   * \param[in] eval_at_dual - Lagrange multipliers where gradient is evaluated
   * \param[in] eval_at_state - state where gradient is evaluated
   * \param[in] eval_at_adjoint - adjoint where gradient is evaluated
   * \param[out] design_wrk - work vector
   *
   * This can also be used to compute the Augmented Lagrangian gradient by
   * passing eval_at_dual = Psi + 0.5*mu*ceq, where Psi are the Lagrange
   * multiplier estimates.
   */
  void EqualsLagrangianReducedGradient(const DesignVector & eval_at_design,
                                       const DualVector & eval_at_dual,
                                       const PDEVector & eval_at_state,
                                       const PDEVector & eval_at_adjoint,
                                       DesignVector & design_wrk);
  
  /*!
   * \brief calling object is set to Jacobian-transposed-vector prod
   * \param[in] eval_at_design - design where PDE Jacobian is evaluated at
   * \param[in] eval_at_state - state where PDE Jacobian is evaluated at
   * \param[in] vector - vector that transposed Jacobian is multiplying
   */
  void EqualsTransJacobianTimesVector(
      const DesignVector & eval_at_design,
      const PDEVector & eval_at_state,
      const PDEVector & vector);

  /*!
   * \brief calling object is set to the constraint-Jacobian-transposed-vector
   * \param[in] eval_at_design - design where constraint Jacobian is evaluated
   * \param[in] eval_at_state - state where constraint Jacobian is evaluated
   * \param[in] vector - dual vector that transposed Jacobian is multiplying
   */
  void EqualsTransCnstrJacTimesVector(
      const DesignVector & eval_at_design,
      const PDEVector & eval_at_state,
      const DualVector & vector);
  
  /*!
   * \brief calculates the objective function at the given design
   * \param[in] eval_at_design - design where objective is evaluated
   *
   * This version of ObjectiveValue does not require the user to use a
   * specific PDEVector; instead the user may compute and store the
   * primal PDE solution as they wish.
   */  
  friend double ObjectiveValue(const DesignVector & eval_at_design);

  friend double ObjectiveValue(const DesignVector & eval_at_design,
                               const PDEVector & eval_at_state);
  
  /*!
   * \brief calculates the Augmented Lagrangian at the given reduced KKT vector
   * \param[in] eval_at_design - design variables where Lagrangian is evaluated
   * \param[in] eval_at_dual - estimated multipliers where Lagrangian evaluated
   * \param[in] eval_at_state - state varibles where Lagrangian is evaluated
   * \param[in] ceq - constraints evaluated at eval_at_design and eval_at_state
   * \param[in] mu - penalty parameter: 0.5*mu multiplies penatly
   */
  friend double AugmentedLagrangian(const DesignVector & eval_at_design,
                                    const DualVector & eval_at_dual,
                                    const PDEVector & eval_at_state,
                                    const DualVector & ceq, const double & mu);
  
  /*!
   * \brief indicates to the user where current solution is stored
   * \param[in] iter - current nonlinear iteration
   * \param[in] current_design - current design solution vector
   * \param[in] current_state - current solution to PDE
   * \param[in] current_adjoint - current solution to adjoint PDE
   */
  friend void CurrentSolution(const int & iter,
                              const DesignVector & current_design,
                              const PDEVector & current_state,
                              const PDEVector & current_adjoint);

  /*!
   * \brief overloaded function that includes current estimate of multipliers
   */
  friend void CurrentSolution(const int & iter,
                              const DesignVector & current_design,
                              const PDEVector & current_state,
                              const PDEVector & current_adjoint,
                              const DualVector & current_dual);

  // these need to be friends because the Jacobian and preconditioner
  // may depend on the design variable
  friend class JacobianStateProduct;
  friend class JacobianTAdjointProduct;
  friend class StatePreconditioner;
  friend class AdjointPreconditioner;

  friend class PDEVector;
  friend class DualVector;
};

// ======================================================================
/*!
 * \class PDEVector
 * \brief represents vectors associated with the state variables
 *
 * A PDEVector object is used to represent a vector in state or
 * adjoint space.  The class inherits from UserVector, so that data is
 * stored and manipulated by the user in the implementation of their
 * choice
 */
class PDEVector : public UserVector<pde> {
 public:

  /*!
   * \brief inlined default constructor
   */
  PDEVector() {}

  /*!
   * \brief inlined object destructor
   */
  ~PDEVector() {}

  /*!
   * \brief constructor that sets elements to a constant
   * \param[in] val - value assigned to each element of PDEVector
   */
  explicit PDEVector(const double & val) : UserVector<pde>(val) {}
  
  /*!
   * \brief copy constructor with deep copy (potentially expensive)
   * \param[in] u - PDEVector being copied to calling object
   */
  PDEVector(const PDEVector & u) : UserVector<pde>(u) {}

  /*!
   * \brief PDEVector=PDEVector assignment operator with deep copy
   * \param[in] u - PDEVector value being assigned
   */
  PDEVector & operator=(const PDEVector & u) {
    UserVector<pde>::operator=(u);
  };

  /*!
   * \brief PDEVector=double assignment operator
   * \param[in] val - value assigned to each element of PDEVector
   */
  PDEVector & operator=(const double & val) {
    UserVector<pde>::operator=(val);
  };

  /*!
   * \brief state equations are evaluated and stored in calling object
   * \param[in] eval_at_design - design where PDE is evaluated at
   * \param[in] eval_at_state - state where PDE is evaluated at
   */
  void EqualsStateEquations(const DesignVector & eval_at_design,
                            const PDEVector & eval_at_state) {
    UserMemory::EvaluatePDE(index_, eval_at_design.index_,
                            eval_at_state.index_);
  }

  /*!
   * \brief set calling object to the solution of the primal PDE
   * \param[in] solve_at_design - design where PDE is evaluated
   * \returns true if solver is successful, false otherwise
   */
  bool EqualsPrimalSolution(const DesignVector & solve_at_design) {
    return UserMemory::SolvePDE(index_, solve_at_design.index_);
  }

  /*!
   * \brief set calling object to the solution of a linearized PDE
   * \param[in] solve_at_design - design where PDE is evaluated
   * \param[in] solve_at_state - state where PDE is evaluated
   * \param[in] rhs_vector - source vector on right-hand side of linearized eqn
   * \param[in] rel_tol - requested relative tolerance for final residual norm
   * \returns true if solver is successful, false otherwise
   */
  bool EqualsLinearizedSolution(const DesignVector & solve_at_design,
                                const PDEVector & solve_at_state,
                                const PDEVector & rhs_vector,
                                const double & rel_tol = 1e-12) {
    return UserMemory::SolveLinearized(index_, solve_at_design.index_,
                                       rhs_vector.index_, solve_at_state.index_,
                                       rel_tol);
  }

  /*!
   * \brief set calling object to the solution of the (first-order) adjoint PDE
   * \param[in] solve_at_design - design where PDE is evaluated
   * \param[in] solve_at_state - state where PDE is evaluated
   * \returns true if solver is successful, false otherwise
   */
  bool EqualsAdjointSolution(const DesignVector & solve_at_design,
                             const PDEVector & solve_at_state,
                             const double & rel_tol = 1e-12) {
    return UserMemory::SolveAdjoint(index_, solve_at_design.index_,
                                    -1, solve_at_state.index_, rel_tol);
  }

  /*!
   * \brief set calling object to the solution of an adjoint PDE
   * \param[in] solve_at_design - design where PDE is evaluated
   * \param[in] solve_at_state - state where PDE is evaluated
   * \param[in] rhs_vector - source vector on right-hand side of adjoint eqn
   * \param[in] rel_tol - requested relative tolerance for final residual norm
   * \returns true if solver is successful, false otherwise
   */
  bool EqualsAdjointSolution(const DesignVector & solve_at_design,
                             const PDEVector & solve_at_state,
                             const PDEVector & rhs_vector,
                             const double & rel_tol = 1e-12) {
    return UserMemory::SolveAdjoint(index_, solve_at_design.index_,
                                    rhs_vector.index_, solve_at_state.index_,
                                    rel_tol);
  }

  /*!
   * \brief object set to solution of the adjoint PDE for Augmented Lagrangian
   * \param[in] solve_at_design - design where PDE is evaluated
   * \param[in] solve_at_state - state where PDE is evaluated
   * \param[in] dual_plus_ceq - multipliers plus mu*constraints
   * \param[out] pde_work - work space for the rhs
   * \param[in] krylov_size - maximum number of Krylov vectors for solve
   */
  void EqualsAugLagrangianAdjointSolution(const DesignVector & solve_at_design,
                                          const PDEVector & solve_at_state,
                                          const DualVector & dual_plus_ceq,
                                          PDEVector & pde_work,
                                          const int & krylov_size);
  
  /*!
   * \brief calling object is set to state component of gradient
   * \param[in] eval_at_design - design where gradient is evaluated at
   * \param[in] eval_at_state - state where gradient is evaluated at
   */
  void EqualsObjectiveGradient(const DesignVector & eval_at_design,
                               const PDEVector & eval_at_state);

  /*!
   * \brief calling object is set to Jacobian-vector product
   * \param[in] eval_at_design - design where Jacobian is evaluated at
   * \param[in] eval_at_state - state where Jacobian is evaluated at
   * \param[in] vector - vector that Jacobian is multiplying
   */
  void EqualsJacobianTimesVector(
      const DesignVector & eval_at_design,
      const PDEVector & eval_at_state,
      const DesignVector & vector);

  /*!
   * \brief calling object is set to Jacobian-vector product
   * \param[in] eval_at_design - design where Jacobian is evaluated at
   * \param[in] eval_at_state - state where Jacobian is evaluated at
   * \param[in] vector - vector that Jacobian is multiplying
   */
  void EqualsJacobianTimesVector(
      const DesignVector & eval_at_design,
      const PDEVector & eval_at_state,
      const PDEVector & vector);

  /*!
   * \brief calling object is set to Jacobian-transposed-vector prod
   * \param[in] eval_at_design - design where Jacobian is evaluated at
   * \param[in] eval_at_state - state where Jacobian is evaluated at
   * \param[in] vector - vector that transposed Jacobian is multiplying
   */
  void EqualsTransJacobianTimesVector(
      const DesignVector & eval_at_design,
      const PDEVector & eval_at_state,
      const PDEVector & vector);

  /*!
   * \brief calling object is set to the constraint-Jacobian-transposed-vector
   * \param[in] eval_at_design - design where constraint Jacobian is evaluated
   * \param[in] eval_at_state - state where constraint Jacobian is evaluated
   * \param[in] vector - dual vector that transposed Jacobian is multiplying
   */
  void EqualsTransCnstrJacTimesVector(
      const DesignVector & eval_at_design,
      const PDEVector & eval_at_state,
      const DualVector & vector);
  
  /*!
   * \brief calculates the objective function at the given design/state
   * \param[in] eval_at_design - design where objective is evaluated
   * \param[in] eval_at_state - state where objective is evaluated
   */
  friend double ObjectiveValue(const DesignVector & eval_at_design,
                               const PDEVector & eval_at_state);

  /*!
   * \brief indicates to the user where current solution is stored
   * \param[in] iter - current nonlinear iteration
   * \param[in] current_design - current design solution vector
   * \param[in] current_state - current solution to PDE
   * \param[in] current_adjoint - current solution to adjoint PDE
   */
  friend void CurrentSolution(const int & iter,
                              const DesignVector & current_design,
                              const PDEVector & current_state,
                              const PDEVector & current_adjoint);

  /*!
   * \brief overloaded function that includes current estimate of multipliers
   */
  friend void CurrentSolution(const int & iter,
                              const DesignVector & current_design,
                              const PDEVector & current_state,
                              const PDEVector & current_adjoint,
                              const DualVector & current_dual);

  // these need to be friends because the Jacobian and preconditioner
  // may depend on the state variable
  friend class JacobianStateProduct;
  friend class JacobianTAdjointProduct;
  friend class StatePreconditioner;
  friend class AdjointPreconditioner;

  friend class DesignVector;
  friend class DualVector;
};

// ======================================================================
/*!
 * \class DualVector
 * \brief storage and operations for vectors in the dual space
 *
 * The DualVector class represents vectors that lie in the dual (constraint)
 * space. The class inherits from UserVector, so that data is stored
 * and manipulated by the user in the implementation of their
 * choice.
 */
class DualVector : public UserVector<dual> {
 public:
  
  /*!
   * \brief inlined default constructor
   */
  DualVector() {}
  
  /*!
   * \brief inlined object destructor
   */
  ~DualVector() {}

  /*!
   * \brief constructor that sets all elements to a constant
   * \param[in] val - value assigned to each element of DualVector
   */
  explicit DualVector(const double & val) : UserVector<dual>(val) {}
  
  /*!
   * \brief copy constructor with deep copy (potentially expensive)
   * \param[in] u - DualVector being copied to calling object
   */
  DualVector(const DualVector & u) : UserVector<dual>(u) {}

  /*!
   * \brief DualVector=DualVector assignment operator with deep copy
   * \param[in] u - DualVector value being assigned
   */
  DualVector & operator=(const DualVector & u) {
    UserVector<dual>::operator=(u);
  };
  
  /*!
   * \brief DualVector=double assignment operator
   * \param[in] val - value assigned to each element of DualVector
   */
  DualVector & operator=(const double & val) {
    UserVector<dual>::operator=(val);
  };

  /*!
   * \brief copy the "target" subspace of given DesignVector to given DualVector
   * \param[in] design - DesignVector being copied
   */
  void Convert(const DesignVector & design);
  
  /*!
   * \brief calling object is set to the constraint value
   * \param[in] eval_at_design - design where constraint is evaluated at
   * \param[in] eval_at_state - state where constraint is evaluated at
   */
  void EqualsConstraints(const DesignVector & eval_at_design,
                         const PDEVector & eval_at_state);

  /*!
   * \brief calling object is set to the constraint-Jacobian-vector product
   * \param[in] eval_at_design - design where constraint Jacobian is evaluated
   * \param[in] eval_at_state - state where constraint Jacobian is evaluated
   * \param[in] vector - design vector that Jacobian is multiplying
   */
  void EqualsCnstrJacTimesVector(const DesignVector & eval_at_design,
                                 const PDEVector & eval_at_state,
                                 const DesignVector & vector);

  /*!
   * \brief calling object is set to the constraint-Jacobian-vector product
   * \param[in] eval_at_design - design where constraint Jacobian is evaluated
   * \param[in] eval_at_state - state where constraint Jacobian is evaluated
   * \param[in] vector - state vector that Jacobian is multiplying
   */
  void EqualsCnstrJacTimesVector(const DesignVector & eval_at_design,
                                 const PDEVector & eval_at_state,
                                 const PDEVector & vector);

  /*!
   * \brief indicates to the user where current solution is stored
   * \param[in] iter - current nonlinear iteration
   * \param[in] current_design - current design solution vector
   * \param[in] current_state - current solution to PDE
   * \param[in] current_adjoint - current solution to adjoint PDE
   * \param[in] current_dual - current Lagrange multipliers
   */  
  friend void CurrentSolution(const int & iter,
                              const DesignVector & current_design,
                              const PDEVector & current_state,
                              const PDEVector & current_adjoint,
                              const DualVector & current_dual);
  
#if 0  
  // these need to be friends because the Jacobian and preconditioner
  // may depend on the design variable
  friend class JacobianStateProduct;
  friend class JacobianTAdjointProduct;
  friend class StatePreconditioner;
  friend class AdjointPreconditioner;
#endif

  friend class DesignVector;
  friend class PDEVector;
};

} // namespace kona
