/**
 * \file quasi_newton.cpp
 * \brief definitions of QuasiNewton and derived class member functions
 * \author Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#include <deque>
#include <vector>
#include <limits>
#include "./vectors.hpp"
#include "./quasi_newton.hpp"

namespace kona {
// ==============================================================================
QuasiNewton::QuasiNewton(const int & max_stored, ostream& out) {
  // check that max_stored is valid
  if (max_stored <= 0) {
    cerr << "QuasiNewton(setMaxStored): "
	 << "invalid maximum number of stored corrections: max_stored = "
	 << max_stored << endl;
    throw(-1);
  }
  max_stored_ = max_stored;
  SetOutputStream(out);
}
// ==============================================================================
void QuasiNewton::SetOutputStream(ostream& out) {
#if 0
  if (!(out.good())) {
    cerr << "QuasiNewton::SetOutputStream: "
	 << "ostream is not good for i/o operations." << endl;
    throw(-1);
  }
#endif
  out_ = &out;
}
// ==============================================================================
void QuasiNewton::set_norm_init(const double & norm) {
  if (norm < 0.0) {
    cerr << "QuasiNewton(set_norm_init): "
	 << "invalid value for norm_init = " << norm << endl;
    throw(-1);
  }
  norm_init_ = norm;
}
// ==============================================================================
void QuasiNewton::SetInvHessianToIdentity() {
  init_Hessian_.reset(new DesignVector);
  *init_Hessian_ = 1.0;
}
// ==============================================================================
void QuasiNewton::AddCorrection(const DesignVector & s_new,
				const DesignVector & y_new) {
  // remove oldest elements from s_ and y_ if maximum reached
  if (static_cast<int>(s_.size()) == max_stored_) {
    s_.pop_front();
    y_.pop_front();
  }
  s_.push_back(s_new);
  y_.push_back(y_new);
}
// ======================================================================
#if 0
bool QuasiNewton::equalsInvHessian(const vector<DesignVector> &
                                        hess) {
  // assume that hess contains the same number of elements as the size
  // of the design vectors
  int num_dim = hess.size();
  DesignVector prod;
  const double kEpsilon = std::numeric_limits<double>::epsilon();

  for (int i = 0; i < num_dim; ++i) {
    applyInvHessianApprox(hess[i], prod);
    // check that prod is equal to the ith column of the identity
    for (int j = 0; j < num_dim; ++j) {
      if (j == i) {
        if (fabs(prod[j] - 1.0) > kEpsilon) return false;
      } else {
        if (fabs(prod[j]) > kEpsilon) return false;
      }
    }
  }
  // if we get here, the quasi-Newton method must define a matrix that
  // is the inverse of hess
  return true;
}
#endif
// ==============================================================================
LimitedMemoryBFGS::LimitedMemoryBFGS(const int & max_stored, ostream& out) : 
    QuasiNewton(max_stored, out) {
  lambda_ = 0.0;
}
// ==============================================================================
void LimitedMemoryBFGS::MemoryRequired(int& num_vector) const {
  num_vector = 2*max_stored_ + 1;
}
// ==============================================================================
void LimitedMemoryBFGS::set_lambda(const double & lambda) {
  if (lambda < 0.0) { // || (lambda > 1.0) ) {
    cerr << "LimitedMemoryBFGS(set_lambda): "
	 << "invalid value for parameter lambda = "
         << lambda << endl;
    throw(-1);    
  }
  lambda_ = lambda;
}
// ==============================================================================
void LimitedMemoryBFGS::AddCorrection(const DesignVector & s_new,
                                      const DesignVector & y_new) {
  // only add new information if curvature condition is satisfied  
  double curvature = InnerProd(s_new, y_new);
#ifdef VERBOSE_DEBUG
  *out_ << "s_new.Norm2() = " << s_new.Norm2() << endl;
  *out_ << "y_new.Norm2() = " << y_new.Norm2() << endl;
  *out_ << "curvature = " << curvature << endl;
#endif
  if (curvature < std::numeric_limits<double>::epsilon()) {
    *out_ << "LimitedMemoryBFGS::AddCorrection(): "
          << "correction skipped due to curvature condition." << endl;
    return;
  }
  //if (curvature < sqrt(numeric_limits<double>::epsilon())) return;

  // remove oldest elements if maximum reached
  if (static_cast<int>(s_.size()) == max_stored_) {
    s_.pop_front();
    y_.pop_front();
    s_dot_y_.pop_front();
    s_dot_s_.pop_front();
  }
  s_.push_back(s_new);
  y_.push_back(y_new);
  s_dot_y_.push_back(curvature);
  double s_norm2 = InnerProd(s_new, s_new);
  s_dot_s_.push_back(s_norm2);
}
// ==============================================================================
void LimitedMemoryBFGS::ApplyInvHessianApprox(DesignVector & u,
                                              DesignVector & v) const {
  int num_stored = s_.size();
  ublas::vector<double> rho(num_stored, 0.0);
  ublas::vector<double> alpha(num_stored, 0.0);
  // calculate the reciprocal of the curvature
  for (int i = 0; i < num_stored; i++)
    rho(i) = 1.0/(lambda_*s_dot_s_[i] + s_dot_y_[i]);

  v = u;
  for (int i = num_stored-1; i >= 0; --i) {
    alpha(i) = rho(i) * InnerProd(s_[i], v);
    if (lambda_ > 0.0) 
      v.EqualsAXPlusBY(1.0, v, -alpha(i)*lambda_, s_[i]);
    v.EqualsAXPlusBY(1.0, v, -alpha(i), y_[i]);
  }

  // TODO(J.Hicken): use init_Hessian explicitly here 
  if (num_stored > 0) {
    int i = num_stored-1; //num_stored-1; ????
    double yTy = InnerProd(y_[i], y_[i]);
    if (lambda_ > 0.0)
      yTy += 2.0*lambda_*s_dot_y_[i] + lambda_*lambda_*s_dot_s_[i];
    double scale = 1.0/(rho(i)*yTy);
    v *= scale;
  } else {
    v /= norm_init_;
  }

  double beta;
  for (int i = 0; i < num_stored; ++i) {
    beta = rho(i)*InnerProd(y_[i], v);
    if (lambda_ > 0.0)
      beta += rho(i)*lambda_*InnerProd(s_[i], v);
    v.EqualsAXPlusBY(1.0, v, (alpha(i) - beta), s_[i]);
  }
}
// ==============================================================================
void LimitedMemoryBFGS::ApplyHessianApprox(const DesignVector & u,
                                           DesignVector & v) const {
  cerr << "LimitedMemoryBFGS::ApplyHessianApprox: not implemented" << endl;
  throw(-1);
}
// ==============================================================================
GlobalizedBFGS::GlobalizedBFGS(const int & max_stored, ostream& out) : 
    QuasiNewton(max_stored, out) { 
  lambda_ = 0.0;
}
// ==============================================================================
void GlobalizedBFGS::MemoryRequired(int& num_vector) const {
  num_vector = 2*max_stored_ + 1;
  num_vector += max_stored_; // needed for ApplyHessianApprox
}
// ==============================================================================
void GlobalizedBFGS::set_lambda(const double & lambda) {
  if (lambda < 0.0) { // || (lambda > 1.0) ) {
    cerr << "GlobalizedBFGS(set_lambda): "
	 << "invalid value for continuation parameter lambda = "
         << lambda << endl;
    throw(-1);    
  }
  lambda_ = lambda;
}
// ==============================================================================
void GlobalizedBFGS::AddCorrection(const DesignVector & s_new,
                                   const DesignVector & y_new) {
  // only add new information if curvature condition is satisfied
  double curvature = InnerProd(s_new, y_new);
  if (curvature < std::numeric_limits<double>::epsilon()) {
    //if (curvature < sqrt(numeric_limits<double>::epsilon())) {
    *out_ << "GloblizedBFGS::AddCorrection(): "
          << "correction skipped due to curvature condition." << endl;
    return;
  }
  //*out_ << "GloblizedBFGS::AddCorrection(): correction added." << endl;

  // remove oldest elements if maximum reached
  if (static_cast<int>(s_.size()) == max_stored_) {
    s_.pop_front();
    y_.pop_front();
    s_dot_y_.pop_front();
    s_dot_s_.pop_front();
  }
  s_.push_back(s_new);
  y_.push_back(y_new);
  s_dot_y_.push_back(curvature);
  double s_norm2 = InnerProd(s_new, s_new);
  s_dot_s_.push_back(s_norm2);
}
// ==============================================================================
void GlobalizedBFGS::ApplyInvHessianApprox(DesignVector & u,
                                           DesignVector & v) const {
#if 0
  v = u;
  *out_ << "GlobalizedBFGS::ApplyInvHessian: deactivated!!!" << endl;
  return;
#endif
  int num_stored = s_.size();
  ublas::vector<double> rho(num_stored, 0.0);
  ublas::vector<double> alpha(num_stored, 0.0);
  // calculate the reciprocal of the curvature
  for (int i = 0; i < num_stored; i++) {
    rho(i) = lambda_*norm_init_*s_dot_s_[i] + (1.0-lambda_)*s_dot_y_[i];
    rho(i) = 1.0/rho(i);
  }

  v = u;
  for (int i = num_stored-1; i >= 0; --i) {
    alpha(i) = rho(i) * InnerProd(s_[i], v);
    v.EqualsAXPlusBY(1.0, v, -alpha(i)*lambda_*norm_init_, s_[i]);
    v.EqualsAXPlusBY(1.0, v, -alpha(i)*(1.0-lambda_), y_[i]);
  }

  // TODO(J.Hicken): use init_Hessian explicitly here 
  // The following uses the scaling suggested in Nocedal and Wright
  if (num_stored > 0) {
    int i = num_stored-1;
    double yTy = lambda_*lambda_*norm_init_*norm_init_*s_dot_s_[i] 
        + 2.0*(1.0-lambda_)*lambda_*norm_init_*s_dot_y_[i]
        + (1.0-lambda_)*(1.0-lambda_)*InnerProd(y_[i], y_[i]);
    double scale = 1.0/(rho(i)*yTy);
    v *= scale;
  } else {
    v /= norm_init_;
  }

  double beta;
  for (int i = 0; i < num_stored; ++i) {
    beta = rho(i) * (lambda_*norm_init_*InnerProd(s_[i], v)
                     + (1.0-lambda_)*InnerProd(y_[i], v));
    v.EqualsAXPlusBY(1.0, v, (alpha(i) - beta), s_[i]);
  }
}
// ==============================================================================
void GlobalizedBFGS::ApplyHessianApprox(const DesignVector & u,
                                        DesignVector & v) const {
  int num_stored = s_.size();
  std::vector<DesignVector> Bs(num_stored, u);
  // initialize the B*s products
  for (int i = 0; i < num_stored; ++i) {
    Bs[i] = s_[i];
  }
  v = u;
  for (int i = 0; i < num_stored; ++i) {
    double fac1 = InnerProd(y_[i], u)/s_dot_y_[i];
    double fac2 = InnerProd(Bs[i], u)/InnerProd(s_[i], Bs[i]);
    v.EqualsAXPlusBY(1.0, v, fac1, y_[i]);
    v.EqualsAXPlusBY(1.0, v, -fac2, Bs[i]);
    for (int j = i+1; j < num_stored; ++j) {
      fac1 = InnerProd(y_[i], s_[j])/s_dot_y_[i];
      fac2 = InnerProd(Bs[i], s_[j])/InnerProd(s_[i], Bs[i]);
      Bs[j].EqualsAXPlusBY(1.0, Bs[j], fac1, y_[i]);
      Bs[j].EqualsAXPlusBY(1.0, Bs[j], -fac2, Bs[i]);
    }
  }
}
// ==============================================================================
GlobalizedSR1::GlobalizedSR1(const int & max_stored, ostream& out) : 
    QuasiNewton(max_stored, out) { 
  lambda_ = 0.0;
}
// ==============================================================================
void GlobalizedSR1::MemoryRequired(int& num_vector) const {
  num_vector = 3*max_stored_ + 1 + 2 + 1;
}
// ==============================================================================
void GlobalizedSR1::set_lambda(const double & lambda) {
  if (lambda < 0.0) { // || (lambda > 1.0) ) {
    cerr << "GlobalizedSR1(set_lambda): "
	 << "invalid value for continuation parameter lambda = "
         << lambda << endl;
    throw(-1);    
  }
  lambda_ = lambda;
}
// ==============================================================================
void GlobalizedSR1::AddCorrection(const DesignVector & s_new,
                                  const DesignVector & y_new) {
  // only add new information if new denominator is sufficiently large
  const double threshold = 1.e-8;

  DesignVector y_copy, tmp;
  y_copy = y_new;
  ApplyInvHessianApprox(y_copy, tmp);
  tmp -= s_new;
  double norm_resid = tmp.Norm2();
  double norm_y_new = y_new.Norm2();
  double prod = fabs(InnerProd(y_new, tmp));

  if ( (prod < threshold*norm_resid*norm_y_new) ||
       (prod < std::numeric_limits<double>::epsilon()) ) {
    *out_ << "GloblizedSR1::AddCorrection(): "
          << "correction skipped due to threshold condition." << endl;
    return;
  }

  // remove oldest elements if maximum reached
  if (static_cast<int>(s_.size()) == max_stored_) {
    s_.pop_front();
    y_.pop_front();
  }
  s_.push_back(s_new);
  y_.push_back(y_new);
}
// ==============================================================================
void GlobalizedSR1::ApplyInvHessianApprox(DesignVector & u,
                                          DesignVector & v) const {
  int num_stored = s_.size();
  if (num_stored == 0) {
    v = u;
    v /= norm_init_;
    return;
  }
  ublas::vector<double> alpha(num_stored, 0.0);
  const double threshold = 1.e-8;
  vector<DesignVector> z_(num_stored);

  // first build the z_[i] = s_[i] - H_k * y_[i];

  for (int i = 0; i < num_stored; i++) {
    z_[i].EqualsAXPlusBY(1.0 - lambda_, s_[i], 
                         (lambda_ - 1.0)/norm_init_, y_[i]);
  }
  // check the threshold condition on z_[0]
  alpha(0) = (1.0 - lambda_)*InnerProd(z_[0], y_[0])
      + lambda_*norm_init_*InnerProd(z_[0], s_[0]);
  double norm_grad = (1.0 - lambda_)*(1.0 - lambda_)*InnerProd(y_[0], y_[0])
      + lambda_*lambda_*norm_init_*norm_init_*InnerProd(s_[0], s_[0])
      + 2.0*lambda_*norm_init_*(1.0 - lambda_)*InnerProd(y_[0], s_[0]);
  norm_grad = sqrt(norm_grad);
  *out_ << "fabs(alpha(0)) = " << fabs(alpha(0)) << endl;
  *out_ << "norm_grad = " << norm_grad << endl;
  *out_ << "z_[0].Norm2() = " << z_[0].Norm2() << endl;
  if ( (fabs(alpha(0)) < threshold*norm_grad*z_[0].Norm2()) ||
       (fabs(alpha(0)) < std::numeric_limits<double>::epsilon()) ) {
    *out_ << "GloblizedSR1::ApplyInvHessianApprox(): "
          << "correction skipped due to threshold condition." << endl;
    alpha(0) = 0.0;
  } else {
    alpha(0) = 1.0/alpha(0);
  }
  *out_ << "alpha(0) = " << alpha(0) << endl;
  for (int i = 1; i < num_stored; i++) {
    for (int j = i; j < num_stored; j++) {
      double prod = (1.0 - lambda_)*InnerProd(z_[i-1], y_[j]) 
          + lambda_*norm_init_*InnerProd(z_[i-1], s_[j]);
      z_[j].EqualsAXPlusBY(1.0, z_[j], -alpha(i-1)*prod, z_[i-1]);
    }
    // z_[i] is now ready
    // check the threshold condition on z_[i]
    alpha(i) = (1.0 - lambda_)*InnerProd(z_[i], y_[i])
        + lambda_*norm_init_*InnerProd(z_[i], s_[i]);
    norm_grad = (1.0 - lambda_)*(1.0 - lambda_)*InnerProd(y_[i], y_[i])
        + lambda_*lambda_*norm_init_*norm_init_*InnerProd(s_[i], s_[i])
        + 2.0*lambda_*norm_init_*(1.0 - lambda_)*InnerProd(y_[i], s_[i]);
    norm_grad = sqrt(norm_grad);
    if ( (fabs(alpha(i)) < threshold*norm_grad*z_[i].Norm2()) ||
         (fabs(alpha(i)) < std::numeric_limits<double>::epsilon()) ) {
      *out_ << "GloblizedSR1::ApplyInvHessianApprox(): "
            << "correction skipped due to threshold condition." << endl;
      alpha(i) = 0.0;
    } else {
      alpha(i) = 1.0/alpha(i);
    } 
  }
  // now compute v = H * u 
  v = u;
  for (int i = 0; i < num_stored; i++) {
    v.EqualsAXPlusBY(1.0, v, alpha(i)*InnerProd(z_[i], u), z_[i]);
  }
}
// ==============================================================================
void GlobalizedSR1::ApplyHessianApprox(const DesignVector & u,
                                       DesignVector & v) const {
  int num_stored = s_.size();
  std::vector<DesignVector> Bs(num_stored, u);
  // initialize the B*s products
  for (int i = 0; i < num_stored; ++i) {
    Bs[i] = s_[i];
  }
  v = u;
  for (int i = 0; i < num_stored; ++i) {
    double denom = 1.0/(InnerProd(y_[i], s_[i]) - InnerProd(Bs[i], s_[i]));
    double fac = (InnerProd(y_[i], u) - InnerProd(Bs[i], u))*denom;
    v.EqualsAXPlusBY(1.0, v, fac, y_[i]);
    v.EqualsAXPlusBY(1.0, v, -fac, Bs[i]);
    for (int j = i+1; j < num_stored; ++j) {
      fac = (InnerProd(y_[i], s_[j]) - InnerProd(Bs[i], s_[j]))*denom;
      Bs[j].EqualsAXPlusBY(1.0, Bs[j], fac, y_[i]);
      Bs[j].EqualsAXPlusBY(1.0, Bs[j], -fac, Bs[i]);
    }
  }
}
// ==============================================================================
} // namepace kona
