/**
 * \file kona.hpp
 * \brief header file for user accessible functions
 * \author Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#pragma once

#include <string>
#include <map>
#include <boost/program_options.hpp>

using std::string;
using std::map;
using boost::program_options::variables_map;

/*!
 * \brief entry point for Kona library execution
 * \param[in] userFunc - pointer to function that manages user memory
 * \param[in] optns - a map of options that override values in kona.cfg
 *
 * This is the main function that the user calls to initiate the
 * optimization process.  It is assumed that a valid Kona
 * configuration file is in the current run directory, and that
 * userFunc has been properly defined to perform the necessary tasks.
 */
void KonaOptimize(int (*userFunc)(int, int, int*, int, double*),
                  const map<string,string> & optns);

/*!
 * \brief reads user options from a configuration file and merges with optns
 * \param[in] optns - map of options that override values in file
 * \param[out] options - merged options for Optimizer object
 */
void ReadConfigFile(const map<string,string> & optns,
                    variables_map& merged_options);
