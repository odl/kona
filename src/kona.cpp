/**
 * \file kona.cpp
 * \brief definitions of user accessible functions
 * \author Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#include <fstream>
#include "kona.hpp"
#include "user_memory.hpp"
#include "krylov.hpp" // for kEpsilon
#include "optimizer.hpp"
#include "verifier.hpp"
#include "reduced_space_quasi_newton.hpp"
#include "reduced_space_newton_krylov.hpp"
#include "trust_region_rsnk.hpp"
#include "equality_constrained_rsnk.hpp"

using std::string;
using std::map;
using std::ifstream;

// ==============================================================================
void KonaOptimize(int (*userFunc)(int, int, int*, int, double*),
                  const std::map<std::string,std::string> & optns) {
  // read in user options
  variables_map all_options;
  ReadConfigFile(optns, all_options);
  if ( (all_options["opt_method"].as<string>() == "verify") ||
       (all_options["opt_method"].as<string>() == "quasi_newton") ||
       (all_options["opt_method"].as<string>() == "reduced") ||
       (all_options["opt_method"].as<string>() == "trust_reduced") ||
       (all_options["opt_method"].as<string>() == "cnstr_reduced") ) {

    // create a new Optimizer object based on user options
    boost::scoped_ptr<kona::Optimizer> optimizer;
    if (all_options["opt_method"].as<string>() == "verify")
      optimizer.reset(new kona::Verifier());
    else if (all_options["opt_method"].as<string>() == "quasi_newton")
      optimizer.reset(new kona::ReducedSpaceQuasiNewton());
    else if (all_options["opt_method"].as<string>() == "reduced")
      optimizer.reset(new kona::ReducedSpaceNewtonKrylov());
    else if (all_options["opt_method"].as<string>() == "trust_reduced")
      optimizer.reset(new kona::TrustRegionRSNK());
    else if (all_options["opt_method"].as<string>() == "cnstr_reduced")
      optimizer.reset(new kona::EqualityConstrainedRSNK());
    
    // set options and find memory requirements
    optimizer->SetOptions(all_options);
    int num_design, num_state, num_dual;
    optimizer->MemoryRequirements(num_design, num_state, num_dual);
    cout << "num_design = " << num_design << endl;
    cout << "num_state  = " << num_state << endl;
    cout << "num_dual   = " << num_dual << endl;

    // Request user memory allocation
    kona::UserMemory * memory = kona::UserMemory::Allocate(
        userFunc, num_design, num_state, num_dual);

    // solve the optimization problem
    optimizer->Solve();

    // this ensures that all objects are cleaned up before calling Deallocate
    optimizer.reset();
    
    // clean up and output
    memory->Deallocate();
    
  } else {
    // deprecated
    kona::OldOptimizer pde_opt;
    pde_opt.SetOptions(all_options);
    
    // determine how much user memory is required and initialize user
    // memory
    int num_design_vec, num_state_vec, num_dual_vec;
    pde_opt.MemoryRequirements(num_design_vec, num_state_vec, num_dual_vec);
    kona::UserMemory * memory = kona::UserMemory::Allocate(
        userFunc, num_design_vec, num_state_vec, num_dual_vec);
    
    // perform optimization
    pde_opt.Optimize();
    
    // clean up and output
    memory->Deallocate();
  }
}
// ==============================================================================
void ReadConfigFile(const map<string,string> & optns,
                    variables_map& merged_options) {
  namespace po = boost::program_options;
  po::options_description desc;

  // options related to the type of optimizer algorithm
  desc.add_options()
      ("opt_method", po::value<string>()->default_value("lnks"),
       "type of PDE-constrained optimization algorithm to use");
  
  // options related to verification
  desc.add_options()
      ("verify.design_vec", po::value<bool>()->default_value(true),
       "if true, verify the design vector linear algebra routines");
  desc.add_options()
      ("verify.pde_vec", po::value<bool>()->default_value(true),
       "if true, verify the pde vector linear algebra routines");
  desc.add_options()
      ("verify.dual_vec", po::value<bool>()->default_value(false),
       "if true, verify the dual vector linear algebra routines");
  desc.add_options()
      ("verify.gradient", po::value<bool>()->default_value(true),
       "if true, verify the derivatives of the objective");
  desc.add_options()
      ("verify.jacobian", po::value<bool>()->default_value(true),
       "if true, verify the derivatives of the state Jacobian");
  desc.add_options()
      ("verify.cnstr_jac", po::value<bool>()->default_value(false),
       "if true, verify the derivatives of the constraint Jacobian");
  desc.add_options()
      ("verify.red_grad", po::value<bool>()->default_value(false),
       "if true, verify the total derivative of the objective");
  desc.add_options()
      ("verify.linear_solve", po::value<bool>()->default_value(false),
       "if true, verify the forward and reverse linear solves");
  desc.add_options()
      ("verify.hessian_prod", po::value<bool>()->default_value(false),
       "if true, verify the Hessian-vector product");
  
  // options related to output
  desc.add_options()
      ("hist_file", po::value<string>()->default_value("kona_hist.dat"),
       "filename for the Kona convergence history information");
  desc.add_options()
      ("send_info_to_cout", po::value<bool>()->default_value(false),
       "if true, information is sent to cout rather than info_file");
  desc.add_options()
      ("info_file", po::value<string>()->default_value("kona_info.dat"),
       "filename for dumping general information (screen file)");
  
  // options related to the global nonlinear iterations
  desc.add_options()
      ("max_iter", po::value<int>()->default_value(50), 
       "maximum number of nonlinear (outer) Optimizer iterations");
  desc.add_options()
      ("des_tol", po::value<double>()->default_value(1E-6),
       "convergence tolerance for the design component of the gradient");
  desc.add_options()
      ("pde_tol", po::value<double>()->default_value(1E-6),
       "convergence tolerance for the discretized PDE");
  desc.add_options()
      ("adj_tol", po::value<double>()->default_value(1E-6),
       "convergence tolerance for the adjoint equations");
  desc.add_options()
      ("ceq_tol", po::value<double>()->default_value(1E-6),
       "convergence tolerance for the equality constraints");
  desc.add_options()
      ("init_merit_param", po::value<double>()->default_value(1e-1),
       "initial merit function penalty parameter value");
  
  // options related to trust-region globalization
  desc.add_options()
      ("trust.max_radius", po::value<double>()->default_value(1E16),
       "maximum allowable trust-region radius");
  desc.add_options()
      ("trust.init_radius", po::value<double>()->default_value(1E16),
       "initial trust-region radius");
  desc.add_options()
      ("trust.tol", po::value<double>()->default_value(0.1),
       "allowable model variation tolerance");
  
  // options related to the inner homotopy-map iterations
  desc.add_options()
      ("inner.max_iter", po::value<int>()->default_value(50), 
       "maximum number of inner nonlinear Optimizer iterations");
  desc.add_options()
      ("inner.lambda_init", po::value<double>()->default_value(0.9),
       "initial value of the homotopy/continuation parameter");
  desc.add_options()
      ("inner.des_tol", po::value<double>()->default_value(1E-2),
       "convergence tolerance for the design component of the gradient "
       "for the inner homotopy-map iterations");
  desc.add_options()
      ("inner.pde_tol", po::value<double>()->default_value(1E-2),
       "convergence tolerance for the discretized PDE "
       "for the inner homotopy-map iterations");
  desc.add_options()
      ("inner.adj_tol", po::value<double>()->default_value(1E-2),
       "convergence tolerance for the adjoint equations "
       "for the inner homotopy-map iterations");
  desc.add_options()
      ("inner.ceq_tol", po::value<double>()->default_value(1E-2),
       "convergence tolerance for the equality constraint equations "
       "for the inner homotopy-map iterations");

  // options related to Quasi-Newton method
  desc.add_options()
      ("quasi_newton.type", po::value<string>()->default_value("lbfgs"),
       "type of quasi-Newton method to use in LNKS preconditioner");  
  desc.add_options()
      ("quasi_newton.max_stored", po::value<int>()->default_value(10),
       "maximum number of updates stored for the quasi-Newton "
       "approximation");
  desc.add_options()
      ("quasi_newton.matvec_update", po::value<bool>()->default_value(false),
       "if true, update the quasi-Newton method during (reduced-space) "
       "Hessian-vector products");
  
  // options related to the line-search
  desc.add_options()
      ("line_search.type", po::value<string>()->default_value("backtrack"),
       "type of line search used");
  desc.add_options()
      ("line_search.alpha_init", po::value<double>()->default_value(1.0),
       "initial step used in strong-Wolfe line search");
  desc.add_options()
      ("line_search.alpha_min", po::value<double>()->default_value(
          kona::kEpsilon),
       "minimum step allowed by backtracking line search");
  desc.add_options()
      ("line_search.alpha_max", po::value<double>()->default_value(10.0),
       "maximum step allowed by strong-Wolfe line search");
  desc.add_options()
      ("line_search.sufficient", po::value<double>()->default_value(1e-4),
       "sufficient decrease parameter for Armijo condition");
  desc.add_options()
      ("line_search.reduct_fac", po::value<double>()->default_value(0.5),
       "step size reduction factor used in backtracking line search");
  desc.add_options()
      ("line_search.curv_cond", po::value<double>()->default_value(0.9),
       "curvature condition parameter in Wolfe conditions");
  desc.add_options()
      ("line_search.max_iter", po::value<double>()->default_value(20),
       "maximum number of step attempts permitted");
  
  // options related to the reduced-space inexact-Newton algorithm
  desc.add_options()
      ("reduced.krylov_size", po::value<int>()->default_value(10),
       "Krylov subspace size used in reduced-Hessian product");
  desc.add_options()
      ("reduced.product_fac", po::value<double>()->default_value(0.001),
       "factor 1/C that scales tolerances for reduced-Hessian product adjoints");
  desc.add_options()
      ("reduced.dynamic_tol", po::value<bool>()->default_value(false),
       "if true, the accuracy of the Hessian-vector product is dynamic");
  desc.add_options()
      ("reduced.krylov_file", po::value<string>()->
       default_value("kona_reduced_krylov.dat"),
       "output file for convergence history of Krylov solves used in "
       "reduced Hessian product");
  desc.add_options()
      ("reduced.krylov_check", po::value<bool>()->default_value(true),
       "if true, the final residual is recomputed explicited and checked");  
  desc.add_options()
      ("reduced.precond", po::value<string>()->default_value("quasi_newton"),
       "type of preconditioner for reduced-Hessian inversion");
  desc.add_options()
      ("reduced.bound_type", po::value<string>()->default_value("fixed"),
       "type of bound to use for Hessian-vector product error");
  desc.add_options()
      ("reduced.bound_frozen", po::value<bool>()->default_value(true),
       "if true, the Hessian-vector error bound is frozen after first iter");
  desc.add_options()
      ("reduced.bound_approx", po::value<bool>()->default_value(false),
       "if true, preconditioners replace solves in bound estimate");
  desc.add_options()
      ("reduced.nu", po::value<double>()->default_value(0.95),
       "nu factor that partitions error between solver and product");    
  desc.add_options()
      ("reduced.qn_hessian", po::value<bool>()->default_value(false),
       "if true, approximate the Lagrangian Hessian using Quasi-Newton");    
  
  // options related to the augmented Lagrangian method
  desc.add_options()
      ("aug_lag.mu_init", po::value<double>()->default_value(10.0),
       "initial value of the augmented-Lagrangian penalty parameter");
  desc.add_options()
      ("aug_lag.mu_pow", po::value<double>()->default_value(1.0),
       "controls the rate at which the mu parameter increases");
  
  // options related to the inexact-Newton algorithm of Bryd et al.
  desc.add_options()
      ("inexact.kappa_tol", po::value<double>()->default_value(0.01),
       "tolerance used in termination test 1");
  desc.add_options()
      ("inexact.eps_tol", po::value<double>()->default_value(0.01),
       "tolerance for constraints used in termination test 2");
  desc.add_options()
      ("inexact.tau", po::value<double>()->default_value(0.2),
       "parameter in the merit function penalty update condition");
  desc.add_options()
      ("inexact.psi", po::value<double>()->default_value(10.0),
       "threshold that determines if a step is sufficiently normal");
  desc.add_options()
      ("inexact.beta", po::value<double>()->default_value(10.0),
       "tolerance for gradient used in termination test 2");
  desc.add_options()
      ("inexact.mu", po::value<double>()->default_value(1.e-8),
       "slope parameter in Armijo sufficient decrease condition");
  
  // options related to the Krylov iterative solver
  desc.add_options()
      ("krylov.solver", po::value<string>()->default_value("fgmres"),
       "krylov iterative solver used in the Optimizer algorithm");
  desc.add_options()
      ("krylov.tolerance", po::value<double>()->default_value(0.01),
       "tolerance that relative residual is reduced below in krylov "
       "iterative solver");
  desc.add_options()
      ("krylov.space_size", po::value<int>()->default_value(5),
       "number of Krylov subspace vectors stored in GMRES-like solvers");
  desc.add_options()
      ("krylov.max_iter", po::value<int>()->default_value(5),
       "maximum number of matrix-vector products permitted during "
       "linear-problem solution");
  desc.add_options()
      ("krylov.file",
       po::value<string>()->default_value("kona_krylov.dat"),
       "file where Krylov output information is written");
  desc.add_options()
      ("krylov.check_res", po::value<bool>()->default_value(true),
       "if true, the final residual is recomputed explicited and checked");
  desc.add_options()
      ("krylov.A_norm_est", po::value<double>()->default_value(1.0),
       "estimate of the constraint Jacobian norm");
  desc.add_options()
      ("krylov.dual_tol", po::value<double>()->default_value(1.0),
       "constraint tolerance used in the FFOM with SMART method");
  
  // first, store any options provided by the optns map (these values
  // override those in the kona.cfg file)
  vector<string> args;
  map<string,string>::const_iterator it;
  for (it = optns.begin(); it != optns.end(); it++) {
    args.push_back(string("--").append(it->first));
    args.push_back(it->second);
  }
  try {
    po::store(po::command_line_parser(args).options(desc).run(), merged_options);
  } catch (const boost::program_options::error& e) {
    cerr << "Optimizer (ReadConfigFile): " << e.what() << endl;
    throw(-1);
  }

  // next, store options provided in the kona.cfg configuration file
  // The seg fault disappears if the following is commented out.
  string config_name("kona.cfg");
  ifstream config_file(config_name.c_str());
  if (!config_file) {
    cerr << "Optimizer (ReadConfigFile): unable to open configuration file."
         << endl << "check that " << config_name 
         << " exists in current (run) directory." << endl;
    throw(-1);
  }
  try {
    po::store(po::parse_config_file(config_file, desc), merged_options);   
    po::notify(merged_options);
  } catch (const boost::program_options::error& e) {
    cerr << "Optimizer (ReadConfigFile): " << e.what() << endl;
    throw(-1);
  } catch (...) {
    cerr << "Optimizer (ReadConfigFile): program_options threw an exception."
         << endl;
    throw(-1);   
  }

}
// ==============================================================================
