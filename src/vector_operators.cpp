/**
 * \file vector_operators.cpp
 * \brief definitions of members of various matrix-vector operations
 * \author Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#include <boost/scoped_ptr.hpp>
#include <boost/shared_array.hpp>

#include "./vectors.hpp"
#include "./vector_operators.hpp"
#include "./quasi_newton.hpp"

namespace kona {

// ======================================================================

template <>
void IdentityMatrix<DesignVector>::MemoryRequired(ptree & num_required) {
  num_required.put("design", 0);
  num_required.put("state", 0);
  num_required.put("dual", 0);
}

// ======================================================================


template <>
void IdentityPreconditioner<DesignVector>::MemoryRequired(ptree & num_required) {
  num_required.put("design", 0);
  num_required.put("state", 0);
  num_required.put("dual", 0);
}

// ======================================================================
  
JacobianStateProduct::JacobianStateProduct(
    const DesignVector & design_eval, const PDEVector & state_eval) {  
  eval_at_design_ = design_eval.index_;
  eval_at_state_  = state_eval.index_;
}

// ======================================================================

void JacobianStateProduct::operator()(const PDEVector & u,
				      PDEVector & v) {
  UserMemory::JacobianOperation(jacvec_s, eval_at_design_,
                                eval_at_state_, u.index_, v.index_);
}

// ======================================================================

JacobianTAdjointProduct::JacobianTAdjointProduct(
    const DesignVector & design_eval, const PDEVector & state_eval) {  
  eval_at_design_ = design_eval.index_;
  eval_at_state_  = state_eval.index_;
}

// ======================================================================

void JacobianTAdjointProduct::operator()(const PDEVector & u,
			     PDEVector & v) {
  UserMemory::JacobianOperation(tjacvec_s, eval_at_design_,
                                eval_at_state_, u.index_, v.index_);
}

// ======================================================================

StatePreconditioner::StatePreconditioner(
    const DesignVector & design_eval, const PDEVector & state_eval) {  
  eval_at_design_ = design_eval.index_;
  eval_at_state_  = state_eval.index_;
  diag_ = 0.0;
  UserMemory::BuildPreconditioner(eval_at_design_, eval_at_state_);
}

// ======================================================================

void StatePreconditioner::set_diagonal(const double & diag) {
  diag_ = diag;
}
                                     
// ======================================================================

void StatePreconditioner::operator()(PDEVector & u,
				     PDEVector & v) {
  UserMemory::PreconditionOperation(precond_s, eval_at_design_,
                                    eval_at_state_, u.index_, v.index_,
                                    diag_);
}

// ======================================================================

AdjointPreconditioner::AdjointPreconditioner(
    const DesignVector & design_eval, const PDEVector & state_eval) {  
  eval_at_design_ = design_eval.index_;
  eval_at_state_  = state_eval.index_;
}

// ======================================================================

void AdjointPreconditioner::operator()(PDEVector & u,
				       PDEVector & v) {
  UserMemory::PreconditionOperation(tprecond_s, eval_at_design_,
                                    eval_at_state_, u.index_, v.index_);
}

// ======================================================================

ReducedHessianProduct::ReducedHessianProduct(
    const int& krylov_size, ostream& outstream) {
  krylov_size_ = krylov_size;
  outstream_ = &outstream;
  bound_type_ = "fixed";
  sens_norm_ = 1.0;
  dudx_norm_ = 1.0;
  dpsidx_norm_ = 1.0;
}

// ======================================================================

ReducedHessianProduct::ReducedHessianProduct(
    const DesignVector & eval_at_design, const PDEVector & eval_at_state,
    const PDEVector & eval_at_adjoint, const DesignVector & reduced_grad,
    const PDEVector & adjoint_res, DesignVector & design_work1,
    DesignVector & design_work2, PDEVector & pde_work1,
    PDEVector & pde_work2, PDEVector & pde_work3,
    PDEVector & pde_work4, PDEVector & pde_work5,
    const int & krylov_size, const double & product_fac,
    ostream & outstream, const double & lambda, const double & scale) {
  eval_at_design_ = &eval_at_design;
  design_norm_ = eval_at_design_->Norm2();
  eval_at_state_ = &eval_at_state;
  state_norm_ = eval_at_state_->Norm2();
  eval_at_adjoint_ = &eval_at_adjoint;
  adjoint_res_ = &adjoint_res;
  reduced_grad_ = &reduced_grad;
  pde_work1_ = &pde_work1;
  pde_work2_ = &pde_work2;
  pde_work3_ = &pde_work3;
  w_adj_ = &pde_work4;
  lambda_adj_ = &pde_work5;
  design_work1_ = &design_work1;
  pert_design_  = &design_work2;
  krylov_size_ = krylov_size;
  product_fac_ = product_fac;
  product_tol_ = 1.0;
  outstream_ = &outstream;
  lambda_ = lambda;
  scale_ = scale;
  bound_type_ = "fixed";
  sens_norm_ = 1.0; // this is defined by set_sens_norm (if necessary)
  dudx_norm_ = 1.0; // defined by compute/set_sens_norms
  dpsidx_norm_ = 1.0; // defined by compute/set_sens_norms
  quasi_newton_ = NULL; // this is defined by a set_quasi_newton (if necessary)
}

// ======================================================================

void ReducedHessianProduct::Initialize(
    const DesignVector & eval_at_design, const PDEVector & eval_at_state,
    const PDEVector & eval_at_adjoint, const DesignVector & reduced_grad,
    const PDEVector & adjoint_res, std::vector<DesignVector>& design_work,
    std::vector<PDEVector>& pde_work,
    const ptree& prod_param, ostream & outstream) {
  if ( (design_work.size() < 2) || (pde_work.size() < 5) ) {
    cerr << "ReducedHessianProduct::Initialize():"
         << "design_work and/or pde_work vectors have too few elements" << endl;
    throw(-1);
  }
  
  eval_at_design_ = &eval_at_design;
  design_norm_ = eval_at_design_->Norm2();
  eval_at_state_ = &eval_at_state;
  state_norm_ = eval_at_state_->Norm2();
  eval_at_adjoint_ = &eval_at_adjoint;
  adjoint_res_ = &adjoint_res;
  reduced_grad_ = &reduced_grad;
  pde_work1_ = &pde_work[0];
  pde_work2_ = &pde_work[1];
  pde_work3_ = &pde_work[2];
  w_adj_ = &pde_work[3];
  lambda_adj_ = &pde_work[4];
  design_work1_ = &design_work[0];
  pert_design_  = &design_work[1];
  outstream_ = &outstream;
  
  product_fac_ = prod_param.get<double>("product_fac");
  product_tol_ = 1.0;
  lambda_ = prod_param.get<double>("lambda", 0.0);
  scale_ = prod_param.get<double>("scale", 0.0);
  
  quasi_newton_ = NULL;

  // determine parameters for 2nd-order adjoint tolerances
  if (!prod_param.get<bool>("bound_frozen")) {
    if (prod_param.get<string>("bound_type") == "common")
      // use a common tolerance for 2nd-order adjoints
      sens_norm_ = compute_sens_norm(prod_param.get<bool>("bound_approx"));
    else if (prod_param.get<string>("bound_type") == "independent")
      // use distinct tolerances for 2nd-order adjoints
      compute_sens_norms(dudx_norm_, dpsidx_norm_,
                         prod_param.get<bool>("bound_approx"));
    else {
      // use a fixed tolerance for adjoints
      bound_type_ = "fixed";
      sens_norm_ = 1.0;
      dudx_norm_ = 1.0;
      dpsidx_norm_ = 1.0; 
    }
  }

}

// ======================================================================

ReducedHessianProduct::~ReducedHessianProduct() {
  // after switching to scoped_ptr, these are now unecessary
  //delete primal_matvec_;
  //delete primal_precond_;
  //delete adjoint_matvec_;
  //delete adjoint_precond_;
}

// ======================================================================

void ReducedHessianProduct::MemoryRequired(ptree& num_required) {
  num_required.put("design", 3 /* for estimated sensitivity matrix norm */
                   + 1 /* for CheckProduct */ );
  num_required.put("state", 0);
  num_required.put("dual", 0);
}
// ======================================================================

void ReducedHessianProduct::set_quasi_newton(QuasiNewton * quasi_newton) {
  quasi_newton_ = quasi_newton;
}

// ======================================================================

void ReducedHessianProduct::set_product_tol(const double & product_tol) {
  if (product_tol <= 0.0) {
    cerr << "ReducedHessianProduct::set_product_tol(): "
	 << "given tolerance is <= zero." << endl;
    cerr << "product_tol = " << product_tol << endl;
    throw(-1);
  }
  product_tol_ = product_tol;
}

// ======================================================================

double ReducedHessianProduct::compute_sens_norm(const bool & approx) {
  bound_type_ = "common";
  // build the Sensitivity-product needed to compute bound on the
  // Hessian-vector product error
  double product_tol = 1e-6;
  MatrixVectorProduct<DesignVector>* sens_vec = new SensitivityProduct(
      *eval_at_design_, *eval_at_state_, *eval_at_adjoint_, *reduced_grad_,
      *adjoint_res_, *design_work1_, *pert_design_, *pde_work1_, *pde_work2_,
      *pde_work3_, *w_adj_, *lambda_adj_, krylov_size_, product_tol,
      approx, *outstream_);
  // does the tolerance product_tol make sense here? 
  int sens_iters = 0;
  int max_iters = 30;
  sens_norm_ =
      Lanczos<DesignVector>(max_iters, 0.1, *sens_vec, sens_iters, cout);
  sens_norm_ = sqrt(sens_norm_);
  *outstream_ << "Estimate of Norm of Sensitivity matrix = "
              << sens_norm_ << endl;
  return sens_norm_;
}

// ======================================================================

void ReducedHessianProduct::compute_sens_norms(double & dudx_norm,
                                               double & dpsidx_norm,
                                               const bool & approx) {
  bound_type_ = "independent";
  // build the sensitivity-product dudx
  double product_tol = 1e-6;
  MatrixVectorProduct<DesignVector>* dudx_prod = new DuDxProduct(
      *eval_at_design_, *eval_at_state_, *pde_work1_, *pde_work2_,
      krylov_size_, product_tol, approx, *outstream_);
  // does the tolerance product_tol make sense here? 
  int sens_iters = 0;
  int max_iters = 2;
  dudx_norm_ =
      Lanczos<DesignVector>(max_iters, 0.1, *dudx_prod, sens_iters, cout);
  dudx_norm_ = sqrt(dudx_norm_);
#ifdef VERBOSE_DEBUG
  *outstream_ << "ReducedHessianProduct::compute_sens_norms(): "
              << "Estimate of Norm of dudx sensitivity matrix = "
              << dudx_norm_ << endl;
#endif
  dudx_norm = dudx_norm_;

  MatrixVectorProduct<DesignVector>* dpsidx_prod = new DpsiDxProduct(
      *eval_at_design_, *eval_at_state_, *eval_at_adjoint_, *reduced_grad_,
      *adjoint_res_, *design_work1_, *pert_design_, *pde_work1_, *pde_work2_,
      *pde_work3_, *w_adj_, *lambda_adj_, krylov_size_, product_tol,
      approx, *outstream_);
  // does the tolerance product_tol make sense here? 
  sens_iters = 0;
  max_iters = 2;
  dpsidx_norm_ =
      Lanczos<DesignVector>(max_iters, 0.1, *dpsidx_prod, sens_iters, cout);
  dpsidx_norm_ = sqrt(dpsidx_norm_);
#ifdef VERBOSE_DEBUG
  *outstream_ << "ReducedHessianProduct::compute_sens_norms(): "
              << "Estimate of Norm of dpsidx sensitivity matrix = "
              << dpsidx_norm_ << endl;
#endif
  dpsidx_norm = dpsidx_norm_;
  // TEMP: This is to allow the norms to be extracted from the screen output
  //*outstream << ">> " << dudx_norm_ << " " << dpsidx_norm_ << endl;
}

// ======================================================================

void ReducedHessianProduct::set_sens_norm(const double & sens_norm) {
  if (sens_norm <= 0.0) {
    cerr << "ReducedHessianProduct::set_sens_norm(): "
	 << "given norm is <= zero." << endl;
    cerr << "sens_norm = " << sens_norm << endl;
    throw(-1);
  }
  bound_type_ = "common";
  sens_norm_ = sens_norm;
  dudx_norm_ = -1.0; // to catch surprises
  dpsidx_norm_ = -1.0;
}

// ======================================================================

void ReducedHessianProduct::set_sens_norms(const double & dudx_norm,
                                           const double & dpsidx_norm) {
  if (dudx_norm <= 0.0) {
    cerr << "ReducedHessianProduct::set_sens_norms(): "
	 << "given norm is <= zero." << endl;
    cerr << "dudx_norm = " << dudx_norm << endl;
    throw(-1);
  }
  if (dpsidx_norm <= 0.0) {
    cerr << "ReducedHessianProduct::set_sens_norms(): "
	 << "given norm is <= zero." << endl;
    cerr << "dpsidx_norm = " << dpsidx_norm << endl;
    throw(-1);
  }
  bound_type_ = "independent";
  dudx_norm_ = dudx_norm;
  dpsidx_norm_ = dpsidx_norm;
  sens_norm_ = -1.0; // to catch surprises
}

// ======================================================================

void ReducedHessianProduct::operator()(const DesignVector & u,
                                       DesignVector & v) {
  // First part: state-independent part of the Hessian-vector product
  // NOTE: we can do this at the very beginning, because this
  // part of the product does not depend on w_adj_ or lambda_adj_

  double epsilon_fd = 0.1*std::max(design_norm_, u.Norm2()); //CalcEpsilon(design_norm_, u.Norm2());
  *outstream_ << "ReducedHessianProduct: eps for design = " << epsilon_fd
              << endl;
  double eps_r = 1.0/epsilon_fd;
  pert_design_->EqualsAXPlusBY(1.0, *eval_at_design_, epsilon_fd, u);
  v.EqualsObjectiveGradient(*pert_design_, *eval_at_state_);
  design_work1_->EqualsTransJacobianTimesVector(*pert_design_, 
                                                *eval_at_state_,
                                                *eval_at_adjoint_);
  v += *design_work1_;

  // original
  v.EqualsAXPlusBY(eps_r, v, -eps_r, *reduced_grad_);
#if 0
  // temp
  v.EqualsAXPlusBY(1.0, v, -1.0, *reduced_grad_);
  cout << "relative change in reduced gradient = " 
       << v.Norm2()/reduced_grad_->Norm2() << endl;
  v *= eps_r;
#endif


  //cout << "ReducedHessianProduct: u.Norm2() = " << u.Norm2() << endl;
  //cout << "\tinitial product terms: v.Norm2() = " << v.Norm2() << endl;

  // build RHS for first adjoint system, and solve for adjoint w (stored
  // in *w_adj)
  pde_work1_->EqualsJacobianTimesVector(*eval_at_design_, *eval_at_state_,
                                        u);
  *pde_work1_ *= -1.0;
  *w_adj_ = 0.0;
  int iters;
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# Forward adjoint problem in reduced-Hessian product" << endl;
  // In the notation of the inexact-Hessian paper, product_fac = (1.0 - nu)/ C
  // where C is the unknown bound
  double rel_tol = product_tol_*product_fac_/pde_work1_->Norm2();

  if (bound_type_ == "independent")
    rel_tol /= (2.0*dpsidx_norm_);
  else if (bound_type_ == "common")
    rel_tol /= (sqrt(2.0)*sens_norm_);
  else // bound_type == "fixed"
    rel_tol = product_fac_;
  w_adj_->EqualsLinearizedSolution(*eval_at_design_, *eval_at_state_,
                                   *pde_work1_, rel_tol);
  
#if 0
  // tried using just preconditioner to define an approximate Hessian; works
  // well for early outer iterations, but slow to converge later on.
  primal_precond_->operator()(*pde_work1_, *w_adj_);
#endif
  //cout << "\tforward adjoint (w) = " << w_adj_->Norm2() << endl;

  // build RHS for second adjoint system (lambda_adj_)...
  
  // first part of LHS: perturb eval_at_design_, evaluate the adjoint
  // equation residual, finally take difference
  pde_work1_->EqualsObjectiveGradient(*pert_design_, *eval_at_state_);

  //cout << "ReducedHessianProduct::operator(): "
  //     << "norm of dJdQ = " << pde_work1_->Norm2() << endl;

  pde_work2_->EqualsTransJacobianTimesVector(*pert_design_, *eval_at_state_,
                                             *eval_at_adjoint_);

  //cout << "ReducedHessianProduct::operator(): "
  //     << "norm of A^T*psi = " << pde_work2_->Norm2() << endl;

  *pde_work1_ += *pde_work2_;
  //cout << "ReducedHessianProduct::operator(): "
  //     << "norm of pde_work1_ = " << pde_work1_->Norm2() << endl;

  //*pde_work1_ += *pde_work2_;
  //cout << "ReducedHessianProduct::operator(): "
  //     << "norm of adjoint_res_ = " << adjoint_res_->Norm2() << endl;

  // at this point *pde_work1 should contain the perturbed adjoint
  // residual, so take difference with unperturbed adjoint residual
  // NOTE: signs on eps_r move vector to RHS for subsequent solve

  // original
  pde_work1_->EqualsAXPlusBY(-eps_r, *pde_work1_, eps_r, *adjoint_res_);
#if 0
  // TEMP
  pde_work1_->EqualsAXPlusBY(-1.0, *pde_work1_, 1.0, *adjoint_res_);
  cout << "relative change in adjoint residual = " 
       << pde_work1_->Norm2()/adjoint_res_->Norm2() << endl;
  *pde_work1_ *= eps_r;
#endif

  // second part of LHS: perturb eval_at_state_, evaluate the adjoint
  // equation residual, finally take difference
  epsilon_fd = 0.0001*std::max(state_norm_, w_adj_->Norm2()); //CalcEpsilon(state_norm_, w_adj_->Norm2());
  *outstream_ << "ReducedHessianProduct: eps for state = " << epsilon_fd
              << endl;
  eps_r = 1.0/epsilon_fd;
  pde_work2_->EqualsAXPlusBY(1.0, *eval_at_state_, epsilon_fd, *w_adj_);
  pde_work3_->EqualsObjectiveGradient(*eval_at_design_, *pde_work2_);
  lambda_adj_->EqualsAXPlusBY(-eps_r, *pde_work3_, eps_r, *adjoint_res_);
  pde_work3_->EqualsTransJacobianTimesVector(*eval_at_design_, *pde_work2_,
                                             *eval_at_adjoint_);
  lambda_adj_->EqualsAXPlusBY(1.0, *lambda_adj_, -eps_r, *pde_work3_);
  *pde_work1_ += *lambda_adj_;
  *lambda_adj_ = 0.0;
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# Reverse adjoint problem in reduced-Hessian product" << endl;

  //cout << "ReducedHessianProduct::operator(): "
  //     << "norm of reverse adjoint rhs vector = " << pde_work1_->Norm2() << endl;
  rel_tol = product_tol_*product_fac_/pde_work1_->Norm2();

  if (bound_type_ == "independent")
    rel_tol /= (2.0*dudx_norm_);
  else if (bound_type_ == "common")
    rel_tol /= (sqrt(2.0)*sens_norm_);
  else // bound_type == "fixed"
    rel_tol = product_fac_;
  lambda_adj_->EqualsAdjointSolution(*eval_at_design_, *eval_at_state_,
                                     *pde_work1_, rel_tol);
  
#if 0
  adjoint_precond_->operator()(*pde_work1_, *lambda_adj_);
#endif

  // Assemble remaining pieces of the Hessian-vector product

  // Apply lambda_adj_ to design-part of the Jacobian
  design_work1_->EqualsTransJacobianTimesVector(*eval_at_design_,
                                                *eval_at_state_,
                                                *lambda_adj_);
  v += *design_work1_;
  
  // Apply w_adj_ to the cross-derivative part of Hessian
  // NOTE: pde_work2_ still holds the perturbed state
  design_work1_->EqualsObjectiveGradient(*eval_at_design_, *pde_work2_);
  pert_design_->EqualsTransJacobianTimesVector(*eval_at_design_,
                                               *pde_work2_,
                                               *eval_at_adjoint_);
  *design_work1_ += *pert_design_;
  design_work1_->EqualsAXPlusBY(eps_r, *design_work1_, -eps_r,
                                *reduced_grad_);
  v += *design_work1_;

  // update the quasi-Newton method if necessary
  if (quasi_newton_ != NULL) {
    quasi_newton_->AddCorrection(u, v);
  }

  // add globalization if necessary
  if (lambda_ > kEpsilon) {
    v.EqualsAXPlusBY((1.0 - lambda_), v, lambda_*scale_, u);
  }

#if 0
  // uncomment to compute Hessian-product error and compare with bound
  if (fabs(u.Norm2()) > kEpsilon)
    CheckProduct(u, v, *res_w, *res_lam);
#endif
#if 0
  cout << "End of ReducedHessianProduct::operator(): " << endl;
  cout << "\tNorm of u = " << u.Norm2() << endl;
  cout << "\tNorm of v = " << v.Norm2() << endl;
#endif
}

// ======================================================================

void ReducedHessianProduct::CheckProduct(const DesignVector & u,
                                         const DesignVector & z,
                                         const double & res_w,
                                         const double & res_lam) {
  //cout << "Beginning CheckProduct..." << endl;
  // v is a local design vector that needs to be accounted for in memory
  DesignVector v;
  
  // First part: state-independent part of the Hessian-vector product
  // NOTE: we can do this at the very beginning, because this
  // part of the product does not depend on w_adj_ or lambda_adj_
  double epsilon_fd = CalcEpsilon(design_norm_, u.Norm2());
  double eps_r = 1.0/epsilon_fd;
  pert_design_->EqualsAXPlusBY(1.0, *eval_at_design_, epsilon_fd, u);
  v.EqualsObjectiveGradient(*pert_design_, *eval_at_state_);
  design_work1_->EqualsTransJacobianTimesVector(*pert_design_, 
                                                *eval_at_state_,
                                                *eval_at_adjoint_);
  v += *design_work1_;
  v.EqualsAXPlusBY(eps_r, v, -eps_r, *reduced_grad_);

  // build RHS for first adjoint system, and solve for adjoint w (stored
  // in *w_adj)
  pde_work1_->EqualsJacobianTimesVector(*eval_at_design_, *eval_at_state_,
                                        u);
  *pde_work1_ *= -1.0;
  *w_adj_ = 0.0;
  int iters;
  double rel_tol = 1e-10;
  w_adj_->EqualsLinearizedSolution(*eval_at_design_, *eval_at_state_,
                                   *pde_work1_, rel_tol);
  //FGMRES<PDEVector>(krylov_size_, rel_tol, *pde_work1_, *w_adj_,
  //                  *primal_matvec_, *primal_precond_, iters, *outstream_);
  //cout << "\tCheckProduct: after first adjoint solve..." << endl;
  
  // build RHS for second adjoint system (lambda_adj_)...
  
  // first part of LHS: perturb eval_at_design_, evaluate the adjoint
  // equation residual, finally take difference
  pde_work1_->EqualsObjectiveGradient(*pert_design_, *eval_at_state_);
  pde_work2_->EqualsTransJacobianTimesVector(*pert_design_, *eval_at_state_,
                                             *eval_at_adjoint_);
  *pde_work1_ += *pde_work2_;
  // at this point *pde_work1 should contain the perturbed adjoint
  // residual, so take difference with unperturbed adjoint residual
  // NOTE: signs on eps_r move vector to RHS for subsequent solve
  pde_work1_->EqualsAXPlusBY(-eps_r, *pde_work1_, eps_r, *adjoint_res_);

  // second part of LHS: perturb eval_at_state_, evaluate the adjoint
  // equation residual, finally take difference
  epsilon_fd = CalcEpsilon(state_norm_, w_adj_->Norm2());
  eps_r = 1.0/epsilon_fd;
  pde_work2_->EqualsAXPlusBY(1.0, *eval_at_state_, epsilon_fd, *w_adj_);
  pde_work3_->EqualsObjectiveGradient(*eval_at_design_, *pde_work2_);
  lambda_adj_->EqualsAXPlusBY(-eps_r, *pde_work3_, eps_r, *adjoint_res_);
  pde_work3_->EqualsTransJacobianTimesVector(*eval_at_design_, *pde_work2_,
                                             *eval_at_adjoint_);
  lambda_adj_->EqualsAXPlusBY(1.0, *lambda_adj_, -eps_r, *pde_work3_);
  *pde_work1_ += *lambda_adj_;
  *lambda_adj_ = 0.0;
  lambda_adj_->EqualsAdjointSolution(*eval_at_design_, *eval_at_state_,
                                     *pde_work1_, rel_tol);
  //FGMRES<PDEVector>(krylov_size_, rel_tol, *pde_work1_, *lambda_adj_,
  //                  *adjoint_matvec_, *adjoint_precond_, iters,
  //                  *outstream_);
  //cout << "\tCheckProduct: after second adjoint solve..." << endl;
  
  // Assemble remaining pieces of the Hessian-vector product

  // Apply lambda_adj_ to design-part of the Jacobian
  design_work1_->EqualsTransJacobianTimesVector(*eval_at_design_,
                                                *eval_at_state_,
                                                *lambda_adj_);
  v += *design_work1_;
  
  // Apply w_adj_ to the cross-derivative part of Hessian
  // NOTE: pde_work2_ still holds the perturbed state
  design_work1_->EqualsObjectiveGradient(*eval_at_design_, *pde_work2_);
  pert_design_->EqualsTransJacobianTimesVector(*eval_at_design_,
                                               *pde_work2_,
                                               *eval_at_adjoint_);
  *design_work1_ += *pert_design_;
  design_work1_->EqualsAXPlusBY(eps_r, *design_work1_, -eps_r,
                                *reduced_grad_);
  v += *design_work1_;

  // add globalization if necessary
  if (lambda_ > kEpsilon) {
    v.EqualsAXPlusBY((1.0 - lambda_), v, lambda_*scale_, u);
  }
  //cout << "\tCheckProduct: finished computing v..." << endl;
  
  // compute true error in Hessian-vector product
  v -= z;
  double prod_err = v.Norm2();
  //cout << "\tCheckProduct: after computing norm of error = " << prod_err << endl;
#if 0
  cout << "\tsens_norm_ = " << sens_norm_ << endl;
  cout << "\tres_w      = " << res_w << endl;
  cout << "\tres_lam     = " << res_lam << endl;
#endif
  // compute the bound  
  double bound;
  if (bound_type_ == "common")
    bound = sqrt(res_w*res_w + res_lam*res_lam)*sens_norm_;
  else if (bound_type_ == "independent")
    bound = res_w*dpsidx_norm_ + res_lam*dudx_norm_;

  //cout << "\tCheckProduct: after computing bound = " << bound << endl;
  
  std::ios_base::fmtflags old = cout.setf(ios::scientific, ios::floatfield);
  std::streamsize digs = cout.precision();
  std::streamsize width = cout.width();
  cout << "| " << std::setw(16) << std::setprecision(12) << prod_err << "  "
       << bound << " " << bound/prod_err << endl;
  cout.precision(digs);
  cout.width(width);
  cout.setf(old, ios::floatfield);
}

// ======================================================================

AugLagrangianHessianProduct::AugLagrangianHessianProduct(
    const DesignVector & eval_at_design, const DualVector & eval_at_dual,
    const PDEVector & eval_at_state, const PDEVector & eval_at_adjoint,
    const DesignVector & reduced_grad, const PDEVector & adjoint_res,
    DesignVector & design_work1, DesignVector & design_work2,
    PDEVector & pde_work1, PDEVector & pde_work2, PDEVector & pde_work3,
    PDEVector & pde_work4, PDEVector & pde_work5, DualVector & dual_work1,
    const double & mu, const int & krylov_size, const double & product_fac,
    ostream & outstream, const double & lambda, const double & scale) {
  eval_at_design_ = &eval_at_design;
  design_norm_ = eval_at_design_->Norm2();
  eval_at_dual_ = &eval_at_dual;
  mu_ = mu;
  dual_plus_ceq_ = &dual_work1;
  eval_at_state_ = &eval_at_state;
  state_norm_ = eval_at_state_->Norm2();
  eval_at_adjoint_ = &eval_at_adjoint;
  adjoint_res_ = &adjoint_res;
  reduced_grad_ = &reduced_grad;
  pde_work1_ = &pde_work1;
  pde_work2_ = &pde_work2;
  pde_work3_ = &pde_work3;
  w_adj_ = &pde_work4;
  lambda_adj_ = &pde_work5;
  design_work1_ = &design_work1;
  pert_design_  = &design_work2;
  krylov_size_ = krylov_size;
  product_fac_ = product_fac;
  product_tol_ = 1.0;
  outstream_ = &outstream;
  lambda_ = lambda;
  scale_ = scale;
  bound_type_ = "fixed";
  sens_norm_ = 1.0; // this is defined by set_sens_norm (if necessary)
  dudx_norm_ = 1.0; // defined by compute/set_sens_norms
  dpsidx_norm_ = 1.0; // defined by compute/set_sens_norms
  quasi_newton_ = NULL; // this is defined by a set_quasi_newton (if necessary)

  // define the primal problem matrix-vector product and preconditioner
  primal_matvec_ = new JacobianStateProduct(*eval_at_design_, *eval_at_state_);
  primal_precond_ = new StatePreconditioner(*eval_at_design_, *eval_at_state_); 

  // define the adjoint problem matrix-vector product and preconditioner
  adjoint_matvec_ = new JacobianTAdjointProduct(*eval_at_design_,
                                                *eval_at_state_);
  adjoint_precond_ = new AdjointPreconditioner(*eval_at_design_,
                                               *eval_at_state_);
}

// ======================================================================

AugLagrangianHessianProduct::~AugLagrangianHessianProduct() {
  delete primal_matvec_;
  delete primal_precond_;
  delete adjoint_matvec_;
  delete adjoint_precond_;
}

// ======================================================================

void AugLagrangianHessianProduct::set_quasi_newton(QuasiNewton * quasi_newton) {
  quasi_newton_ = quasi_newton;
}

// ======================================================================

void AugLagrangianHessianProduct::set_product_tol(const double & product_tol) {
  if (product_tol <= 0.0) {
    cerr << "AugLagrangianHessianProduct::set_product_tol(): "
	 << "given tolerance is <= zero." << endl;
    cerr << "product_tol = " << product_tol << endl;
    throw(-1);
  }
  product_tol_ = product_tol;
}

// ======================================================================

double AugLagrangianHessianProduct::compute_sens_norm(const bool & approx) {
  bound_type_ = "common";
  // build the Sensitivity-product needed to compute bound on the
  // Hessian-vector product error
  double product_tol = 1e-6;
  MatrixVectorProduct<DesignVector>* sens_vec = new SensitivityProduct(
      *eval_at_design_, *eval_at_state_, *eval_at_adjoint_, *reduced_grad_,
      *adjoint_res_, *design_work1_, *pert_design_, *pde_work1_, *pde_work2_,
      *pde_work3_, *w_adj_, *lambda_adj_, krylov_size_, product_tol,
      approx, *outstream_);
  // does the tolerance product_tol make sense here? 
  int sens_iters = 0;
  int max_iters = 30;
  sens_norm_ =
      Lanczos<DesignVector>(max_iters, 0.1, *sens_vec, sens_iters, cout);
  sens_norm_ = sqrt(sens_norm_);
  cout << "Estimate of Norm of Sensitivity matrix = "
       << sens_norm_ << endl;
  return sens_norm_;
}

// ======================================================================

void AugLagrangianHessianProduct::compute_sens_norms(double & dudx_norm,
                                               double & dpsidx_norm,
                                               const bool & approx) {
  bound_type_ = "independent";
  // build the sensitivity-product dudx
  double product_tol = 1e-6;
  MatrixVectorProduct<DesignVector>* dudx_prod = new DuDxProduct(
      *eval_at_design_, *eval_at_state_, *pde_work1_, *pde_work2_,
      krylov_size_, product_tol, approx, *outstream_);
  // does the tolerance product_tol make sense here? 
  int sens_iters = 0;
  int max_iters = 2;
  dudx_norm_ =
      Lanczos<DesignVector>(max_iters, 0.1, *dudx_prod, sens_iters, cout);
  dudx_norm_ = sqrt(dudx_norm_);
#ifdef VERBOSE_DEBUG
  cout << "AugLagrangianHessianProduct::compute_sens_norms(): "
       << "Estimate of Norm of dudx sensitivity matrix = " << dudx_norm_ << endl;
#endif
  dudx_norm = dudx_norm_;

  MatrixVectorProduct<DesignVector>* dpsidx_prod = new DpsiDxProduct(
      *eval_at_design_, *eval_at_state_, *eval_at_adjoint_, *reduced_grad_,
      *adjoint_res_, *design_work1_, *pert_design_, *pde_work1_, *pde_work2_,
      *pde_work3_, *w_adj_, *lambda_adj_, krylov_size_, product_tol,
      approx, *outstream_);
  // does the tolerance product_tol make sense here? 
  sens_iters = 0;
  max_iters = 2;
  dpsidx_norm_ =
      Lanczos<DesignVector>(max_iters, 0.1, *dpsidx_prod, sens_iters, cout);
  dpsidx_norm_ = sqrt(dpsidx_norm_);
#ifdef VERBOSE_DEBUG
  cout << "AugLagrangianHessianProduct::compute_sens_norms(): "
       << "Estimate of Norm of dpsidx sensitivity matrix = "
       << dpsidx_norm_ << endl;
#endif
  dpsidx_norm = dpsidx_norm_;
  // TEMP: This is to allow the norms to be extracted from the screen output
  cout << ">> " << dudx_norm_ << " " << dpsidx_norm_ << endl;
}

// ======================================================================

void AugLagrangianHessianProduct::set_sens_norm(const double & sens_norm) {
  if (sens_norm <= 0.0) {
    cerr << "AugLagrangianHessianProduct::set_sens_norm(): "
	 << "given norm is <= zero." << endl;
    cerr << "sens_norm = " << sens_norm << endl;
    throw(-1);
  }
  bound_type_ = "common";
  sens_norm_ = sens_norm;
  dudx_norm_ = -1.0; // to catch surprises
  dpsidx_norm_ = -1.0;
}

// ======================================================================

void AugLagrangianHessianProduct::set_sens_norms(const double & dudx_norm,
                                           const double & dpsidx_norm) {
  if (dudx_norm <= 0.0) {
    cerr << "AugLagrangianHessianProduct::set_sens_norms(): "
	 << "given norm is <= zero." << endl;
    cerr << "dudx_norm = " << dudx_norm << endl;
    throw(-1);
  }
  if (dpsidx_norm <= 0.0) {
    cerr << "AugLagrangianHessianProduct::set_sens_norms(): "
	 << "given norm is <= zero." << endl;
    cerr << "dpsidx_norm = " << dpsidx_norm << endl;
    throw(-1);
  }
  bound_type_ = "independent";
  dudx_norm_ = dudx_norm;
  dpsidx_norm_ = dpsidx_norm;
  sens_norm_ = -1.0; // to catch surprises
}

// ======================================================================

void AugLagrangianHessianProduct::operator()(const DesignVector & u,
                                             DesignVector & v) {
  // First part: state-independent part of the Hessian-vector product
  // NOTE: we can do this at the very beginning, because this
  // part of the product does not depend on w_adj_ or lambda_adj_
  double epsilon_fd = CalcEpsilon(design_norm_, u.Norm2());
  *outstream_ << "AugLagrangianHessianProduct: eps for design = " << epsilon_fd
              << endl;
  double eps_r = 1.0/epsilon_fd;
  pert_design_->EqualsAXPlusBY(1.0, *eval_at_design_, epsilon_fd, u);
  v.EqualsObjectiveGradient(*pert_design_, *eval_at_state_);
  design_work1_->EqualsTransJacobianTimesVector(*pert_design_, 
                                                *eval_at_state_,
                                                *eval_at_adjoint_);
  v += *design_work1_;
  dual_plus_ceq_->EqualsConstraints(*pert_design_, *eval_at_state_);
  dual_plus_ceq_->EqualsAXPlusBY(1.0, *eval_at_dual_, mu_, *dual_plus_ceq_);
  design_work1_->EqualsTransCnstrJacTimesVector(*pert_design_, *eval_at_state_,
                                                *dual_plus_ceq_);
  v += *design_work1_;
  v.EqualsAXPlusBY(eps_r, v, -eps_r, *reduced_grad_);
  
  //cout << "AugLagrangianHessianProduct: u.Norm2() = " << u.Norm2() << endl;
  //cout << "\tinitial product terms: v.Norm2() = " << v.Norm2() << endl;

  // build RHS for first adjoint system, and solve for adjoint w (stored
  // in *w_adj)
  pde_work1_->EqualsJacobianTimesVector(*eval_at_design_, *eval_at_state_,
                                        u);
  *pde_work1_ *= -1.0;
  *w_adj_ = 0.0;
  int iters;
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# Forward adjoint problem in AugLag. Hessian product" << endl;
  // In the notation of the inexact-Hessian paper, product_fac = (1.0 - nu)/ C
  // where C is the unknown bound
  double rel_tol = product_tol_*product_fac_/pde_work1_->Norm2();

  if (bound_type_ == "independent")
    rel_tol /= (2.0*dpsidx_norm_);
  else if (bound_type_ == "common")
    rel_tol /= (sqrt(2.0)*sens_norm_);
  rel_tol = 1e-8;
  w_adj_->EqualsLinearizedSolution(*eval_at_design_, *eval_at_state_,
                                   *pde_work1_, rel_tol);
  
#if 0
  // tried using just preconditioner to define an approximate Hessian; works
  // well for early outer iterations, but slow to converge later on.
  primal_precond_->operator()(*pde_work1_, *w_adj_);
#endif
  //cout << "\tforward adjoint (w) = " << w_adj_->Norm2() << endl;

  // build RHS for second adjoint system (lambda_adj_)...
  
  // first part of LHS: perturb eval_at_design_, evaluate the adjoint
  // equation residual, finally take difference
  pde_work1_->EqualsObjectiveGradient(*pert_design_, *eval_at_state_);
  pde_work2_->EqualsTransJacobianTimesVector(*pert_design_, *eval_at_state_,
                                             *eval_at_adjoint_);
  *pde_work1_ += *pde_work2_;
  pde_work2_->EqualsTransCnstrJacTimesVector(*pert_design_, *eval_at_state_,
                                             *dual_plus_ceq_);
  *pde_work1_ += *pde_work2_;
  
  // at this point *pde_work1 should contain the perturbed adjoint
  // residual, so take difference with unperturbed adjoint residual
  // NOTE: signs on eps_r move vector to RHS for subsequent solve
  pde_work1_->EqualsAXPlusBY(-eps_r, *pde_work1_, eps_r, *adjoint_res_);

  // second part of LHS: perturb eval_at_state_, evaluate the adjoint
  // equation residual, finally take difference
  epsilon_fd = CalcEpsilon(state_norm_, w_adj_->Norm2());
  *outstream_ << "AugLagrangianHessianProduct: eps for state = " << epsilon_fd
              << endl;
  eps_r = 1.0/epsilon_fd;
  pde_work2_->EqualsAXPlusBY(1.0, *eval_at_state_, epsilon_fd, *w_adj_);
  pde_work3_->EqualsObjectiveGradient(*eval_at_design_, *pde_work2_);
  *lambda_adj_ = *pde_work3_;
  pde_work3_->EqualsTransJacobianTimesVector(*eval_at_design_, *pde_work2_,
                                             *eval_at_adjoint_);
  *lambda_adj_ += *pde_work3_;
  dual_plus_ceq_->EqualsConstraints(*eval_at_design_, *pde_work2_);
  dual_plus_ceq_->EqualsAXPlusBY(1.0, *eval_at_dual_, mu_, *dual_plus_ceq_);
  pde_work3_->EqualsTransCnstrJacTimesVector(*eval_at_design_, *pde_work2_,
                                             *dual_plus_ceq_);
  *lambda_adj_ += *pde_work3_;
  lambda_adj_->EqualsAXPlusBY(-eps_r, *lambda_adj_, eps_r, *adjoint_res_);
  *pde_work1_ += *lambda_adj_;
  *lambda_adj_ = 0.0;
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# Reverse adjoint problem in AugLag. Hessian product" << endl;
  rel_tol = product_tol_*product_fac_/pde_work1_->Norm2();

  if (bound_type_ == "independent")
    rel_tol /= (2.0*dudx_norm_);
  else if (bound_type_ == "common")
    rel_tol /= (sqrt(2.0)*sens_norm_);
  rel_tol = 1e-8;
  lambda_adj_->EqualsAdjointSolution(*eval_at_design_, *eval_at_state_,
                                     *pde_work1_, rel_tol);
  
#if 0
  adjoint_precond_->operator()(*pde_work1_, *lambda_adj_);
#endif

  // Assemble remaining pieces of the Hessian-vector product

  // Apply lambda_adj_ to design-part of the Jacobian
  design_work1_->EqualsTransJacobianTimesVector(*eval_at_design_,
                                                *eval_at_state_,
                                                *lambda_adj_);
  v += *design_work1_;
  
  // Apply w_adj_ to the cross-derivative part of Hessian NOTE: pde_work2_ still
  // holds the perturbed state, and dual_plus_ceq_ is still perturbed.
  design_work1_->EqualsObjectiveGradient(*eval_at_design_, *pde_work2_);
  pert_design_->EqualsTransJacobianTimesVector(*eval_at_design_,
                                               *pde_work2_,
                                               *eval_at_adjoint_);
  *design_work1_ += *pert_design_;
  pert_design_->EqualsTransCnstrJacTimesVector(*eval_at_design_, *pde_work2_,
                                               *dual_plus_ceq_);
  *design_work1_ += *pert_design_;
  design_work1_->EqualsAXPlusBY(eps_r, *design_work1_, -eps_r,
                                *reduced_grad_);
  v += *design_work1_;

  // update the quasi-Newton method if necessary
  if (quasi_newton_ != NULL) {
    quasi_newton_->AddCorrection(u, v);
  }

  // add globalization if necessary
  if (lambda_ > kEpsilon) {
    v.EqualsAXPlusBY((1.0 - lambda_), v, lambda_*scale_, u);
  }

#if 0
  // uncomment to compute Hessian-product error and compare with bound
  if (fabs(u.Norm2()) > kEpsilon)
    CheckProduct(u, v, *res_w, *res_lam);
#endif
#if 0
  cout << "End of AugLagrangianHessianProduct::operator(): " << endl;
  cout << "\tNorm of u = " << u.Norm2() << endl;
  cout << "\tNorm of v = " << v.Norm2() << endl;
#endif
}

// ======================================================================
ApproxReducedHessianProduct::ApproxReducedHessianProduct(
    const DesignVector & eval_at_design, const PDEVector & eval_at_state,
    const PDEVector & eval_at_adjoint, const DesignVector & reduced_grad,
    const PDEVector & adjoint_res, DesignVector & design_work1,
    DesignVector & design_work2, PDEVector & pde_work1, PDEVector & pde_work2,
    PDEVector & pde_work3, PDEVector & pde_work4, PDEVector & pde_work5,
    ostream & outstream, const double & lambda, const double & scale) {
  eval_at_design_ = &eval_at_design;
  design_norm_ = eval_at_design_->Norm2();
  eval_at_state_ = &eval_at_state;
  state_norm_ = eval_at_state_->Norm2();
  eval_at_adjoint_ = &eval_at_adjoint;
  adjoint_res_ = &adjoint_res;
  reduced_grad_ = &reduced_grad;
  pde_work1_ = &pde_work1;
  pde_work2_ = &pde_work2;
  pde_work3_ = &pde_work3;
  w_adj_ = &pde_work4;
  lambda_adj_ = &pde_work5;
  design_work1_ = &design_work1;
  pert_design_  = &design_work2;
  outstream_ = &outstream;
  lambda_ = lambda;
  scale_ = scale;

  // define the primal problem preconditioner
  primal_precond_ = new StatePreconditioner(*eval_at_design_, *eval_at_state_); 

  // define the adjoint problem preconditioner
  adjoint_precond_ = new AdjointPreconditioner(*eval_at_design_,
                                               *eval_at_state_);
}

// ======================================================================

ApproxReducedHessianProduct::~ApproxReducedHessianProduct() {
  delete primal_precond_;
  delete adjoint_precond_;
}

// ======================================================================

void ApproxReducedHessianProduct::set_lambda(const double & lambda) {
  lambda_ = lambda;
}

// ======================================================================

void ApproxReducedHessianProduct::operator()(const DesignVector & u,
                                       DesignVector & v) {
  // First part: state-independent part of the Hessian-vector product
  // NOTE: we can do this at the very beginning, because this
  // part of the product does not depend on w_adj_ or lambda_adj_
  double epsilon_fd = CalcEpsilon(design_norm_, u.Norm2());
#if 0
  *outstream_ << "ApproxReducedHessianProduct: eps for design = "
              << epsilon_fd << endl;
#endif
  double eps_r = 1.0/epsilon_fd;
  pert_design_->EqualsAXPlusBY(1.0, *eval_at_design_, epsilon_fd, u);
  v.EqualsObjectiveGradient(*pert_design_, *eval_at_state_);
  design_work1_->EqualsTransJacobianTimesVector(*pert_design_, 
                                                *eval_at_state_,
                                                *eval_at_adjoint_);
  v += *design_work1_;

  // original
  v.EqualsAXPlusBY(eps_r, v, -eps_r, *reduced_grad_);

#if 0
  // build RHS for first adjoint system, and solve for adjoint w (stored
  // in *w_adj)
  pde_work1_->EqualsJacobianTimesVector(*eval_at_design_, *eval_at_state_,
                                        u);
  *pde_work1_ *= -1.0;
  *w_adj_ = 0.0;
  int iters;
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# Forward adjoint problem in reduced-Hessian product" << endl;
  FGMRES<PDEVector>(krylov_size_, krylov_tol_, *pde_work1_, *w_adj_,
                    *primal_matvec_, *primal_precond_, iters, *outstream_);
#endif
  
  // build RHS for first adjoint system, and approximately solve for adjoint w
  // (stored in *w_adj)
  pde_work1_->EqualsJacobianTimesVector(*eval_at_design_, *eval_at_state_, u);
  *pde_work1_ *= -1.0;
  primal_precond_->operator()(*pde_work1_, *w_adj_);

  // build RHS for second adjoint system (lambda_adj_)...
  
  // first part of LHS: perturb eval_at_design_, evaluate the adjoint
  // equation residual, finally take difference
  pde_work1_->EqualsObjectiveGradient(*pert_design_, *eval_at_state_);
  pde_work2_->EqualsTransJacobianTimesVector(*pert_design_, *eval_at_state_,
                                             *eval_at_adjoint_);
  *pde_work1_ += *pde_work2_;

  // at this point *pde_work1 should contain the perturbed adjoint
  // residual, so take difference with unperturbed adjoint residual
  // NOTE: signs on eps_r move vector to RHS for subsequent solve

  // original
  pde_work1_->EqualsAXPlusBY(-eps_r, *pde_work1_, eps_r, *adjoint_res_);
  
  // second part of LHS: perturb eval_at_state_, evaluate the adjoint
  // equation residual, finally take difference
  epsilon_fd = CalcEpsilon(state_norm_, w_adj_->Norm2());
#if 0
  *outstream_ << "ApproxReducedHessianProduct: eps for state = " << epsilon_fd
              << endl;
#endif
  eps_r = 1.0/epsilon_fd;
  pde_work2_->EqualsAXPlusBY(1.0, *eval_at_state_, epsilon_fd, *w_adj_);
  pde_work3_->EqualsObjectiveGradient(*eval_at_design_, *pde_work2_);
  lambda_adj_->EqualsAXPlusBY(-eps_r, *pde_work3_, eps_r, *adjoint_res_);
  pde_work3_->EqualsTransJacobianTimesVector(*eval_at_design_, *pde_work2_,
                                             *eval_at_adjoint_);
  lambda_adj_->EqualsAXPlusBY(1.0, *lambda_adj_, -eps_r, *pde_work3_);
  *pde_work1_ += *lambda_adj_;
  *lambda_adj_ = 0.0;
  adjoint_precond_->operator()(*pde_work1_, *lambda_adj_);

  // Assemble remaining pieces of the Hessian-vector product

  // Apply lambda_adj_ to design-part of the Jacobian
  design_work1_->EqualsTransJacobianTimesVector(*eval_at_design_,
                                                *eval_at_state_,
                                                *lambda_adj_);
  v += *design_work1_;
  
  // Apply w_adj_ to the cross-derivative part of Hessian
  // NOTE: pde_work2_ still holds the perturbed state
  design_work1_->EqualsObjectiveGradient(*eval_at_design_, *pde_work2_);
  pert_design_->EqualsTransJacobianTimesVector(*eval_at_design_,
                                               *pde_work2_,
                                               *eval_at_adjoint_);
  *design_work1_ += *pert_design_;
  design_work1_->EqualsAXPlusBY(eps_r, *design_work1_, -eps_r,
                                *reduced_grad_);
  v += *design_work1_;

  // add globalization if necessary
  if (lambda_ > kEpsilon) {
    //v.EqualsAXPlusBY((1.0 - lambda_), v, lambda_*scale_, u);
    v.EqualsAXPlusBY(1.0, v, lambda_*scale_, u);
  }
#if 0
  cout << "End of ApproxReducedHessianProduct::operator(): " << endl;
  cout << "\tNorm of u = " << u.Norm2() << endl;
  cout << "\tNorm of v = " << v.Norm2() << endl;
#endif
}

// ======================================================================

ReducedSpacePreconditioner::ReducedSpacePreconditioner(
    QuasiNewton * quasi_newton) {
  quasi_newton_ = quasi_newton;
  diag_ = 0.0;
}

// ======================================================================

void ReducedSpacePreconditioner::MemoryRequired(ptree& num_required) {
  num_required.put("design", 0);
  num_required.put("state", 0);
  num_required.put("dual", 0);
}

// ======================================================================

void ReducedSpacePreconditioner::set_diagonal(const double & diag) {
  diag_ = diag;
  quasi_newton_->set_lambda(diag_);
  // TEMP: need to make set_lambda a QuasiNewton member...
  //static_cast<GlobalizedBFGS*>(quasi_newton_)->set_lambda(diag_);
}

// ======================================================================

void ReducedSpacePreconditioner::operator()(DesignVector & u,
                                            DesignVector & v) {
  quasi_newton_->ApplyInvHessianApprox(u, v);
  //v = u;
}
// ======================================================================

NestedReducedSpacePreconditioner::NestedReducedSpacePreconditioner(
    ostream& outstream) {
  if (!(outstream.good())) {
    cerr << "NestedReducedSpacePreconditioner::"
         << "NestedReducedSpacePreconditioner():"
	 << "ostream is not good for i/o operations." << endl;
    throw(-1);
  }
  outstream_ = &outstream;
}
    
// ======================================================================

NestedReducedSpacePreconditioner::NestedReducedSpacePreconditioner(
    const DesignVector & eval_at_design, const PDEVector & eval_at_state,
    const PDEVector & eval_at_adjoint, const DesignVector & reduced_grad,
    const PDEVector & adjoint_res, DesignVector & design_work1,
    DesignVector & design_work2, PDEVector & pde_work1, PDEVector & pde_work2,
    PDEVector & pde_work3, PDEVector & pde_work4, PDEVector & pde_work5, 
    ostream & outstream, const double & lambda,
    const double & scale) {
  // define the reduced Hessian-vector product
  mat_vec_ = new ApproxReducedHessianProduct(
      eval_at_design, eval_at_state, eval_at_adjoint, reduced_grad, adjoint_res,
      design_work1, design_work2, pde_work1, pde_work2, pde_work3, pde_work4,
      pde_work5, outstream, lambda, scale);
  outstream_ = &outstream;
}

// ======================================================================

NestedReducedSpacePreconditioner::~NestedReducedSpacePreconditioner() {
  delete mat_vec_;
}

// ======================================================================

void NestedReducedSpacePreconditioner::MemoryRequired(ptree& num_required) {
  num_required.put("design", 7); // for MINRES (should be based on solver)
  num_required.put("state", 0);
  num_required.put("dual", 0);
}
// ======================================================================

void NestedReducedSpacePreconditioner::set_diagonal(const double & diag) {
  // Add set_lambda (or set diag) to base class??...
  static_cast<ApproxReducedHessianProduct*>(mat_vec_)
            ->set_lambda(diag);
}

// ======================================================================

void NestedReducedSpacePreconditioner::Initialize(
    const DesignVector & eval_at_design, const PDEVector & eval_at_state,
    const PDEVector & eval_at_adjoint, const DesignVector & reduced_grad,
    const PDEVector & adjoint_res, std::vector<DesignVector>& design_work,
    std::vector<PDEVector> & pde_work, const ptree& prec_param,
    ostream & outstream) {
  // define the (approximate) reduced Hessian-vector product
  mat_vec_ = new ApproxReducedHessianProduct(
      eval_at_design, eval_at_state, eval_at_adjoint, reduced_grad, adjoint_res,
      design_work[0], design_work[1], pde_work[0], pde_work[1], pde_work[2],
      pde_work[3], pde_work[4], outstream);
  outstream_ = &outstream;
}

// ======================================================================

void NestedReducedSpacePreconditioner::operator()(DesignVector & u,
                                                  DesignVector & v) {
  IdentityPreconditioner<DesignVector> precond; // do-nothing preconditioner
  double tol = 0.1;
  int m = 20;
  int iters;
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# preconditioner in reduced-Hessian inversion" << endl;
  MINRES<DesignVector>(m, tol, u, v, *mat_vec_, precond, iters, *outstream_,
                       true);
}

// ======================================================================

SensitivityProduct::SensitivityProduct(
    const DesignVector & eval_at_design, const PDEVector & eval_at_state,
    const PDEVector & eval_at_adjoint, const DesignVector & reduced_grad,
    const PDEVector & adjoint_res, DesignVector & design_work1,
    DesignVector & design_work2, PDEVector & pde_work1,
    PDEVector & pde_work2, PDEVector & pde_work3,
    PDEVector & pde_work4, PDEVector & pde_work5,
    const int & krylov_size, const double & krylov_tol,
    const bool & approx, ostream & outstream) {
  eval_at_design_ = &eval_at_design;
  design_norm_ = eval_at_design_->Norm2();
  eval_at_state_ = &eval_at_state;
  state_norm_ = eval_at_state_->Norm2();
  eval_at_adjoint_ = &eval_at_adjoint;
  adjoint_res_ = &adjoint_res;
  reduced_grad_ = &reduced_grad;
  pde_work1_ = &pde_work1;
  pde_work2_ = &pde_work2;
  pde_work3_ = &pde_work3;
  v_ = &pde_work4;
  lambda_ = &pde_work5;
  design_work1_ = &design_work1;
  pert_design_  = &design_work2;
  cout << "krylov_size = " << krylov_size << endl;
  krylov_size_ = krylov_size;
  cout << "krylov_tol =" << krylov_tol << endl;
  if (krylov_tol <= 0.0) {
    cerr << "SensitivityProduct::set_krylov_tol(): "
	 << "given tolerance is <= zero." << endl;
    throw(-1);
  }
  krylov_tol_ = krylov_tol;
  outstream_ = &outstream;
  approx_ = approx;
  // define the primal problem matrix-vector product and preconditioner
  primal_matvec_ = new JacobianStateProduct(*eval_at_design_, *eval_at_state_);
  primal_precond_ = new StatePreconditioner(*eval_at_design_, *eval_at_state_); 

  // define the adjoint problem matrix-vector product and preconditioner
  adjoint_matvec_ = new JacobianTAdjointProduct(*eval_at_design_,
                                                *eval_at_state_);
  adjoint_precond_ = new AdjointPreconditioner(*eval_at_design_,
                                               *eval_at_state_);
}

// ======================================================================

SensitivityProduct::~SensitivityProduct() {
  delete primal_matvec_;
  delete primal_precond_;
  delete adjoint_matvec_;
  delete adjoint_precond_;
}

// ======================================================================

void SensitivityProduct::operator()(const DesignVector & w,
                                       DesignVector & z) {
  // perturb eval_at_design by epsilon_fd*w
  double epsilon_fd = CalcEpsilon(design_norm_, w.Norm2());
  *outstream_ << "SensitivityProduct: eps for design = " << epsilon_fd
              << endl;
  double eps_r = 1.0/epsilon_fd;
  pert_design_->EqualsAXPlusBY(1.0, *eval_at_design_, epsilon_fd, w);
  
  // Step 1: compute v = (du/dx)*w = - (dR/du)^{-1} (dR/dx)w
  pde_work1_->EqualsJacobianTimesVector(*eval_at_design_, *eval_at_state_,
                                        w);  
  *pde_work1_ *= -1.0;
  *v_ = 0.0;
  int iters;
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# 1st Forward adjoint problem in Sens. product" << endl;
  if (approx_)
    primal_precond_->operator()(*pde_work1_, *v_);
  else {
    FGMRES<PDEVector>(krylov_size_, krylov_tol_, *pde_work1_, *v_,
                      *primal_matvec_, *primal_precond_, iters, *outstream_);
    cout << "\tkrylov_tol_ = " << krylov_tol_ << endl;
    cout << "\tNorm of pde_work1_ = " << pde_work1_->Norm2() << endl;
  }
  
  // Step 2: compute lambda = (dpsi/dx)*w = - (dR/du)^{-T}[(dS/dx)*w + (dS/du)v]
  
  // perturb eval_at_design_, evaluate the adjoint equation residual, finally
  // take difference to find (dS/dx)*w
  pde_work1_->EqualsObjectiveGradient(*pert_design_, *eval_at_state_);
  pde_work2_->EqualsTransJacobianTimesVector(*pert_design_, *eval_at_state_,
                                             *eval_at_adjoint_);
  *pde_work1_ += *pde_work2_; // *pde_work1 should now contain pert. adjoint res
  // NOTE: signs on eps_r move vector to RHS for subsequent solve
  pde_work1_->EqualsAXPlusBY(-eps_r, *pde_work1_, eps_r, *adjoint_res_);

  // Next, perturb eval_at_state_, evaluate the adjoint equation residual,
  // finally take difference to find (dS/du)*v
  epsilon_fd = CalcEpsilon(state_norm_, v_->Norm2());
  *outstream_ << "SensitivityProduct: eps for state = " << epsilon_fd
              << endl;
  eps_r = 1.0/epsilon_fd;
  pde_work2_->EqualsAXPlusBY(1.0, *eval_at_state_, epsilon_fd, *v_);
  pde_work3_->EqualsObjectiveGradient(*eval_at_design_, *pde_work2_);
  lambda_->EqualsAXPlusBY(-eps_r, *pde_work3_, eps_r, *adjoint_res_);
  pde_work3_->EqualsTransJacobianTimesVector(*eval_at_design_, *pde_work2_,
                                             *eval_at_adjoint_);
  lambda_->EqualsAXPlusBY(1.0, *lambda_, -eps_r, *pde_work3_);
  *pde_work1_ += *lambda_;
  *lambda_ = 0.0;
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# 1st Reverse adjoint problem in Sens. product" << endl;
  if (approx_)
    adjoint_precond_->operator()(*pde_work1_, *lambda_);
  else {
    FGMRES<PDEVector>(krylov_size_, krylov_tol_, *pde_work1_, *lambda_,
                      *adjoint_matvec_, *adjoint_precond_, iters, *outstream_);
  }
  
  // Step 3: compute z = (du/dx)^{T} v = - (dR/dx)^T (dR/du)^{-T} v
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# 2nd Reverse adjoint problem in Sens. product" << endl;
  if (approx_)
    adjoint_precond_->operator()(*v_, *pde_work1_);
  else {
    *pde_work1_ = 0.0;
    FGMRES<PDEVector>(krylov_size_, krylov_tol_, *v_, *pde_work1_,
                      *adjoint_matvec_, *adjoint_precond_, iters, *outstream_);
  }
  z.EqualsTransJacobianTimesVector(*eval_at_design_, *eval_at_state_,
                                   *pde_work1_);
  cout << "\tNorm of z = " << z.Norm2() << endl;
  // !!!! still need to multiply z by -1 !!!! (see below)

  // Step 4: compute sigma = (dpsi/dx)^{T} lambda
  //                       = -[dS/dx + dS/du*du/dx]^T (dR/du)^{-1} lambda

  *pde_work1_ = 0.0;
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# 2nd Forward adjoint problem in Sens. product" << endl;
  if (approx_)
    primal_precond_->operator()(*lambda_, *pde_work1_);
  else {
    FGMRES<PDEVector>(krylov_size_, krylov_tol_, *lambda_, *pde_work1_,
                      *primal_matvec_, *primal_precond_, iters, *outstream_);
  }
    
  // compute dS/dx ^{T} pde_work1_
  epsilon_fd = CalcEpsilon(state_norm_, pde_work1_->Norm2());
  *outstream_ << "SensitivityProduct: eps for state = " << epsilon_fd
              << endl;
  eps_r = 1.0/epsilon_fd;
  pde_work2_->EqualsAXPlusBY(1.0, *eval_at_state_, epsilon_fd, *pde_work1_);
  design_work1_->EqualsObjectiveGradient(*eval_at_design_, *pde_work2_);
  pert_design_->EqualsTransJacobianTimesVector(*eval_at_design_,
                                               *pde_work2_,
                                               *eval_at_adjoint_);
  *design_work1_ += *pert_design_;
  design_work1_->EqualsAXPlusBY(eps_r, *design_work1_, -eps_r,
                                *reduced_grad_);
  z += *design_work1_;  // !!! needs to be multiplied by -1 still
  z *= -1.0; // done!!
  cout << "\tNorm of z = " << z.Norm2() << endl;

  // compute dS/du^{T} pde_work1 using FD
  pde_work3_->EqualsObjectiveGradient(*eval_at_design_, *pde_work2_);
  lambda_->EqualsAXPlusBY(eps_r, *pde_work3_, -eps_r, *adjoint_res_);
  pde_work3_->EqualsTransJacobianTimesVector(*eval_at_design_, *pde_work2_,
                                             *eval_at_adjoint_);
  lambda_->EqualsAXPlusBY(1.0, *lambda_, eps_r, *pde_work3_);

  // compute (du/dx)^{T} * (dS/du)^{T} pde_work1
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# 3rd Reverse adjoint problem in Sens. product" << endl;
  if (approx_)
    adjoint_precond_->operator()(*lambda_, *pde_work1_);
  else {
    *pde_work1_ = 0.0;
    FGMRES<PDEVector>(krylov_size_, krylov_tol_, *lambda_, *pde_work1_,
                      *adjoint_matvec_, *adjoint_precond_, iters, *outstream_);
  }
  design_work1_->EqualsTransJacobianTimesVector(
      *eval_at_design_, *eval_at_state_, *pde_work1_);
  // the quantity held in design_work1_ actually has the correct sign
  z += *design_work1_;

  cout << "End of SensitivityProduct::operator(): " << endl;
  cout << "\tNorm of w = " << w.Norm2() << endl;
  cout << "\tNorm of z = " << z.Norm2() << endl;
}

// ======================================================================
  
DuDxProduct::DuDxProduct(
    const DesignVector & eval_at_design, const PDEVector & eval_at_state,
    PDEVector & pde_work1, PDEVector & pde_work2,
    const int & krylov_size, const double & krylov_tol,
    const bool & approx, ostream & outstream) {
  eval_at_design_ = &eval_at_design;
  eval_at_state_ = &eval_at_state;
  pde_work_ = &pde_work1;
  v_ = &pde_work2;
  cout << "krylov_size = " << krylov_size << endl;
  krylov_size_ = krylov_size;
  cout << "krylov_tol =" << krylov_tol << endl;
  if (krylov_tol <= 0.0) {
    cerr << "DuDxProduct::set_krylov_tol(): "
	 << "given tolerance is <= zero." << endl;
    throw(-1);
  }
  krylov_tol_ = krylov_tol;
  outstream_ = &outstream;
  approx_ = approx;
  // define the primal problem matrix-vector product and preconditioner
  primal_matvec_ = new JacobianStateProduct(*eval_at_design_, *eval_at_state_);
  primal_precond_ = new StatePreconditioner(*eval_at_design_, *eval_at_state_); 

  // define the adjoint problem matrix-vector product and preconditioner
  adjoint_matvec_ = new JacobianTAdjointProduct(*eval_at_design_,
                                                *eval_at_state_);
  adjoint_precond_ = new AdjointPreconditioner(*eval_at_design_,
                                               *eval_at_state_);
}

// ======================================================================

DuDxProduct::~DuDxProduct() {
  delete primal_matvec_;
  delete primal_precond_;
  delete adjoint_matvec_;
  delete adjoint_precond_;
}

// ======================================================================

void DuDxProduct::operator()(const DesignVector & w, DesignVector & z) {
  
  // Step 1: compute v = (du/dx)*w = - (dR/du)^{-1} (dR/dx)w
  pde_work_->EqualsJacobianTimesVector(*eval_at_design_, *eval_at_state_, w); 
  *pde_work_ *= -1.0;
  int iters;
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# Forward adjoint problem in dudx product" << endl;
  if (approx_)
    primal_precond_->operator()(*pde_work_, *v_);
  else {
    *v_ = 0.0;
    FGMRES<PDEVector>(krylov_size_, krylov_tol_, *pde_work_, *v_,
                      *primal_matvec_, *primal_precond_, iters, *outstream_);
  }
  cout << "\tkrylov_tol_ = " << krylov_tol_ << endl;
  cout << "\tNorm of pde_work_ = " << pde_work_->Norm2() << endl;
  
  // Step 2: compute z = (du/dx)^{T} v = - (dR/dx)^T (dR/du)^{-T} v
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# Reverse adjoint problem in dudx product" << endl;
  if (approx_)
    adjoint_precond_->operator()(*v_, *pde_work_);
  else {
    *pde_work_ = 0.0;
    FGMRES<PDEVector>(krylov_size_, krylov_tol_, *v_, *pde_work_,
                      *adjoint_matvec_, *adjoint_precond_, iters, *outstream_);
  }
  z.EqualsTransJacobianTimesVector(*eval_at_design_, *eval_at_state_,
                                   *pde_work_);
  cout << "\tNorm of z = " << z.Norm2() << endl;
  z *= -1.0;
}

// ======================================================================

DpsiDxProduct::DpsiDxProduct(
    const DesignVector & eval_at_design, const PDEVector & eval_at_state,
    const PDEVector & eval_at_adjoint, const DesignVector & reduced_grad,
    const PDEVector & adjoint_res, DesignVector & design_work1,
    DesignVector & design_work2, PDEVector & pde_work1,
    PDEVector & pde_work2, PDEVector & pde_work3,
    PDEVector & pde_work4, PDEVector & pde_work5,
    const int & krylov_size, const double & krylov_tol,
    const bool & approx, ostream & outstream) {
  eval_at_design_ = &eval_at_design;
  design_norm_ = eval_at_design_->Norm2();
  eval_at_state_ = &eval_at_state;
  state_norm_ = eval_at_state_->Norm2();
  eval_at_adjoint_ = &eval_at_adjoint;
  adjoint_res_ = &adjoint_res;
  reduced_grad_ = &reduced_grad;
  pde_work1_ = &pde_work1;
  pde_work2_ = &pde_work2;
  pde_work3_ = &pde_work3;
  v_ = &pde_work4;
  lambda_ = &pde_work5;
  design_work1_ = &design_work1;
  pert_design_  = &design_work2;
  cout << "krylov_size = " << krylov_size << endl;
  krylov_size_ = krylov_size;
  cout << "krylov_tol =" << krylov_tol << endl;
  if (krylov_tol <= 0.0) {
    cerr << "DpsiDxProduct::set_krylov_tol(): "
	 << "given tolerance is <= zero." << endl;
    throw(-1);
  }
  krylov_tol_ = krylov_tol;
  outstream_ = &outstream;
  approx_ = approx;
  // define the primal problem matrix-vector product and preconditioner
  primal_matvec_ = new JacobianStateProduct(*eval_at_design_, *eval_at_state_);
  primal_precond_ = new StatePreconditioner(*eval_at_design_, *eval_at_state_); 

  // define the adjoint problem matrix-vector product and preconditioner
  adjoint_matvec_ = new JacobianTAdjointProduct(*eval_at_design_,
                                                *eval_at_state_);
  adjoint_precond_ = new AdjointPreconditioner(*eval_at_design_,
                                               *eval_at_state_);
}

// ======================================================================

DpsiDxProduct::~DpsiDxProduct() {
  delete primal_matvec_;
  delete primal_precond_;
  delete adjoint_matvec_;
  delete adjoint_precond_;
}

// ======================================================================

void DpsiDxProduct::operator()(const DesignVector & w,
                                       DesignVector & z) {
  // perturb eval_at_design by epsilon_fd*w
  double epsilon_fd = CalcEpsilon(design_norm_, w.Norm2());
  *outstream_ << "DpsiDxProduct: eps for design = " << epsilon_fd
              << endl;
  double eps_r = 1.0/epsilon_fd;
  pert_design_->EqualsAXPlusBY(1.0, *eval_at_design_, epsilon_fd, w);
  
  // Step 1: compute v = (du/dx)*w = - (dR/du)^{-1} (dR/dx)w
  pde_work1_->EqualsJacobianTimesVector(*eval_at_design_, *eval_at_state_,
                                        w);  
  *pde_work1_ *= -1.0;
  int iters;
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# 1st Forward adjoint problem in dpsidx product" << endl;
  if (approx_)
    primal_precond_->operator()(*pde_work1_, *v_);
  else {
    *v_ = 0.0;
    FGMRES<PDEVector>(krylov_size_, krylov_tol_, *pde_work1_, *v_,
                      *primal_matvec_, *primal_precond_, iters, *outstream_);
  }
  cout << "\tkrylov_tol_ = " << krylov_tol_ << endl;
  cout << "\tNorm of pde_work1_ = " << pde_work1_->Norm2() << endl;
  // Step 2: compute lambda = (dpsi/dx)*w = - (dR/du)^{-T}[(dS/dx)*w + (dS/du)v]
  
  // perturb eval_at_design_, evaluate the adjoint equation residual, finally
  // take difference to find (dS/dx)*w
  pde_work1_->EqualsObjectiveGradient(*pert_design_, *eval_at_state_);
  pde_work2_->EqualsTransJacobianTimesVector(*pert_design_, *eval_at_state_,
                                             *eval_at_adjoint_);
  *pde_work1_ += *pde_work2_; // *pde_work1 should now contain pert. adjoint res
  // NOTE: signs on eps_r move vector to RHS for subsequent solve
  pde_work1_->EqualsAXPlusBY(-eps_r, *pde_work1_, eps_r, *adjoint_res_);

  // Next, perturb eval_at_state_, evaluate the adjoint equation residual,
  // finally take difference to find (dS/du)*v
  epsilon_fd = CalcEpsilon(state_norm_, v_->Norm2());
  *outstream_ << "DpsiDxProduct: eps for state = " << epsilon_fd
              << endl;
  eps_r = 1.0/epsilon_fd;
  pde_work2_->EqualsAXPlusBY(1.0, *eval_at_state_, epsilon_fd, *v_);
  pde_work3_->EqualsObjectiveGradient(*eval_at_design_, *pde_work2_);
  lambda_->EqualsAXPlusBY(-eps_r, *pde_work3_, eps_r, *adjoint_res_);
  pde_work3_->EqualsTransJacobianTimesVector(*eval_at_design_, *pde_work2_,
                                             *eval_at_adjoint_);
  lambda_->EqualsAXPlusBY(1.0, *lambda_, -eps_r, *pde_work3_);
  *pde_work1_ += *lambda_;
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# 1st Reverse adjoint problem in dpsidx product" << endl;
  if (approx_)
    adjoint_precond_->operator()(*pde_work1_, *lambda_);
  else {
    *lambda_ = 0.0;
    FGMRES<PDEVector>(krylov_size_, krylov_tol_, *pde_work1_, *lambda_,
                      *adjoint_matvec_, *adjoint_precond_, iters, *outstream_);
  }
  // Step 3: compute sigma = (dpsi/dx)^{T} lambda
  //                       = -[dS/dx + dS/du*du/dx]^T (dR/du)^{-1} lambda
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# 2nd Forward adjoint problem in dpsidx product" << endl;
  if (approx_)
    primal_precond_->operator()(*lambda_, *pde_work1_);
  else {
    *pde_work1_ = 0.0;
    FGMRES<PDEVector>(krylov_size_, krylov_tol_, *lambda_, *pde_work1_,
                      *primal_matvec_, *primal_precond_, iters, *outstream_);
  }
  // compute dS/dx ^{T} pde_work1_
  epsilon_fd = CalcEpsilon(state_norm_, pde_work1_->Norm2());
  *outstream_ << "DpsiDxProduct: eps for state = " << epsilon_fd
              << endl;
  eps_r = 1.0/epsilon_fd;
  pde_work2_->EqualsAXPlusBY(1.0, *eval_at_state_, epsilon_fd, *pde_work1_);
  design_work1_->EqualsObjectiveGradient(*eval_at_design_, *pde_work2_);
  pert_design_->EqualsTransJacobianTimesVector(*eval_at_design_,
                                               *pde_work2_,
                                               *eval_at_adjoint_);
  *design_work1_ += *pert_design_;
  design_work1_->EqualsAXPlusBY(eps_r, *design_work1_, -eps_r,
                                *reduced_grad_);
  z = *design_work1_;  // !!! needs to be multiplied by -1
  z *= -1.0; // done!!
  cout << "\tNorm of z = " << z.Norm2() << endl;

  // compute dS/du^{T} pde_work1 using FD
  pde_work3_->EqualsObjectiveGradient(*eval_at_design_, *pde_work2_);
  lambda_->EqualsAXPlusBY(eps_r, *pde_work3_, -eps_r, *adjoint_res_);
  pde_work3_->EqualsTransJacobianTimesVector(*eval_at_design_, *pde_work2_,
                                             *eval_at_adjoint_);
  lambda_->EqualsAXPlusBY(1.0, *lambda_, eps_r, *pde_work3_);

  // compute (du/dx)^{T} * (dS/du)^{T} pde_work1
  *outstream_ << "#---------------------------------------------------" << endl;
  *outstream_ << "# 2nd Reverse adjoint problem in dpsidx product" << endl;
  if (approx_)
    adjoint_precond_->operator()(*lambda_, *pde_work1_);
  else {
    *pde_work1_ = 0.0;
    FGMRES<PDEVector>(krylov_size_, krylov_tol_, *lambda_, *pde_work1_,
                      *adjoint_matvec_, *adjoint_precond_, iters, *outstream_);
  }
  design_work1_->EqualsTransJacobianTimesVector(
      *eval_at_design_, *eval_at_state_, *pde_work1_);
  // the quantity held in design_work1_ actually has the correct sign
  z += *design_work1_;

  cout << "End of DpsiDxProduct::operator(): " << endl;
  cout << "\tNorm of w = " << w.Norm2() << endl;
  cout << "\tNorm of z = " << z.Norm2() << endl;
}

// ======================================================================

ConstraintJacobianProduct::ConstraintJacobianProduct(ostream& outstream) {
  outstream_ = &outstream;
}

// ======================================================================

ConstraintJacobianProduct::ConstraintJacobianProduct(
    const DesignVector & eval_at_design, const PDEVector & eval_at_state,
    DesignVector & design_work1, PDEVector & pde_work1, PDEVector & pde_work2,
    DualVector & dual_work1, ostream & outstream, const bool& approx) {
  eval_at_design_ = &eval_at_design;
  eval_at_state_ = &eval_at_state;
  pde_work1_ = &pde_work1;
  adj_ = &pde_work2;
  design_work_ = &design_work1;
  dual_work_ = &dual_work1;
  outstream_ = &outstream;
  transpose_ = false;
  design_ = true;
  target_ = true;
  approx_ = approx;

  // define the primal problem matrix-vector product and preconditioner
  primal_matvec_.reset(new JacobianStateProduct(*eval_at_design_,
                                                *eval_at_state_));
  primal_precond_.reset(new StatePreconditioner(*eval_at_design_,
                                                *eval_at_state_)); 

  // define the adjoint problem matrix-vector product and preconditioner
  adjoint_matvec_.reset(new JacobianTAdjointProduct(*eval_at_design_,
                                                    *eval_at_state_));
  adjoint_precond_.reset(new AdjointPreconditioner(*eval_at_design_,
                                                   *eval_at_state_));
}

// ======================================================================

ConstraintJacobianProduct::~ConstraintJacobianProduct() {
  // delete primal_matvec_;
  // delete primal_precond_;
  // delete adjoint_matvec_;
  // delete adjoint_precond_;
}

// ======================================================================

void ConstraintJacobianProduct::MemoryRequired(ptree& num_required) {
  // the present version (see operator() below) has no internal memory req.
  num_required.put("design", 0);
  num_required.put("state", 0);
  num_required.put("dual", 0);
}

// ======================================================================

void ConstraintJacobianProduct::Transpose() {
  transpose_ = true;
}

// ======================================================================
void ConstraintJacobianProduct::Untranspose() {
  transpose_ = false;
}

// ======================================================================

void ConstraintJacobianProduct::RestrictToDesign() {
  design_ = true;
  target_ = false;
}

// ======================================================================

void ConstraintJacobianProduct::RestrictToTarget() {
  design_ = false;
  target_ = true;
}

// ======================================================================

void ConstraintJacobianProduct::Unrestrict() {
  design_ = true;
  target_ = true;
}

// ======================================================================

void ConstraintJacobianProduct::operator()(const DesignVector & u,
                                           DesignVector & v) {
  // check that at least one of design_ or target_ is true
  if ( (!design_) && (!target_) ) {
    cerr << "ConstraintJacobianProduct::operator()(): "
	 << "both design_ and target_ components are set to false." << endl;
    throw(-1);
  }

  if (transpose_) { // Compute v = A^{T} (u)_(target subspace)

    // Convert the u vector's target subspace to a dual vector
    dual_work_->Convert(u);
    
    // Compute (dc/dx)^T * dual_work_ and add to v
    design_work_->EqualsTransCnstrJacTimesVector(*eval_at_design_,
                                                 *eval_at_state_, *dual_work_);
    v = *design_work_;
    // build RHS for adjoint system and solve (adjoint stored in *adj_)
    pde_work1_->EqualsTransCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                               *dual_work_);
    *pde_work1_ *= -1.0;
    *adj_ = 0.0;
    //*outstream_ << "#---------------------------------------------------"
    //            << endl;
    //*outstream_ << "# Reverse adjoint in Constraint Jacobian product" << endl;
    if (approx_) {
      adjoint_precond_->operator()(*pde_work1_, *adj_);
    } else {
      double rel_tol = 1e-4;
      adj_->EqualsAdjointSolution(*eval_at_design_, *eval_at_state_,
                                *pde_work1_, rel_tol);
    }
    // Apply lambda_adj_ to design-part of the Jacobian
    design_work_->EqualsTransJacobianTimesVector(*eval_at_design_,
                                                  *eval_at_state_,
                                                  *adj_);
    v += *design_work_;

    // restrict if necessary
    if ( (design_) && (!target_) )
      v.RestrictToDesign();
    if ( (!design_) && (target_) )
      v.RestrictToTarget();

  } else { // Compute (v)_(target subspace) = A* u

    // restrict if necessary
    *design_work_ = u;
    if ( (design_) && (!target_) )
      design_work_->RestrictToDesign();
    if ( (!design_) && (target_) )
      design_work_->RestrictToTarget();
    
    // Compute (dc/dx) * u and store in dual_work_
    dual_work_->EqualsCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                          *design_work_);
    v.Convert(*dual_work_);
    // build RHS for adjoint system and solve (adjoint stored in *adj_)
    pde_work1_->EqualsJacobianTimesVector(*eval_at_design_, *eval_at_state_,
                                          *design_work_);
    *pde_work1_ *= -1.0;
    *adj_ = 0.0;
    //*outstream_ << "#---------------------------------------------------"
    //            << endl;
    //*outstream_ << "# Forward adjoint Constraint Jacobian product" << endl;
    if (approx_) {
      primal_precond_->operator()(*pde_work1_, *adj_);
    } else {
      double rel_tol = 1e-4;
      adj_->EqualsLinearizedSolution(*eval_at_design_, *eval_at_state_,
                                     *pde_work1_, rel_tol);
    }
    // finish the dual part of the KKT-matrix-vector product; add (dc/du)*adj_ to
    // v.dual_
    dual_work_->EqualsCnstrJacTimesVector(*eval_at_design_, *eval_at_state_,
                                          *adj_);
    design_work_->Convert(*dual_work_);
    v += *design_work_;
  }
}

} // namespace kona
