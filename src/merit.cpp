/**
 * \file line_search.cpp
 * \brief definitions of members of LineSearch and derived classes 
 * \author Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#include "./merit.hpp"
#include "./krylov.hpp"
#include <math.h>

namespace kona {
// ==============================================================================
GlobalizedMerit::GlobalizedMerit(
    DesignVector & x_init, const double & lambda, const double & scale,
    DesignVector & search_dir, DesignVector & x_start,
    const double & p_dot_grad, PDEVector & state_work,
    PDEVector & adjoint_work) {
  lambda_ = lambda;
  scale_ = scale;
  x_init_ = &x_init;
  x_start_ = &x_start;
  search_dir_ = &search_dir;
  state_work_ = &state_work;
  adjoint_work_ = &adjoint_work;
  alpha_last_ = -1.0;
  obj_start_ = ObjectiveValue(*x_start_, *state_work_);
  p_dot_grad_ = p_dot_grad;
}
// ==============================================================================
void GlobalizedMerit::MemoryRequired(int& design_memory, int& pde_memory,
                                     int& dual_memory) {
  design_memory = 2;  // design_work_ and dJdX in eval
  pde_memory = 0;
  dual_memory = 0;
}
// ==============================================================================
double GlobalizedMerit::EvalFunc(double alpha) {
  design_work_.EqualsAXPlusBY(1.0, *x_start_, alpha, *search_dir_);
  //CurrentSolution(design_work_, *state_work_, *adjoint_work_);
  double merit;
  double obj;
  if (fabs(alpha) < kEpsilon) {
    merit = obj_start_;
  } else if (state_work_->EqualsPrimalSolution(design_work_)) {
    // solver was successful in solving PDE
    merit = ObjectiveValue(design_work_, *state_work_);
    alpha_last_ = alpha;
  } else {
    // solver failed to solve PDE
    merit = 1.e+100;
  }
  merit = (1.0 - lambda_)*merit;
  design_work_ -= (*x_init_);
  merit += 0.5*lambda_*scale_*InnerProd(design_work_, design_work_);
  return merit;
}
// ==============================================================================
double GlobalizedMerit::EvalGrad(double alpha) {
  design_work_.EqualsAXPlusBY(1.0, *x_start_, alpha, *search_dir_);
  double diff_merit;
  if (fabs(alpha) < kEpsilon) {
    // use stored p_dot_grad_ values
    diff_merit = (1.0 - lambda_)*p_dot_grad_;
  } else {
    if (fabs(alpha - alpha_last_) > kEpsilon) {
      // the primal is out-of date, so recompute
      state_work_->EqualsPrimalSolution(design_work_);
    }
    adjoint_work_->EqualsAdjointSolution(design_work_, *state_work_);
    // first, add contribution due to actual objective function
    DesignVector dJdX;
    dJdX.EqualsObjectiveGradient(design_work_, *state_work_);
    diff_merit = (1.0 - lambda_)*InnerProd(dJdX, *search_dir_);
    dJdX.EqualsTransJacobianTimesVector(design_work_, *state_work_,
                                      *adjoint_work_);
    diff_merit += (1.0 - lambda_)*InnerProd(dJdX, *search_dir_);
  }
  // finally, add contribution from quadractic homotopy term
  diff_merit += lambda_*scale_*InnerProd(design_work_, *search_dir_);
  diff_merit -= lambda_*scale_*InnerProd(*x_init_, *search_dir_);
  return diff_merit;  
}
// ==============================================================================
L2Merit::L2Merit(const double & mu, const DesignVector & design_start,
                 const PDEVector & state_start, const DesignVector & design_dir,
                 const PDEVector & state_dir, DesignVector & design_work,
                 PDEVector & state_work, PDEVector & eqn_work) {
  mu_ = mu;
  design_start_ = &design_start;
  state_start_ = &state_start;
  design_dir_ = &design_dir;
  state_dir_ = &state_dir;
  design_work_ = &design_work;
  state_work_ = &state_work;
  eqn_work_ = &eqn_work;
}
// ==============================================================================
void L2Merit::MemoryRequired(int& design_memory, int& pde_memory,
                             int& dual_memory) {
  design_memory = 0;
  pde_memory = 0;
  dual_memory = 0;
}
// ==============================================================================
double L2Merit::EvalFunc(double alpha) {
  design_work_->EqualsAXPlusBY(1.0, *design_start_, alpha, *design_dir_);
  state_work_->EqualsAXPlusBY(1.0, *state_start_, alpha, *state_dir_);
  double merit = ObjectiveValue(*design_work_, *state_work_);
  eqn_work_->EqualsStateEquations(*design_work_, *state_work_);
  merit += mu_*eqn_work_->Norm2();
  return merit;
}
// ==============================================================================
double L2Merit::EvalGrad(double alpha) {
  cerr << "L2Merit(EvalGrad): "
       << "L2Merit is not set-up to evaluate its derivative" << endl;
  throw(-1);
}
// ==============================================================================
double L2Merit::EstimateDirDerivative(const double & eqn_norm,
                                      const double & lin_eqn_norm) {
  //cout << "inside L2Merit::EstimateDirDerivative()" << endl;
  state_work_->EqualsObjectiveGradient(*design_start_, *state_start_);
  design_work_->EqualsObjectiveGradient(*design_start_, *state_start_);
  double Dmerit = InnerProd(*state_work_, *state_dir_)
      + InnerProd(*design_work_, *design_dir_);
  Dmerit -= mu_*(eqn_norm - lin_eqn_norm);
  return Dmerit;
}
// ==============================================================================
ReducedSpaceL2Merit::ReducedSpaceL2Merit(
    const double& pi, const DesignVector& design_start,
    const PDEVector& state_start, const DualVector& ceq_start,
    const DesignVector& design_dir, DesignVector& design_work,
    PDEVector& state_work, DualVector& dual_work) {
  if (pi < 0.0) {
    cerr << "ReducedSpaceL2Merit(constructor): "
         << "penalty parameter should be non-negative: pi = " << pi << endl;
    throw(-1);
  }
  pi_ = pi;
  design_start_ = &design_start;
  design_dir_ = &design_dir;
  design_work_ = &design_work;
  state_work_ = &state_work;
  dual_work_ = &dual_work;

  // compute the initial value of the merit function
  merit_start_ = ObjectiveValue(design_start, state_start);
  merit_start_ += pi_*ceq_start.Norm2();
#if 1      
  // check that we have a descent direction
  double merit = EvalFunc(0.0);
  double merit_pert = EvalFunc(1e-7);
  double dmerit = (merit_pert - merit)*1e+7;
  cout << "Diff(L2Merit) = " << dmerit << endl;
#endif
}
// ==============================================================================
void ReducedSpaceL2Merit::MemoryRequired(int& design_memory, int& pde_memory,
                                         int& dual_memory) {
  // all memory requirements are accounted for through work space based into the
  // constructor
  design_memory = 0;
  pde_memory = 0;
  dual_memory = 0;
}
// ==============================================================================
double ReducedSpaceL2Merit::EvalFunc(double alpha) {
  design_work_->EqualsAXPlusBY(1.0, *design_start_, alpha, *design_dir_);
  double merit = 0.0;
  if (fabs(alpha) < kEpsilon) {
    merit = merit_start_;
  } else if (state_work_->EqualsPrimalSolution(*design_work_)) {
    // PDE solver successfully converged
    merit = ObjectiveValue(*design_work_, *state_work_);
    dual_work_->EqualsConstraints(*design_work_, *state_work_);
    merit += pi_*dual_work_->Norm2();
  } else {
    // PDE solver failed to converge
    merit = 1.e+100;
  }
  return merit;
}
// ==============================================================================
double ReducedSpaceL2Merit::EvalGrad(double alpha) {
  cerr << "ReducedSpaceL2Merit(EvalGrad): "
       << "ReducedSpaceL2Merit is not set-up to evaluate its derivative" << endl;
  throw(-1);
}
// ==============================================================================
} // namespace kona
