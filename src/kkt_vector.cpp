/**
 * \file kkt_vector.cpp
 * \brief definitions of KKTVector class member and friend functions
 * \author Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#include <algorithm>
#include <vector>
#include "./kkt_vector.hpp"
#include "./quasi_newton.hpp"
#include "./user_memory.hpp"
#include "./vectors.hpp"
#include "./krylov.hpp"

#include <boost/program_options.hpp>

using std::max;
using std::vector;

namespace po = boost::program_options;

namespace kona {

// ======================================================================

KKTProduct::KKTProduct(KKTVector* eval_at_vector,
                       KKTVector* initial_kkt) {
  if ( (eval_at_vector == NULL) || (initial_kkt == NULL) ) {
    cerr << "KKTProduct(constructor): "
	 << "eval_at_vector and/or initial_kkt are NULL pointers" << endl;
    throw(-1);
  }
  eval_at_ = eval_at_vector;
  eval_at_norm_ = eval_at_->Norm2();
  initial_residual_ = initial_kkt;
}

// ======================================================================

void KKTProduct::operator()(const KKTVector & u, KKTVector & v) {
  // find the perturbation parameter
  double epsilon_fd = CalcEpsilon(eval_at_norm_, u.Norm2());
  // set the perturbed state: perturb_kkt = *eval_at + epsilon_fd_*u;
  KKTVector perturb_kkt(u);
  perturb_kkt *= epsilon_fd;
  perturb_kkt += *eval_at_;
  v.EqualsKKTConditions(perturb_kkt);
  v -= *initial_residual_;
  v /= epsilon_fd;
}

// ======================================================================

GlobalizedKKTProduct::GlobalizedKKTProduct(
    KKTVector* eval_at_vector, KKTVector* initial_kkt,
    const double & lambda, DesignVector* design, const double & scale,
    const double design_scale, const double state_scale,
    const double adjoint_scale) {
  if (scale <= 0.0) {
    cerr << "GlobalizedKKTProduct(constructor): "
	 << "invalid value of scale = " << scale << endl;
    throw(-1);
  }
  if ( (design_scale <= 0.0) || (state_scale <= 0.0) || (adjoint_scale <= 0.0) ) {
    cerr << "GlobalizedKKTProduct(constructor): "
	 << "invalid value of design/state/adjoint_scale = "
         << design_scale << "/" << state_scale << "/" << adjoint_scale << endl;
    throw(-1);
  }
  if (eval_at_vector == NULL) {
    cerr << "GlobalizedKKTProduct(constructor): "
	 << "eval_at_vector is a NULL pointer" << endl;
    throw(-1);
  }
  if (initial_kkt == NULL) {
    cerr << "GlobalizedKKTProduct(constructor): "
	 << "initial_kkt is a NULL pointer" << endl;
    throw(-1);
  }
  if (design == NULL) {
    cerr << "GlobalizedKKTProduct(constructor): "
	 << "design is a NULL pointer" << endl;
    throw(-1);
  }
  eval_at_ = eval_at_vector;
  eval_at_norm_ = eval_at_->Norm2();
  initial_residual_ = initial_kkt;
  initial_residual_->design_ /= design_scale;
  initial_residual_->state_ /= state_scale;
  initial_residual_->adjoint_ /= adjoint_scale;
  lambda_param_ = lambda; 
  init_design_ = design;
  scale_ = scale;
  design_scale_ = 1.0/design_scale;
  state_scale_ = 1.0/state_scale;
  adjoint_scale_ = 1.0/adjoint_scale;
}

// ======================================================================

void GlobalizedKKTProduct::operator()(const KKTVector & u,
                                      KKTVector & v) {
  // find the perturbation parameter
  double epsilon_fd = CalcEpsilon(eval_at_norm_, u.Norm2());
#if 0
  cout << "GlobalizedKKTProduct::operator(): epsilon_fd = " 
       << epsilon_fd << ": eval_at_norm_ = " << eval_at_norm_
       << ": u.Norm2() = " << u.Norm2() << endl;
#endif

  // set the perturbed state: perturb_kkt = *eval_at + epsilon_fd_*u;
  KKTVector perturb_kkt(u);
  perturb_kkt *= epsilon_fd;
  perturb_kkt += *eval_at_;
  v.EqualsKKTConditions(perturb_kkt);
  v.AddGlobalization(perturb_kkt, lambda_param_, *init_design_,
                     scale_);
  v.design_ *= design_scale_;
  v.state_ *= state_scale_;
  v.adjoint_ *= adjoint_scale_;
  v -= (*initial_residual_);
  v /= epsilon_fd;
  
#if 0
  // WARNING: if this is uncommented, an additional KKTVector is needed
  double eval_at_design_prod = InnerProd(eval_at_->design_, eval_at_->design_);
  double eval_at_state_prod = InnerProd(eval_at_->state_, eval_at_->state_);
  double eval_at_adjoint_prod = InnerProd(eval_at_->adjoint_, eval_at_->adjoint_);

  // find the perturbation parameters
  double epsilon_design, epsilon_state, epsilon_adjoint;
  epsilon_design = CalcEpsilon(sqrt(eval_at_design_prod),
                               sqrt(InnerProd(u.design_, u.design_)));
  epsilon_state = CalcEpsilon(sqrt(eval_at_state_prod),
                               sqrt(InnerProd(u.state_, u.state_)));
  epsilon_adjoint = CalcEpsilon(sqrt(eval_at_adjoint_prod),
                               sqrt(InnerProd(u.adjoint_, u.adjoint_)));

  // calculate the design portion of the product
  KKTVector perturb_kkt, Ku;
  perturb_kkt = 0.0;
  perturb_kkt.design_ = u.design_;
  perturb_kkt.design_ *= epsilon_design;
  perturb_kkt += *eval_at_;
  Ku.EqualsKKTConditions(perturb_kkt);
  Ku.AddGlobalization(perturb_kkt, lambda_param_, *init_design_,
                     scale_);
  Ku -= (*initial_residual_);
  Ku /= epsilon_design;
  perturb_kkt = 0.0;
  v = Ku;
  
  // calculate the state portion of the product
  perturb_kkt.state_ = u.state_;
  perturb_kkt.state_ *= epsilon_state;
  perturb_kkt += *eval_at_;
  Ku.EqualsKKTConditions(perturb_kkt);
  Ku.AddGlobalization(perturb_kkt, lambda_param_, *init_design_,
                     scale_);
  Ku -= (*initial_residual_);
  Ku /= epsilon_state;
  perturb_kkt = 0.0;
  v += Ku;

  // calculate the adjoint portion of the product
  perturb_kkt.adjoint_ = u.adjoint_;
  perturb_kkt.adjoint_ *= epsilon_adjoint;
  perturb_kkt += *eval_at_;
  Ku.EqualsKKTConditions(perturb_kkt);
  Ku.AddGlobalization(perturb_kkt, lambda_param_, *init_design_,
                     scale_);
  Ku -= (*initial_residual_);
  Ku /= epsilon_adjoint;
  //perturb_kkt.adjoint_ = 0.0;
  v += Ku;  
#endif
#if 0
  // to use this, initial_residual_ has to be the residual without
  // globalization terms... this is problematic if it is a pointer
  v.design_ *= (1.0 - lambda_param_);
  DesignVector design_work(u.design_);
  design_work *= lambda_param_;
  v.design_ += design_work;

  cout << "value of v after global terms: ";
  v.UpdateUser();
#endif
}

// ======================================================================

RegularizedKKTProduct::RegularizedKKTProduct(KKTVector* eval_at_vector,
                                             KKTVector* initial_kkt,
                                             const double & lambda) {
  if (eval_at_vector == NULL) {
    cerr << "GlobalizedKKTProduct(constructor): "
	 << "eval_at_vector is a NULL pointer" << endl;
    throw(-1);
  }
  if (initial_kkt == NULL) {
    cerr << "GlobalizedKKTProduct(constructor): "
	 << "initial_kkt is a NULL pointer" << endl;
    throw(-1);
  }
  eval_at_ = eval_at_vector;
  eval_at_norm_ = eval_at_->Norm2();
  initial_residual_ = initial_kkt;
  lambda_param_ = lambda; 
}

// ======================================================================

void RegularizedKKTProduct::operator()(const KKTVector & u,
                                       KKTVector & v) {
  // find the perturbation parameter
  double epsilon_fd = CalcEpsilon(eval_at_norm_, u.Norm2());
#if 0
  cout << "RegularizedKKTProduct::operator(): epsilon_fd = " 
       << epsilon_fd << ": eval_at_norm_ = " << eval_at_norm_
       << ": u.Norm2() = " << u.Norm2() << endl;
#endif
  // set the perturbed state: kkt_work = *eval_at + epsilon_fd_*u;
  KKTVector kkt_work(u);
  kkt_work *= epsilon_fd;
  kkt_work += *eval_at_;
  v.EqualsKKTConditions(kkt_work);
  v -= (*initial_residual_);
  v /= epsilon_fd;
  // add the regularization, if necessary
  if (lambda_param_ > kEpsilon) {
    kkt_work.design_ = u.design_;
    kkt_work.state_ = u.state_;
    kkt_work.adjoint_ = 0.0;
    v.EqualsAXPlusBY(1.0, v, lambda_param_, kkt_work);
  }
}

// ======================================================================

KKTPreconditioner::KKTPreconditioner(KKTVector* eval_at_vector,
                                     QuasiNewton* quasi_newton) :
    precond_state_(eval_at_vector->design_, eval_at_vector->state_),
    precond_adjoint_(eval_at_vector->design_, eval_at_vector->state_) {
  eval_at_ = eval_at_vector;
  precond_design_ = quasi_newton;
}

// ======================================================================

void KKTPreconditioner::operator()(KKTVector & u, KKTVector & v) {
  //v = u;
  //return;

  // Step 1: compute v.adjoint_ = A_s^{-T} u.state_
  precond_adjoint_(u.state_, v.adjoint_);
  // Step 2: compute v.design_ = B_z^{-1}(u.design_ - A_d^{T} v.adjoint_)
  v.design_.EqualsTransJacobianTimesVector(
      eval_at_->design_, eval_at_->state_, v.adjoint_);
  v.design_ *= -1.0;
  v.design_ += u.design_;
  // assume following can be performed in-place
  precond_design_->ApplyInvHessianApprox(v.design_, v.design_);   
  // Step 3: compute v.state_ = A_s^{-1}(u.adjoint_ - A_d v.design_)
  v.state_.EqualsJacobianTimesVector(eval_at_->design_, eval_at_->state_,
                                     v.design_);
  // looks like an additional PDEVector is unavoidable...
  PDEVector pde_work(u.adjoint_);
  pde_work -= v.state_;
  precond_state_(pde_work, v.state_);

  
#if 0
  // Step 1: compute v.adjoint_ = A_s^{-T} u.adjoint_
  precond_adjoint_(u.adjoint_, v.adjoint_);
  // Step 2: compute v.design_ = B_z^{-1}(u.design_ - A_d^{T} v.adjoint_)
  v.design_.EqualsTransJacobianTimesVector(
      eval_at_->design_, eval_at_->state_, v.adjoint_);
  v.design_ *= -1.0;
  v.design_ += u.design_;
  // assume following can be performed in-place
  precond_design_->ApplyInvHessianApprox(v.design_, v.design_);   
  // Step 3: compute v.state_ = A_s^{-1}(u.state_ - A_d v.design_)
  v.state_.EqualsJacobianTimesVector(eval_at_->design_, eval_at_->state_,
                                     v.design_);
  // looks like an additional PDEVector is unavoidable...
  PDEVector pde_work(u.state_); 
  pde_work -= v.state_;
  precond_state_(pde_work, v.state_);
#endif
#if 0
  KKTVector temp(u);
  temp -= v;
  cout << "KKTPreconditioner: u - v:" << endl << "\t";
  temp.UpdateUser();
#endif
}

// ======================================================================

const DesignVector & KKTVector::get_design() const {
  return design_;
}

// ======================================================================

const PDEVector & KKTVector::get_state() const {
  return state_;
}

// ======================================================================

const PDEVector & KKTVector::get_adjoint() const {
  return adjoint_;
}

// ======================================================================

void KKTVector::set_design(const DesignVector & design) {
  design_ = design;
}

// ======================================================================

void KKTVector::set_state(const PDEVector & state) {
  state_ = state;
}

// ======================================================================

void KKTVector::set_adjoint(const PDEVector & adjoint) {
  adjoint_ = adjoint;
}

// ======================================================================

KKTVector & KKTVector::operator=(const KKTVector & x) {
  if (this == &x) return *this; // KKTVector being assigned to itself

  design_ = x.design_;
  state_ = x.state_;
  adjoint_ = x.adjoint_;
  return *this;
}

// ======================================================================

KKTVector & KKTVector::operator=(const double & val) {
  design_ = val;
  state_ = val;
  adjoint_ = val;
  return *this;
}

// ======================================================================

KKTVector & KKTVector::operator+=(const KKTVector & x) {
  design_ += x.design_;
  state_ += x.state_;
  adjoint_ += x.adjoint_;
  return *this;
}

// ======================================================================

KKTVector & KKTVector::operator-=(const KKTVector & x) {
  design_ -= x.design_;
  state_ -= x.state_;
  adjoint_ -= x.adjoint_;
  return *this;
}

// ======================================================================

KKTVector & KKTVector::operator*=(const double & val) {
  design_ *= val;
  state_ *= val;
  adjoint_ *= val;
  return *this;
}

// ======================================================================

KKTVector & KKTVector::operator/=(const double & val) {
  design_ /= val;
  state_ /= val;
  adjoint_ /= val;
  return *this;
}

// ======================================================================

void KKTVector::EqualsAXPlusBY(const double & a, const KKTVector & x,
                               const double & b, const KKTVector & y) {
  design_.EqualsAXPlusBY(a, x.design_, b, y.design_);
  state_.EqualsAXPlusBY(a, x.state_, b, y.state_);
  adjoint_.EqualsAXPlusBY(a, x.adjoint_, b, y.adjoint_);
}

// ======================================================================

double KKTVector::Norm2() const {
  // call an inner product on KKTVector
  double val = InnerProd(*this, *this);
  if (val < 0.0) {
    cerr << "KKTVector(Norm2): "
	 << "inner product of KKTVector is negative" << endl;
    throw(-1);
  }
  return sqrt(val);
}

// ======================================================================

double InnerProd(const KKTVector & x, const KKTVector & y) {
  double prod = 0.0;
  prod += InnerProd(x.design_, y.design_);
  prod += InnerProd(x.state_, y.state_);
  prod += InnerProd(x.adjoint_, y.adjoint_);
  return prod;
}

// ======================================================================

void KKTVector::EqualsInitialGuess() {
  design_.EqualsInitialDesign();
  state_.EqualsPrimalSolution(design_);
  adjoint_.EqualsAdjointSolution(design_, state_);
  //adjoint_ = 0.0; // adjoint = 0 for the initial functional
}

// ======================================================================

void KKTVector::solveForPrimalAndAdjoint() {
  state_.EqualsPrimalSolution(design_);
  adjoint_.EqualsAdjointSolution(design_, state_);
}

// ======================================================================

void KKTVector::EqualsKKTConditions(const KKTVector & x) {
  // design component of KKT conditions
  // need a temporary design vector; this can be circumvented if
  // necessary by creating a PlusEqualsObjectiveGradient
  DesignVector design_work;
  design_work.EqualsObjectiveGradient(x.design_, x.state_);
  design_.EqualsTransJacobianTimesVector(x.design_, x.state_,
                                         x.adjoint_);
  design_ += design_work;

  // state component of KKT conditions
  JacobianTAdjointProduct trans_jac_vec(x.design_, x.state_);
  trans_jac_vec(x.adjoint_, adjoint_);
  state_.EqualsObjectiveGradient(x.design_, x.state_);
  state_ += adjoint_;

  // adjont component of KKT conditions
  adjoint_.EqualsStateEquations(x.design_, x.state_);
}

// ======================================================================

void KKTVector::CalcLinearPrimalDualNorms(
    const KKTVector & dX, const KKTVector & Res, 
    MatrixVectorProduct<KKTVector> & mat_vec, double & primal_norm,
    double & adjoint_norm) {
  mat_vec(dX, *this);
  *this += Res;
  double design_norm;
  // note that we had to reverse adjoint_norm and primal_norm here
  CalcNorms(design_norm, adjoint_norm, primal_norm);
  primal_norm = sqrt(primal_norm*primal_norm + design_norm*design_norm);
}

// ======================================================================

double KKTVector::HessianProductCheck(
    MatrixVectorProduct<KKTVector> & mat_vec, KKTVector & work1,
    KKTVector & work2) const {
  work1.design_ = design_;
  work1.state_ = state_;
  work1.adjoint_ = 0.0;
  mat_vec(work1, work2);
  double dT_W_d = InnerProd(work2.design_, design_);
  dT_W_d += InnerProd(work2.state_, state_);
  dT_W_d *= 0.5;
  return dT_W_d;
}

// ======================================================================

void KKTVector::SMARTTests(const po::variables_map & opts, const KKTVector & X,
                           const KKTVector & Res, const double & mu,
                           const double & mu_old, const double & primal_norm,
                           const double & adjoint_norm, const double & dT_W_d,
                           KKTVector & work, bool & term1, bool & term2,
                           bool & modify_hess, double & mu_trial) const {
  cout << "Beginning S.M.A.R.T. tests..." << endl;
  
  const double theta = 1.e-8;
  const double sigma = opts["inexact.tau"].as<double>()*(
      1.0 - opts["inexact.eps_tol"].as<double>());
  const DesignVector & design = X.get_design();
  const PDEVector & state = X.get_state();

  // estimate the tangential and normal components of d
  work.state_.EqualsJacobianTimesVector(X.design_, X.state_, state_);
  work.adjoint_.EqualsJacobianTimesVector(X.design_, X.state_, design_);
  work.state_ += work.adjoint_;
  double Ad_norm2 = work.state_.Norm2();
  Ad_norm2 *= Ad_norm2;
  double design_norm = design_.Norm2();
  double state_norm = state_.Norm2();
  double d_norm2 = design_norm*design_norm + state_norm*state_norm;  
  // NOTE: currently, the 2-norm of the Jacobian is approximated with this
  // assumption.
  double A_norm2_est = 1.0; 
  double nu = Ad_norm2/A_norm2_est;
  double upsilon = max(0.0, d_norm2 - Ad_norm2/A_norm2_est);
  cout << "d_norm2 = " << d_norm2 << endl;
  cout << "lower bound for normal component: nu     = " << nu << endl;
  cout << "upper bound for tang. component: upsilon = " << upsilon << endl; 

  
  // compute the innerprod of the objective gradient with dX (both design and
  // state components)
  work.state_.EqualsObjectiveGradient(X.design_, X.state_);
  work.design_.EqualsObjectiveGradient(X.design_, X.state_);
  double grad_dot_d = InnerProd(work.state_, state_) 
      + InnerProd(work.design_, design_);

  // compute the nonlinear primal and adjoint residual norms
  double cnstr_norm = Res.adjoint_.Norm2();
  double tmp_norm1 = Res.design_.Norm2();
  double tmp_norm2 = Res.state_.Norm2();
  double lgrngn_norm = sqrt(tmp_norm1*tmp_norm1 + tmp_norm2*tmp_norm2);

  // check the model reduction condtion, equation (3.12) from paper
  double red_cond_rhs = std::max(dT_W_d, theta*upsilon)
      + sigma*mu*std::max(cnstr_norm, adjoint_norm - cnstr_norm);
  bool red_cond = false; 
  if (-grad_dot_d + mu*(cnstr_norm - adjoint_norm) >= red_cond_rhs)
    red_cond = true;
  
  cout << "red_cond_rhs = " << red_cond_rhs << endl;
  cout << "dT_W_d, theta*upsilon = " << dT_W_d << ", " << theta*upsilon << endl;
  cout << "sigma*mu*(cnstr_norm, adjoint_norm - cnstr_norm) = "
       << sigma*mu*cnstr_norm << ", " << sigma*mu*(adjoint_norm - cnstr_norm) 
       << endl;
  cout << "red_cond_lhs = " << -grad_dot_d + mu*(cnstr_norm - adjoint_norm) << endl;
  cout << "grad_dot_d = " << grad_dot_d << endl;

  cout << "\tModel reduction condition: " << std::boolalpha << red_cond << endl;

  // termination test 1
  term1 = false;
  if (red_cond) {
    double lin_norm = sqrt(primal_norm*primal_norm + adjoint_norm*adjoint_norm);
    double nonlin_norm = sqrt(lgrngn_norm*lgrngn_norm + cnstr_norm*cnstr_norm);
    if (lin_norm < opts["inexact.kappa_tol"].as<double>()*nonlin_norm)
      term1 = true;
  }
  cout << "\tTermination test 1 result: " << std::boolalpha << term1 << endl;

  // termination test 2
  term2 = false;
  if ( (adjoint_norm <= opts["inexact.eps_tol"].as<double>()*cnstr_norm) && 
       (primal_norm <= opts["inexact.beta"].as<double>()*cnstr_norm) ) {
    if ( (dT_W_d >= theta*upsilon) || 
         (opts["inexact.psi"].as<double>()*nu >= upsilon) )
      term2 = true;
  }
  cout << "\tTermination test 2 result: " << std::boolalpha << term2 << endl;

  // check if we will need to modify the Hessian of the Lagrangian
  modify_hess = false;
  if (!term1 && !term2) {    
    red_cond_rhs = std::max(dT_W_d, theta*upsilon) 
        + sigma*mu_old*std::max(cnstr_norm, adjoint_norm - cnstr_norm);
    if (-grad_dot_d + mu_old*(cnstr_norm - adjoint_norm) < red_cond_rhs) {
        cout << "\tModify Hessian Strategy: (4.7a) is satisfied." << endl;
        if (dT_W_d < theta*upsilon)
          cout << "\tModify Hessian Strategy: (4.7b) is satisfied." << endl;
        if (opts["inexact.psi"].as<double>()*nu < upsilon)
          cout << "\tModify Hessian Strategy: (4.7c) is satisfied." << endl;
        if ( (dT_W_d < theta*upsilon) && 
             (opts["inexact.psi"].as<double>()*nu < upsilon) )
          modify_hess = true;
      }
  } else {
    // one of term1 or term2 is true, so we may need mu_trial
    mu_trial = (grad_dot_d + std::max(dT_W_d, theta*upsilon))
        / ( (1.0 - opts["inexact.tau"].as<double>())
            *(cnstr_norm - adjoint_norm) );
    cout << "setting mu_trial = " << mu_trial << endl;
  }
}

// ======================================================================

void KKTVector::CalcNorms(double & design_norm, double & primal_norm,
                          double & adjoint_norm) const {
  // WARNING: definition of primal_norm and adjoint_norm switched
  design_norm = design_.Norm2();
  primal_norm = adjoint_.Norm2();
  adjoint_norm   = state_.Norm2();
}

// ======================================================================

bool KKTVector::InnerConverged(vector<double> & norms,
                               const double & design_init,
                               const double & design_tol,
                               const double & primal_init,
                               const double & primal_tol,
                               const double & adjoint_init,
                               const double & adjoint_tol) const {
  if ( (design_tol < 0.0) || (primal_tol < 0.0) || (adjoint_tol < 0.0) ) {
    cerr << "KKTVector(InnerConverged): invalid tolerance" << endl;
    cerr << "design_tol = " << design_tol << endl;
    cerr << "primal_tol = " << primal_tol << endl;
    cerr << "adjoint_tol   = " << adjoint_tol << endl;
    throw(-1);
  }
  if (norms.size() != 4) {
    cerr << "KKTVector(InnerConverged): invalid norms.size()" << endl;
  }
  double design_norm = design_.Norm2();
  double primal_norm = adjoint_.Norm2();
  double adjoint_norm   = state_.Norm2();
  cout << "design_norm : design_norm_init = "
       << design_norm << " : " << design_init << endl;
  cout << "primal_norm : primal_norm_init = "
       << primal_norm << " : " << primal_init << endl;
  cout << "adjoint_norm : adjoint_norm_init     = "
       << adjoint_norm << " : " << adjoint_init << endl;
  cout << endl;

  double kkt_init = sqrt(design_init*design_init
                         + primal_init*primal_init
                         + adjoint_init*adjoint_init);
  double kkt_norm = sqrt(design_norm*design_norm
                         + primal_norm*primal_norm
                         + adjoint_norm*adjoint_norm);
  cout << "kkt_norm : kkt_norm_init = "
       << kkt_norm << " : " << kkt_init << endl;
  
  norms[0] = kkt_norm;
  norms[1] = design_norm;
  norms[2] = primal_norm;
  norms[3] = adjoint_norm;
#if 0
  // this works for many problems
  bool conv = true;
  if (kkt_init > 100.0*kEpsilon) {
    if (kkt_norm >= kkt_init*design_tol) conv = false;
  } else {
    if (kkt_norm >= design_tol) conv = false;
  }
  return conv;
#endif

  // this uses absolute tolerances
  bool conv = true;
  if (design_norm >= design_tol)
    conv = false;
  if (primal_norm >= primal_tol)
    conv = false;
  if (adjoint_norm >= adjoint_tol)
    conv = false;
  return conv;

#if 0
  if (design_init > 100.0*kEpsilon) {
    if (design_norm >= design_init*design_tol) conv = false;
  } else {
    if (design_norm >= design_tol) conv = false;
  }
  if (primal_init > 100.0*kEpsilon) {
    if (primal_norm >= primal_init*primal_tol) conv = false;
  } else {
    if (primal_norm >= primal_tol) conv = false;
  }
  if (adjoint_init > 100.0*kEpsilon) {
    if (adjoint_norm >= adjoint_init*adjoint_tol) conv = false;
  } else {
    if (adjoint_norm >= adjoint_tol) conv = false;
  }
  return conv;
#endif
  
#if 0
  if ( ( (design_norm <= design_tol*design_init) ||
         (design_norm <= 10.0*kEpsilon) ) &&
       ( (primal_norm <= primal_tol*primal_init) ||
         (primal_norm <= 10.0*kEpsilon) ) &&
       ( (adjoint_norm   <= adjoint_tol*adjoint_init) ||
         (adjoint_norm   <= 10.0*kEpsilon) ) ) {
    return true;
  } else {
    return false;
  }
#endif
}

// ======================================================================

bool KKTVector::OuterConverged(vector<double> & norms,
                               const double & design_init,
                               const double & design_tol,
                               const double & primal_init,
                               const double & primal_tol,
                               const double & adjoint_init,
                               const double & adjoint_tol) const {
  if ( (design_tol < 0.0) || (primal_tol < 0.0) || (adjoint_tol < 0.0) ) {
    cerr << "KKTVector(OuterConverged): invalid tolerance" << endl;
    cerr << "design_tol = " << design_tol << endl;
    cerr << "primal_tol = " << primal_tol << endl;
    cerr << "adjoint_tol   = " << adjoint_tol << endl;
    throw(-1);
  }
  if (norms.size() != 4) {
    cerr << "KKTVector(OuterConverged): invalid norms.size()" << endl;
  }

  double design_norm = design_.Norm2();
  double primal_norm = adjoint_.Norm2();
  double adjoint_norm   = state_.Norm2();
  double kkt_norm = sqrt(design_norm*design_norm
                         + primal_norm*primal_norm
                         + adjoint_norm*adjoint_norm);
  norms[0] = kkt_norm;
  norms[1] = design_norm;
  norms[2] = primal_norm;
  norms[3] = adjoint_norm;

  cout << "design_norm : design_norm_init = "
       << design_norm << " : " << design_init << endl;
  cout << "primal_norm : primal_norm_init = "
       << primal_norm << " : " << primal_init << endl;
  cout << "adjoint_norm : adjoint_norm_init     = "
       << adjoint_norm << " : " << adjoint_init << endl;
  cout << endl;

#if 0
  double kkt_init = sqrt(design_init*design_init
                         + primal_init*primal_init
                         + adjoint_init*adjoint_init);
  double kkt_norm = sqrt(design_norm*design_norm
                         + primal_norm*primal_norm
                         + adjoint_norm*adjoint_norm);
  cout << "kkt_norm : kkt_norm_init = "
       << kkt_norm << " : " << kkt_init << endl;
  
  bool conv = true;
  if (kkt_init > 100.0*kEpsilon) {
    if (kkt_norm >= kkt_init*design_tol) conv = false;
  } else {
    if (kkt_norm >= design_tol) conv = false;
  }
  return conv;
#endif
#if 0
  // this set works for most problems
  bool conv = true;
  if (design_norm > std::max(design_tol*design_init, 100.0*kEpsilon))
    conv = false;
  if (primal_norm > primal_tol*primal_init) //std::max(primal_tol*primal_init, 100.0*kEpsilon))
    conv = false;
  if (adjoint_norm > adjoint_tol*adjoint_init) //std::max(adjoint_tol*adjoint_init, 100.0*kEpsilon))
    conv = false;
  return conv;
#endif

  // this uses absolute tolerances
  bool conv = true;
  if (design_norm > design_tol)
    conv = false;
  if (primal_norm > primal_tol) //std::max(primal_tol*primal_init, 100.0*kEpsilon))
    conv = false;
  if (adjoint_norm > adjoint_tol) //std::max(adjoint_tol*adjoint_init, 100.0*kEpsilon))
    conv = false;
  return conv;

#if 0
  if (design_init > 100.0*kEpsilon) {
    if (design_norm >= design_init*design_tol) conv = false;
  } else {
    if (design_norm >= design_tol) conv = false;
  }
  if (primal_init > 100.0*kEpsilon) {
    if (primal_norm >= primal_init*primal_tol) conv = false;
  } else {
    if (primal_norm >= primal_tol) conv = false;
  }
  if (adjoint_init > 100.0*kEpsilon) {
    if (adjoint_norm >= adjoint_init*adjoint_tol) conv = false;
  } else {
    if (adjoint_norm >= adjoint_tol) conv = false;
  }
  return conv;
#endif
  
#if 0
  if ( ( (design_norm <= design_tol*design_init) ||
         (design_norm <= 10.0*kEpsilon) ) &&
       ( (primal_norm <= primal_tol*primal_init) ||
         (primal_norm <= 10.0*kEpsilon) ) &&
       ( (adjoint_norm   <= adjoint_tol*adjoint_init) ||
         (adjoint_norm   <= 10.0*kEpsilon) ) ) {
    return true;
  } else {
    return false;
  }
#endif
}

// ======================================================================

bool KKTVector::InexactConverged(vector<double> & norms,
                                 const double & design_tol,
                                 const double & adjoint_tol) const {
  if ( (design_tol < 0.0) || (adjoint_tol < 0.0) ) {
    cerr << "KKTVector(InexactConverged): invalid tolerance" << endl;
    cerr << "design_tol = " << design_tol << endl;
    cerr << "adjoint_tol   = " << adjoint_tol << endl;
    throw(-1);
  }
  if (norms.size() != 4) {
    cerr << "KKTVector(OuterConverged): invalid norms.size()" << endl;
  }

  double design_norm = design_.Norm2();
  double primal_norm = adjoint_.Norm2();
  double adjoint_norm   = state_.Norm2();
  double kkt_norm = sqrt(design_norm*design_norm
                         + primal_norm*primal_norm
                         + adjoint_norm*adjoint_norm);
  norms[0] = kkt_norm;
  norms[1] = design_norm;
  norms[2] = primal_norm;
  norms[3] = adjoint_norm;

  cout << "design_norm : design_tol = "
       << design_norm << " : " << design_tol << endl;
  cout << "primal_norm =              "
       << primal_norm << endl;
  cout << "adjoint_norm : adjoint_tol     = "
       << adjoint_norm << " : " << adjoint_tol << endl;
  cout << endl;
 
  bool conv = true;
  if (design_norm > design_tol)
    conv = false;
  if (primal_norm > adjoint_tol)
    conv = false;
  if (adjoint_norm > adjoint_tol)
    conv = false;
  return conv;

}

// ======================================================================

void KKTVector::AddGlobalization(
    const KKTVector & x, const double & lambda,
    const DesignVector & init_design, const double & init_norm) {
  // scale the design component by the initial L2 norm
  design_ *= (1.0 - lambda);
  // this work vector is already accounted for in EqualsKKTConditions
  DesignVector design_work(x.design_);
  design_work -= init_design;
  design_work *= lambda*init_norm;
  design_ += design_work;
}

// ======================================================================

void KKTVector::UpdateUser(const int & iter) const {
  CurrentSolution(iter, design_, state_, adjoint_);
}

} // namespace kona
