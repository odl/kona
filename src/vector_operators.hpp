/**
 * \file vector_operators.hpp
 * \brief header file for various types of matrix-vector operators
 * \author Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#pragma once

#include <math.h>
#include <ostream>
#include <iostream>
#include <vector>
#include <string>
#include <boost/scoped_ptr.hpp>

#include "./user_memory.hpp"
#include "./krylov.hpp"

using std::cerr;
using std::cout;
using std::endl;
using std::vector;
using std::string;

namespace kona {

// forward declarations
class DesignVector;
class DualVector;
class PDEVector;
class QuasiNewton;

// ======================================================================

/*!
 * \class IdentityMatrix
 * \brief identity matrix operator
 */
template <class Vec>
class IdentityMatrix : public MatrixVectorProduct<Vec> {
 public:

  IdentityMatrix() {}; ///< class constructor

  ~IdentityMatrix() {}; ///< class destructor

  void MemoryRequired(ptree & num_required) { num_required.put("num_vec", 0); }
  
  /*!
   * \brief operator that defines the product
   * \param[in] u - PDEVector that is being multiplied
   * \param[out] v - PDEVector that is the result of the multiplication
   */
  void operator()(const Vec & u, Vec & v) { v = u; }
};
template <>
void IdentityMatrix<DesignVector>::MemoryRequired(ptree & num_required);

// ======================================================================

/*!
 * \class IdentityPreconditioner
 * \brief do-nothing preconditioner
 */
template <class Vec>
class IdentityPreconditioner : public Preconditioner<Vec> {
 public:

  IdentityPreconditioner() {}; ///< class constructor

  ~IdentityPreconditioner() {}; ///< class destructor

  void MemoryRequired(ptree & num_required) { num_required.put("num_vec", 0); }
  
  /*!
   * \brief operator that defines the preconditioner
   * \param[in] u - PDEVector that is being preconditioned 
   * \param[out] v - PDEVector that is the result of the preconditioning
   */
  void operator()(Vec & u, Vec & v) { v = u; }
};
template <>
void IdentityPreconditioner<DesignVector>::MemoryRequired(ptree & num_required);

// ======================================================================

/*!
 * \class JacobianStateProduct
 * \brief specialization of matrix-vector product for PDE Jacobian
 */
class JacobianStateProduct : public MatrixVectorProduct<PDEVector> {
 public:

  /*!
   * \brief default constructor for class; system matrix is linear
   */
  JacobianStateProduct() {
    eval_at_design_ = -1;
    eval_at_state_ = -1;
  }

  /*!
   * \brief constructor for class
   * \param[in] design_eval - Jacobian is evaluated at this design
   * \param[in] state_eval - Jacobian is evaluated at this state
   */
  JacobianStateProduct(const DesignVector & design_eval,
                       const PDEVector & state_eval);

  ~JacobianStateProduct() {} ///< class destructor

  /*!
   * \brief operator that defines Jacobian-PDEVector product
   * \param[in] u - PDEVector that is being multiplied by the Jacobian
   * \param[out] v - PDEVector that is the result of the product
   */
  void operator()(const PDEVector & u, PDEVector & v);

 private:
  int eval_at_design_; /// evaluate Jacobian at this design index
  int eval_at_state_; /// evaluate Jacobian at this state index
};

// ======================================================================

/*!
 * \class JacobianTAdjointProduct
 * \brief specialization of matrix-vector product for transposed Jacobian
 */
class JacobianTAdjointProduct :
    public MatrixVectorProduct<PDEVector> {
 public:

  /*!
   * \brief default constructor for class; system matrix is linear
   */
  JacobianTAdjointProduct() {
    eval_at_design_ = -1;
    eval_at_state_ = -1;
  }

  /*!
   * \brief constructor for class
   * \param[in] design_eval - Jacobian is evaluated at this design
   * \param[in] state_eval - Jacobian is evaluated at this state
   */
  JacobianTAdjointProduct(const DesignVector & design_eval,
                          const PDEVector & state_eval);

  ~JacobianTAdjointProduct() {} ///< class destructor

  /*!
   * \brief operator that defines transposed-Jacobian-Adjoint product
   * \param[in] u - PDEVector that is being multiplied
   * \param[out] v - PDEVector that is the result of the product
   */
  void operator()(const PDEVector & u, PDEVector & v);

 private:
  int eval_at_design_; /// evaluate Jacobian at this design index
  int eval_at_state_; /// evaluate Jacobian at this state index
};

// ======================================================================

/*!
 * \class StatePreconditioner
 * \brief specialization of preconditioner for state system
 */
class StatePreconditioner : public Preconditioner<PDEVector> {
 public:

  /*!
   * \brief default constructor; preconditioner is independent
   */
  StatePreconditioner() {
    eval_at_design_ = -1;
    eval_at_state_ = -1;
  }

  /*!
   * \brief constructor for class
   * \param[in] design_eval - preconditioner is evaluated at this design
   * \param[in] state_eval - preconditioner is evaluated at this state
   */  
  StatePreconditioner(const DesignVector & design_eval,
                      const PDEVector & state_eval);

  ~StatePreconditioner() {} ///< class destructor

  /*!
   * \brief defines a diagonal matrix to add to preconditioner
   * \param[in] diag - the value on the diagonal
   *
   * This is needed primarily for testing of the FITR algorithm
   */
  void set_diagonal(const double & diag);
  
  /*!
   * \brief operator that defines the state preconditioner
   * \param[in] u - PDEVector that is being preconditioned
   * \param[out] v - preconditioned PDEVector
   */
  void operator()(PDEVector & u, PDEVector & v);

 private:
  int eval_at_design_; /// evaluate preconditioner at this design index
  int eval_at_state_; /// evaluate preconditioner at this state index
  double diag_; /// value of diagonal matrix added to preconditioner 
};

// ======================================================================

/*!
 * \class AdjointPreconditioner
 * \brief specialization of preconditioner for adjoint system
 */
class AdjointPreconditioner : public Preconditioner<PDEVector> {
 public:

  /*!
   * \brief default constructor; preconditioner is independent
   */
  AdjointPreconditioner() {
    eval_at_design_ = -1;
    eval_at_state_ = -1;
  }

  /*!
   * \brief constructor for class
   * \param[in] design_eval - preconditioner is evaluated at this design
   * \param[in] state_eval - preconditioner is evaluated at this state
   *
   * It is assumed that the adjoint preconditioner is always built with
   * the same design and state as the state preconditioner.  Hence, this
   * constructor is not responsible for asking the user to build the
   * preconditioner; this responsibility rests with a constructor of
   * the StatePreconditioner class.
   */  
  AdjointPreconditioner(const DesignVector & design_eval,
                      const PDEVector & state_eval);

  ~AdjointPreconditioner() {} ///< class destructor

  /*!
   * \brief operator that defines the Adjoint preconditioner
   * \param[in] u - PDEVector that is being preconditioned
   * \param[out] v - preconditioned PDEVector
   */
  void operator()(PDEVector & u, PDEVector & v);

 private:
  int eval_at_design_; /// evaluate preconditioner at this design index
  int eval_at_state_; /// evaluate preconditioner at this state index
};

// ======================================================================
/*!
 * \class ReducedHessianProduct
 * \brief specialization of matrix-vector product for reduced Hessian
 */
class ReducedHessianProduct : public MatrixVectorProduct<DesignVector> {
 public:

  /*!
   * \brief constructor for class
   * \param[in] krylov_size - number of Krylov iterations used in prod
   * \param[in/out] outstream - ostream object to write output to
   */
  ReducedHessianProduct(const int& krylov_size, ostream& outstream = cout);
  
  /*!
   * \brief constructor for class
   * \param[in] eval_at_design - design to evaluate Hessian at
   * \param[in] eval_at_state - state to evaluate Hessian at
   * \param[in] eval_at_adjoint - adjoint to evaluate Hessian at
   * \param[in] reduced_grad - the reduced gradient at eval_at_*
   * \param[in] adjoint_res - the adjoint equation residual at eval_at_*
   * \param[out] design_work1 - design work space
   * \param[out] design_work2 - design work space
   * \param[out] pde_work1 - state or adjoint work space
   * \param[out] pde_work2 - state or adjoint work space
   * \param[out] pde_work3 - state or adjoint work space
   * \param[out] pde_work4 - state or adjoint work space
   * \param[out] pde_work5 - state or adjoint work space
   * \param[in] krylov_size - number of Krylov iterations used in prod
   * \param[in] product_fac - scales product_tol_ which is used in adjoint tols.
   * \param[in/out] outstream - ostream object to write output to
   * \param[in] lambda - continuation parameter for globalization
   * \param[in] scale - scaling for globalization terms
   */
  ReducedHessianProduct(
      const DesignVector & eval_at_design, const PDEVector & eval_at_state,
      const PDEVector & eval_at_adjoint, const DesignVector & reduced_grad,
      const PDEVector & adjoint_res, DesignVector & design_work1,
      DesignVector & design_work2, PDEVector & pde_work1,
      PDEVector & pde_work2, PDEVector & pde_work3,
      PDEVector & pde_work4, PDEVector & pde_work5, 
      const int & krylov_size, const double & product_fac,
      ostream & outstream, const double & lambda = 0.0,
      const double & scale = 1.0);

  /*!
   * \brief destructor for class
   */
  ~ReducedHessianProduct();

  /*!
   * \brief returns the user-vector memory requirements
   * \param[in,out] num_required - stores the memory requirements
   *
   * On exit, num_required holds three elements with labels "design", "state",
   * and "dual"; thus, one each for the number of DesignVectors, PDEVectors, and
   * DualVectors, respectively.
   */
  void MemoryRequired(ptree& num_required);
  
  /*!
   * \brief defines the QuasiNewton object that should be updated during products
   * \param[in/out] quasi_newton - quasi-Newtom object that will be updated
   */
  void set_quasi_newton(QuasiNewton * quasi_newton);

  /*!
   * \brief prepares the operator for use
   * \param[in] eval_at_design - design to evaluate Hessian at
   * \param[in] eval_at_state - state to evaluate Hessian at
   * \param[in] eval_at_adjoint - adjoint to evaluate Hessian at
   * \param[in] reduced_grad - the reduced gradient at eval_at_*
   * \param[in] adjoint_res - the adjoint equation residual at eval_at_*
   * \param[out] design_work - array of DesignVectors for work
   * \param[out] pde_work - array of PDEVectors for work
   * \param[in] prod_param - input parameters
   * \param[in/out] outstream - ostream object to write output to
   *
   * This is the counterpart to the long version of the constructor above.  This
   * is called when the product is not rebuilt each iteration, yet various
   * quantities must still be redefined.
   */
  void Initialize(
      const DesignVector & eval_at_design, const PDEVector & eval_at_state,
      const PDEVector & eval_at_adjoint, const DesignVector & reduced_grad,
      const PDEVector & adjoint_res, std::vector<DesignVector>& design_work,
      std::vector<PDEVector> & pde_work, const ptree& prod_param,
      ostream & outstream = std::cout);
  
  /*!
   * \brief defines the desired accuracy of the Hessian-vector product
   * \param[in] product_tol - the desired accuracy
   */
  void set_product_tol(const double & product_tol);

  /*!
   * \brief computes the (estimate) of the 2-norm of the sensitivity matrix
   * \returns the estimate of the 2-norm (>0)
   *
   * Here, the sensitivity matrix refers to |du/dx^{T} dpsi/dx^{T}|, where u are
   * the state variables and psi are the adjoint variables.
   * \param[in] approx - if true, approximate primal/dual with precond.
   */
  double compute_sens_norm(const bool & approx = false);

  /*!
   * \brief computes the (estimate) of the 2-norm of the sensitivity matrices
   * \param[out] dudx_norm - the estimate of the 2-norm of dudx
   * \param[out] dpsidx_norm - the estimate of the 2-norm of dpsidx
   * \param[in] approx - if true, approximate primal/dual with precond.
   */
  void compute_sens_norms(double & dudx_norm, double & dpsidx_norm,
                          const bool & approx = false);
      
  /*!
   * \brief defines the (estimate) of the 2-norm of the sensitivity matrix
   * \param[in] sens_norm - the estimate of the 2-norm (>0)
   */
  void set_sens_norm(const double & sens_norm);

  /*!
   * \brief defines the (estimates) of the 2-norm of the sensitivity matrices
   * \param[in] dudx_norm = estimate of the 2-norm of dudx
   * \param[in] dpsidx_norm = estimate of the 2-norm of dpsidx
   */
  void set_sens_norms(const double & dudx_norm, const double & dpsidx_norm);
  
  /*!
   * \brief operator that defines the Reduced-Hessian product
   * \param[in] u - DesignVector that is being multiplied
   * \param[out] v - result of Hessian-vector product
   */
  void operator()(const DesignVector & u, DesignVector & v);

 protected:

  DesignVector const* eval_at_design_; ///< design to evaluate Hessian at
  double design_norm_; ///< L2 norm of eval_at_design_;
  PDEVector const* eval_at_state_; ///< state to evaluate Hessian at
  double state_norm_; ///< L2 norm of eval_at_state_;
  PDEVector const* eval_at_adjoint_; ///< adjoint to evaluate Hessian at
  DesignVector const* reduced_grad_; ///< the reduced gradient at eval_at_*
  PDEVector const* adjoint_res_; ///< the adjoint equation residual at eval_at_*
  DesignVector * pert_design_; ///< perturbation to eval_at_design
  DesignVector * design_work1_; ///< design work space
  PDEVector * w_adj_; ///< the forward problem adjoint
  PDEVector * lambda_adj_; ///< the reverse problem adjoint
  PDEVector * pde_work1_; ///< state or adjoint work space
  PDEVector * pde_work2_; ///< state or adjoint work space
  PDEVector * pde_work3_; ///< state or adjoint work space
  int krylov_size_; ///< number of Krylov iterations used in prod
  double product_fac_; ///< factor that scales product_tol (see next)
  double product_tol_; // dynamic tolerance to solve linear systems in prod
  ostream * outstream_;
  boost::scoped_ptr<MatrixVectorProduct<PDEVector> >
  primal_matvec_; // forward problem matvec
  boost::scoped_ptr<Preconditioner<PDEVector> >
  primal_precond_; // forward problem preconditioner
  boost::scoped_ptr<MatrixVectorProduct<PDEVector> >
  adjoint_matvec_; // reverse problem matvec
  boost::scoped_ptr<Preconditioner<PDEVector> >
  adjoint_precond_; // reverse problem preconditioner
  double lambda_; ///< continuation parameter for globalization
  double scale_; // scaling factor for globalization term
  string bound_type_; // method used to bound Hessian-vector product error
  double sens_norm_; // estimate of 2-norm of the sensitivity matrix
  double dudx_norm_; // estimate of 2-norm of the sens. matrix dudx
  double dpsidx_norm_; // estimate of 2-norm of the sens. matrix dpsidx
  QuasiNewton * quasi_newton_; ///< preconditioner for design

 private:

  /*!
   * \brief checks the accuracy of the Reduced-Hessian product
   * \param[in] u - DesignVector that is being multiplied
   * \param[in] z - result of the inexact Hessian-vector product
   * \param[in] res_w - residual norm of the "w" forward adjoint problem
   * \param[in] rew_lam - residual norm of the "lambda" forward adjoint problem
   *
   * This routine is for diagnostics and to produce data comparing the error in
   * the reduced-Hessian product with the bound based on the sensitivity matrix.
   */
  void CheckProduct(const DesignVector & u, const DesignVector & z,
                    const double & res_w, const double & res_lam);
  
};
// ======================================================================
/*!
 * \class AugLagrangianHessianProduct
 * \brief specialization of matrix-vector product for reduced Hessian
 */
class AugLagrangianHessianProduct : public MatrixVectorProduct<DesignVector> {
 public:

  /*!
   * \brief constructor for class
   * \param[in] eval_at_design - design to evaluate Hessian at
   * \param[in] eval_at_dual - multiplier to evaluate Hessian at
   * \param[in] eval_at_state - state to evaluate Hessian at
   * \param[in] eval_at_adjoint - adjoint to evaluate Hessian at
   * \param[in] reduced_grad - the Aug. Lagrangian reduced gradient at eval_at_*
   * \param[in] adjoint_res - the adjoint equation residual at eval_at_*
   * \param[out] design_work1 - design work space
   * \param[out] design_work2 - design work space
   * \param[out] pde_work1 - state or adjoint work space
   * \param[out] pde_work2 - state or adjoint work space
   * \param[out] pde_work3 - state or adjoint work space
   * \param[out] pde_work4 - state or adjoint work space
   * \param[out] pde_work5 - state or adjoint work space
   * \param[out] dual_work1 - dual work space
   * \param[in] mu - penalty parameter
   * \param[in] krylov_size - number of Krylov iterations used in prod
   * \param[in] product_fac - scales product_tol_ which is used in adjoint tols.
   * \param[in/out] outstream - ostream object to write output to
   * \param[in] lambda - continuation parameter for globalization
   * \param[in] scale - scaling for globalization terms
   */
  AugLagrangianHessianProduct(
      const DesignVector & eval_at_design, const DualVector & eval_at_dual,
      const PDEVector & eval_at_state, const PDEVector & eval_at_adjoint,
      const DesignVector & reduced_grad, const PDEVector & adjoint_res,
      DesignVector & design_work1, DesignVector & design_work2,
      PDEVector & pde_work1, PDEVector & pde_work2, PDEVector & pde_work3,
      PDEVector & pde_work4, PDEVector & pde_work5, DualVector & dual_work1,
      const double & mu, const int & krylov_size, const double & product_fac,
      ostream & outstream, const double & lambda = 0.0,
      const double & scale = 1.0);

  /*!
   * \brief destructor for class
   */
  ~AugLagrangianHessianProduct();

  /*!
   * \brief defines the QuasiNewton object that should be updated during products
   * \param[in/out] quasi_newton - quasi-Newtom object that will be updated
   */
  void set_quasi_newton(QuasiNewton * quasi_newton);

  /*!
   * \brief defines the desired accuracy of the AugLag. Hessian-vector product
   * \param[in] product_tol - the desired accuracy
   */
  void set_product_tol(const double & product_tol);

  /*!
   * \brief computes the (estimate) of the 2-norm of the sensitivity matrix
   * \returns the estimate of the 2-norm (>0)
   *
   * Here, the sensitivity matrix refers to |du/dx^{T} dpsi/dx^{T}|, where u are
   * the state variables and psi are the adjoint variables.
   * \param[in] approx - if true, approximate primal/dual with precond.
   */
  double compute_sens_norm(const bool & approx = false);

  /*!
   * \brief computes the (estimate) of the 2-norm of the sensitivity matrices
   * \param[out] dudx_norm - the estimate of the 2-norm of dudx
   * \param[out] dpsidx_norm - the estimate of the 2-norm of dpsidx
   * \param[in] approx - if true, approximate primal/dual with precond.
   */
  void compute_sens_norms(double & dudx_norm, double & dpsidx_norm,
                          const bool & approx = false);
      
  /*!
   * \brief defines the (estimate) of the 2-norm of the sensitivity matrix
   * \param[in] sens_norm - the estimate of the 2-norm (>0)
   */
  void set_sens_norm(const double & sens_norm);

  /*!
   * \brief defines the (estimates) of the 2-norm of the sensitivity matrices
   * \param[in] dudx_norm = estimate of the 2-norm of dudx
   * \param[in] dpsidx_norm = estimate of the 2-norm of dpsidx
   */
  void set_sens_norms(const double & dudx_norm, const double & dpsidx_norm);

  /*!
   * \brief operator that defines the Reduced-Hessian product
   * \param[in] u - DesignVector that is being multiplied
   * \param[out] v - result of Hessian-vector product
   */
  void operator()(const DesignVector & u, DesignVector & v);

 protected:

  DesignVector const* eval_at_design_; ///< design to evaluate Hessian at
  double design_norm_; ///< L2 norm of eval_at_design_;
  DualVector const* eval_at_dual_; ///< multiplier to evaluate Hessian at
  double mu_; ///< penalty factor
  DualVector * dual_plus_ceq_; ///< multipliers plus 0.5*mu*C
  PDEVector const* eval_at_state_; ///< state to evaluate Hessian at
  double state_norm_; ///< L2 norm of eval_at_state_;
  PDEVector const* eval_at_adjoint_; ///< adjoint to evaluate Hessian at
  DesignVector const* reduced_grad_; ///< the reduced gradient at eval_at_*
  PDEVector const* adjoint_res_; ///< the adjoint equation residual at eval_at_*
  DesignVector * pert_design_; ///< perturbation to eval_at_design
  DesignVector * design_work1_; ///< design work space
  PDEVector * w_adj_; ///< the forward problem adjoint
  PDEVector * lambda_adj_; ///< the reverse problem adjoint
  PDEVector * pde_work1_; ///< state or adjoint work space
  PDEVector * pde_work2_; ///< state or adjoint work space
  PDEVector * pde_work3_; ///< state or adjoint work space
  int krylov_size_; ///< number of Krylov iterations used in prod
  double product_fac_; ///< factor that scales product_tol (see next)
  double product_tol_; // dynamic tolerance to solve linear systems in prod
  ostream * outstream_;
  MatrixVectorProduct<PDEVector>* primal_matvec_; // forward problem matvec
  Preconditioner<PDEVector>* primal_precond_; // forward problem preconditioner
  MatrixVectorProduct<PDEVector>* adjoint_matvec_; // reverse problem matvec
  Preconditioner<PDEVector>* adjoint_precond_; // reverse problem preconditioner
  double lambda_; ///< continuation parameter for globalization
  double scale_; // scaling factor for globalization term
  string bound_type_; // method used to bound Hessian-vector product error
  double sens_norm_; // estimate of 2-norm of the sensitivity matrix
  double dudx_norm_; // estimate of 2-norm of the sens. matrix dudx
  double dpsidx_norm_; // estimate of 2-norm of the sens. matrix dpsidx
  QuasiNewton * quasi_newton_; ///< preconditioner for design

 private:
#if 0
  /*!
   * \brief checks the accuracy of the Reduced-Hessian product
   * \param[in] u - DesignVector that is being multiplied
   * \param[in] z - result of the inexact Hessian-vector product
   * \param[in] res_w - residual norm of the "w" forward adjoint problem
   * \param[in] rew_lam - residual norm of the "lambda" forward adjoint problem
   *
   * This routine is for diagnostics and to produce data comparing the error in
   * the reduced-Hessian product with the bound based on the sensitivity matrix.
   */
  void CheckProduct(const DesignVector & u, const DesignVector & z,
                    const double & res_w, const double & res_lam);
#endif
};
// ======================================================================
/*!
 * \class ApproxReducedHessianProduct
 * \brief defines an approximate reduced Hessian-vector product
 */
class ApproxReducedHessianProduct : public MatrixVectorProduct<DesignVector> {
 public:

  /*!
   * \brief constructor for class
   * \param[in] eval_at_design - design to evaluate Hessian at
   * \param[in] eval_at_state - state to evaluate Hessian at
   * \param[in] eval_at_adjoint - adjoint to evaluate Hessian at
   * \param[in] reduced_grad - the reduced gradient at eval_at_*
   * \param[in] adjoint_res - the adjoint equation residual at eval_at_*
   * \param[out] design_work1 - design work space
   * \param[out] design_work2 - design work space
   * \param[out] pde_work1 - state or adjoint work space
   * \param[out] pde_work2 - state or adjoint work space
   * \param[out] pde_work3 - state or adjoint work space
   * \param[out] pde_work4 - state or adjoint work space
   * \param[out] pde_work5 - state or adjoint work space
   * \param[in/out] outstream - ostream object to write output to
   * \param[in] lambda - continuation parameter for globalization
   * \param[in] scale - scaling for globalization terms
   */
  ApproxReducedHessianProduct(
      const DesignVector & eval_at_design, const PDEVector & eval_at_state,
      const PDEVector & eval_at_adjoint, const DesignVector & reduced_grad,
      const PDEVector & adjoint_res, DesignVector & design_work1,
      DesignVector & design_work2, PDEVector & pde_work1, PDEVector & pde_work2,
      PDEVector & pde_work3, PDEVector & pde_work4, PDEVector & pde_work5, 
      ostream & outstream, const double & lambda = 0.0,
      const double & scale = 1.0);

  /*!
   * \brief destructor for class
   */
  ~ApproxReducedHessianProduct();

  /*!
   * \brief defines the diagonal matrix added to the Hessian
   * \param[in] lambda - new value for lambda
   */
  void set_lambda(const double & lambda);
  
  /*!
   * \brief operator that defines the Reduced-Hessian product
   * \param[in] u - DesignVector that is being multiplied
   * \param[out] v - result of Hessian-vector product
   */
  void operator()(const DesignVector & u, DesignVector & v);

 protected:

  DesignVector const* eval_at_design_; ///< design to evaluate Hessian at
  double design_norm_; ///< L2 norm of eval_at_design_;
  PDEVector const* eval_at_state_; ///< state to evaluate Hessian at
  double state_norm_; ///< L2 norm of eval_at_state_;
  PDEVector const* eval_at_adjoint_; ///< adjoint to evaluate Hessian at
  DesignVector const* reduced_grad_; ///< the reduced gradient at eval_at_*
  PDEVector const* adjoint_res_; ///< the adjoint equation residual at eval_at_*
  DesignVector * pert_design_; ///< perturbation to eval_at_design
  DesignVector * design_work1_; ///< design work space
  PDEVector * w_adj_; ///< the forward problem adjoint
  PDEVector * lambda_adj_; ///< the reverse problem adjoint
  PDEVector * pde_work1_; ///< state or adjoint work space
  PDEVector * pde_work2_; ///< state or adjoint work space
  PDEVector * pde_work3_; ///< state or adjoint work space
  ostream * outstream_;
  MatrixVectorProduct<PDEVector>* primal_matvec_; // forward problem matvec
  Preconditioner<PDEVector>* primal_precond_; // forward problem preconditioner
  MatrixVectorProduct<PDEVector>* adjoint_matvec_; // reverse problem matvec
  Preconditioner<PDEVector>* adjoint_precond_; // reverse problem preconditioner
  double lambda_; ///< continuation parameter for globalization
  double scale_; // scaling factor for globalization term
};

// ======================================================================

/*!
 * \class ReducedSpacePreconditioner
 * \brief specialization of preconditioner for reduced-space problem
 */
class ReducedSpacePreconditioner : public Preconditioner<DesignVector> {
 public:

  /*!
   * \brief class constructor that builds preconditioner
   * \param[in] quasi_newton - Quasi-Newton object to approximate Hess.
   */
  ReducedSpacePreconditioner(QuasiNewton * quasi_newton);

  /*!
   * \brief returns the user-vector memory requirements
   * \param[in,out] num_required - stores the memory requirements
   *
   * On exit, num_required holds three elements with labels "design", "state",
   * and "dual"; thus, one each for the number of DesignVectors, PDEVectors, and
   * DualVectors, respectively.
   */
  void MemoryRequired(ptree& num_required);
  
  /*!
   * \brief updates the diagonal matrix added to the preconditioner matrix
   * \param[in] diag - the value on the diagonal
   */
  void set_diagonal(const double & diag);
  
  /*!
   * \brief operator that defines KKT preconditioner
   * \param[in] u - DesignVector that is being preconditioned
   * \param[out] v - DesignVector that is the result of the operation
   */
  void operator()(DesignVector & u, DesignVector & v);
  
 protected:
  QuasiNewton * quasi_newton_; ///< preconditioner for design
  double diag_; ///< value of diagonal matrix added to preconditioner
};

// ======================================================================

/*!
 * \class NestedReducedSpacePreconditioner
 * \brief specialization of preconditioner for reduced-space problem
 */
class NestedReducedSpacePreconditioner : public Preconditioner<DesignVector> {
 public:
  
  /*!
   * \brief basic class constructor
   * \param[in/out] outstream - ostream object to write output to
   */
  NestedReducedSpacePreconditioner(ostream& outstream = cout);
  
  /*!
   * \brief class constructor that builds preconditioner
   * \param[in] eval_at_design - design to evaluate Hessian at
   * \param[in] eval_at_state - state to evaluate Hessian at
   * \param[in] eval_at_adjoint - adjoint to evaluate Hessian at
   * \param[in] reduced_grad - the reduced gradient at eval_at_*
   * \param[in] adjoint_res - the adjoint equation residual at eval_at_*
   * \param[out] design_work1 - design work space
   * \param[out] design_work2 - design work space
   * \param[out] pde_work1 - state or adjoint work space
   * \param[out] pde_work2 - state or adjoint work space
   * \param[out] pde_work3 - state or adjoint work space
   * \param[out] pde_work4 - state or adjoint work space
   * \param[out] pde_work5 - state or adjoint work space
   * \param[in/out] outstream - ostream object to write output to
   * \param[in] lambda - continuation parameter for globalization
   * \param[in] scale - scaling for globalization terms
   */
  NestedReducedSpacePreconditioner(
      const DesignVector & eval_at_design, const PDEVector & eval_at_state,
      const PDEVector & eval_at_adjoint, const DesignVector & reduced_grad,
      const PDEVector & adjoint_res, DesignVector & design_work1,
      DesignVector & design_work2, PDEVector & pde_work1, PDEVector & pde_work2,
      PDEVector & pde_work3, PDEVector & pde_work4, PDEVector & pde_work5, 
      ostream & outstream, const double & lambda = 0.0,
      const double & scale = 0.0);
  
  /*!
   * \brief destructor for class
   */
  ~NestedReducedSpacePreconditioner();

  /*!
   * \brief returns the user-vector memory requirements
   * \param[in,out] num_required - stores the memory requirements
   *
   * On exit, num_required holds three elements with labels "design", "state",
   * and "dual"; thus, one each for the number of DesignVectors, PDEVectors, and
   * DualVectors, respectively.
   */
  void MemoryRequired(ptree& num_required);
  
  /*!
   * \brief updates the diagonal matrix added to the preconditioner matrix
   * \param[in] diag - the value on the diagonal
   */
  void set_diagonal(const double & diag);

  /*!
   * \brief prepares the operator for use
   * \param[in] eval_at_design - design to evaluate Hessian at
   * \param[in] eval_at_state - state to evaluate Hessian at
   * \param[in] eval_at_adjoint - adjoint to evaluate Hessian at
   * \param[in] reduced_grad - the reduced gradient at eval_at_*
   * \param[in] adjoint_res - the adjoint equation residual at eval_at_*
   * \param[out] design_work - array of DesignVectors for work
   * \param[out] pde_work - array of PDEVectors for work
   * \param[in] prec_param - input parameters
   * \param[in/out] outstream - ostream object to write output to
   *
   * This is the counterpart to the long version of the constructor above.  This
   * is called when the preconditioner is not rebuilt each iteration, yet
   * various quantities must still be redefined.
   */
  void Initialize(
      const DesignVector & eval_at_design, const PDEVector & eval_at_state,
      const PDEVector & eval_at_adjoint, const DesignVector & reduced_grad,
      const PDEVector & adjoint_res, std::vector<DesignVector>& design_work,
      std::vector<PDEVector> & pde_work, const ptree& prec_param,
      ostream & outstream = std::cout);
  
  /*!
   * \brief operator that defines KKT preconditioner
   * \param[in] u - KKTVector that is being preconditioned
   * \param[out] v - KKTVector that is the result of the operation
   */
  void operator()(DesignVector & u, DesignVector & v);
  
 protected:
  MatrixVectorProduct<DesignVector> * mat_vec_; ///< reduced-Hessian product
  double diag_; ///< value of diagonal matrix added to preconditioner
  ostream * outstream_;
};

// ======================================================================
/*!
 * \class SensitivityProduct
 * \brief specialization of matrix-vector product for a sensitivity product
 *
 * To bound the error in the Hessian-vector product, we need to approximate the
 * two norm of the matrix A = (du/dx^T dpsi/dx^T)^T, where du/dx and dpsi/dx
 * denote the sensitivities of the state and adjoint variables, respectively.
 * To find this norm, we use Lanczos applied to the matrix A^T A; the
 * matrix-vector products for this matrix are defined by this class.
 */
class SensitivityProduct : public MatrixVectorProduct<DesignVector> {
 public:

  /*!
   * \brief constructor for class
   * \param[in] eval_at_design - design to evaluate matrix at
   * \param[in] eval_at_state - state to evaluate matrix at
   * \param[in] eval_at_adjoint - adjoint to evaluate matrix at
   * \param[in] reduced_grad - the reduced gradient at eval_at_* (needed?)
   * \param[in] adjoint_res - the adjoint equation residual at eval_at_*
   * \param[out] design_work1 - design work space
   * \param[out] design_work2 - design work space
   * \param[out] pde_work1 - state or adjoint work space
   * \param[out] pde_work2 - state or adjoint work space
   * \param[out] pde_work3 - state or adjoint work space
   * \param[out] pde_work4 - state or adjoint work space
   * \param[out] pde_work5 - state or adjoint work space
   * \param[in] krylov_size - number of Krylov iterations used in prod
   * \param[in] krylov_tol - tolerance (absolute) to solve linear systems in prod
   * \param[in] approx - if true, approximate primal/dual with precond.
   * \param[in/out] outstream - ostream object to write output to
   */
  SensitivityProduct(
      const DesignVector & eval_at_design, const PDEVector & eval_at_state,
      const PDEVector & eval_at_adjoint, const DesignVector & reduced_grad,
      const PDEVector & adjoint_res, DesignVector & design_work1,
      DesignVector & design_work2, PDEVector & pde_work1,
      PDEVector & pde_work2, PDEVector & pde_work3,
      PDEVector & pde_work4, PDEVector & pde_work5, 
      const int & krylov_size, const double & krylov_tol,
      const bool & approx,ostream & outstream);

  /*!
   * \brief destructor for class
   */
  ~SensitivityProduct();

  /*!
   * \brief operator that defines the sensitivity-matrix product
   * \param[in] u - DesignVector that is being multiplied
   * \param[out] v - result of sensitivity-matrix-vector product
   */
  void operator()(const DesignVector & w, DesignVector & z);

 protected:

  DesignVector const* eval_at_design_; ///< design to evaluate at
  double design_norm_; ///< L2 norm of eval_at_design_;
  PDEVector const* eval_at_state_; ///< state to evaluate at
  double state_norm_; ///< L2 norm of eval_at_state_;
  PDEVector const* eval_at_adjoint_; ///< adjoint to evaluate at
  DesignVector const* reduced_grad_; ///< the reduced gradient at eval_at_*
  PDEVector const* adjoint_res_; ///< the adjoint equation residual at eval_at_*
  DesignVector * pert_design_; ///< perturbation to eval_at_design
  DesignVector * design_work1_; ///< design work space
  PDEVector * v_; ///< the forward problem adjoint
  PDEVector * lambda_; ///< the reverse problem adjoint
  PDEVector * pde_work1_; ///< state or adjoint work space
  PDEVector * pde_work2_; ///< state or adjoint work space
  PDEVector * pde_work3_; ///< state or adjoint work space
  int krylov_size_; ///< number of Krylov iterations used in prod
  double krylov_tol_; ///< tolerance to solve linear systems in prod
  ostream * outstream_;
  bool approx_; // if true, the primal/dual are approximated with preconditioner
  MatrixVectorProduct<PDEVector>* primal_matvec_; // forward problem matvec
  Preconditioner<PDEVector>* primal_precond_; // forward problem preconditioner
  MatrixVectorProduct<PDEVector>* adjoint_matvec_; // reverse problem matvec
  Preconditioner<PDEVector>* adjoint_precond_; // reverse problem preconditioner
};

// ======================================================================
/*!
 * \class DuDxProduct
 * \brief specialization of matrix-vector product for a dudx^T*dudx matrix
 *
 * One way to bound the error in the Hessian-vector product, involves an
 * approximation the two norm of the matrix A = du/dx, the sensitivities of the
 * state variables.  To find this norm, we use Lanczos applied to the matrix A^T
 * A; the matrix-vector products for this matrix are defined by this class.
 */
class DuDxProduct : public MatrixVectorProduct<DesignVector> {
 public:

  /*!
   * \brief constructor for class
   * \param[in] eval_at_design - design to evaluate matrix at
   * \param[in] eval_at_state - state to evaluate matrix at
   * \param[out] pde_work1 - state or adjoint work space
   * \param[out] pde_work2 - state or adjoint work space
   * \param[in] krylov_size - number of Krylov iterations used in prod
   * \param[in] krylov_tol - tolerance (absolute) to solve linear systems in prod
   * \param[in] approx - if true, approximate primal/dual with precond.
   * \param[in/out] outstream - ostream object to write output to
   */
  DuDxProduct(
      const DesignVector & eval_at_design, const PDEVector & eval_at_state,
      PDEVector & pde_work1, PDEVector & pde_work2,
      const int & krylov_size, const double & krylov_tol,
      const bool & approx, ostream & outstream);

  /*!
   * \brief destructor for class
   */
  ~DuDxProduct();

  /*!
   * \brief operator that defines the dudx^{T}*dudx product
   * \param[in] u - DesignVector that is being multiplied
   * \param[out] v - result of dudx^T*dudx-matrix-vector product
   */
  void operator()(const DesignVector & w, DesignVector & z);

 protected:

  DesignVector const* eval_at_design_; ///< design to evaluate at
  PDEVector const* eval_at_state_; ///< state to evaluate at
  PDEVector * v_; ///< the forward problem adjoint
  PDEVector * pde_work_; ///< state or adjoint work space
  int krylov_size_; ///< number of Krylov iterations used in prod
  double krylov_tol_; ///< tolerance to solve linear systems in prod
  ostream * outstream_;
  bool approx_; // if true, the primal/dual are approximated with preconditioner
  MatrixVectorProduct<PDEVector>* primal_matvec_; // forward problem matvec
  Preconditioner<PDEVector>* primal_precond_; // forward problem preconditioner
  MatrixVectorProduct<PDEVector>* adjoint_matvec_; // reverse problem matvec
  Preconditioner<PDEVector>* adjoint_precond_; // reverse problem preconditioner
};

// ======================================================================
/*!
 * \class DpsiDxProduct
 * \brief specialization of matrix-vector product for a dpsidx^T*dpsidx matrix
 *
 * One way to bound the error in the Hessian-vector product, involves an
 * approximation the two norm of the matrix A = dpsi/dx, the sensitivities of
 * the state variables.  To find this norm, we use Lanczos applied to the matrix
 * A^T A; the matrix-vector products for this matrix are defined by this class.
 */
class DpsiDxProduct : public MatrixVectorProduct<DesignVector> {
 public:

  /*!
   * \brief constructor for class
   * \param[in] eval_at_design - design to evaluate matrix at
   * \param[in] eval_at_state - state to evaluate matrix at
   * \param[in] eval_at_adjoint - adjoint to evaluate matrix at
   * \param[in] reduced_grad - the reduced gradient at eval_at_* (needed?)
   * \param[in] adjoint_res - the adjoint equation residual at eval_at_*
   * \param[out] design_work1 - design work space
   * \param[out] design_work2 - design work space
   * \param[out] pde_work1 - state or adjoint work space
   * \param[out] pde_work2 - state or adjoint work space
   * \param[out] pde_work3 - state or adjoint work space
   * \param[out] pde_work4 - state or adjoint work space
   * \param[out] pde_work5 - state or adjoint work space
   * \param[in] krylov_size - number of Krylov iterations used in prod
   * \param[in] krylov_tol - tolerance (absolute) to solve linear systems in prod
   * \param[in] approx - if true, approximate primal/dual with precond.
   * \param[in/out] outstream - ostream object to write output to
   */
  DpsiDxProduct(
      const DesignVector & eval_at_design, const PDEVector & eval_at_state,
      const PDEVector & eval_at_adjoint, const DesignVector & reduced_grad,
      const PDEVector & adjoint_res, DesignVector & design_work1,
      DesignVector & design_work2, PDEVector & pde_work1,
      PDEVector & pde_work2, PDEVector & pde_work3,
      PDEVector & pde_work4, PDEVector & pde_work5, 
      const int & krylov_size, const double & krylov_tol,
      const bool & approx, ostream & outstream);

  /*!
   * \brief destructor for class
   */
  ~DpsiDxProduct();

  /*!
   * \brief operator that defines the sensitivity-matrix product
   * \param[in] u - DesignVector that is being multiplied
   * \param[out] v - result of sensitivity-matrix-vector product
   */
  void operator()(const DesignVector & w, DesignVector & z);

 protected:

  DesignVector const* eval_at_design_; ///< design to evaluate at
  double design_norm_; ///< L2 norm of eval_at_design_;
  PDEVector const* eval_at_state_; ///< state to evaluate at
  double state_norm_; ///< L2 norm of eval_at_state_;
  PDEVector const* eval_at_adjoint_; ///< adjoint to evaluate at
  DesignVector const* reduced_grad_; ///< the reduced gradient at eval_at_*
  PDEVector const* adjoint_res_; ///< the adjoint equation residual at eval_at_*
  DesignVector * pert_design_; ///< perturbation to eval_at_design
  DesignVector * design_work1_; ///< design work space
  PDEVector * v_; ///< the forward problem adjoint
  PDEVector * lambda_; ///< the reverse problem adjoint
  PDEVector * pde_work1_; ///< state or adjoint work space
  PDEVector * pde_work2_; ///< state or adjoint work space
  PDEVector * pde_work3_; ///< state or adjoint work space
  int krylov_size_; ///< number of Krylov iterations used in prod
  double krylov_tol_; ///< tolerance to solve linear systems in prod
  ostream * outstream_;
  bool approx_; // if true, the primal/dual are approximated with preconditioner
  MatrixVectorProduct<PDEVector>* primal_matvec_; // forward problem matvec
  Preconditioner<PDEVector>* primal_precond_; // forward problem preconditioner
  MatrixVectorProduct<PDEVector>* adjoint_matvec_; // reverse problem matvec
  Preconditioner<PDEVector>* adjoint_precond_; // reverse problem preconditioner
};


// ======================================================================
/*!
 * \class ConstraintJacobianProduct
 * \brief specialization of matrix-vector product for constraint Jacobian
 */
class ConstraintJacobianProduct : public MatrixVectorProduct<DesignVector> {
 public:

  /*!
   * \brief constructor for class
   * \param[in/out] outstream - ostream object to write output to
   */
  ConstraintJacobianProduct(ostream& outstream = cout);
  
  /*!
   * \brief constructor for class
   * \param[in] eval_at_design - design vector to evaluate constraint Jacobian at
   * \param[in] eval_at_state - state to evaluate constraint Jacobian at
   * \param[out] design_work1 - design work space
   * \param[out] pde_work1 - state or adjoint work space
   * \param[out] pde_work2 - state or adjoint work space
   * \param[out] dual_work1 - dual work space
   * \param[in/out] outstream - ostream object to write output to
   * \param[in] approx - if true, the PDE solves are replaced with precond. apps
   */
  ConstraintJacobianProduct(
      const DesignVector & eval_at_design, const PDEVector & eval_at_state,
      DesignVector & design_work1, PDEVector & pde_work1, PDEVector & pde_work2,
      DualVector & dual_work1, ostream & outstream, const bool& approx = true);
  
  /*!
   * \brief destructor for class
   */
  ~ConstraintJacobianProduct();

  /*!
   * \brief returns the user-vector memory requirements
   * \param[in,out] num_required - stores the memory requirements
   *
   * On exit, num_required holds three elements with labels "design", "state",
   * and "dual"; thus, one each for the number of DesignVectors, PDEVectors, and
   * DualVectors, respectively.
   */
  void MemoryRequired(ptree& num_required);
  
  /*!
   * \brief product will use transposed constraint Jacobian
   */
  void Transpose();

  /*!
   * \brief product will use constraint Jacobian
   */
  void Untranspose();

  /*!
   * \brief product will be restricted to "design" subspace (no "target" comp.)
   */
  void RestrictToDesign();

  /*!
   * \brief product will be restricted to "target" subspace (no "design" comp.)
   */
  void RestrictToTarget();

  /*!
   * \brief product will be applied to both subspaces
   */
  void Unrestrict();
  
  /*!
   * \brief operator that defines the Constraint-Jacobian product
   * \param[in] u - DesignVector that is being multiplied
   * \param[out] v - result of Constraint-Jacobian-vector product
   */
  void operator()(const DesignVector & u, DesignVector & v);

 protected:

  bool transpose_; ///< indicates if transposed constraint Jacobian is needed
  bool design_; ///< indicates that design subspace product to be included
  bool target_; ///< indicates that target subspace product to be included
  bool approx_; ///< indicates that the PDE solves should be approximate
  DesignVector const* eval_at_design_; ///< design to evaluate KKT-matrix at
  PDEVector const* eval_at_state_; ///< state to evaluate KKT-matrix at
  DesignVector * design_work_; ///< design work space
  PDEVector * adj_; ///< the forward problem adjoint
  PDEVector * pde_work1_; ///< state or adjoint work space
  DualVector * dual_work_; ///< dual work space
  ostream * outstream_;
  boost::scoped_ptr<MatrixVectorProduct<PDEVector> >
  primal_matvec_; // forward problem matvec
  boost::scoped_ptr<Preconditioner<PDEVector> >
  primal_precond_; // forward problem preconditioner
  boost::scoped_ptr<MatrixVectorProduct<PDEVector> >
  adjoint_matvec_; // reverse problem matvec
  boost::scoped_ptr<Preconditioner<PDEVector> >
  adjoint_precond_; // reverse problem preconditioner
};

} // namespace kona
