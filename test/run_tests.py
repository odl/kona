#!/usr/bin/python -u

# Runs the Kona unit tests

import numpy as np
import subprocess as sub
import glob as glob
import os

print 'Running Kona unit tests...'

# add current directory to the library path, and find all the tests
# os.environ["LD_LIBRARY_PATH"] += os.pathsep + "../"
unit_tests = glob.glob('./test_*.bin')

# loop over the tests
numfail = 0
failed_tests = []
for test in unit_tests:
    print 'Running unit test ',test
    nfail = sub.call([test, '>& screen'])
    if (nfail < 0):
        print 'unit test terminated by signal = ', nfail
        numfail += 1
    else:
        numfail += nfail
    if (nfail != 0):
        failed_tests.append(test)

print
if (numfail > 0):
    print '***** Kona unit tests had ',numfail,' total failures *****'
    print '***** the following binary had failures:'
    for test in failed_tests:
        print '***** ',test
else:
    print '***** All Kona unit tests passed *****'
    # delete any unnecessary data files that were created if all tests passed
    sub.call(['rm', '-f', 'krylov_out.dat', 'kona_hist.dat', 'kona_krylov.dat', \
              'kona_reduced_krylov.dat', 'screen'])
    #sub.call(['rm', '-f', '

