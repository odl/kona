/**
 * \file usrfun.hpp
 * \brief header with generic user function used for testing
 * \author  Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

static int numDesign = -1; // local copy of the number of design vectors
static int numState = -1; // local copy of the number of state vectors
static int numDual = -1; // local copy of the number of constraint/dual vectors
double **design;     // global memory to act like the design vectors
double **state;      // global memory to act like the state vectors
double **dual;       // global memory to act like the dual vectors
double **mat;        // storage for the matrix
static bool no_prec = false;

// local user function
int userFunc(int request, int leniwrk, int *iwrk, int lendwrk,
	     double *dwrk) {
  switch (request) {
    case kona::allocmem: {// allocate iwrk[0] design vectors and
      // iwrk[1] state vectors
      if (numDesign >= 0) { // free design memory first
        if (design == NULL) {
          cerr << "userFunc: "
               << "design array is NULL but numDesign > 0" << endl;
          throw(-1);
        }
        for (int i = 0; i < numDesign; i++)
          delete [] design[i];
        delete [] design;
      }  
      if (numState >= 0) { // free state memory first
        if (state == NULL) {
          cerr << "userFunc: "
               << "state array is NULL but numState > 0" << endl;
          throw(-1);
        }
        for (int i = 0; i < numState; i++)
          delete [] state[i];
        delete [] state;
      }       
      if (numDual >= 0) { // free dual memory first
        if (dual == NULL) {
          cerr << "userFunc: "
               << "dual array is NULL but numDual > 0" << endl;
          throw(-1);
        }
        for (int i = 0; i < numDual; i++)
          delete [] dual[i];
        delete [] dual;
      }      
      numDesign = iwrk[0];
      numState = iwrk[1];
      numDual = iwrk[2];
      assert(numDesign >= 0);
      assert(numState >= 0);
      assert(numDual >= 0);
      design = new double * [numDesign];
      for (int i = 0; i < numDesign; i++)
        design[i] = new double[nDV];
      state = new double * [numState];
      for (int i = 0; i < numState; i++)
        state[i] = new double[nVar];
      dual = new double * [numDual];
      for (int i = 0; i < numDual; i++)
        dual[i] = new double[nCeq];      
      break;
    }
    case kona::axpby_d: {// using design array set
      // iwrk[0] = dwrk[0]*iwrk[1] + dwrk[1]*iwrk[2]
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numDesign));
      assert(j < numDesign);
      assert(k < numDesign);
      
      double scalj = dwrk[0];
      double scalk = dwrk[1];
      if (j == -1) {
        if (k == -1) { // if both indices = -1, then all elements = scalj
          for (int n = 0; n < nDV; n++)
            design[i][n] = scalj;
          
        } else { // if just j = -1 ...
          if (scalk == 1.0) {
            // direct copy of vector k with no scaling
            for (int n = 0; n < nDV; n++)
              design[i][n] = design[k][n];
            
          } else {
            // scaled copy of vector k
            for (int n = 0; n < nDV; n++)
              design[i][n] = scalk*design[k][n];
          }
        }
      } else if (k == -1) { // if just k = -1 ...
        if (scalj == 1.0) {
          // direct copy of vector j with no scaling
          for (int n = 0; n < nDV; n++)
            design[i][n] = design[j][n];
          
        } else {
          // scaled copy of vector j
          for (int n = 0; n < nDV; n++)
            design[i][n] = scalj*design[j][n];
        }
      } else { // otherwise, full axpby
        for (int n = 0; n < nDV; n++)
          design[i][n] = scalj*design[j][n] + scalk*design[k][n];
      }
      break;
    }
    case kona::axpby_s: {// using state array set
      // iwrk[0] = dwrk[0]*iwrk[1] + dwrk[1]*iwrk[2]
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numState));
      assert(j < numState);
      assert(k < numState);

      double scalj = dwrk[0];
      double scalk = dwrk[1];
      if (j == -1) {
        if (k == -1) { // if both indices = -1, then all elements = scalj
          for (int n = 0; n < nVar; n++)
            state[i][n] = scalj;

        } else { // if just j = -1 ...
          if (scalk == 1.0) {
            // direct copy of vector k with no scaling
            for (int n = 0; n < nVar; n++)
              state[i][n] = state[k][n];

          } else {
            // scaled copy of vector k
            for (int n = 0; n < nVar; n++)
              state[i][n] = scalk*state[k][n];
          }
        }
      } else if (k == -1) { // if just k = -1 ...
        if (scalj == 1.0) {
          // direct copy of vector j with no scaling
          for (int n = 0; n < nVar; n++)
            state[i][n] = state[j][n];

        } else {
          // scaled copy of vector j
          for (int n = 0; n < nVar; n++)
            state[i][n] = scalj*state[j][n];
        }
      } else { // otherwise, full axpby
        for (int n = 0; n < nVar; n++)
          state[i][n] = scalj*state[j][n] + scalk*state[k][n];
      }
      break;
    }
    case kona::axpby_c: {// using dual array set
      // iwrk[0] = dwrk[0]*iwrk[1] + dwrk[1]*iwrk[2]
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numDual));
      assert(j < numDual);
      assert(k < numDual);

      double scalj = dwrk[0];
      double scalk = dwrk[1];
      if (j == -1) {
        if (k == -1) { // if both indices = -1, then all elements = scalj
          for (int n = 0; n < nCeq; n++)
            dual[i][n] = scalj;

        } else { // if just j = -1 ...
          if (scalk == 1.0) {
            // direct copy of vector k with no scaling
            for (int n = 0; n < nCeq; n++)
              dual[i][n] = dual[k][n];

          } else {
            // scaled copy of vector k
            for (int n = 0; n < nCeq; n++)
              dual[i][n] = scalk*dual[k][n];
          }
        }
      } else if (k == -1) { // if just k = -1 ...
        if (scalj == 1.0) {
          // direct copy of vector j with no scaling
          for (int n = 0; n < nCeq; n++)
            dual[i][n] = dual[j][n];

        } else {
          // scaled copy of vector j
          for (int n = 0; n < nCeq; n++)
            dual[i][n] = scalj*dual[j][n];
        }
      } else { // otherwise, full axpby
        for (int n = 0; n < nCeq; n++)
          dual[i][n] = scalj*dual[j][n] + scalk*dual[k][n];
      }
      break;
    }      
    case kona::innerprod_d: {// using design array set
      // dwrk[0] = (iwrk[0])^{T} * iwrk[1]
      int i = iwrk[0];
      int j = iwrk[1];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numDesign));
      dwrk[0] = 0.0;
      for (int n = 0; n < nDV; n++)
        dwrk[0] += design[i][n]*design[j][n];
      break;
    } 
    case kona::innerprod_s: {// using state array set
      // dwrk[0] = (iwrk[0])^{T} * iwrk[1]
      int i = iwrk[0];
      int j = iwrk[1];
      assert((i >= 0) && (i < numState));
      assert((j >= 0) && (j < numState));
      dwrk[0] = 0.0;
      for (int n = 0; n < nVar; n++)
        dwrk[0] += state[i][n]*state[j][n];
      break;
    }
    case kona::innerprod_c: {// using dual array set
      // dwrk[0] = (iwrk[0])^{T} * iwrk[1]
      int i = iwrk[0];
      int j = iwrk[1];
      assert((i >= 0) && (i < numDual));
      assert((j >= 0) && (j < numDual));
      dwrk[0] = 0.0;
      for (int n = 0; n < nCeq; n++)
        dwrk[0] += dual[i][n]*dual[j][n];
      break;
    }
    case kona::jacvec_s: {// apply state component of the Jacobian-vector
      // product to vector iwrk[2]
      // recall; iwrk[0], iwrk[1] denote where Jacobian is evaluated
      // and in this case, they are not needed
      int i = iwrk[2];
      int j = iwrk[3];
      assert((i >= 0) && (i < numState));
      assert((j >= 0) && (j < numState));
      for (int n = 0; n < nVar; n++) {
        state[j][n] = 0.0;
        for (int k = 0; k < nVar; k++) {
          state[j][n] += mat[n][k]*state[i][k];
        }
      }
      break;
    }
    case kona::precond_s: {// apply primal preconditioner to iwrk[2]
      // recall; iwrk[0], iwrk[1] denote where preconditioner is
      // evaluated and in this case, they are not needed
      int i = iwrk[2];
      int j = iwrk[3];
      assert((i >= 0) && (i < numState));
      assert((j >= 0) && (j < numState));

      if (no_prec) {
        // do-nothing preconditioner
        for (int n = 0; n < nVar; n++) {
          state[j][n] = state[i][n];
        }
      } else {
        // upwinding with strong inlet boundary condition
        state[j][0] = state[i][0];
        for (int n = 1; n < nVar; n++) {
          state[j][n] = state[i][n] + state[j][n-1];
        }
      }
      break;
    }
    default: {
      cerr << "userFunc: "
           << "unrecognized request value: request = "
           << request << endl;
      throw(-1);
      break;
    }
  }
  return 0;
}

void writeOutcome(const bool & pass) {
  if (pass) {
    cout << "pass" << endl;
  } else {
    cout << "fail" << endl;
  }
}
