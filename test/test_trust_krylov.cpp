/**
 * \file test_trust_krylov.cpp
 * \brief unit test for the FITR Krylov iterative solver
 * \author  Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#include <assert.h>
#include <limits>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/property_tree/ptree.hpp>
#include "../src/user_memory.hpp"
#include "../src/vectors.hpp"
#include "../src/vector_operators.hpp"
#include "../src/krylov.hpp"

using std::numeric_limits;
using kona::PDEVector;
using boost::property_tree::ptree;

const unsigned int log2Nvar = 12;
static double condnum = 1000.0;
const int nDV = 0; // size of design vectors
const int nVar = 1 << log2Nvar;  // size of state vectors
static int numDesign = -1; // number of design vectors
static int numState = -1; // number of state vectors
double **design;     // global memory to act like the design vectors
double **state;      // global memory to act like the state vectors
//static bool no_prec = false;

int userFunc(int request, int leniwrk, int *iwrk, int lendwrk,
	     double *dwrk);

void applyHadamard(const unsigned int & log2N, double * u, double * v);

void applyDiagonal(const unsigned int & log2N, double * u,
                   const bool & invert = false, const double & diag = 0.0);

void writeOutcome(const bool & pass);

int main(int argc, char *argv[]) {
  if (argc == 2) {
    condnum = 1000.0;
  }
  
  // allocate state vector memory
  int m = 60;
  //
  int nd = 0;
  //int ns = 46;
  //int ns = 66;
  //int ns = 86;
  int ns = 2*(m+1) + 3; //126+1;
  ns += 2*m + 1;
  kona::UserMemory::Allocate(userFunc, nd, ns);
  bool pass;
  int numfail = 0;

#if 0
  // test Hadamard and Diagonal
  double * u = new double[nVar];
  double * v = new double[nVar];
  double * w = new double[nVar];
  for (int j = 0; j < nVar; j++) {
    for (int i = 0; i < nVar; i++)
      u[i] = 0.0;
    u[j] = 1.0;
    applyHadamard(log2Nvar, u, v);
    applyDiagonal(log2Nvar, v);
    applyHadamard(log2Nvar, v, w);
    for (int i = 0; i < nVar; i++)
      cout << w[i] << " ";
    cout << endl;
  }
  delete [] u;
  delete [] v;
  delete [] w;
  throw(-1);
#endif
  
  //cout << "\tbefore initializing PDEVector b ..." << endl;
  PDEVector b;
  for (int i = 0; i < nVar; i++)
    state[0][i] = 0.0;
  state[0][0] = 1.0;

  //cout << "\tbefore initializing PDEVector x ..." << endl;
  PDEVector x;
  x = 0.0;

  PDEVector y;

  pass = true;
  //int m = 20;
  //int m = 30;
  //int m = 40;

#if 0
  double tol = 1.0e-6;
  kona::MatrixVectorProduct<PDEVector>* mat_vec =
      new kona::JacobianStateProduct();
  kona::Preconditioner<PDEVector>* precond =
      new kona::StatePreconditioner();
  int iters;
  double radius = 1.e+16; //00.0;
  double pred;
  bool active;
  kona::FITR_old<PDEVector>(m, tol, radius, b, x, *mat_vec, *precond, iters, pred,
                            active, cout, true);
#else
  double tol = 1.0e-6;
  double radius = 1.e+16;
  kona::MatrixVectorProduct<PDEVector>* mat_vec =
      new kona::JacobianStateProduct();
  kona::Preconditioner<PDEVector>* precond =
      new kona::StatePreconditioner();
  
  kona::FITRSolver<PDEVector> solver;
  solver.SubspaceSize(m);
  ptree ptin, ptout;
  ptin.put<double>("tol", tol);
  ptin.put<double>("radius", radius);
  ptin.put<bool>("check", true);
  solver.Solve(ptin, b, x, *mat_vec, *precond, ptout, cout);
  double pred = ptout.get<double>("pred");
  int iters = ptout.get<double>("iters");
#endif
  
  cout << "\ttesting FITRSolver::Solve() solution norm is less than radius...";
  pass = true;
  double nrm = 0.0;
  for (int i = 0; i < nVar; i++)
    nrm += state[1][i]*state[1][i];
  if (sqrt(nrm) - radius > sqrt(kona::kEpsilon)) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;
  //cout << "norm(x) = " << sqrt(nrm) << endl;

  cout << "\ttesting FITR::Solve() predicted reduction...";
  pass = true;
  double ared = 0.0;
  mat_vec->operator()(x, y);
  for (int i = 0; i < nVar; i++)
    ared += state[1][i]*state[0][i] - 0.5*state[1][i]*state[2][i];
  if (fabs(ared - pred) > 1.0) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  radius = 100.0;
  ptin.put<double>("radius", radius);
  solver.ReSolve(ptin, b, x, ptout, cout);
  pred = ptout.get<double>("pred");
  iters = ptout.get<double>("iters");
  
  cout << "\ttesting FITR::ReSolve() solution norm is equal to radius...";
  pass = true;
  nrm = 0.0;
  for (int i = 0; i < nVar; i++)
    nrm += state[1][i]*state[1][i];
  if (sqrt(nrm) - radius > sqrt(tol)*radius) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;
  //cout << "norm(x) - radius= " << sqrt(nrm) - radius << endl;

  cout << "\ttesting FITR::ReSolve() predicted reduction...";
  pass = true;
  ared = 0.0;
  mat_vec->operator()(x, y);
  for (int i = 0; i < nVar; i++)
    ared += state[1][i]*state[0][i] - 0.5*state[1][i]*state[2][i];
  if (fabs(ared - pred) > 1.0) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;
  
#if 0
  for (int i = 0; i < nVar; i++)
    state[0][i] = 0.0;
  state[0][0] = 1.0;
  x = 0.0;
  kona::FFOMSolver<PDEVector> ffom;
  ffom.SubspaceSize(m);
  ptin.clear();
  ptout.clear();
  ptin.put<double>("tol", tol);
  ptin.put<bool>("check", true);
  ffom.Solve(ptin, b, x, *mat_vec, *precond, ptout, cout);
  iters = ptout.get<double>("iters");
  cout << "\ttesting FFOM solution norm is less than radius...";
  pass = true;
  nrm = 0.0;
  for (int i = 0; i < nVar; i++)
    nrm += state[1][i]*state[1][i];
  if (sqrt(nrm) - radius > sqrt(kona::kEpsilon)) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;
#endif
  
  cout << "\tcompleted Krylov Trust-region unit test: "
       << "number of failures = " << numfail << endl;
  
  return numfail;
}

// ==============================================================================
int userFunc(int request, int leniwrk, int *iwrk, int lendwrk,
	     double *dwrk) {
  switch (request) {
    case kona::allocmem: {// allocate iwrk[0] design vectors and
      // iwrk[1] state vectors
      if (numDesign >= 0) { // free design memory first
        if (design == NULL) {
          cerr << "userFunc: "
               << "design array is NULL but numDesign > 0" << endl;
          throw(-1);
        }
        for (int i = 0; i < numDesign; i++)
          delete [] design[i];
        delete [] design;
      }        
      if (numState >= 0) { // free state memory first
        if (state == NULL) {
          cerr << "userFunc: "
               << "state array is NULL but numState > 0" << endl;
          throw(-1);
        }
        for (int i = 0; i < numState; i++)
          delete [] state[i];
        delete [] state;
      }
      numDesign = iwrk[0];
      numState = iwrk[1];
      assert(numDesign >= 0);
      assert(numState >= 0);
      design = new double * [numDesign];
      for (int i = 0; i < numDesign; i++)
        design[i] = new double[nDV];
      state = new double * [numState];
      for (int i = 0; i < numState; i++)
        state[i] = new double[nVar];
      break;
    }
    case kona::axpby_d: {// using design array set
      // iwrk[0] = dwrk[0]*iwrk[1] + dwrk[1]*iwrk[2]
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numDesign));
      assert(j < numDesign);
      assert(k < numDesign);
      
      double scalj = dwrk[0];
      double scalk = dwrk[1];
      if (j == -1) {
        if (k == -1) { // if both indices = -1, then all elements = scalj
          for (int n = 0; n < nDV; n++)
            design[i][n] = scalj;
          
        } else { // if just j = -1 ...
          if (scalk == 1.0) {
            // direct copy of vector k with no scaling
            for (int n = 0; n < nDV; n++)
              design[i][n] = design[k][n];
            
          } else {
            // scaled copy of vector k
            for (int n = 0; n < nDV; n++)
              design[i][n] = scalk*design[k][n];
          }
        }
      } else if (k == -1) { // if just k = -1 ...
        if (scalj == 1.0) {
          // direct copy of vector j with no scaling
          for (int n = 0; n < nDV; n++)
            design[i][n] = design[j][n];
          
        } else {
          // scaled copy of vector j
          for (int n = 0; n < nDV; n++)
            design[i][n] = scalj*design[j][n];
        }
      } else { // otherwise, full axpby
        for (int n = 0; n < nDV; n++)
          design[i][n] = scalj*design[j][n] + scalk*design[k][n];
      }
      break;
    }
    case kona::axpby_s: {// using state array set
      // iwrk[0] = dwrk[0]*iwrk[1] + dwrk[1]*iwrk[2]
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numState));
      assert(j < numState);
      assert(k < numState);

      double scalj = dwrk[0];
      double scalk = dwrk[1];
      if (j == -1) {
        if (k == -1) { // if both indices = -1, then all elements = scalj
          for (int n = 0; n < nVar; n++)
            state[i][n] = scalj;

        } else { // if just j = -1 ...
          if (scalk == 1.0) {
            // direct copy of vector k with no scaling
            for (int n = 0; n < nVar; n++)
              state[i][n] = state[k][n];

          } else {
            // scaled copy of vector k
            for (int n = 0; n < nVar; n++)
              state[i][n] = scalk*state[k][n];
          }
        }
      } else if (k == -1) { // if just k = -1 ...
        if (scalj == 1.0) {
          // direct copy of vector j with no scaling
          for (int n = 0; n < nVar; n++)
            state[i][n] = state[j][n];

        } else {
          // scaled copy of vector j
          for (int n = 0; n < nVar; n++)
            state[i][n] = scalj*state[j][n];
        }
      } else { // otherwise, full axpby
        for (int n = 0; n < nVar; n++)
          state[i][n] = scalj*state[j][n] + scalk*state[k][n];
      }
      break;
    }
    case kona::innerprod_d: {// using design array set
      // dwrk[0] = (iwrk[0])^{T} * iwrk[1]
      int i = iwrk[0];
      int j = iwrk[1];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numDesign));
      dwrk[0] = 0.0;
      for (int n = 0; n < nDV; n++)
        dwrk[0] += design[i][n]*design[j][n];
      break;
    } 
    case kona::innerprod_s: {// using state array set
      // dwrk[0] = (iwrk[0])^{T} * iwrk[1]
      int i = iwrk[0];
      int j = iwrk[1];
      assert((i >= 0) && (i < numState));
      assert((j >= 0) && (j < numState));
      dwrk[0] = 0.0;
      for (int n = 0; n < nVar; n++)
        dwrk[0] += state[i][n]*state[j][n];
      break;
    }
    case kona::jacvec_s: {// apply state component of the Jacobian-vector
      // product to vector iwrk[2]
      // recall; iwrk[0], iwrk[1] denote where Jacobian is evaluated
      // and in this case, they are not needed

      // The system matrix is defined by A = H^{T} Lambda H, where H is a
      // Hadamard matrix of order 2^log2Nvar, and Lambda is the diagonal matrix
      // of eigenvalues.  The Jacobian-vector product is computed by applying
      // the H matrices using the Sylvester algorithm
      int i = iwrk[2];
      int j = iwrk[3];
      assert((i >= 0) && (i < numState));
      assert((j >= 0) && (j < numState));
      double * tmpvec = new double[nVar];      
      applyHadamard(log2Nvar, state[i], tmpvec);
      applyDiagonal(log2Nvar, tmpvec);
      applyHadamard(log2Nvar, tmpvec, state[j]);
      delete [] tmpvec;
      break;
    }
    case kona::precond_s: {// apply primal preconditioner to iwrk[2]
      // recall; iwrk[0], iwrk[1] denote where preconditioner is
      // evaluated and in this case, they are not needed
      int i = iwrk[2];
      int j = iwrk[3];
      assert((i >= 0) && (i < numState));
      assert((j >= 0) && (j < numState));
      //cout << "\tUserFunc diag = " << dwrk[0] << endl;
      double * tmpvec = new double[nVar];      
      applyHadamard(log2Nvar, state[i], tmpvec);
      applyDiagonal(log2Nvar, tmpvec, true, dwrk[0]);
      applyHadamard(log2Nvar, tmpvec, state[j]);
      delete [] tmpvec;
#if 0
      // do-nothing preconditioner
      for (int n = 0; n < nVar; n++) {
        state[j][n] = state[i][n];
      }
#endif
      break;
    }
    default: {
      cerr << "userFunc: "
           << "unrecognized request value: request = "
           << request << endl;
      throw(-1);
      break;
    }
  }
  return 0;
}

void applyHadamard(const unsigned int & log2N, double * u, double * v) {
  if (log2N == 0) {
    v[0] = u[0];
    return;
  }  
  int Ndiv2 = 1 << (log2N-1);
  applyHadamard(log2N-1, u, v);
  applyHadamard(log2N-1, (u+Ndiv2), (v+Ndiv2));
  for (int i = 0; i < Ndiv2; i++) {
    double tmp = v[i];
    v[i] = tmp + v[Ndiv2+i];
    v[Ndiv2+i] = tmp - v[Ndiv2+i];
  }
}

void applyDiagonal(const unsigned int & log2N, double * u, const bool & invert,
                   const double & diag) {
  static boost::random::mt19937 gen;
  static boost::random::uniform_real_distribution<double> dist(0.05, 1.95);
  
  // we also take care of the normalization of the Hadamard matrix here
  int N = 1 << log2N;
  int Ndiv2 = 1 << (log2N-1);
  double fac = 1.0/static_cast<double>(N);
  if (invert) {
    fac = 1.0/fac;
    for (int i = 0; i < N; i++) {
      double randfac = dist(gen);
      // SPD matrix with condition number condnum
      double i2 = static_cast<double>(i*i);
      double N2 = static_cast<double>(N*N);
      u[i] *= randfac*fac*N2/(i2 + (N2 - i2)/condnum);
      
      //u[i] *= (fac*randfac/(diag + static_cast<double>((i+1)*(i+1)))); // SPD
      //u[i] *= (fac*randfac/(diag - static_cast<double>((i+1)*(i+1)))); // SND
      //u[i] *= (fac*randfac/(diag - 200.0 + static_cast<double>((i+1)*(i+1))));
      //u[i] *= fac*randfac/(diag + 0.01 + static_cast<double>(i+1-Ndiv2));
      
    }
  } else {
    for (int i = 0; i < N; i++) {
      // SPD matrix with condition number condnum
      double i2 = static_cast<double>(i*i);
      double N2 = static_cast<double>(N*N);
      u[i] *= fac*(i2 + (N2 - i2)/condnum)/N2;
      
      //u[i] *= (fac*(static_cast<double>((i+1)*(i+1)))); // SPD
      //u[i] *= -(fac*(static_cast<double>((i+1)*(i+1)))); // SND
      //u[i] *= (fac*(-200.0 + static_cast<double>((i+1)*(i+1))));
      //u[i] *= fac*(0.01 + static_cast<double>(i+1-Ndiv2));
    }
  }
}

void writeOutcome(const bool & pass) {
  if (pass) {
    cout << "pass" << endl;
  } else {
    cout << "fail" << endl;
  }
}

