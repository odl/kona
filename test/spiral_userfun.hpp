/**
 * \file spiral_usrfun.hpp
 * \brief header with spiral user function used for testing
 * \author  Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

static int numDesign = -1; // local copy of the number of design vectors
static int numState = -1; // local copy of the number of state vectors
static double **design;   // global memory to act like the design vectors
static double **state;    // global memory to act like the state vectors
static double **mat;      // storage for the matrix
static int func_eval = 0;
static int grad_eval = 0;

// local user function
int userFunc(int request, int leniwrk, int *iwrk, int lendwrk,
	     double *dwrk) {
  switch (request) {
    case kona::allocmem: {// allocate iwrk[0] design vectors and
      // iwrk[1] state vectors
      if (numDesign >= 0) { // free design memory first
        if (design == NULL) {
          cerr << "userFunc: "
               << "design array is NULL but numDesign > 0" << endl;
          throw(-1);
        }
        for (int i = 0; i < numDesign; i++)
          delete [] design[i];
        delete [] design;
      }        
      if (numState >= 0) { // free state memory first
        if (state == NULL) {
          cerr << "userFunc: "
               << "state array is NULL but numState > 0" << endl;
          throw(-1);
        }
        for (int i = 0; i < numState; i++)
          delete [] state[i];
        delete [] state;
      }
      numDesign = iwrk[0];
      numState = iwrk[1];
      assert(numDesign >= 0);
      assert(numState >= 0);
      design = new double * [numDesign];
      for (int i = 0; i < numDesign; i++)
        design[i] = new double[nDV];
      state = new double * [numState];
      for (int i = 0; i < numState; i++)
        state[i] = new double[nVar];
      break;
    }
    case kona::axpby_d: {// using design array set
      // iwrk[0] = dwrk[0]*iwrk[1] + dwrk[1]*iwrk[2]
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numDesign));
      assert(j < numDesign);
      assert(k < numDesign);
      
      double scalj = dwrk[0];
      double scalk = dwrk[1];
      if (j == -1) {
        if (k == -1) { // if both indices = -1, then all elements = scalj
          for (int n = 0; n < nDV; n++)
            design[i][n] = scalj;
          
        } else { // if just j = -1 ...
          if (scalk == 1.0) {
            // direct copy of vector k with no scaling
            for (int n = 0; n < nDV; n++)
              design[i][n] = design[k][n];
            
          } else {
            // scaled copy of vector k
            for (int n = 0; n < nDV; n++)
              design[i][n] = scalk*design[k][n];
          }
        }
      } else if (k == -1) { // if just k = -1 ...
        if (scalj == 1.0) {
          // direct copy of vector j with no scaling
          for (int n = 0; n < nDV; n++)
            design[i][n] = design[j][n];
          
        } else {
          // scaled copy of vector j
          for (int n = 0; n < nDV; n++)
            design[i][n] = scalj*design[j][n];
        }
      } else { // otherwise, full axpby
        for (int n = 0; n < nDV; n++)
          design[i][n] = scalj*design[j][n] + scalk*design[k][n];
      }
      break;
    }
    case kona::axpby_s: {// using state array set
      // iwrk[0] = dwrk[0]*iwrk[1] + dwrk[1]*iwrk[2]
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numState));
      assert(j < numState);
      assert(k < numState);

      double scalj = dwrk[0];
      double scalk = dwrk[1];
      if (j == -1) {
        if (k == -1) { // if both indices = -1, then all elements = scalj
          for (int n = 0; n < nVar; n++)
            state[i][n] = scalj;

        } else { // if just j = -1 ...
          if (scalk == 1.0) {
            // direct copy of vector k with no scaling
            for (int n = 0; n < nVar; n++)
              state[i][n] = state[k][n];

          } else {
            // scaled copy of vector k
            for (int n = 0; n < nVar; n++)
              state[i][n] = scalk*state[k][n];
          }
        }
      } else if (k == -1) { // if just k = -1 ...
        if (scalj == 1.0) {
          // direct copy of vector j with no scaling
          for (int n = 0; n < nVar; n++)
            state[i][n] = state[j][n];

        } else {
          // scaled copy of vector j
          for (int n = 0; n < nVar; n++)
            state[i][n] = scalj*state[j][n];
        }
      } else { // otherwise, full axpby
        for (int n = 0; n < nVar; n++)
          state[i][n] = scalj*state[j][n] + scalk*state[k][n];
      }
      break;
    }
    case kona::innerprod_d: {// using design array set
      // dwrk[0] = (iwrk[0])^{T} * iwrk[1]
      int i = iwrk[0];
      int j = iwrk[1];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numDesign));
      dwrk[0] = 0.0;
      for (int n = 0; n < nDV; n++)
        dwrk[0] += design[i][n]*design[j][n];
      break;
    } 
    case kona::innerprod_s: {// using state array set
      // dwrk[0] = (iwrk[0])^{T} * iwrk[1]
      int i = iwrk[0];
      int j = iwrk[1];
      assert((i >= 0) && (i < numState));
      assert((j >= 0) && (j < numState));
      dwrk[0] = 0.0;
      for (int n = 0; n < nVar; n++)
        dwrk[0] += state[i][n]*state[j][n];
      break;
    }
    case kona::eval_obj: {// evaluate the objective function
      int i = iwrk[0];
      int j = iwrk[1];
      assert((i >= 0) && (i < numDesign));
      assert((j >= -1) && (j < numState));
      double x1;
      double x2;
      double x3 = design[i][0];
      double theta = 0.5*(x3 + pi);
      double alpha = 0.5*(x3 - pi);
      double cos_theta = cos(theta);
      double sin_theta = sin(theta);
      double cos_alpha = cos(alpha);
      double sin_alpha = sin(alpha);
      if (j == -1) { // need to evaluate PDE first
        x1 = x3*x3*(cos_theta*cos_alpha - sin_theta*sin_alpha);
        x2 = x3*x3*(sin_theta*cos_alpha + cos_theta*sin_alpha);
      }else {
        x1 = state[j][0];
        x2 = state[j][1];
      }
      // NOTE: it is easy to show that x1 = x3*x3*cos(x3) and that
      // x2 = x3*x3*sin(x3); therefore, the reduced objective is
      // J = 0.5*(x3^4 + x3^2)
      dwrk[0] = 0.5*(x1*x1 + x2*x2 + x3*x3);
      func_eval++;
      break;
    }
    case kona::eval_pde: {// evaluate PDE at (design,state) =
                         // (iwrk[0],iwrk[1])
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numState));
      double x1 = state[j][0];
      double x2 = state[j][1];
      double x3 = design[i][0];
      double theta = 0.5*(x3 + pi);
      double alpha = 0.5*(x3 - pi);
      double cos_theta = cos(theta);
      double sin_theta = sin(theta);
      double cos_alpha = cos(alpha);
      double sin_alpha = sin(alpha);
#if 0
      state[k][0] =  cos_theta*x1 + sin_theta*x2 - x3*cos_alpha;
      state[k][1] = -sin_theta*x1 + cos_theta*x2 - x3*sin_alpha;
#endif
      state[k][0] =  cos_theta*x1 + sin_theta*x2 - x3*x3*cos_alpha;
      state[k][1] = -sin_theta*x1 + cos_theta*x2 - x3*x3*sin_alpha;
      break;
    }
    case kona::jacvec_d: {// apply design component of the Jacobian-vec
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numDesign));
      assert((m >= 0) && (m < numState));
      double x1 = state[j][0];
      double x2 = state[j][1];
      double x3 = design[i][0];
      double theta = 0.5*(x3 + pi);
      double alpha = 0.5*(x3 - pi);
      double cos_theta = cos(theta);
      double sin_theta = sin(theta);
      double cos_alpha = cos(alpha);
      double sin_alpha = sin(alpha);
      double u3 = design[k][0];
#if 0
      state[m][0] = -sin_theta*x1 + cos_theta*x2 + x3*sin_alpha 
          - 2.0*cos_alpha;
      state[m][1] = -cos_theta*x1 - sin_theta*x2 - x3*cos_alpha
          - 2.0*sin_alpha;
#endif
      state[m][0] = -sin_theta*x1 + cos_theta*x2 + x3*x3*sin_alpha 
          - 4.0*x3*cos_alpha;
      state[m][1] = -cos_theta*x1 - sin_theta*x2 - x3*x3*cos_alpha
          - 4.0*x3*sin_alpha;
      state[m][0] *= 0.5*u3;
      state[m][1] *= 0.5*u3;
      break;
    }
    case kona::jacvec_s: {// apply state component of the Jacobian-vector
      // product to vector iwrk[2]
      // recall; iwrk[0], iwrk[1] denote where Jacobian is evaluated
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numState));
      assert((m >= 0) && (m < numState));
      double x1 = state[j][0];
      double x2 = state[j][1];
      double x3 = design[i][0];
      double theta = 0.5*(x3 + pi);
      double alpha = 0.5*(x3 - pi);
      double cos_theta = cos(theta);
      double sin_theta = sin(theta);
      double cos_alpha = cos(alpha);
      double sin_alpha = sin(alpha);
      double u1 = state[k][0];
      double u2 = state[k][1];
      state[m][0] =  cos_theta*u1 + sin_theta*u2;
      state[m][1] = -sin_theta*u1 + cos_theta*u2;
      break;
    }
    case kona::tjacvec_d: {// apply design component of Jacobian to adj
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numState));
      assert((m >= 0) && (m < numDesign));
      double x1 = state[j][0];
      double x2 = state[j][1];
      double x3 = design[i][0];
      double theta = 0.5*(x3 + pi);
      double alpha = 0.5*(x3 - pi);
      double cos_theta = cos(theta);
      double sin_theta = sin(theta);
      double cos_alpha = cos(alpha);
      double sin_alpha = sin(alpha);
      double adj1 = state[k][0];
      double adj2 = state[k][1];
#if 0
      design[m][0] = (-sin_theta*x1 + cos_theta*x2 + x3*sin_alpha 
                      - 2.0*cos_alpha)*adj1;
      design[m][0] += (-cos_theta*x1 - sin_theta*x2 - x3*cos_alpha
                       - 2.0*sin_alpha)*adj2;
#endif
      design[m][0] = (-sin_theta*x1 + cos_theta*x2 + x3*x3*sin_alpha 
                      - 4.0*x3*cos_alpha)*adj1;
      design[m][0] += (-cos_theta*x1 - sin_theta*x2 - x3*x3*cos_alpha
                       - 4.0*x3*sin_alpha)*adj2;
      design[m][0] *= 0.5;
      break;
    }
    case kona::tjacvec_s: {// apply state component of Jacobian to adj
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numState));
      assert((m >= 0) && (m < numState));
      double x1 = state[j][0];
      double x2 = state[j][1];
      double x3 = design[i][0];
      double theta = 0.5*(x3 + pi);
      double alpha = 0.5*(x3 - pi);
      double cos_theta = cos(theta);
      double sin_theta = sin(theta);
      double cos_alpha = cos(alpha);
      double sin_alpha = sin(alpha);
      double u1 = state[k][0];
      double u2 = state[k][1];
      state[m][0] = cos_theta*u1 - sin_theta*u2;
      state[m][1] = sin_theta*u1 + cos_theta*u2;
      break;
    }
    case kona::eval_precond: {// build the preconditioner if necessary
      break;
    } 
    case kona::precond_s: {// apply primal preconditioner to iwrk[2]
      // recall; iwrk[0], iwrk[1] denote where preconditioner is
      // evaluated and in this case, they are not needed
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numState));
      assert((m >= 0) && (m < numState));
#if 1
      // do-nothing preconditioner
      for (int n = 0; n < nVar; n++) {
        state[m][n] = state[k][n];
      }
#else
      double x3 = design[i][0];
      double theta = 0.5*(x3 + pi);
      double cos_theta = cos(theta);
      double sin_theta = sin(theta);
      state[m][0] = cos_theta*state[k][0] - sin_theta*state[k][1];
      state[m][1] = sin_theta*state[k][0] + cos_theta*state[k][1];
#endif
      break;
    }
    case kona::tprecond_s: {// apply adjoint preconditioner to iwrk[2]
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numState));
      assert((m >= 0) && (m < numState));
#if 1
      // do-nothing preconditioner
      for (int n = 0; n < nVar; n++) {
        state[m][n] = state[k][n];
      }
#else
      double x3 = design[i][0];
      double theta = 0.5*(x3 + pi);
      double cos_theta = cos(theta);
      double sin_theta = sin(theta);
      state[m][0] = cos_theta*state[k][0] + sin_theta*state[k][1];
      state[m][1] = -sin_theta*state[k][0] + cos_theta*state[k][1];
#endif
      break;
    }
    case kona::grad_d: {// design component of objective gradient
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numDesign));
      double x1 = state[j][0];
      double x2 = state[j][1];
      double x3 = design[i][0];
      design[k][0] = x3;
      grad_eval++;
      break;
    }
    case kona::grad_s: {// state component of objective gradient
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numState));
      double x1 = state[j][0];
      double x2 = state[j][1];
      double x3 = design[i][0];
      state[k][0] = x1;
      state[k][1] = x2;
      break;
    }
    case kona::initdesign: {// initialize the design variables
      int i = iwrk[0];
      assert((i >= 0) && (i < numDesign));
      design[i][0] = 8.0*pi; //8.0*pi;
      break;
    }
    case kona::solve: {// solve the primal equations
      int i = iwrk[0];
      int j = iwrk[1];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      double x3 = design[i][0];
      double theta = 0.5*(x3 + pi);
      double alpha = 0.5*(x3 - pi);
      double cos_theta = cos(theta);
      double sin_theta = sin(theta);
      double cos_alpha = cos(alpha);
      double sin_alpha = sin(alpha);
#if 0
      state[j][0] = x3*(cos_theta*cos_alpha - sin_theta*sin_alpha);
      state[j][1] = x3*(sin_theta*cos_alpha + cos_theta*sin_alpha);
#endif
      state[j][0] = x3*x3*(cos_theta*cos_alpha - sin_theta*sin_alpha);
      state[j][1] = x3*x3*(sin_theta*cos_alpha + cos_theta*sin_alpha);
      break;
    }
    case kona::linsolve: {// solve the linearized primal problem
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numState));
      assert((m >= 0) && (m < numState));      
      double x1 = state[j][0];
      double x2 = state[j][1];
      double x3 = design[i][0];
      double theta = 0.5*(x3 + pi);
      double alpha = 0.5*(x3 - pi);
      double cos_theta = cos(theta);
      double sin_theta = sin(theta);
      double cos_alpha = cos(alpha);
      double sin_alpha = sin(alpha);
      double u1 = state[k][0];
      double u2 = state[k][1];
      state[m][0] = cos_theta*u1 - sin_theta*u2;
      state[m][1] = sin_theta*u1 + cos_theta*u2;
      break;
    } 
    case kona::adjsolve: {// solve the dual equations
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((m >= 0) && (m < numState));
      double x1 = state[j][0];
      double x2 = state[j][1];
      double x3 = design[i][0];
      double theta = 0.5*(x3 + pi);
      double alpha = 0.5*(x3 - pi);
      double cos_theta = cos(theta);
      double sin_theta = sin(theta);
      double cos_alpha = cos(alpha);
      double sin_alpha = sin(alpha);
      if (k == -1) {
        state[m][0] = -cos_theta*x1 - sin_theta*x2;
        state[m][1] =  sin_theta*x1 - cos_theta*x2;
      } else {
        assert((k >= 0) && (k < numState));
        x1 = state[k][0];
        x2 = state[k][1];
        state[m][0] =  cos_theta*x1 + sin_theta*x2;
        state[m][1] = -sin_theta*x1 + cos_theta*x2;
        iwrk[1] = -10;// temporary
      }
      break;
    }
    case kona::info: {// supplies information to user
      // current design is in iwrk[0]
      // current pde solution is in iwrk[1]
      // current adjoint solution is in iwrk[2]
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      cout << "(design), (state), (adjoint) = (" 
           << design[i][0] << "), (" 
           << state[j][0] << "," << state[j][1] << "), ("
           << state[k][0] << "," << state[k][1] << ")" << endl;
      //break;

      // The following code is used to compute the norm of the sensitivity
      // matrix, which can then be used to verify SensitivityProduct
      double y = state[j][0];
      double z = state[j][1];
      double psi1 = state[k][0];
      double psi2 = state[k][1];
      double x = design[i][0];
      double theta = 0.5*(x + pi);
      double alpha = 0.5*(x - pi);
      double cos_theta = cos(theta);
      double sin_theta = sin(theta);
      double cos_alpha = cos(alpha);
      double sin_alpha = sin(alpha);
      
      double dydx = 2.0*x*cos(x) - x*x*sin(x);
      double dzdx = 2.0*x*sin(x) + x*x*cos(x);
      double dpsi1, dpsi2, tmp;
      dpsi1 = sin_theta*psi1 + cos_theta*psi2;
      dpsi2 = -cos_theta*psi1 + sin_theta*psi2;
      dpsi1 = 0.5*dpsi1 - dydx;
      dpsi2 = 0.5*dpsi2 - dzdx;
      tmp = dpsi1;
      dpsi1 = cos_theta*dpsi1 + sin_theta*dpsi2;
      dpsi2 = -sin_theta*tmp  + cos_theta*dpsi2;

      double sens_norm = sqrt(dydx*dydx + dzdx*dzdx + dpsi1*dpsi1 + dpsi2*dpsi2);
      cout << "Analytical sensitivity norm: " << sens_norm << endl;
      
      break;
    }
    default: {
      cerr << "userFunc: "
           << "unrecognized request value: request = "
           << request << endl;
      throw(-1);
      break;
    }
  }
  return 0;
}

void writeOutcome(const bool & pass) {
  if (pass) {
    cout << "pass" << endl;
  } else {
    cout << "fail" << endl;
  }
}
