/**
 * \file test_reduced_hessian.cpp
 * \brief test of the reduced-Hessian vector product
 * \author  Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#include <assert.h>
#include "../src/user_memory.hpp"
#include "../src/kona.hpp"
#include <boost/math/constants/constants.hpp>
#include "../src/vectors.hpp"
#include "../src/vector_operators.hpp"

using std::string;
using std::map;

const double pi = boost::math::constants::pi<double>();
const int nDV = 1; // size of the design vector
const int nVar = 2;  // size of state vector
#include "./spiral_userfun.hpp"

using namespace kona;

int main(int argc, char *argv[]) {

  // allocate memory
  int krylov_size = 1;
  int nd = 4 + 2;
  int ns = 8 + 2*(krylov_size + 1);
  UserMemory::Allocate(userFunc, nd, ns);

  DesignVector init_design, init_grad, design_work1, design_work2;
  init_design.EqualsInitialDesign();
  PDEVector init_state, init_adjoint, adjoint_res;
  init_state.EqualsPrimalSolution(init_design);
  init_adjoint.EqualsAdjointSolution(init_design, init_state);
  
  init_grad.EqualsObjectiveGradient(init_design, init_state);
  design_work1.EqualsTransJacobianTimesVector(init_design, init_state,
                                              init_adjoint);
  init_grad += design_work1;
  adjoint_res = 0.0;

  PDEVector pde_work1, pde_work2, pde_work3, pde_work4, pde_work5; 
  double krylov_tol = 0.001;
  //ReducedHessianProduct matvec(

  MatrixVectorProduct<DesignVector>* matvec = new ReducedHessianProduct(
      init_design, init_state, init_adjoint, init_grad,
      adjoint_res, design_work1, design_work2, pde_work1, pde_work2, pde_work3,
      pde_work4, pde_work5, krylov_size, krylov_tol, cout);

  DesignVector u, v;
  u = 1.0;
  matvec->operator()(u, v);
  int iter = -1;
  CurrentSolution(iter, v, init_state, init_adjoint);
  // NOTE: it is easy to show that x1 = x3*x3*cos(x3) and that
  // x2 = x3*x3*sin(x3); therefore, the reduced objective is
  // J = 0.5*(x3^4 + x3^2) and the Hessian-vector product will be
  // H*u = (2.0*x3*x3 + 1)*1 = 3790.9280900183135
}
