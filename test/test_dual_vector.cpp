/**
 * \file test_dual_vector.cpp
 * \brief unit test for the DualVector class
 * \author  Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#include <assert.h>
#include <limits>
#include "../src/vectors.hpp"

const int nDV = 0; // size of the design vector
const int nVar = 0; // size of state vector
const int nCeq = 2; // size of dual vector
#include "./usrfun.hpp"

using std::numeric_limits;
using kona::DualVector;

int main(int argc, char *argv[]) {
  // allocate memory
  int nd = 0;
  int ns = 0;
  int nc = 3;
  kona::UserMemory::Allocate(userFunc, nd, ns, nc);

  // define some dual vectors
  DualVector u, v;
  DualVector *w = new DualVector;
  
  const double initval[3] = {1.0, 2.0, -3.0};
  bool pass;
  int numfail = 0;

  u = initval[0];
  v = initval[1];
  *w = initval[2];

  // tell cout to display bools as words
  cout << std::boolalpha;

  cout << "\ttesting assignment operator (DualVector=double)...";
  pass = true;
  for (int i = 0; i < ns; i++)
    for (int j = 0; j < nVar; j++)
      if (dual[i][j] != initval[i]) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting assignment operator (DualVector=DualVector)...";
  v = u;
  pass = true;
  for (int j = 0; j < nVar; j++)
    if (dual[0][j] != dual[1][j]) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting assignment operator (DualVector=*DualVector)...";
  u = *w;
  pass = true;
  for (int j = 0; j < nVar; j++)
    if (dual[0][j] != dual[2][j]) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting assignment operator (*DualVector=DualVector)...";
  *w = v;
  pass = true;
  for (int j = 0; j < nVar; j++)
    if (dual[1][j] != dual[2][j]) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting compound addition operator (DualVector+=DualVector)...";
  u += v;
  pass = true;
  for (int j = 0; j < nVar; j++) {
    if (dual[0][j] != -2) pass = false;
    if (dual[1][j] !=  1) pass = false;
  }
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting compound addition operator (*DualVector+=DualVector)...";
  *w += u;
  pass = true;
  for (int j = 0; j < nVar; j++) {
    if (dual[0][j] != -2) pass = false;
    if (dual[2][j] != -1) pass = false;
  }
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting compound subtraction operator (DualVector-=DualVector)...";
  v -= u;
  pass = true;
  for (int j = 0; j < nVar; j++) {
    if (dual[0][j] != -2) pass = false;
    if (dual[1][j] !=  3) pass = false;
  }
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting compound subtraction operator "
       << "(DualVector-=*DualVector)...";
  u -= *w;
  pass = true;
  for (int j = 0; j < nVar; j++) {
    if (dual[0][j] != -1) pass = false;
    if (dual[2][j] != -1) pass = false;
  }
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting compound multiplication (DualVector*=double)...";
  u *= 0.5;
  pass = true;
  for (int j = 0; j < nVar; j++) {
    if (dual[0][j] != -0.5) pass = false;
  }
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting compound multiplication (*DualVector*=double)...";
  *w *= 2.0;
  pass = true;
  for (int j = 0; j < nVar; j++) {
    if (dual[2][j] != -2.0) pass = false;
  }
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting compound division (DualVector /=double)...";
  u /= -5.0;
  pass = true;
  for (int j = 0; j < nVar; j++) {
    if (dual[0][j] != 0.1) pass = false;
  }
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting general linear combination (EqualsAXPlusBY)...";
  u.EqualsAXPlusBY(1.0/3.0, v, 1.0/2.0, *w);
  pass = true;
  double eps = numeric_limits<double>::epsilon();
  for (int j = 0; j < nVar; j++) {
    if (fabs(dual[0][j]) > eps) pass = false;
  }
  writeOutcome(pass);
  if (!pass) numfail++;

  // explict change of dual
  dual[0][0] = 0.5;
  dual[0][1] = -0.5;

  cout << "\ttesting inner product "
       << "(InnerProd(DualVector,*DualVector))...";
  double prod = InnerProd(u, *w);
  pass = true;
  if (prod != 0.0) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting Norm2 (DualVector.Norm2)...";
  double norm = u.Norm2();
  pass = true;
  if (norm != sqrt(0.5)) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

#if 0
  // uncomment to display values in dual array
  for (int i = 0; i < ns; i++) {
    cout << "\tdual[" << i << "] = ";
    for (int j = 0; j < nVar; j++)
      cout << dual[i][j] << " ";
    cout << endl;
  }
#endif

  cout << "\tcompleted DualVector unit test: "
       << "number of failures = " << numfail << endl;

  delete w;
  return numfail;
}
