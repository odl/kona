/**
 * \file test_kona_constrained.cpp
 * \brief simple test of the kona library
 * \author  Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#include <assert.h>
#include "../src/user_memory.hpp"
#include "../src/kona.hpp"

using std::string;
using std::map;
using std::cout;
using std::endl;
using std::cerr;

static int obj_eval = 0;
static int grad_eval = 0;
static int ceq_eval = 0;
static int jac_eval = 0;

// local user function
int userFunc(int request, int leniwrk, int *iwrk, int lendwrk,
	     double *dwrk);

int main(int argc, char *argv[]) {

  map<string,string> optns; // <-- empty optns
  optns["opt_method"] = "cnstr_reduced";
  //optns["opt_method"] = "aug_lag";
  //optns["opt_method"] = "verify";
  //optns["aug_lag.mu_init"] = "2.0";
  optns["aug_lag.mu_init"] = "10.0"; //1.00";
  optns["aug_lag.mu_pow"] = "0.500";

  optns["trust.init_radius"] = "0.5"; //"0.5"; // "1.0";
  optns["trust.max_radius"] = "1.0"; // "2.0";
#if 0
  optns["opt_method"] = "verify";
  optns["verify.dual_vec"] = "true";
  optns["verify.cnstr_jac"] = "true";
  optns["verify.red_grad"] = "true";
  optns["verify.hessian_prod"] = "false";
#endif
  optns["max_iter"] = "100";
  optns["inner.max_iter"] = "100";
  optns["inner.lambda_init"] = "0.99";
  optns["des_tol"] = "1.e-6";
  optns["ceq_tol"] = "1.e-6";
  //optns["krylov.solver"] = "ffom_smart";
  optns["krylov.solver"] = "fitr";
  //optns["krylov.solver"] = "fisqp";
  optns["krylov.space_size"] = "3";
  optns["krylov.check"] = "true";
  optns["krylov.tolerance"] = "0.01"; //"0.5"; //"0.01";
  //optns["krylov.dual_tol"] = "0.1";
  optns["reduced.precond"] = "none"; //"quasi_newton";

  //optns["quasi_newton.type"] = "lbfgs";
  //optns["quasi_newton.max_stored"] = "3";
  //optns["quasi_newton.matvec_update"] = "true";
  
  KonaOptimize(userFunc, optns);
  cout << "\tnumber of function evaluations: " << obj_eval << endl;
  cout << "\tnumber of gradient evaluations: " << grad_eval << endl;
  cout << "\tnumber of eq.cnstr evaluations: " << ceq_eval << endl;
  cout << "\tnumber of Jacobian evaluations: " << jac_eval << endl;

}

// local user function
int userFunc(int request, int leniwrk, int *iwrk, int lendwrk,
	     double *dwrk) {
  const int nDV = 3; // size of the design vector
  const int nVar = 0;  // size of state vector
  const int nCeq = 1; // size of the dual vector
  static int numDesign = -1; // local copy of the number of design vectors
  static int numState = -1; // local copy of the number of state vectors
  static int numDual = -1; // local copy of the number of dual vectors
  static double **design;     // global memory to act like the design vectors
  static double **state;      // global memory to act like the state vectors
  static double **dual;       // global memory to act like the dual vectors
  
  switch (request) {
    case kona::allocmem: {// allocate iwrk[0] design vectors and
      // iwrk[1] state vectors
      if (numDesign >= 0) { // free design memory first
        if (design == NULL) {
          cerr << "userFunc: "
               << "design array is NULL but numDesign > 0" << endl;
          throw(-1);
        }
        for (int i = 0; i < numDesign; i++)
          delete [] design[i];
        delete [] design;
      }
      if (numState >= 0) { // free state memory first
        if (state == NULL) {
          cerr << "userFunc: "
               << "state array is NULL but numState > 0" << endl;
          throw(-1);
        }
        for (int i = 0; i < numState; i++)
          delete [] state[i];
        delete [] state;
      }
      if (numDual >= 0) { // free dual memory first
        if (dual == NULL) {
          cerr << "userFunc: "
               << "dual array is NULL but numDual > 0" << endl;
          throw(-1);
        }
        for (int i = 0; i < numDual; i++)
          delete [] dual[i];
        delete [] dual;
      }      
      numDesign = iwrk[0];
      numState = iwrk[1];
      numDual = iwrk[2];
      assert(numDesign >= 0);
      assert(numState >= 0);
      assert(numDual >= 0);
      design = new double * [numDesign];
      for (int i = 0; i < numDesign; i++)
        design[i] = new double[nDV];
      state = new double * [numState];
      for (int i = 0; i < numState; i++)
        state[i] = new double[nVar];
      dual = new double * [numDual];
      for (int i = 0; i < numDual; i++)
        dual[i] = new double[nCeq];      
      break;
    }
    case kona::axpby_d: {// using design array set
      // iwrk[0] = dwrk[0]*iwrk[1] + dwrk[1]*iwrk[2]
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numDesign));
      assert(j < numDesign);
      assert(k < numDesign);
      
      double scalj = dwrk[0];
      double scalk = dwrk[1];
      if (j == -1) {
        if (k == -1) { // if both indices = -1, then all elements = scalj
          for (int n = 0; n < nDV; n++)
            design[i][n] = scalj;
          
        } else { // if just j = -1 ...
          if (scalk == 1.0) {
            // direct copy of vector k with no scaling
            for (int n = 0; n < nDV; n++)
              design[i][n] = design[k][n];
            
          } else {
            // scaled copy of vector k
            for (int n = 0; n < nDV; n++)
              design[i][n] = scalk*design[k][n];
          }
        }
      } else if (k == -1) { // if just k = -1 ...
        if (scalj == 1.0) {
          // direct copy of vector j with no scaling
          for (int n = 0; n < nDV; n++)
            design[i][n] = design[j][n];
          
        } else {
          // scaled copy of vector j
          for (int n = 0; n < nDV; n++)
            design[i][n] = scalj*design[j][n];
        }
      } else { // otherwise, full axpby
        for (int n = 0; n < nDV; n++)
          design[i][n] = scalj*design[j][n] + scalk*design[k][n];
      }
      break;
    }
    case kona::axpby_s: {// using state array set
      // iwrk[0] = dwrk[0]*iwrk[1] + dwrk[1]*iwrk[2]
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numState));
      assert(j < numState);
      assert(k < numState);

      double scalj = dwrk[0];
      double scalk = dwrk[1];
      if (j == -1) {
        if (k == -1) { // if both indices = -1, then all elements = scalj
          for (int n = 0; n < nVar; n++)
            state[i][n] = scalj;

        } else { // if just j = -1 ...
          if (scalk == 1.0) {
            // direct copy of vector k with no scaling
            for (int n = 0; n < nVar; n++)
              state[i][n] = state[k][n];

          } else {
            // scaled copy of vector k
            for (int n = 0; n < nVar; n++)
              state[i][n] = scalk*state[k][n];
          }
        }
      } else if (k == -1) { // if just k = -1 ...
        if (scalj == 1.0) {
          // direct copy of vector j with no scaling
          for (int n = 0; n < nVar; n++)
            state[i][n] = state[j][n];

        } else {
          // scaled copy of vector j
          for (int n = 0; n < nVar; n++)
            state[i][n] = scalj*state[j][n];
        }
      } else { // otherwise, full axpby
        for (int n = 0; n < nVar; n++)
          state[i][n] = scalj*state[j][n] + scalk*state[k][n];
      }
      break;
    }
    case kona::axpby_c: {// using dual array set
      // iwrk[0] = dwrk[0]*iwrk[1] + dwrk[1]*iwrk[2]
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numDual));
      assert(j < numDual);
      assert(k < numDual);

      double scalj = dwrk[0];
      double scalk = dwrk[1];
      if (j == -1) {
        if (k == -1) { // if both indices = -1, then all elements = scalj
          for (int n = 0; n < nCeq; n++)
            dual[i][n] = scalj;

        } else { // if just j = -1 ...
          if (scalk == 1.0) {
            // direct copy of vector k with no scaling
            for (int n = 0; n < nCeq; n++)
              dual[i][n] = dual[k][n];

          } else {
            // scaled copy of vector k
            for (int n = 0; n < nCeq; n++)
              dual[i][n] = scalk*dual[k][n];
          }
        }
      } else if (k == -1) { // if just k = -1 ...
        if (scalj == 1.0) {
          // direct copy of vector j with no scaling
          for (int n = 0; n < nCeq; n++)
            dual[i][n] = dual[j][n];

        } else {
          // scaled copy of vector j
          for (int n = 0; n < nCeq; n++)
            dual[i][n] = scalj*dual[j][n];
        }
      } else { // otherwise, full axpby
        for (int n = 0; n < nCeq; n++)
          dual[i][n] = scalj*dual[j][n] + scalk*dual[k][n];
      }
      break;
    }
    case kona::innerprod_d: {// using design array set
      // dwrk[0] = (iwrk[0])^{T} * iwrk[1]
      int i = iwrk[0];
      int j = iwrk[1];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numDesign));
      dwrk[0] = 0.0;
      for (int n = 0; n < nDV; n++)
        dwrk[0] += design[i][n]*design[j][n];
      break;
    } 
    case kona::innerprod_s: {// using state array set
      // dwrk[0] = (iwrk[0])^{T} * iwrk[1]
      int i = iwrk[0];
      int j = iwrk[1];
      assert((i >= 0) && (i < numState));
      assert((j >= 0) && (j < numState));
      dwrk[0] = 0.0;
      for (int n = 0; n < nVar; n++)
        dwrk[0] += state[i][n]*state[j][n];
      break;
    }
    case kona::innerprod_c: {// using dual array set
      // dwrk[0] = (iwrk[0])^{T} * iwrk[1]
      int i = iwrk[0];
      int j = iwrk[1];
      assert((i >= 0) && (i < numDual));
      assert((j >= 0) && (j < numDual));
      dwrk[0] = 0.0;
      for (int n = 0; n < nCeq; n++)
        dwrk[0] += dual[i][n]*dual[j][n];
      break;
    }      
    case kona::eval_obj: {// evaluate objective value
      int i = iwrk[0];
      int j = iwrk[1];
      assert((i >= 0) && (i < numDesign));
      assert((j >= -1) && (j < numState));
      double x = design[i][0];
      double y = design[i][1];
      double z = design[i][2];
      dwrk[0] = x + y + z;
      obj_eval++;
      break;
    }
    case kona::eval_pde: {// evaluate PDE at (design,state) =
                         // (iwrk[0],iwrk[1])
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numState));
      for (int n = 0; n < nVar; n++) {
        state[k][n] = 0.0;
      }
      break;
    }
    case kona::eval_ceq: {// evaluate the equality constraints
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numDual));
      double x = design[i][0];
      double y = design[i][1];
      double z = design[i][2];
      dual[k][0] = x*x + y*y + z*z - 3.0;
      ceq_eval++;
      break;
    }      
    case kona::jacvec_d: {// apply design component of the Jacobian-vec
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numDesign));
      assert((m >= 0) && (m < numState));
      for (int n = 0; n < nVar; n++) {
        state[m][n] = 0.0;
      }
      break;
    }
    case kona::jacvec_s: {// apply state component of the Jacobian-vector
      // product to vector iwrk[2] ... do nothing since no Jacobian
      // recall; iwrk[0], iwrk[1] denote where Jacobian is evaluated
      // and in this case, they are not needed
      int i = iwrk[2];
      int j = iwrk[3];
      assert((i >= 0) && (i < numState));
      assert((j >= 0) && (j < numState));
      for (int n = 0; n < nVar; n++) {
        state[j][n] = 0.0;
      }
      break;
    }
    case kona::tjacvec_d: {// apply design component of Jacobian to adj
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numState));
      assert((m >= 0) && (m < numDesign));
      for (int n = 0; n < nDV; n++) {
        design[m][n] = 0.0;
      }
      break;
    }
    case kona::tjacvec_s: {// apply state component of Jacobian to adj
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numState));
      assert((m >= 0) && (m < numState));
      for (int n = 0; n < nVar; n++) {
        design[m][n] = 0.0;
      }
      break;
    }
    case kona::eval_precond: {// build the preconditioner if necessary
      break;
    } 
    case kona::precond_s: {// apply primal preconditioner to iwrk[2]
      // do nothing since no Jacobian
      // recall; iwrk[0], iwrk[1] denote where preconditioner is
      // evaluated and in this case, they are not needed
      int i = iwrk[2];
      int j = iwrk[3];
      assert((i >= 0) && (i < numState));
      assert((j >= 0) && (j < numState));
      // do-nothing preconditioner
      for (int n = 0; n < nVar; n++) {
        state[j][n] = state[i][n];
      }
      break;
    }
    case kona::tprecond_s: {// apply adjoint preconditioner to iwrk[2]
      int i = iwrk[2];
      int j = iwrk[3];
      assert((i >= 0) && (i < numState));
      assert((j >= 0) && (j < numState));
      // do-nothing preconditioner
      for (int n = 0; n < nVar; n++) {
        state[j][n] = state[i][n];
      }
      break;
    }
    case kona::ceqjac_d: {// design component of equality constraint Jacobian
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numDesign));
      assert((m >= 0) && (m < numDual));
      double x = design[i][0];
      double y = design[i][1];
      double z = design[i][2];
      dual[m][0] = 2.0*(x*design[k][0] + y*design[k][1] + z*design[k][2]);
      jac_eval++;
      break;
    }
    case kona::ceqjac_s: {// state component of equality constraint Jacobian
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numState));
      assert((m >= 0) && (m < numDual));
      dual[m][0] = 0.0;
      break;
    }
    case kona::tceqjac_d: {// apply design component of constraint Jac to dual
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numDual));
      assert((m >= 0) && (m < numDesign));
      double x = design[i][0];
      double y = design[i][1];
      double z = design[i][2];
      design[m][0] = 2.0*x*dual[k][0];
      design[m][1] = 2.0*y*dual[k][0];
      design[m][2] = 2.0*z*dual[k][0];
      jac_eval++;
      break;
    }
    case kona::tceqjac_s: {// apply state component of constraint Jac to dual
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numDual));
      assert((m >= 0) && (m < numState));
      for (int n = 0; n < nVar; n++) {
        state[m][n] = 0.0;
      }
      break;
    } 
    case kona::grad_d: {// design component of objective gradient
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numDesign));
      double x = design[i][0];
      double y = design[i][1];
      double z = design[i][2];
      design[k][0] = 1.0;
      design[k][1] = 1.0;
      design[k][2] = 1.0;
      grad_eval++;
      break;
    }
    case kona::grad_s: {// state component of objective gradient
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numState));
      break;
    }
    case kona::initdesign: {// intiailize the design variables
      int i = iwrk[0];
      assert((i >= 0) && (i < numDesign));
#if 1
      design[i][0] = 0.51;
      design[i][1] = 0.52;
      design[i][2] = 0.53;
#else
      design[i][0] = -0.51;
      design[i][1] = -0.52;
      design[i][2] = -0.53;
#endif
      break;
    }
    case kona::solve: {// solve the primal equations
      iwrk[0] = 1;
      break;
    }
    case kona::linsolve: {// solve the linearized equations
      break;
    }
    case kona::adjsolve: {// solve the dual equations
      iwrk[1] = -10; // temporary
      break;
    }
    case kona::debug: {
      int i = iwrk[0];
      int j = iwrk[1];
      design[i][0] = 0.0;
      design[i][1] = 0.0;
      design[i][2] = 0.0;
      design[i][j] = 1.0;
      break;
    }
    case kona::rank: {
      // set iwrk[0] to rank of calling process
      iwrk[0] = 0;
      break;
    }      
    case kona::info: {// supplies information to user
      // current design is in iwrk[0]
      // current pde solution is in iwrk[1]
      // current adjoint solution is in iwrk[2]
      int i = iwrk[0];
      int j = iwrk[3];
      cout << "current design = (" << design[i][0] << "," 
           << design[i][1] << "," << design[i][2] << ")" << endl;
      cout << "current dual = (" << dual[j][0] << ")" << endl;
      break;
    }
    default: {
      cerr << "userFunc: "
           << "unrecognized request value: request = "
           << request << endl;
      throw(-1);
      break;
    }
  }
  return 0;
}
