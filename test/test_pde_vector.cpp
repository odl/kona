/**
 * \file test_pde_vector.cpp
 * \brief unit test for the PDEVector class
 * \author  Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#include <assert.h>
#include <limits>
#include "../src/vectors.hpp"

const int nDV = 0; // size of the design vector
const int nVar = 2; // size of state vector
const int nCeq = 0; // size of dual vector
#include "./usrfun.hpp"

using std::numeric_limits;
using kona::PDEVector;

int main(int argc, char *argv[]) {
  // allocate memory
  int nd = 0;
  int ns = 3;
  kona::UserMemory::Allocate(userFunc, nd, ns);

  // define some state vectors
  PDEVector u, v;
  PDEVector *w = new PDEVector;
  
  const double initval[3] = {1.0, 2.0, -3.0};
  bool pass;
  int numfail = 0;

  u = initval[0];
  v = initval[1];
  *w = initval[2];

  // tell cout to display bools as words
  cout << std::boolalpha;

  cout << "\ttesting assignment operator (PDEVector=double)...";
  pass = true;
  for (int i = 0; i < ns; i++)
    for (int j = 0; j < nVar; j++)
      if (state[i][j] != initval[i]) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting assignment operator (PDEVector=PDEVector)...";
  v = u;
  pass = true;
  for (int j = 0; j < nVar; j++)
    if (state[0][j] != state[1][j]) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting assignment operator (PDEVector=*PDEVector)...";
  u = *w;
  pass = true;
  for (int j = 0; j < nVar; j++)
    if (state[0][j] != state[2][j]) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting assignment operator (*PDEVector=PDEVector)...";
  *w = v;
  pass = true;
  for (int j = 0; j < nVar; j++)
    if (state[1][j] != state[2][j]) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting compound addition operator (PDEVector+=PDEVector)...";
  u += v;
  pass = true;
  for (int j = 0; j < nVar; j++) {
    if (state[0][j] != -2) pass = false;
    if (state[1][j] !=  1) pass = false;
  }
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting compound addition operator (*PDEVector+=PDEVector)...";
  *w += u;
  pass = true;
  for (int j = 0; j < nVar; j++) {
    if (state[0][j] != -2) pass = false;
    if (state[2][j] != -1) pass = false;
  }
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting compound subtraction operator (PDEVector-=PDEVector)...";
  v -= u;
  pass = true;
  for (int j = 0; j < nVar; j++) {
    if (state[0][j] != -2) pass = false;
    if (state[1][j] !=  3) pass = false;
  }
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting compound subtraction operator "
       << "(PDEVector-=*PDEVector)...";
  u -= *w;
  pass = true;
  for (int j = 0; j < nVar; j++) {
    if (state[0][j] != -1) pass = false;
    if (state[2][j] != -1) pass = false;
  }
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting compound multiplication (PDEVector*=double)...";
  u *= 0.5;
  pass = true;
  for (int j = 0; j < nVar; j++) {
    if (state[0][j] != -0.5) pass = false;
  }
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting compound multiplication (*PDEVector*=double)...";
  *w *= 2.0;
  pass = true;
  for (int j = 0; j < nVar; j++) {
    if (state[2][j] != -2.0) pass = false;
  }
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting compound division (PDEVector /=double)...";
  u /= -5.0;
  pass = true;
  for (int j = 0; j < nVar; j++) {
    if (state[0][j] != 0.1) pass = false;
  }
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting general linear combination (EqualsAXPlusBY)...";
  u.EqualsAXPlusBY(1.0/3.0, v, 1.0/2.0, *w);
  pass = true;
  double eps = numeric_limits<double>::epsilon();
  for (int j = 0; j < nVar; j++) {
    if (fabs(state[0][j]) > eps) pass = false;
  }
  writeOutcome(pass);
  if (!pass) numfail++;

  // explict change of state
  state[0][0] = 0.5;
  state[0][1] = -0.5;

  cout << "\ttesting inner product "
       << "(InnerProd(PDEVector,*PDEVector))...";
  double prod = InnerProd(u, *w);
  pass = true;
  if (prod != 0.0) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting Norm2 (PDEVector.Norm2)...";
  double norm = u.Norm2();
  pass = true;
  if (norm != sqrt(0.5)) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

#if 0
  // uncomment to display values in state array
  for (int i = 0; i < ns; i++) {
    cout << "\tstate[" << i << "] = ";
    for (int j = 0; j < nVar; j++)
      cout << state[i][j] << " ";
    cout << endl;
  }
#endif

  cout << "\tcompleted PDEVector unit test: "
       << "number of failures = " << numfail << endl;

  delete w;
  return numfail;
}
