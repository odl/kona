/**
 * \file test_filter.cpp
 * \brief unit test for Filter class
 * \author  Jason Hicken <jason.hicken@gmail.com>
 */

#include "filter.hpp"
using kona::Filter;
using std::cout;
using std::endl;

void writeOutcome(const bool & pass) {
  if (pass) {
    cout << "pass" << endl;
  } else {
    cout << "fail" << endl;
  }
}

int main(int argc, char *argv[]) {

  Filter test_filter;
  cout << "\ttesting Filter...";
  int numfail = 0;
  bool pass = true;
  if (test_filter.Dominates(1.0, 2.0)) pass = false;
  //test_filter.Print();
  if (test_filter.Dominates(2.0, 1.0)) pass = false;
  //test_filter.Print();
  if (test_filter.Dominates(0.5, 5.0)) pass = false;
  //test_filter.Print();
  if (test_filter.Dominates(0.0, 3.0)) pass = false;
  //test_filter.Print();
  if (!test_filter.Dominates(0.5, 5.0)) pass = false;
  //test_filter.Print();
  if (test_filter.Dominates(-1.0, 4.0)) pass = false;
  //test_filter.Print();
  
  writeOutcome(pass);
  if (!pass) numfail++;
  cout << "\tcompleted Filter unit test: "
       << "number of failures = " << numfail << endl;
  return numfail;
}
