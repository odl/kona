/**
 * \file test_kkt_vector.cpp
 * \brief unit test for the KKTVector class
 * \author  Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#include <assert.h>
#include <limits>

#include "../src/user_memory.hpp"
#include "../src/kkt_vector.hpp"

const int nDV = 2; // size of design vectors
const int nVar = 2; // size of state vectors
const int nCeq = 0; // size of dual vectors (none used here
#include "./usrfun.hpp"

using std::numeric_limits;
using kona::KKTVector;

int main(int argc, char *argv[]) {
  // allocate state vector memory
  int nd = 3;
  int ns = 6;
  kona::UserMemory::Allocate(&userFunc, nd, ns);

  // define some KKT vectors
  KKTVector u;
  KKTVector *v = new KKTVector;

  bool pass;
  int numfail = 0;

  cout << "\ttesting assignment operator (KKTVector=double)...";
  u = 1.0;
  *v = -1.0;
  pass = true;
  // the first two state vectors should be assigned to u
  for (int i = 0; i < 2; i++)
    for (int j = 0; j < nVar; j++)
      if (state[i][j] != 1.0) pass = false;
  for (int i = 0; i < 1; i++)
    for (int j = 0; j < nDV; j++)
      if (design[i][j] != 1.0) pass = false;
  // the second two state vectors should be assigned to v
  for (int i = 2; i < 4; i++)
    for (int j = 0; j < nVar; j++)
      if (state[i][j] != -1.0) pass = false;
  for (int i = 1; i < 2; i++)
    for (int j = 0; j < nDV; j++)
      if (design[i][j] != -1.0) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting assignment operator (KKTVector=(*KKTVector))...";
  u = *v;
  // v = -1.0
  // u = -1.0;
  pass = true;
  for (int i = 0; i < 2; i++)
    for (int j = 0; j < nVar; j++)
      if (state[i][j] != -1.0) pass = false;
  for (int i = 0; i < 1; i++)
    for (int j = 0; j < nDV; j++)
      if (design[i][j] != -1.0) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting compound addition operator (*KKTVector+=KKTVector)...";
  *v += u;
  // u = -1.0;
  // v = -2.0;
  pass = true;
  for (int i = 2; i < 4; i++)
    for (int j = 0; j < nVar; j++)
      if (state[i][j] != -2.0) pass = false;
  for (int i = 1; i < 2; i++)
    for (int j = 0; j < nDV; j++)
      if (design[i][j] != -2.0) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting compound subtraction operator (KKTVector-=(*KKTVector))...";
  u -= *v;
  // u =  1.0;
  // v = -2.0;
  pass = true;
  for (int i = 0; i < 2; i++)
    for (int j = 0; j < nVar; j++)
      if (state[i][j] != 1.0) pass = false;
  for (int i = 0; i < 1; i++)
    for (int j = 0; j < nDV; j++)
      if (design[i][j] != 1.0) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting compound multiplication (KKTVector*=double)...";
  u *= 0.1;
  // u = 0.1;
  // v = -2.0;
  pass = true;
  for (int i = 0; i < 2; i++)
    for (int j = 0; j < nVar; j++)
      if (state[i][j] != 0.1) pass = false;
  for (int i = 0; i < 1; i++)
    for (int j = 0; j < nDV; j++)
      if (design[i][j] != 0.1) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting general linear combination (EqualsAXPlusBY)...";
  KKTVector w;
  w.EqualsAXPlusBY(10.0, u, 0.5, *v);
  // w = 10*u + 0.5*v = 0.0
  double eps = numeric_limits<double>::epsilon();
  pass = true;
  for (int i = 4; i < 6; i++)
    for (int j = 0; j < nVar; j++)
      if (fabs(state[i][j]) > eps) pass = false;
  for (int i = 2; i < 3; i++)
    for (int j = 0; j < nDV; j++)
      if (fabs(design[i][j]) > eps) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting inner product (InnerProd(KKTVector,*KKTVector))...";
  double prod = InnerProd(u, *v);
  pass = true;
  if (fabs(prod + 1.2) > eps) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting norm (KKTVector.Norm2)...";
  double norm = u.Norm2();
  pass = true;
  if (fabs(norm - sqrt(0.06)) > eps) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\tcompleted KKTVector unit test: "
       << "number of failures = " << numfail << endl;

  delete v;
  return numfail;
}
