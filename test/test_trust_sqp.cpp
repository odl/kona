/**
 * \file test_trust_sqp.cpp
 * \brief unit test for the TRISQP Krylov iterative solver
 * \author  Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#include <assert.h>
#include <fstream>
#include "../src/krylov.hpp"
#include <boost/numeric/ublas/vector.hpp>

using std::cout;
using std::endl;
namespace ublas = boost::numeric::ublas;

static const int nVar = 100;
static const int nCeq = 10;

// A front end for ublas vector that is compatible with kona::krylov methods
class Vec : public ublas::vector<double> {
 public:
  Vec() : ublas::vector<double>() {}
  Vec(const int & size, const double & val = 0.0) : 
      ublas::vector<double>(size, val) {}
  Vec(const ublas::vector<double> & u) : 
      ublas::vector<double>(u) {}
  Vec(const Vec & u) :
      ublas::vector<double>(static_cast<ublas::vector<double> >(u)) {}
  ~Vec() {}
  friend double InnerProd(const Vec & u, const Vec & v) {
    return ublas::inner_prod(u, v); }
  double Norm2() const { return norm_2(*this); }
  void EqualsAXPlusBY(const double & a, const Vec & x,
                      const double & b, const Vec & y) {
    for (int i = 0; i < size(); i++)
      this->operator()(i) = a*x(i) + b*y(i);
  }
  void operator=(const double & val) { ublas::vector<double>::operator=(
      ublas::scalar_vector<double>(size(), val)); }
};

// class for compound KKT vectors
class KKTVec {
 public:
  KKTVec() {}
  KKTVec(const Vec & primal, const Vec & dual) : prim_(primal), dual_(dual) {}
  KKTVec(const KKTVec & u) : prim_(u.prim_), dual_(u.dual_) {}
  KKTVec(const double & val) : prim_(nVar, val), dual_(nCeq, val) {}
  ~KKTVec() {}
  const Vec & primal() const { return prim_; }
  const Vec & dual() const { return dual_; }
  Vec & primal() { return prim_; }
  Vec & dual() { return dual_; }
  friend double InnerProd(const KKTVec & u, const KKTVec & v) {
    return inner_prod(u.prim_,v.prim_) + inner_prod(u.dual_,v.dual_); }
  double Norm2() const { return sqrt(InnerProd(*this, *this)); }
  void operator=(const double & val) {
    prim_ = val;
    dual_ = val;
  }
  void operator-=(const KKTVec & x) {
    prim_ -= x.prim_;
    dual_ -= x.dual_;
  }
  void operator+=(const KKTVec & x) {
    prim_ += x.prim_;
    dual_ += x.dual_;
  }
  void operator/=(const double & val) {
    prim_ /= val;
    dual_ /= val;
  }
  void EqualsAXPlusBY(const double & a, const KKTVec & x,
                      const double & b, const KKTVec & y) {
    prim_.EqualsAXPlusBY(a, x.prim_, b, y.prim_);
    dual_.EqualsAXPlusBY(a, x.dual_, b, y.dual_);
  }
 private:
  Vec prim_;
  Vec dual_; 
};

// class for KKT-matrix-vector products
class KKTProduct : public kona::MatrixVectorProduct<KKTVec> {
 public:
  KKTProduct() {}
  ~KKTProduct() {}
  void operator()(const KKTVec & u, KKTVec & v);
};

// class for preconditioning KKT systems
class KKTPrecond : public kona::Preconditioner<KKTVec> {
 public:
  KKTPrecond() { lambda_ = 0.0; }
  ~KKTPrecond() {}
  void set_diagonal(const double & diag) {
    lambda_ = diag;
  }
  void operator()(KKTVec & u, KKTVec & v) {
    v.primal().EqualsAXPlusBY(lambda_, u.primal(), 1.0, u.primal());
    v.dual() = u.dual();
  }
 private:
  double lambda_;
};

void writeOutcome(const bool & pass);

// ==============================================================================
int main(int argc, char *argv[]) {

  bool pass;
  int numfail = 0;
  
  Vec x0(nVar, 0.0);
  Vec exact(nVar, 0.0);
  Vec b_primal(nVar, 1.0);
  Vec b_dual(nCeq, 0.0);
  for (int i = 0; i < nVar; i++)
    x0(i) = static_cast<double>(i+1);
  exact = x0;
  for (int i = 0; i < nCeq; i++) {
    b_dual(i) = 2.0*x0(i);
    exact(i) = 2.0*x0(i);
  }
  double norm_sol = exact.Norm2();
  KKTVec b(b_primal, b_dual);
  KKTVec sol(b);
  kona::MatrixVectorProduct<KKTVec>* mat_vec = new KKTProduct();
  kona::Preconditioner<KKTVec>* precond = new KKTPrecond();  

  // open Krylov output file
  std::ofstream fout("krylov_out.dat", std::fstream::app);
  
  cout << "\ttesting TRISQP (trust-region inactive)...";
  pass = true;
  int m = 50;
  double grad_tol = 1e-4;
  double feas_tol = 1e-4;
  double radius = 3.010*norm_sol;
  int iters;
  double pred_opt, pred_feas;
  bool active;
  double mu = 1.0;
  sol.primal() = 0.0;
  sol.dual() = 0.0;
  kona::TRISQP<KKTVec, Vec, Vec> SQP;
  SQP.SubspaceSize(m);
  SQP.Solve(grad_tol, feas_tol, radius, b, sol, *mat_vec, *precond, iters,
            mu, pred_opt, pred_feas, active, fout, true);
  KKTVec tmp(b);
  mat_vec->operator()(sol, tmp);
  double ared_opt = -0.5*InnerProd(sol, tmp) + InnerProd(b, sol);
  tmp.dual() -= b.dual();
  double ared_feas = (InnerProd(b.dual(), b.dual())
                      - InnerProd(tmp.dual(), tmp.dual()));
  if (fabs(pred_opt - ared_opt) > 1e-10*fabs(ared_opt)) pass = false;
  if (fabs(pred_feas - ared_feas) > 1e-10*fabs(ared_feas)) pass = false;
  if (active) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;
  cout << "pred_opt = " << pred_opt << endl;
  cout << "ared_opt = " << ared_opt << endl;
  cout << "pred_feas = " << pred_feas << endl;
  cout << "ared_feas = " << ared_feas << endl;

#if 1
  // TEMP: comparison with FGMRES
  tmp -= b;
  fout << "norm of rhs    = " << b.Norm2() << endl;
  fout << "norm of KKTVec = " << tmp.Norm2()/b.Norm2() << endl;
  sol.primal() = 0.0;
  sol.dual() = 0.0;
  kona::FGMRES<KKTVec>(m, grad_tol, b, sol, *mat_vec, *precond, iters, fout,
                       true);
#endif

  cout << "\ttesting TRISQP (trust-region active)...";
  grad_tol = 1e-10;
  feas_tol = 1e-10;
  pass = true;
  radius = 0.01*norm_sol;
  sol.primal() = 0.0;
  sol.dual() = 0.0;
  mu = 1.0;
  SQP.SubspaceSize(m);
  SQP.Solve(grad_tol, feas_tol, radius, b, sol, *mat_vec, *precond, iters, mu,
            pred_opt, pred_feas, active, fout, true);
  mat_vec->operator()(sol, tmp);
  ared_opt = -0.5*InnerProd(sol, tmp) + InnerProd(b, sol);
  tmp.dual() -= b.dual();
  ared_feas = (InnerProd(b.dual(), b.dual())
               - InnerProd(tmp.dual(), tmp.dual()));
  cout << "sol.dual().Norm2() = " << sol.dual().Norm2() << endl;
  if (fabs(pred_opt - ared_opt) > 1e-10*fabs(ared_opt)) pass = false;
  if (fabs(pred_feas - ared_feas) > 1e-10*fabs(ared_feas)) pass = false;
  if (!active) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "pred_opt = " << pred_opt << endl;
  cout << "ared_opt = " << ared_opt << endl;
  cout << "pred_feas = " << pred_feas << endl;
  cout << "ared_feas = " << ared_feas << endl;
  
#if 0
  for (int i = 0; i < nVar; i++)
    cout << "x(" << i << ") = " << sol.primal()(i) << endl;
  for (int i = 0; i < nCeq; i++)
    cout << "lam(" << i << ") = " << sol.dual()(i) << endl;
#endif

  fout.close();
  return numfail;
}

// ==============================================================================
void KKTProduct::operator()(const KKTVec & u, KKTVec & v) {
  //cout << "KKTProduct::operator(): start..." << endl;
  for (int i = 0; i < nVar; i++)
    v.primal()(i) = u.primal()(i)/(static_cast<double>(i+1));
  //cout << "KKTProduct::operator(): after H*u.primal()..." << endl;
  for (int i = 0; i < nCeq; i++) {
    v.dual()(i) = u.primal()(i);
    v.primal()(i) += u.dual()(i);
  }
  //cout << "KKTProduct::operator(): after A*u.primal(), A^T*u.dual()..." << endl;
}

void writeOutcome(const bool & pass) {
  if (pass) {
    cout << "pass" << endl;
  } else {
    cout << "fail" << endl;
  }
}
