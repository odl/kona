/**
 * \file test_spiral.cpp
 * \brief test of the kona library based on "spiral" problem
 * \author  Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#include <assert.h>
#include "../src/user_memory.hpp"
#include "../src/kona.hpp"
#include <boost/math/constants/constants.hpp>

using std::string;
using std::map;
using std::cout;
using std::endl;
using std::cerr;

const double pi = boost::math::constants::pi<double>();
const int nDV = 1; // size of the design vector
const int nVar = 2;  // size of state vector
#include "./spiral_userfun.hpp"

int main(int argc, char *argv[]) {
  
  map<string,string> optns; // <-- empty optns
  //optns["opt_method"] = "reduced";
  //optns["krylov.solver"] = "fgmres";
  optns["krylov.space_size"] = "1";
  optns["krylov.tol"] = "0.5";
  optns["reduced.precond"] = "none";
  KonaOptimize(userFunc, optns);
  cout << "\tnumber of function evaluations: " << func_eval << endl;
  cout << "\tnumber of gradient evaluations: " << grad_eval << endl;


#if 0
  double epsilon = 1.E-8;
  int leniwrk = 4;
  int lendwrk = 2;
  int* iwrk = new int[leniwrk];
  double* dwrk = new double[lendwrk];
  iwrk[0] = 10;
  iwrk[1] = 20;
  int ierr = userFunc(kona::allocmem, leniwrk, iwrk, lendwrk, dwrk);

  // set initial design
  iwrk[0] = 0;
  ierr = userFunc(kona::initdesign, leniwrk, iwrk, lendwrk, dwrk);
  double init_design[1];
  init_design[0] = design[iwrk[0]][0];

  // set initial state and adjoint
  iwrk[1] = 0;
  ierr = userFunc(kona::solve, leniwrk, iwrk, lendwrk, dwrk);
  double init_state[2];
  init_state[0] = state[iwrk[1]][0];
  init_state[1] = state[iwrk[1]][1];
  double init_adj[2];
  init_adj[0] = 0.0;
  init_adj[1] = 0.0;

  // test jacvec_d
  double dRdXU[2], dRdXU_fd[2];
  iwrk[0] = 0;
  iwrk[1] = 0;
  iwrk[2] = 1;
  design[iwrk[2]][0] = 1.0;
  iwrk[3] = 1;
  ierr = userFunc(kona::jacvec_d, leniwrk, iwrk, lendwrk, dwrk);
  dRdXU[0] = state[iwrk[3]][0];
  dRdXU[1] = state[iwrk[3]][1];

  iwrk[0] = 0;
  iwrk[1] = 0;
  iwrk[2] = 1;
  ierr = userFunc(kona::eval_pde, leniwrk, iwrk, lendwrk, dwrk);
  dRdXU_fd[0] = state[iwrk[2]][0];
  dRdXU_fd[1] = state[iwrk[2]][1];
  design[iwrk[0]][0] -= epsilon*1.0;
  ierr = userFunc(kona::eval_pde, leniwrk, iwrk, lendwrk, dwrk);
  design[iwrk[0]][0] += epsilon*1.0;
  dRdXU_fd[0] -= state[iwrk[2]][0];
  dRdXU_fd[1] -= state[iwrk[2]][1];
  dRdXU_fd[0] /= epsilon;
  dRdXU_fd[1] /= epsilon;

  cout << "\tdRdXU    = " << dRdXU[0] << " " << dRdXU[1] << endl;
  cout << "\tdRdXU_fd = " << dRdXU_fd[0] << " " << dRdXU_fd[1] << endl;

  // test jacvec_s
  double dRdQU[2], dRdQU_fd[2];
  iwrk[0] = 0;
  iwrk[1] = 0;
  iwrk[2] = 1;
  state[iwrk[2]][0] = 1.0;
  state[iwrk[2]][1] = 1.0;
  iwrk[3] = 2;
  ierr = userFunc(kona::jacvec_s, leniwrk, iwrk, lendwrk, dwrk);
  dRdQU[0] = state[iwrk[3]][0];
  dRdQU[1] = state[iwrk[3]][1];

  iwrk[0] = 0;
  iwrk[1] = 0;
  iwrk[2] = 1;
  ierr = userFunc(kona::eval_pde, leniwrk, iwrk, lendwrk, dwrk);
  dRdQU_fd[0] = state[iwrk[2]][0];
  dRdQU_fd[1] = state[iwrk[2]][1];
  state[iwrk[1]][0] -= epsilon*1.0;
  state[iwrk[1]][1] -= epsilon*1.0;
  ierr = userFunc(kona::eval_pde, leniwrk, iwrk, lendwrk, dwrk);
  state[iwrk[1]][0] += epsilon*1.0;
  state[iwrk[1]][1] += epsilon*1.0;
  dRdQU_fd[0] -= state[iwrk[2]][0];
  dRdQU_fd[1] -= state[iwrk[2]][1];
  dRdQU_fd[0] /= epsilon;
  dRdQU_fd[1] /= epsilon;

  cout << "\tdRdQU    = " << dRdQU[0] << " " << dRdQU[1] << endl;
  cout << "\tdRdQU_fd = " << dRdQU_fd[0] << " " << dRdQU_fd[1] << endl;
#endif

}
