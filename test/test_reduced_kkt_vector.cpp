/**
 * \file test_reduced_kkt_vector.cpp
 * \brief unit test for the ReducedKKTVector class
 * \author  Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#include <assert.h>
#include <limits>

#include "../src/user_memory.hpp"
#include "../src/reduced_kkt_vector.hpp"

const int nDV = 2; // size of design vectors
const int nVar = 0; // size of state vectors (none used here)
const int nCeq = 2; // size of dual vectors
#include "./usrfun.hpp"

using std::numeric_limits;
using kona::ReducedKKTVector;

int main(int argc, char *argv[]) {
  // allocate state vector memory
  int nd = 3;
  int ns = 0;
  int nc = 3;
  kona::UserMemory::Allocate(&userFunc, nd, ns, nc);

  // define some KKT vectors
  ReducedKKTVector u;
  ReducedKKTVector *v = new ReducedKKTVector;

  bool pass;
  int numfail = 0;

  cout << "\ttesting assignment operator (ReducedKKTVector=double)...";
  u = 1.0;
  *v = -1.0;
  pass = true;
  // the first dual vectors should be assigned to u
  for (int i = 0; i < 1; i++)
    for (int j = 0; j < nCeq; j++)
      if (dual[i][j] != 1.0) pass = false;
  for (int i = 0; i < 1; i++)
    for (int j = 0; j < nDV; j++)
      if (design[i][j] != 1.0) pass = false;
  // the second dual vector should be assigned to v
  for (int i = 1; i < 2; i++)
    for (int j = 0; j < nCeq; j++)
      if (dual[i][j] != -1.0) pass = false;
  for (int i = 1; i < 2; i++)
    for (int j = 0; j < nDV; j++)
      if (design[i][j] != -1.0) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting assignment operator "
       << "(ReducedKKTVector=(*ReducedKKTVector))...";
  u = *v;
  // v = -1.0
  // u = -1.0;
  pass = true;
  for (int i = 0; i < 2; i++)
    for (int j = 0; j < nCeq; j++)
      if (dual[i][j] != -1.0) pass = false;
  for (int i = 0; i < 1; i++)
    for (int j = 0; j < nDV; j++)
      if (design[i][j] != -1.0) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting compound addition operator "
       << "(*ReducedKKTVector+=ReducedKKTVector)...";
  *v += u;
  // u = -1.0;
  // v = -2.0;
  pass = true;
  for (int i = 1; i < 2; i++)
    for (int j = 0; j < nCeq; j++)
      if (dual[i][j] != -2.0) pass = false;
  for (int i = 1; i < 2; i++)
    for (int j = 0; j < nDV; j++)
      if (design[i][j] != -2.0) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting compound subtraction operator "
       << "(ReducedKKTVector-=(*ReducedKKTVector))...";
  u -= *v;
  // u =  1.0;
  // v = -2.0;
  pass = true;
  for (int i = 0; i < 1; i++)
    for (int j = 0; j < nCeq; j++)
      if (dual[i][j] != 1.0) pass = false;
  for (int i = 0; i < 1; i++)
    for (int j = 0; j < nDV; j++)
      if (design[i][j] != 1.0) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting compound multiplication (ReducedKKTVector*=double)...";
  u *= 0.1;
  // u = 0.1;
  // v = -2.0;
  pass = true;
  for (int i = 0; i < 1; i++)
    for (int j = 0; j < nCeq; j++)
      if (dual[i][j] != 0.1) pass = false;
  for (int i = 0; i < 1; i++)
    for (int j = 0; j < nDV; j++)
      if (design[i][j] != 0.1) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting general linear combination (EqualsAXPlusBY)...";
  ReducedKKTVector w;
  w.EqualsAXPlusBY(10.0, u, 0.5, *v);
  // w = 10*u + 0.5*v = 0.0
  double eps = numeric_limits<double>::epsilon();
  pass = true;
  for (int i = 2; i < 3; i++)
    for (int j = 0; j < nCeq; j++)
      if (fabs(dual[i][j]) > eps) pass = false;
  for (int i = 2; i < 3; i++)
    for (int j = 0; j < nDV; j++)
      if (fabs(design[i][j]) > eps) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting inner product "
       << "(InnerProd(ReducedKKTVector,*ReducedKKTVector))...";
  double prod = InnerProd(u, *v);
  pass = true;
  if (fabs(prod + 0.8) > eps) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting norm (ReducedKKTVector.Norm2)...";
  double norm = u.Norm2();
  pass = true;
  if (fabs(norm - sqrt(0.04)) > eps) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\tcompleted ReducedKKTVector unit test: "
       << "number of failures = " << numfail << endl;

  delete v;
  return numfail;
}
