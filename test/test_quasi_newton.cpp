/**
 * \file test_quasi_newton.cpp
 * \brief unit test for the QuasiNewton and derived classes
 * \author  Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 *
 * The QuasiNewton class is tested using Theorem 6.4 from Nocedal and
 * Wright ("Numerical Optimization", second edition, Springer).
 * Specifically, for a strongly convex quadratic function using exact
 * line searches, the approximate Hessian is equal to the exact
 * Hessian after n iterations, where n is the problem dimension.
 */

#include <assert.h>
#include <boost/numeric/ublas/matrix.hpp>
#include <limits>
#include <vector>
#include "../src/user_memory.hpp"
#include "../src/vectors.hpp"
#include "../src/krylov.hpp"
#include "../src/quasi_newton.hpp"

const int nDV = 3;
const int nVar = 0; // size of state vector (not actually used here)
const int nCeq = 0; // size of dual vector (not used here either)
#include "./usrfun.hpp"

int main(int argc, char *argv[]) {

  bool pass;
  int numfail = 0;
  const int max_stored = 3;

  // define the size of the design (and state) vectors
  // the "+ 1" is for the initial hessian
  // the "+ 2" is for s_new and y_new
  // the "+ 2" is for tmps in SR1::addCorrection
  int nd = 3*max_stored + 1 + 2 + 2;
  int ns = 0;
  kona::UserMemory::Allocate(userFunc, nd, ns);

  // define the variables and gradients
  kona::DesignVector s_new, y_new;
  // s_new -> design[0]
  // y_new -> design[1]

  {
    // create a new BFGS object
    kona::QuasiNewton * H = new kona::GlobalizedBFGS(max_stored);
    H->SetInvHessianToIdentity();

    // Hessian matrix is [1 0 0; 0 100 0; 0 0 10]
    // initial iterate is [1 1 1]
    
    // first "iteration"
    for (int n = 0; n < nDV; n++) {
      design[0][n] = 0.0;
      design[1][n] = 0.0;
    }
    design[0][0] = -1.0;
    design[1][0] = -1.0;
  
    H->AddCorrection(s_new, y_new);
    
    // second "iteration"
    for (int n = 0; n < nDV; n++) {
      design[0][n] = 0.0;
      design[1][n] = 0.0;
    }
    design[0][1] = -1.0;
    design[1][1] = -100.0;
    
    H->AddCorrection(s_new, y_new);
    
    // third "iteration"
    for (int n = 0; n < nDV; n++) {
      design[0][n] = 0.0;
      design[1][n] = 0.0;
    }
    design[0][2] = -1.0;
    design[1][2] = -10.0;
    
    H->AddCorrection(s_new, y_new);
    
    // check that approximate inverse Hessian equals exact
    cout << "\ttesting Globalized BFGS on strongly convex quadratic" << endl;
    
    cout << "\t  testing first column of H*H^{-1}...";
    pass = true;
    for (int n = 0; n < nDV; n++) {
      design[0][n] = 0.0;
      design[1][n] = 0.0;
    }
    design[0][0] = 1.0;
    H->ApplyInvHessianApprox(s_new, y_new);
    if ( (fabs(design[1][0] - 1.0) > kona::kEpsilon) ||
         (fabs(design[1][1]) > kona::kEpsilon) ||
         (fabs(design[1][2]) > kona::kEpsilon) )
      pass = false;
    writeOutcome(pass);
    if (!pass) numfail++;
    
    cout << "\t  testing second column of H*H^{-1}...";
    pass = true;
    for (int n = 0; n < nDV; n++) {
      design[0][n] = 0.0;
      design[1][n] = 0.0;
    }
    design[0][1] = 100.0;
    H->ApplyInvHessianApprox(s_new, y_new);
    if ( (fabs(design[1][0]) > kona::kEpsilon) ||
         (fabs(design[1][1] - 1.0) > kona::kEpsilon) ||
         (fabs(design[1][2]) > kona::kEpsilon) )
      pass = false;
    writeOutcome(pass);
    if (!pass) numfail++;

    cout << "\t  testing third column of H*H^{-1}...";
    pass = true;
    for (int n = 0; n < nDV; n++) {
      design[0][n] = 0.0;
      design[1][n] = 0.0;
    }
    design[0][2] = 10.0;
    H->ApplyInvHessianApprox(s_new, y_new);
    if ( (fabs(design[1][0]) > kona::kEpsilon) ||
         (fabs(design[1][1]) > kona::kEpsilon) ||
         (fabs(design[1][2] - 1.0) > kona::kEpsilon) )
      pass = false;
    writeOutcome(pass);
    if (!pass) numfail++;

    cout << "\t  testing first column of H...";
    pass = true;
    for (int n = 0; n < nDV; n++) {
      design[0][n] = 0.0;
      design[1][n] = 0.0;
    }
    design[0][0] = 1.0;
    H->ApplyHessianApprox(s_new, y_new);
    if ( (fabs(design[1][0] - 1.0) > kona::kEpsilon) ||
         (fabs(design[1][1]) > kona::kEpsilon) ||
         (fabs(design[1][2]) > kona::kEpsilon) )
      pass = false;
    writeOutcome(pass);
    if (!pass) numfail++;

    cout << "\t  testing second column of H...";
    pass = true;
    for (int n = 0; n < nDV; n++) {
      design[0][n] = 0.0;
      design[1][n] = 0.0;
    }
    design[0][1] = 1.0;
    H->ApplyHessianApprox(s_new, y_new);
    if ( (fabs(design[1][0]) > kona::kEpsilon) ||
         (fabs(design[1][1] - 100.0) > kona::kEpsilon) ||
         (fabs(design[1][2]) > kona::kEpsilon) )
      pass = false;
    writeOutcome(pass);
    if (!pass) numfail++;

    cout << "\t  testing third column of H...";
    pass = true;
    for (int n = 0; n < nDV; n++) {
      design[0][n] = 0.0;
      design[1][n] = 0.0;
    }
    design[0][2] = 1.0;
    H->ApplyHessianApprox(s_new, y_new);
    if ( (fabs(design[1][0]) > kona::kEpsilon) ||
         (fabs(design[1][1]) > kona::kEpsilon) ||
         (fabs(design[1][2] - 10.0) > kona::kEpsilon) )
      pass = false;
    writeOutcome(pass);
    if (!pass) numfail++;

    delete H;
  }

  {  
    // create a new SR1 object
    kona::QuasiNewton * H = new kona::GlobalizedSR1(max_stored);
    H->SetInvHessianToIdentity();
    H->set_norm_init(1.0);
    static_cast<kona::GlobalizedSR1*>(H)->set_lambda(0.0);

    // Hessian matrix is [1 0 0; 0 100 0; 0 0 10]
    // initial iterate is [1 1 1]
    
    // first "iteration"
    for (int n = 0; n < nDV; n++) {
      design[0][n] = 0.0;
      design[1][n] = 0.0;
    }
    design[0][0] = -1.0;
    design[1][0] = -1.0;  
    H->AddCorrection(s_new, y_new);
    
    // second "iteration"
    for (int n = 0; n < nDV; n++) {
      design[0][n] = 0.0;
      design[1][n] = 0.0;
    }
    design[0][1] = -1.0;
    design[1][1] = -100.0;
    H->AddCorrection(s_new, y_new);
    
    // third "iteration"
    for (int n = 0; n < nDV; n++) {
      design[0][n] = 0.0;
      design[1][n] = 0.0;
    }
    design[0][2] = -1.0;
    design[1][2] = -10.0;
    H->AddCorrection(s_new, y_new);
    
    // check that approximate inverse Hessian equals exact
    cout << "\ttesting limited memory SR1 on strongly convex quadratic" << endl;
    
    cout << "\t  testing first column of H*H^{-1}...";
    pass = true;
    for (int n = 0; n < nDV; n++) {
      design[0][n] = 0.0;
      design[1][n] = 0.0;
    }
    design[0][0] = 1.0;
    H->ApplyInvHessianApprox(s_new, y_new);
    if ( (fabs(design[1][0] - 1.0) > kona::kEpsilon) ||
         (fabs(design[1][1]) > kona::kEpsilon) ||
         (fabs(design[1][2]) > kona::kEpsilon) )
      pass = false;
    writeOutcome(pass);
    if (!pass) numfail++;
    
    cout << "\t  testing second column of H*H^{-1}...";
    pass = true;
    for (int n = 0; n < nDV; n++) {
      design[0][n] = 0.0;
      design[1][n] = 0.0;
    }
    design[0][1] = 100.0;
    H->ApplyInvHessianApprox(s_new, y_new);
    if ( (fabs(design[1][0]) > kona::kEpsilon) ||
         (fabs(design[1][1] - 1.0) > kona::kEpsilon) ||
         (fabs(design[1][2]) > kona::kEpsilon) )
      pass = false;
    writeOutcome(pass);
    if (!pass) numfail++;

    cout << "\t  testing third column of H*H^{-1}...";
    pass = true;
    for (int n = 0; n < nDV; n++) {
      design[0][n] = 0.0;
      design[1][n] = 0.0;
    }
    design[0][2] = 10.0;
    H->ApplyInvHessianApprox(s_new, y_new);
    if ( (fabs(design[1][0]) > kona::kEpsilon) ||
         (fabs(design[1][1]) > kona::kEpsilon) ||
         (fabs(design[1][2] - 1.0) > kona::kEpsilon) )
      pass = false;
    writeOutcome(pass);
    if (!pass) numfail++;

   cout << "\t  testing first column of H...";
    pass = true;
    for (int n = 0; n < nDV; n++) {
      design[0][n] = 0.0;
      design[1][n] = 0.0;
    }
    design[0][0] = 1.0;
    H->ApplyHessianApprox(s_new, y_new);
    if ( (fabs(design[1][0] - 1.0) > kona::kEpsilon) ||
         (fabs(design[1][1]) > kona::kEpsilon) ||
         (fabs(design[1][2]) > kona::kEpsilon) )
      pass = false;
    writeOutcome(pass);
    if (!pass) numfail++;

    cout << "\t  testing second column of H...";
    pass = true;
    for (int n = 0; n < nDV; n++) {
      design[0][n] = 0.0;
      design[1][n] = 0.0;
    }
    design[0][1] = 1.0;
    H->ApplyHessianApprox(s_new, y_new);
    if ( (fabs(design[1][0]) > kona::kEpsilon) ||
         (fabs(design[1][1] - 100.0) > kona::kEpsilon) ||
         (fabs(design[1][2]) > kona::kEpsilon) )
      pass = false;
    writeOutcome(pass);
    if (!pass) numfail++;

    cout << "\t  testing third column of H...";
    pass = true;
    for (int n = 0; n < nDV; n++) {
      design[0][n] = 0.0;
      design[1][n] = 0.0;
    }
    design[0][2] = 1.0;
    H->ApplyHessianApprox(s_new, y_new);
    if ( (fabs(design[1][0]) > kona::kEpsilon) ||
         (fabs(design[1][1]) > kona::kEpsilon) ||
         (fabs(design[1][2] - 10.0) > kona::kEpsilon) )
      pass = false;
    writeOutcome(pass);
    if (!pass) numfail++;

    delete H;
  }

  cout << "\tcompleted QuasiNewton (BFGS, SR1, etc) unit test: "
       << "number of failures = " << numfail << endl;

  return numfail;
}
