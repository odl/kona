/**
 * \file test_kona.cpp
 * \brief simple test of the kona library
 * \author  Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#include <assert.h>
#include "../src/user_memory.hpp"
#include "../src/kona.hpp"

using std::string;
using std::map;
using std::cout;
using std::endl;
using std::cerr;

const int nDV = 2; // size of the design vector
const int nVar = 0;  // size of state vector
#include "./rosenbrock_userfun.hpp"

int main(int argc, char *argv[]) {

  map<string,string> optns; // <-- empty optns 
  KonaOptimize(userFunc, optns);
  cout << "\tnumber of function evaluations: " << func_eval << endl;
  cout << "\tnumber of gradient evaluations: " << grad_eval << endl;

}
