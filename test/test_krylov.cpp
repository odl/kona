/**
 * \file test_krylov.cpp
 * \brief unit test for the Krylov iterative solvers
 * \author  Jason Hicken <jason.hicken@gmail.com>
 * \version 1.0
 */

#include <assert.h>
#include <limits>
//#include <ostream>
//#include <iostream>
#include <fstream>

#include <boost/scoped_ptr.hpp>

#include "../src/user_memory.hpp"
#include "../src/vectors.hpp"
#include "../src/vector_operators.hpp"
#include "../src/krylov.hpp"

const int nDV = 0; // size of design vectors
const int nVar = 21; // size of state vectors
const int nCeq = 0; // size of the dual vectors
#include "./usrfun.hpp"

using std::numeric_limits;
using kona::PDEVector;
namespace ublas = boost::numeric::ublas;

int main(int argc, char *argv[]) {
  // allocate state vector memory
  int nd = 0;
  int ns = 53;
  kona::UserMemory::Allocate(userFunc, nd, ns);
  bool pass;
  int numfail = 0;

  // test basic linear algebra
  int n = 3;
  ublas::matrix<double> A(n, n);
  // The following matrix has eigenvalues (-0.01, 1.0, 100.0)
  A(0,0) = 53.584361076704752;
  A(0,1) = 38.402931935627734;
  A(1,0) = A(0,1);
  A(0,2) = -31.699837356677541;
  A(2,0) = A(0,2);
  A(1,1) = 27.529960220658360;
  A(1,2) = -22.554880790810088;
  A(2,1) = A(1,2);
  A(2,2) = 19.875678702636858;
  
  cout << "\ttesting eigenvalues (computes eigenvalue)...";
  pass = true;
  ublas::vector<double> eig(n, 0.0);
  kona::eigenvalues(n, A, eig);
  if (fabs(eig(0) + 0.01) > 100.0*kona::kEpsilon) pass = false;
  if (fabs(eig(1) - 1.0) > 100.0*kona::kEpsilon) pass = false;
  if (fabs(eig(2) - 100.0) > 1000.0*kona::kEpsilon) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting factorQR (QR factorization)...";
  pass = true;
  A(0,0) = -62.079684116230460;
  A(0,1) =  30.263493223857591;
  A(1,0) = -28.200066688805919;
  A(1,1) =  14.450966607489233;  
  A(2,0) = -73.149634713892738;
  A(2,1) =  37.098390018958455;
  // The QR factorization of the above (3x2) matrix is
  // Q =
  // -0.620796841162305   0.776348834257638  -0.109058560185388
  // -0.282000666888059  -0.350933263086275  -0.892928591061071
  // -0.731496347138927  -0.523572662012089   0.436789150181161
  // R =
  // 100   -50
  // 0    -1
  int nrow = 3;
  int ncol = 2;
  ublas::vector<double> QR;
  kona::factorQR(nrow, ncol, A, QR);
  if (fabs(QR(0) - 100.0) > 100.0*kona::kEpsilon) pass = false;
  if (fabs(QR(nrow) + 50.0) > 100.0*kona::kEpsilon) pass = false;
  if (fabs(QR(nrow+1) + 1.0) > 100.0*kona::kEpsilon) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;
    
  cout << "\ttesting applyQ and solveR (using result of QR factorization)...";
  pass = true;
  ublas::vector<double> rhs(nrow);
  ublas::vector<double> sol(nrow);
  // the rhs below (stored in sol intially) is the first orthogonal vector in Q,
  // so Q^{T} * sol = [1 0]^{T}, which is then used as the rhs for solveR.
  sol(0) = -0.620796841162305;
  sol(1) = -0.282000666888059;
  sol(2) = -0.731496347138927;
  kona::applyQ(nrow, ncol, QR, sol, rhs, true);
  kona::solveR(nrow, ncol, QR, rhs, sol);
  if (fabs(sol(0) - 0.01) > 100.0*kona::kEpsilon) pass = false;
  if (fabs(sol(1)) > 100.0*kona::kEpsilon) pass = false;
  if (fabs(rhs(2)) > 100.0*kona::kEpsilon) pass = false;  // residual
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting computeSVD (finds the SVD of a matrix)...";
  pass = true;
  // Using A as defined above, the SVD is
  // U =
  // -0.617686304967202   0.778825949173661  -0.109058560185388
  // -0.283402222284698  -0.349802389456940  -0.892928591061071
  // -0.733584902421021  -0.520642323696375   0.436789150181160
  // S =
  //  111.8042933558484                 0
  //                  0   0.8944200352103
  //                  0                 0
  // V =
  //  0.894412879019938  -0.447242218314937
  // -0.447242218314937  -0.894412879019938
  ublas::vector<double> Sigma, U, VT;
  kona::computeSVD(nrow, ncol, A, Sigma, U, VT);
  if (fabs(Sigma(0) - 111.8042933558484) > 1000.0*kona::kEpsilon) pass = false;
  if (fabs(Sigma(1) - 0.8944200352103) > 1000.0*kona::kEpsilon) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting solveLeastSquaresOverSphere (constraint inactive)...";
  pass = true;
  A(0,0) = -1.0;
  A(0,1) =  1.0;
  A(1,0) =  1.0;
  A(1,1) =  1.0;
  A(2,0) =  0.0;
  A(2,1) =  1.0;
  nrow = 3;
  ncol = 2;
  rhs(0) = 0.0;
  rhs(1) = 2.0;
  rhs(2) = 0.0;
  double rad = 2.0;
  // Using A and rhs as defined above, the unconstrained least-squares solution
  // is
  // sol =
  //   1.00000000
  //   0.666666666
  kona::computeSVD(nrow, ncol, A, Sigma, U, VT);
  ublas::matrix<double> Umat(nrow,ncol,0.0), VTmat(ncol,ncol,0.0);
  for (int i = 0; i < ncol; i++)
    for (int j = 0; j < nrow; j++)
      Umat(j,i) = U(j+i*nrow);
  for (int i = 0; i < ncol; i++)
    for (int j = 0; j < ncol; j++)
      VTmat(j,i) = VT(j+i*ncol);
  kona::solveLeastSquaresOverSphere(nrow, ncol, rad, Sigma, Umat, VTmat, rhs,
                                    sol);
  if (fabs(sol(0) - 1.0) > kona::kEpsilon) pass = false;
  if (fabs(sol(1) - 2.0/3.0) > kona::kEpsilon) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting solveLeastSquaresOverSphere (constraint active)...";
  pass = true;
  rad = 0.5;
  // With the sphere radius reduced to 0.5, the solution is approximately
  // sol = 
  //   0.382987724401892
  //   0.321434912474455  
  kona::solveLeastSquaresOverSphere(nrow, ncol, rad, Sigma, Umat, VTmat, rhs,
                                    sol);
  if (fabs(sol(0) - 0.382987724401892) > sqrt(kona::kEpsilon)) pass = false;
  if (fabs(sol(1) - 0.321434912474455) > sqrt(kona::kEpsilon)) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting solveUnderdeterminedMinNorm...";
  pass = true;
  // Using A^T from above and rhs(0:1), the minimum norm solution is
  // sol = 
  //   0.6666..
  //   0.6666..
  //   0.6666..
  kona::solveUnderdeterminedMinNorm(ncol, nrow, Sigma, ublas::trans(VTmat),
                                    ublas::trans(Umat), rhs, sol);
  if (fabs(sol(0) - 2.0/3.0) > sqrt(kona::kEpsilon)) pass = false;
  if (fabs(sol(1) - 2.0/3.0) > sqrt(kona::kEpsilon)) pass = false;
  if (fabs(sol(2) - 2.0/3.0) > sqrt(kona::kEpsilon)) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;
  
  cout << "\ttesting factorCholesky and applyU (factor SPD matrix and solve)...";
  pass = true;
  A(0,0) = 3.931544008059447;
  A(0,1) = -4.622828930484834;
  A(1,0) = A(0,1);
  A(0,2) = 1.571893108754884;
  A(2,0) = A(0,2);
  A(1,1) = 5.438436601890520;
  A(1,2) = -1.853920290644159;
  A(2,1) = A(1,2);
  A(2,2) = 0.640029390050034;
  ublas::vector<double> UTU;
  kona::factorCholesky(nrow, A, UTU);
  sol(0) = 0.964888535199277;
  sol(1) = 0.157613081677548;
  sol(2) = 0.970592781760616;
  kona::solveU(n, UTU, sol, rhs, true); 
  kona::solveU(n, UTU, rhs, sol, false);
  if (fabs(sol(0) - 70306.51598209806) > 1e-5) pass = false;
  if (fabs(sol(1) - 71705.07008271456) > 1e-5) pass = false;
  if (fabs(sol(2) - 35032.96463715491) > 1e-5) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting solveTrustReduced (trust radius constraint inactive)...";
  pass = true;
  // This uses the same system as above for the test, except the rhs must be
  // negated to account for the form g'*x + 0.5*x'*A*x
  rad = 1e+6;
  rhs(0) = -0.964888535199277;
  rhs(1) = -0.157613081677548;
  rhs(2) = -0.970592781760616;
  double lambda = 0.0;
  double pred;
  kona::solveTrustReduced(n, A, rad, rhs, sol, lambda, pred);
  if (fabs(sol(0) - 70306.51598209806) > 1e-5) pass = false;
  if (fabs(sol(1) - 71705.07008271456) > 1e-5) pass = false;
  if (fabs(sol(2) - 35032.96463715491) > 1e-5) pass = false;
  if (fabs(lambda) > kona::kEpsilon) pass = false;
  if (fabs(pred - 56571.17544243777) > 1e-5) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting solveTrustReduced (trust radius constraint active)...";
  pass = true;
  rad = 10000.0;
  lambda = 0.0;
  kona::solveTrustReduced(n, A, rad, rhs, sol, lambda, pred);
  if (fabs(sol(0) - 6592.643411099528) > 1e-3*fabs(sol(0))) pass = false;
  if (fabs(sol(1) - 6740.041501068382) > 1e-3*fabs(sol(1))) pass = false;
  if (fabs(sol(2) - 3333.000662760490) > 1e-3*fabs(sol(2))) pass = false;
  if (fabs(lambda - 9.635875530658215e-05) > 1e-3*lambda) pass = false;
  if (fabs(pred - 10147.17333545226) > 1e-3*fabs(pred)) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting solveTrustReduced (indefinite Hessian)...";
  // The matrix A below has the same eigenvalue magnitudes as those above, but
  // the smallest eigenvalue is negative.
  pass = true;
  A(0,0) = 3.931535263699851;
  A(0,1) = -4.622837846534464;
  A(1,0) = A(0,1);
  A(0,2) = 1.571888758188687;
  A(2,0) = A(0,2);
  A(1,1) = 5.438427510779841;
  A(1,2) = -1.853924726631001;
  A(2,1) = A(1,2);
  A(2,2) = 0.640027225520312;
  rhs(0) = -1e-5;
  rhs(1) = -1e-5;
  rhs(2) = -1e-5;
  rad = 10000.0;
  lambda = 0.0;
  kona::solveTrustReduced(n, A, rad, rhs, sol, lambda, pred);
  if (fabs(sol(0) - 6612.245873748023) > 1e-6*fabs(sol(0))) pass = false;
  if (fabs(sol(1) - 6742.073357887492) > 1e-6*fabs(sol(1))) pass = false;
  if (fabs(sol(2) - 3289.779831837675) > 1e-6*fabs(sol(2))) pass = false;
  if (fabs(lambda - 1.000166441038206e-05) > 1e-6*lambda) pass = false;
  if (fabs(pred - 500.1664410153899) > 1e-5*fabs(pred)) pass = false;
  cout << "fabs(pred - 500.1664410153899) = " << fabs(pred - 500.1664410153899) << endl;
  cout << "1e-6*fabs(pred)                = " << 1e-6*fabs(pred) << endl;
  writeOutcome(pass);
  if (!pass) numfail++;
  
  //============================================================================
  // define the linear system
  // The model problem is a linear 1-D advection equation, specifically
  //           du/dx = exp(x),  u(0) = 1.
  // The problem is discretized using the second-order SBP
  // finite-difference operator and the boundary condition is imposed
  // using a penalty term
  mat = new double*[nVar];
  for (int i = 0; i < nVar; i++) {
    mat[i] = new double[nVar];
    for (int j = 0; j < nVar; j++)
      mat[i][j] = 0.0;
  }
#if 0
  // simple identiy matrix
  for (int i = 0; i < nVar; i++)
    mat[i][i] = 2.0;
#endif

  for (int i = 1; i < nVar-1; i++) {
    mat[i][i-1] = -0.5;
    mat[i][i+1] =  0.5;
  }
  mat[0][0] = 0.5;
  mat[0][1] = 0.5;
  mat[nVar-1][nVar-1] = 0.5;
  mat[nVar-1][nVar-2] = -0.5;

  //cout << "\tbefore initializing PDEVector b ..." << endl;
  PDEVector b;
  double h = 1.0/(static_cast<double>(nVar-1));
  for (int i = 0; i < nVar; i++)
    state[0][i] = h*exp(h*static_cast<double>(i));
  state[0][0] = 0.5*state[0][0] + 1.0;
  state[0][nVar-1] *= 0.5;

  // open Krylov output file and set some options
  std::ofstream fout("krylov_out.dat");
  bool check_res = true;
  bool dynamic = false;
  boost::scoped_ptr<double> res(new double);
  
  //cout << "\tbefore initializing PDEVector x ..." << endl;
  PDEVector x;
  x = 0.0;
  
  cout << "\ttesting FGMRES...";
  pass = true;
  int m = nVar;
  double tol = 1.0e-3;
  kona::MatrixVectorProduct<PDEVector>* mat_vec =
      new kona::JacobianStateProduct();
  kona::Preconditioner<PDEVector>* precond =
      new kona::StatePreconditioner();
  int iters;
  kona::FGMRES(m, tol, b, x, *mat_vec, *precond, iters, fout, check_res, dynamic,
               res);
  // test that it converges on iteration 20 to an absolute tolerance of
  // 0.000899145
  //cout << "*res = " << *res << endl;
  if (fabs(*res - 0.000899145) > 1e-8) pass = false;
  if (iters != 20) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;

  cout << "\ttesting FGMRESSolver class...";
  x = 0.0;
  pass = true;
  kona::IterativeSolver<PDEVector,PDEVector>* solver =
      new kona::FGMRESSolver<PDEVector>();
  solver->SubspaceSize(m);
  using boost::property_tree::ptree;
  ptree input_params, output_params;
  input_params.put<double>("tol", tol);
  input_params.put<bool>("check", true);
  solver->Solve(input_params, b, x, *mat_vec, *precond, output_params, fout);
  //cout << "residual norm = " << output_params.get<double>("res") << endl;
  if (fabs(output_params.get<double>("res") - 0.000899145) > 1e-8) pass = false;
  if (output_params.get<int>("iters") != 20) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;
  delete solver;
  
#if 0
  for (int i = 0; i < nVar; i++)
    cout << "\tu[" << i << "] = " << state[1][i] << endl;
#endif

  // test GMRES
  x = 0.0;
  kona::GMRES(m, tol, b, x, *mat_vec, *precond, iters, fout, check_res, dynamic);
  // need to test that it converges on iteration 19 to
  // a tolerance of 0.000819229
  
  // test FFOMSolver class
  cout << "\ttesting FFOMSolver class...";
  x = 0.0;
  pass = true;
  solver = new kona::FFOMSolver<PDEVector>();
  solver->SubspaceSize(m);
  solver->Solve(input_params, b, x, *mat_vec, *precond, output_params, fout);
  //cout << "FFOM residual norm = " << output_params.get<double>("res") << endl;
  if (fabs(output_params.get<double>("res") - 1.32472e-15) > kona::kEpsilon)
    pass = false;
  if (output_params.get<int>("iters") != 21) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;
  delete solver;
  
  // next, to test MINRES, we construct a symmetric positive definite matrix
  for (int i = 0; i < nVar; i++) {
    for (int j = 0; j < nVar; j++)
      mat[i][j] = 0.0;
  }
  for (int i = 1; i < nVar-1; i++) {
    mat[i][i-1] = -1.0;
    mat[i][i]   =  2.0;
    mat[i][i+1] = -1.0;
  }
  mat[0][0] = 2.0;
  mat[0][1] = -1.0;
  mat[nVar-1][nVar-1] = 2.0;
  mat[nVar-1][nVar-2] = -1.0;

  // set rhs vector
  for (int i = 0; i < nVar; i++)
    state[0][i] = 0.0;
  state[0][nVar-1] = 1.0;

  cout << "\ttesting MINRESSolver class...";
  x = 0.0;
  pass = true;
  no_prec = true;
  solver = new kona::MINRESSolver<PDEVector>();
  solver->SubspaceSize(m);
  input_params.put("tol", 1e-3);
  solver->Solve(input_params, b, x, *mat_vec, *precond, output_params, fout);
  cout << "MINRES residual norm = " << output_params.get<double>("res") << endl;
  cout << "MINRES iters = " << output_params.get<int>("iters") << endl;
  if (fabs(output_params.get<double>("res")) > 100.0*kona::kEpsilon)
    pass = false;
  if (output_params.get<int>("iters") != 21) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;
  delete solver;
  
#if 0
  cout << "\tx = ";
  for (int i = 0; i < nVar; i++)
    cout << state[1][i] << " ";
  cout << endl;
#endif
  
  // simple tests of FITR
  cout << "\ttesting FITR solution norm is less than radius...";
  x = 0.0;
  pass = true;
  no_prec = true;
  solver = new kona::FITRSolver<PDEVector>();
  solver->SubspaceSize(m);
  input_params.put("tol", 1e-3);
  double radius = 1.0;
  input_params.put("radius", radius);
  solver->Solve(input_params, b, x, *mat_vec, *precond, output_params, fout);
  double nrm = 0.0;
  for (int i = 0; i < nVar; i++)
    nrm += state[1][i]*state[1][i];
  if (sqrt(nrm) - radius > 1e-4) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;
  
  cout << "\tnorm(x) - radius= " << sqrt(nrm) - radius << endl;
  cout << "\tpred = " << pred << endl;
  
  // simple tests of SteihaugCG
  x = 0.0;
  pass = true;
  no_prec = true;
  solver = new kona::STCGSolver<PDEVector>();
  solver->SubspaceSize(m);
  input_params.put("tol", 1e-3);
  radius = 100.0;
  input_params.put("radius", radius);
  solver->Solve(input_params, b, x, *mat_vec, *precond, output_params, fout);
  //kona::SteihaugCG(m, tol, radius, b, x, *mat_vec, *precond, iters, pred, active,
  //                 fout, check_res);
  cout << "\tx = ";
  for (int i = 0; i < nVar; i++)
    cout << state[1][i] << " ";
  cout << endl;

  cout << "\ttesting SteihaugCG solution norm is less than radius...";
  pass = true;
  x = 0.0;
  input_params.put("tol", 1e-3);
  radius = 1.0;
  input_params.put("radius", radius);
  solver->Solve(input_params, b, x, *mat_vec, *precond, output_params, fout);
  nrm = 0.0;
  for (int i = 0; i < nVar; i++)
    nrm += state[1][i]*state[1][i];
  if (sqrt(nrm) - radius > sqrt(kona::kEpsilon)) pass = false;
  writeOutcome(pass);
  if (!pass) numfail++;
  
  cout << "\tnorm(x) - radius= " << sqrt(nrm) - radius << endl;  

  // test of Lanczos; computes the largest magnitude eigenvalue of mat
  tol = 1.e-3;
  double max_eig = kona::Lanczos<PDEVector>(m, tol, *mat_vec, iters, fout);
  cout << "\ttesting Lanczos method for computing max eigenvalue...";
  pass = true;
  if (fabs(max_eig - 3.979642883761865) > tol*max_eig) pass = false;  
  writeOutcome(pass);
  //cout << "\tmax eigenvalue estimate = " << max_eig << endl;
  if (!pass) numfail++;
  
  // clean up memory
  for (int i = 0; i < nVar; i++)
    delete [] mat[i];
  delete [] mat;
  mat = NULL;

  cout << "\tcompleted Krylov unit test: "
       << "number of failures = " << numfail << endl;

  fout.close();
  return numfail;
}

