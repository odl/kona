# makefile for Kona library
SHELL = /bin/sh

################################################################################
#                            USER DEFINED OPTIONS                              #
################################################################################

# set Python binaries
PYTHON = python
PYTHON_CONFIG = python-config

# set C++ compiler
CXX = g++

# compiler options that may vary (user can change)
CXXFLAGS= -g -gdwarf-2 # produce debugging information

# c preprocessor options
CPPFLAGS= -cpp -DNDEBUG # no debug options
#CPPFLAGS= -cpp -DDEBUG # basic debug options
#CPPFLAGS= -cpp -DDEBUG -DVERBOSE_DEBUG # full debug, with detailed information

# BLAS/LAPACK library dir
LAPACK_LIB_DIR = /usr/bin/lib

################################################################################
#                          DO NOT EDIT BELOW THIS LINE                         #
################################################################################

# extract python directory
$(eval PYTHON_HOME = $(shell $(PYTHON_CONFIG) --prefix))
$(eval PYTHON_INCLUDES = $(shell $(PYTHON_CONFIG) --includes))
PYTHON_LIB_DIR = $(PYTHON_HOME)/lib

#BOOST_DIR= /usr/local/include/boost/
#BOOST_DIR= /usr/local/boost_1_55_0

# directories
SRC_DIR=./src
TEST_DIR=./test
BOOST_DIR= ./boost
PYKONA_DIR= ./pykona

# source and object file names
HEADERS= $(wildcard $(SRC_DIR)/*.hpp)
HEADERS_ALL= $(HEADERS)

SOURCES= $(wildcard $(SRC_DIR)/*.cpp)
SOURCES_ALL = $(SOURCES)

OBJS= $(SOURCES:.cpp=.o)
OBJS_ALL= $(OBJS)

BOOST_LIBS= $(BOOST_DIR)/stage/lib/libboost_program_options.so \
	$(BOOST_DIR)/stage/lib/libboost_python.so
#BOOST_LIBS_PY= $(BOOST_LIBS) $(BOOST_DIR)/stage/lib/libboost_python.so

.SUFFIXES:
.SUFFIXES: .cpp .o
.PHONY: default clean all test

# linker options
export LD_RUN_PATH=$(CURDIR)/$(BOOST_DIR)/stage/lib:$(CURDIR)
LDFLAGS= -L$(LAPACK_LIB_DIR) -lblas -llapack -L$(CURDIR)/$(BOOST_DIR)/stage/lib -lboost_program_options
PYFLAGS= -L$(PYTHON_LIB_DIR) -lpython2.7 -L$(CURDIR)/$(BOOST_DIR)/stage/lib -lboost_python \
	-L$(CURDIR) -lkona

# options that DO NOT vary
ALL_CXXFLAGS= $(CXXFLAGS) -I. -I$(CURDIR)/$(BOOST_DIR) \
	$(PYTHON_INCLUDES) -fPIC

# implicit rule for building c++ objects
%.o : %.cpp $(HEADERS_ALL) Makefile
	@echo "Compiling \""$@"\" from \""$<"\""
	@$(CXX) $(CPPFLAGS) $(ALL_CXXFLAGS) -o $@ -c $<

# export the variables so the Make done in ./test can use them
export

default: all

all: libkona libpykona test

# for folks who want emacs tags
TAGS: $(HEADERS_ALL) $(SOURCES_ALL)
	@echo "creating TAGS file for emacs"
	@find -maxdepth 2 -iname '*.hpp' -print0 -o \
	-iname '*.cpp' -print0 | xargs -0 etags

# rule for Kona library
libkona: $(BOOST_LIBS) $(OBJS) Makefile
	@echo "Compiling Kona shared library"
	@$(CXX) $(ALL_CXXFLAGS) -shared -Wl,-soname,libkona.so.1 \
	-o libkona.so.1.0 $(OBJS) $(LDFLAGS)
	@ln -s -f libkona.so.1.0 libkona.so
	@ln -s -f libkona.so.1.0 libkona.so.1

libpykona: libkona
	@cd $(PYKONA_DIR) && $(MAKE)

# rule for unit tests
test: libkona
	@cd $(TEST_DIR) && $(MAKE)

# rule for program options and boost python
$(BOOST_LIBS): boost.zip
	@echo "Compiling necessary Boost binaries"
	@unzip boost.zip
	@cd $(BOOST_DIR) && \
	./bootstrap.sh --with-libraries=program_options,python && \
	./b2 cxxflags="-w" stage

clean:
	@echo "deleting temporary, object, and binary files"
	@rm -f $(SOURCES:.cpp=.cpp~)
	@rm -f $(HEADERS:.hpp=.hpp~)
	@rm -f $(OBJS_ALL) TAGS
	@rm -f *~ libkona.*
	@cd $(TEST_DIR) && $(MAKE) clean
	@cd $(PYKONA_DIR) && $(MAKE) clean

clean_boost:
	@echo "removing local Boost installation"
	@rm -rf $(CURDIR)/boost
