/**
 * \file template.cpp
 * \brief blank template for Kona tutorial
 * \author  Jason Hicken <jason.hicken@gmail.com>
 */

#include <assert.h>
#include "user_memory.hpp"
#include "kona.hpp"

using std::string;
using std::map;
using std::cout;
using std::endl;
using std::cerr;

/*!
 * \brief locally defined user function
 * \param[in] request - the type of operation being requested
 * \param[in] leniwrk - the length of the iwrk array
 * \param[in,out] iwrk - on input, an array of vector indices
 * \param[in] lendwrk - the length of the dwrk array
 * \param[in,out] dwrk - on input, an array of scalars
 */
int userFunc(int request, int leniwrk, int *iwrk, int lendwrk,
	     double *dwrk);

int main(int argc, char *argv[]) {

  // Options can be passed into Kona in one of two ways;
  // 1) using the kona.cfg file
  // 2) using a std::map
  // In the case of conflicts, options set using the map take priority
  map<string,string> optns;
  //optns["opt_method"] = "quasi_newton";
  //optns["opt_method"] = "verify";
  //optns["max_iter"] = "100";
  //optns["des_tol"] = "1.e-6"; // tolerance for design component of optimality

  // Call Kona
  KonaOptimize(userFunc, optns);
  
}

// ==============================================================================
int userFunc(int request, int leniwrk, int *iwrk, int lendwrk,
	     double *dwrk) {
  const int nDV = 0; //<-- replace with the dimension of the design vector
  const int nVar = 0; // size of a state vector (empty here)
  const int nCeq = 0; // number of equality constraints (empty here)
  static int numDesign = -1; // local copy of the number of design vectors
  static int numState = -1; // local copy of the number of state vectors
  static int numDual = -1; // local copy of the number of dual vectors
  static double **design;     // memory to act like the design vectors
  static double **state;      // memory to act like the state vectors (if needed)
  static double **dual;       // memory to act like the dual vectors (if needed)
  
  switch (request) {
    case kona::allocmem: {
      // allocate iwrk[0] design vectors, iwrk[1] state vectors, and iwrk[2]
      // dual vectors
      numDesign = iwrk[0];
      numState = iwrk[1];
      numDual = iwrk[2];
      assert(numDesign >= 0);
      assert(numState >= 0);
      assert(numDual >= 0);
      design = new double * [numDesign];
      for (int i = 0; i < numDesign; i++)
        design[i] = new double[nDV];
      state = new double * [numState];
      for (int i = 0; i < numState; i++)
        state[i] = new double[nVar];
      dual = new double * [numDual];
      for (int i = 0; i < numDual; i++)
        dual[i] = new double[nCeq];      
      break;
    }
    case kona::axpby_d: {
      // using design array set iwrk[0] = dwrk[0]*iwrk[1] + dwrk[1]*iwrk[2]
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numDesign));
      assert(j < numDesign);
      assert(k < numDesign);
      
      double scalj = dwrk[0];
      double scalk = dwrk[1];
      if (j == -1) {
        if (k == -1) { // if both indices = -1, then all elements = scalj
          for (int n = 0; n < nDV; n++)
            design[i][n] = scalj;
          
        } else { // if just j = -1 ...
          if (scalk == 1.0) {
            // direct copy of vector k with no scaling
            for (int n = 0; n < nDV; n++)
              design[i][n] = design[k][n];
            
          } else {
            // scaled copy of vector k
            for (int n = 0; n < nDV; n++)
              design[i][n] = scalk*design[k][n];
          }
        }
      } else if (k == -1) { // if just k = -1 ...
        if (scalj == 1.0) {
          // direct copy of vector j with no scaling
          for (int n = 0; n < nDV; n++)
            design[i][n] = design[j][n];
          
        } else {
          // scaled copy of vector j
          for (int n = 0; n < nDV; n++)
            design[i][n] = scalj*design[j][n];
        }
      } else { // otherwise, full axpby
        for (int n = 0; n < nDV; n++)
          design[i][n] = scalj*design[j][n] + scalk*design[k][n];
      }
      break;
    }
    case kona::axpby_s: {
      // using state array set iwrk[0] = dwrk[0]*iwrk[1] + dwrk[1]*iwrk[2]
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numState));
      assert(j < numState);
      assert(k < numState);
      // nothing to do here, because nVar = 0; in general, code that is
      // analogous to axpby_d would be needed
      break;
    }
    case kona::axpby_c: {
      // using dual array set iwrk[0] = dwrk[0]*iwrk[1] + dwrk[1]*iwrk[2]
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numDual));
      assert(j < numDual);
      assert(k < numDual);
      // nothing to do here, because nCeq = 0; in general, code that is
      // analogous to axpby_d would be needed
      break;
    }
    case kona::innerprod_d: {
      // using design array set dwrk[0] = (iwrk[0])^{T} * iwrk[1]
      int i = iwrk[0];
      int j = iwrk[1];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numDesign));
      dwrk[0] = 0.0;
      for (int n = 0; n < nDV; n++)
        dwrk[0] += design[i][n]*design[j][n];
      break;
    } 
    case kona::innerprod_s: {
      // using state array set dwrk[0] = (iwrk[0])^{T} * iwrk[1]
      int i = iwrk[0];
      int j = iwrk[1];
      assert((i >= 0) && (i < numState));
      assert((j >= 0) && (j < numState));
      dwrk[0] = 0.0;      
      // nothing to do here, because nVar = 0; in general, code that is
      // analogous to innerprod_d would be needed
      break;
    }
    case kona::innerprod_c: {
      // using dual array set dwrk[0] = (iwrk[0])^{T} * iwrk[1]
      int i = iwrk[0];
      int j = iwrk[1];
      assert((i >= 0) && (i < numDual));
      assert((j >= 0) && (j < numDual));
      dwrk[0] = 0.0;
      // nothing to do here, because nCeq = 0; in general, code that is
      // analogous to innerprod_d would be needed
      break;
    }      
    case kona::eval_obj: {
      // evaluate objective value
      int i = iwrk[0];
      int j = iwrk[1];
      assert((i >= 0) && (i < numDesign));
      assert((j >= -1) && (j < numState));
      throw(-1); //<-- replace this with your objective evaluation
      break;
    }
    case kona::eval_pde: {
      // evaluate PDE at (design,state) = (iwrk[0],iwrk[1])
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numState));
      // This is not a PDE-constrained problem, so do nothing
      break;
    }
    case kona::eval_ceq: {
      // evaluate the equality constraints
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numDual));
      // There are not equality constraints here
      break;
    }      
    case kona::jacvec_d: {
      // apply design component of the Jacobian-vec
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numDesign));
      assert((m >= 0) && (m < numState));
      // There is no PDE constraint, so no PDE Jacobian
      break;
    }
    case kona::jacvec_s: {
      // apply state component of the Jacobian-vector product to vector iwrk[2]
      int i = iwrk[2];
      int j = iwrk[3];
      assert((i >= 0) && (i < numState));
      assert((j >= 0) && (j < numState));
      // There is no PDE constraint, so no PDE Jacobian
      break;
    }
    case kona::tjacvec_d: {
      // apply design component of Jacobian to adj in iwrk[k]; return in iwrk[m]
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numState));
      assert((m >= 0) && (m < numDesign));
      // There is no PDE constraint, BUT the vector m still needs to be set
      for (int n = 0; n < nDV; n++)
        design[m][n] = 0.0;
      break;
    }
    case kona::tjacvec_s: {
      // apply state component of Jacobian to adj
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numState));
      assert((m >= 0) && (m < numState));
      // There is no PDE constraint, so no PDE Jacobian
      break;
    }
    case kona::eval_precond: {
      // build the preconditioner if necessary
      break;
    } 
    case kona::precond_s: {
      // apply primal preconditioner to iwrk[2]
      int i = iwrk[2];
      int j = iwrk[3];
      assert((i >= 0) && (i < numState));
      assert((j >= 0) && (j < numState));
      // There is no PDE constraint, so no PDE Jacobian
      break;
    }
    case kona::tprecond_s: {
      // apply adjoint preconditioner to iwrk[2]
      int i = iwrk[2];
      int j = iwrk[3];
      assert((i >= 0) && (i < numState));
      assert((j >= 0) && (j < numState));
      // There is no PDE constraint, so no PDE Jacobian
      break;
    }
    case kona::ceqjac_d: {
      // design component of equality constraint Jacobian
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numDesign));
      assert((m >= 0) && (m < numDual));
      // There are not constraints, so no Jacobian
      break;
    }
    case kona::ceqjac_s: {
      // state component of equality constraint Jacobian
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numState));
      assert((m >= 0) && (m < numDual));
      // There are not constraints, so no Jacobian
      break;
    }
    case kona::tceqjac_d: {
      // apply design component of constraint Jac to dual
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numDual));
      assert((m >= 0) && (m < numDesign));
      // There are no constraints, BUT the vector m still needs to be set
      for (int n = 0; n < nDV; n++)
        design[m][n] = 0.0;      
      break;
    }
    case kona::tceqjac_s: {
      // apply state component of constraint Jac to dual
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numDual));
      assert((m >= 0) && (m < numState));
      // There are no constraints, so no Jacobian
      break;
    } 
    case kona::grad_d: {
      // design component of objective gradient
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numDesign));
      throw(-1); //<-- replace with your gradient, evaluated at i, returned in k
      break;
    }
    case kona::grad_s: {
      // state component of objective gradient
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numState));
      // No state, so this component does not exist
      break;
    }
    case kona::initdesign: {
      // intiailize the design variables
      int i = iwrk[0];
      assert((i >= 0) && (i < numDesign));
      throw(-1); //<-- replace with code that sets the initial design
      break;
    }
    case kona::solve: {// solve the primal equations
      iwrk[0] = 1;
      break;
    }
    case kona::adjsolve: {// solve the dual equations
      iwrk[1] = -10; // temporary
      break;
    }
    case kona::info: {
      // This supplies information to the user
      // current design is in iwrk[0]
      // current pde solution is in iwrk[1]
      // current adjoint solution is in iwrk[2]
      int i = iwrk[0];
      // you can display your current solution here if desired
      break;
    }
    default: {
      cerr << "userFunc: "
           << "unrecognized request value: request = "
           << request << endl;
      throw(-1);
      break;
    }
  }
  return 0;
}
