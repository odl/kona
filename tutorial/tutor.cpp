/**
 * \file template.cpp
 * \brief blank template for Kona tutorial
 * \author  Jason Hicken <jason.hicken@gmail.com>
 */

#include <assert.h>
#include "user_memory.hpp"
#include "kona.hpp"

using std::string;
using std::map;
using std::cout;
using std::endl;
using std::cerr;

static int numDesign = -1; // local copy of the number of design vectors
static int numState = -1; // local copy of the number of state vectors
double **design;     // global memory to act like the design vectors
double **state;      // global memory to act like the state vectors
double **mat;        // storage for the matrix
static int func_eval = 0;
static int grad_eval = 0;
static int nDV = 2;
static int nVar = 0;

/*!
 * \brief locally defined user function
 * \param[in] request - the type of operation being requested
 * \param[in] leniwrk - the length of the iwrk array
 * \param[in,out] iwrk - on input, an array of vector indices
 * \param[in] lendwrk - the length of the dwrk array
 * \param[in,out] dwrk - on input, an array of scalars
 */
int userFunc(int request, int leniwrk, int *iwrk, int lendwrk,
       double *dwrk);

int main(int argc, char *argv[]) {

  // Options can be passed into Kona in one of two ways;
  // 1) using the kona.cfg file
  // 2) using a std::map
  // In the case of conflicts, options set using the map take priority
  map<string,string> optns;
  optns["opt_method"] = "quasi_newton";
  // optns["opt_method"] = "verify";
  optns["max_iter"] = "100";
  optns["des_tol"] = "1.e-6"; // tolerance for design component of optimality
  optns["send_info_to_cout"] = "true";

  // Call Kona
  KonaOptimize(userFunc, optns);
  
}

// ==============================================================================
// local user function
int userFunc(int request, int leniwrk, int *iwrk, int lendwrk,
       double *dwrk) {

  switch (request) {
    case kona::allocmem: {// allocate iwrk[0] design vectors and
      // iwrk[1] state vectors
      if (numDesign >= 0) { // free design memory first
        if (design == NULL) {
          cerr << "userFunc: "
               << "design array is NULL but numDesign > 0" << endl;
          throw(-1);
        }
        for (int i = 0; i < numDesign; i++)
          delete [] design[i];
        delete [] design;
      }        
      if (numState >= 0) { // free state memory first
        if (state == NULL) {
          cerr << "userFunc: "
               << "state array is NULL but numState > 0" << endl;
          throw(-1);
        }
        for (int i = 0; i < numState; i++)
          delete [] state[i];
        delete [] state;
      }
      numDesign = iwrk[0];
      numState = iwrk[1];
      assert(numDesign >= 0);
      assert(numState >= 0);
      design = new double * [numDesign];
      for (int i = 0; i < numDesign; i++)
        design[i] = new double[nDV];
      state = new double * [numState];
      for (int i = 0; i < numState; i++)
        state[i] = new double[nVar];
      break;
    }
    case kona::axpby_d: {// using design array set
      // iwrk[0] = dwrk[0]*iwrk[1] + dwrk[1]*iwrk[2]
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numDesign));
      assert(j < numDesign);
      assert(k < numDesign);
      
      double scalj = dwrk[0];
      double scalk = dwrk[1];
      if (j == -1) {
        if (k == -1) { // if both indices = -1, then all elements = scalj
          for (int n = 0; n < nDV; n++)
            design[i][n] = scalj;
          
        } else { // if just j = -1 ...
          if (scalk == 1.0) {
            // direct copy of vector k with no scaling
            for (int n = 0; n < nDV; n++)
              design[i][n] = design[k][n];
            
          } else {
            // scaled copy of vector k
            for (int n = 0; n < nDV; n++)
              design[i][n] = scalk*design[k][n];
          }
        }
      } else if (k == -1) { // if just k = -1 ...
        if (scalj == 1.0) {
          // direct copy of vector j with no scaling
          for (int n = 0; n < nDV; n++)
            design[i][n] = design[j][n];
          
        } else {
          // scaled copy of vector j
          for (int n = 0; n < nDV; n++)
            design[i][n] = scalj*design[j][n];
        }
      } else { // otherwise, full axpby
        for (int n = 0; n < nDV; n++)
          design[i][n] = scalj*design[j][n] + scalk*design[k][n];
      }
      break;
    }
    case kona::axpby_s: {// using state array set
      // iwrk[0] = dwrk[0]*iwrk[1] + dwrk[1]*iwrk[2]
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numState));
      assert(j < numState);
      assert(k < numState);

      double scalj = dwrk[0];
      double scalk = dwrk[1];
      if (j == -1) {
        if (k == -1) { // if both indices = -1, then all elements = scalj
          for (int n = 0; n < nVar; n++)
            state[i][n] = scalj;

        } else { // if just j = -1 ...
          if (scalk == 1.0) {
            // direct copy of vector k with no scaling
            for (int n = 0; n < nVar; n++)
              state[i][n] = state[k][n];

          } else {
            // scaled copy of vector k
            for (int n = 0; n < nVar; n++)
              state[i][n] = scalk*state[k][n];
          }
        }
      } else if (k == -1) { // if just k = -1 ...
        if (scalj == 1.0) {
          // direct copy of vector j with no scaling
          for (int n = 0; n < nVar; n++)
            state[i][n] = state[j][n];

        } else {
          // scaled copy of vector j
          for (int n = 0; n < nVar; n++)
            state[i][n] = scalj*state[j][n];
        }
      } else { // otherwise, full axpby
        for (int n = 0; n < nVar; n++)
          state[i][n] = scalj*state[j][n] + scalk*state[k][n];
      }
      break;
    }
    case kona::innerprod_d: {// using design array set
      // dwrk[0] = (iwrk[0])^{T} * iwrk[1]
      int i = iwrk[0];
      int j = iwrk[1];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numDesign));
      dwrk[0] = 0.0;
      for (int n = 0; n < nDV; n++)
        dwrk[0] += design[i][n]*design[j][n];
      break;
    } 
    case kona::innerprod_s: {// using state array set
      // dwrk[0] = (iwrk[0])^{T} * iwrk[1]
      int i = iwrk[0];
      int j = iwrk[1];
      assert((i >= 0) && (i < numState));
      assert((j >= 0) && (j < numState));
      dwrk[0] = 0.0;
      for (int n = 0; n < nVar; n++)
        dwrk[0] += state[i][n]*state[j][n];
      break;
    }
    case kona::eval_obj: {// evaluate objective value
      int i = iwrk[0];
      int j = iwrk[1];
      assert((i >= 0) && (i < numDesign));
      assert((j >= -1) && (j < numState));
      double x = design[i][0];
      double y = design[i][1];
      dwrk[0] = (1.0 - x)*(1.0 - x) + 100.0*(y - x*x)*(y - x*x);
      func_eval++;
      iwrk[0] = 0;
      break;
    }
    case kona::eval_pde: {// evaluate PDE at (design,state) =
                         // (iwrk[0],iwrk[1])
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numState));
      for (int n = 0; n < nVar; n++) {
        state[k][n] = 0.0;
      }
      break;
    }
    case kona::jacvec_d: {// apply design component of the Jacobian-vec
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numDesign));
      assert((m >= 0) && (m < numState));
      for (int n = 0; n < nVar; n++) {
        state[m][n] = 0.0;
      }
      break;
    }
    case kona::jacvec_s: {// apply state component of the Jacobian-vector
      // product to vector iwrk[2] ... do nothing since no Jacobian
      // recall; iwrk[0], iwrk[1] denote where Jacobian is evaluated
      // and in this case, they are not needed
      int i = iwrk[2];
      int j = iwrk[3];
      assert((i >= 0) && (i < numState));
      assert((j >= 0) && (j < numState));
      for (int n = 0; n < nVar; n++) {
        state[j][n] = 0.0;
      }
      break;
    }
    case kona::tjacvec_d: {// apply design component of Jacobian to adj
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numState));
      assert((m >= 0) && (m < numDesign));
      for (int n = 0; n < nDV; n++) {
        design[m][n] = 0.0;
      }
      break;
    }
    case kona::tjacvec_s: {// apply state component of Jacobian to adj
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      int m = iwrk[3];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numState));
      assert((m >= 0) && (m < numState));
      for (int n = 0; n < nVar; n++) {
        design[m][n] = 0.0;
      }
      break;
    }
    case kona::eval_precond: {// build the preconditioner if necessary
      break;
    } 
    case kona::precond_s: {// apply primal preconditioner to iwrk[2]
      // do nothing since no Jacobian
      // recall; iwrk[0], iwrk[1] denote where preconditioner is
      // evaluated and in this case, they are not needed
      int i = iwrk[2];
      int j = iwrk[3];
      assert((i >= 0) && (i < numState));
      assert((j >= 0) && (j < numState));
      // do-nothing preconditioner
      for (int n = 0; n < nVar; n++) {
        state[j][n] = state[i][n];
      }
      iwrk[0] = 0;            
      break;
    }
    case kona::tprecond_s: {// apply adjoint preconditioner to iwrk[2]
      int i = iwrk[2];
      int j = iwrk[3];
      assert((i >= 0) && (i < numState));
      assert((j >= 0) && (j < numState));
      // do-nothing preconditioner
      for (int n = 0; n < nVar; n++) {
        state[j][n] = state[i][n];
      }
      iwrk[0] = 0;
      break;
    }
    case kona::grad_d: {// design component of objective gradient
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numDesign));
      double x = design[i][0];
      double y = design[i][1];
      //design[k][0] = 2.0*x;
      //design[k][1] = 2.0*y;
      design[k][0] = -2.0*(1.0 - x) - 400.0*x*(y - x*x);
      design[k][1] = 200.0*(y - x*x);
      grad_eval++;
      cout << "grad_eval = " << grad_eval << endl;
      break;
    }
    case kona::grad_s: {// state component of objective gradient
      int i = iwrk[0];
      int j = iwrk[1];
      int k = iwrk[2];
      assert((i >= 0) && (i < numDesign));
      assert((j >= 0) && (j < numState));
      assert((k >= 0) && (k < numState));
      break;
    }
    case kona::initdesign: {// intiailize the design variables
      int i = iwrk[0];
      assert((i >= 0) && (i < numDesign));
      design[i][0] = -1.2;
      design[i][1] = 1.0;
      break;
    }
    case kona::solve: {// solve the primal equations
      iwrk[0] = 0;
      break;
    }
    case kona::adjsolve: {// solve the dual equations
      iwrk[0] = 0;
      break;
    }
    case kona::info: {// supplies information to user
      // current design is in iwrk[0]
      // current pde solution is in iwrk[1]
      // current adjoint solution is in iwrk[2]
      int i = iwrk[0];
      cout << "current design = (" << design[i][0] << "," 
           << design[i][1] << ")" << endl;
      break;
    }
    default: {
      cerr << "userFunc: "
           << "unrecognized request value: request = "
           << request << endl;
      throw(-1);
      break;
    }
  }
  return 0;
}

void writeOutcome(const bool & pass) {
  if (pass) {
    cout << "pass" << endl;
  } else {
    cout << "fail" << endl;
  }
}
