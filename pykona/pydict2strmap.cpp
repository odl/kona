#include "pydict2strmap.hpp"

using std::string;
using std::map;
using std::cout;
using std::endl;

map<string, string> pydict2strmap(bp::dict& py_dict)
{
    map<string, string> strmap;
    bp::list keys = py_dict.keys();
    for (int i = 0; i < len(keys); ++i)
    {
        bp::extract<string> extracted_key(keys[i]);
        if(!extracted_key.check())
        {
            cout << 
            "PyDict2StrMap: Key invalid, map might be incomplete!" 
            << endl;  
            continue;                 
        }  
        string key = extracted_key;
        bp::extract<string> extracted_val(py_dict[key]);
        if(!extracted_val.check())
        {
            cout << 
            "PyDict2StrMap: Value invalid, map might be incomplete!" 
            << endl;
            continue;
        }
        string value = extracted_val;
        strmap[key] = value;
    }
    return strmap;
};