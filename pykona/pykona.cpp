#include "pykona.hpp"
#include "pydict2strmap.hpp"

using std::cerr;
using std::cout;
using std::endl;
using std::string;
using std::map;
namespace bp = boost::python;

bp::object PyKona::solver_;
static int num_design_vec = -1; // local copy of the number of design vectors
static int num_state_vec = -1; // local copy of the number of state vectors
static int num_ceq_vec = -1; // local copy of the number of dual vectors
static map<string, string> optns;

int PyKona::Toolbox(int request, int leniwrk, int *iwrk, int lendwrk, 
                    double *dwrk)
{
    switch (request) {
        case kona::rank: {
            iwrk[0] = bp::extract<int>(solver_.attr("get_rank")());
            break;
        }
        
        case kona::allocmem: {
            // allocate iwrk[0] design vectors, iwrk[1] state vectors, and iwrk[2]
            // dual vectors
            num_design_vec = iwrk[0];
            num_state_vec = iwrk[1];
            num_ceq_vec = iwrk[2];
            assert(num_design_vec >= 0);
            assert(num_state_vec >= 0);
            assert(num_ceq_vec >= 0);
            solver_.attr("alloc_memory")(num_design_vec, num_state_vec, 
                                          num_ceq_vec);
            break;
        }
        case kona::axpby_d: {
            // using design array set iwrk[0] = dwrk[0]*iwrk[1] + dwrk[1]*iwrk[2]
            int i = iwrk[0];
            int j = iwrk[1];
            int k = iwrk[2];
            assert(i >= 0);
            assert(i < num_design_vec);
            assert(j < num_design_vec);
            assert(k < num_design_vec);
            solver_.attr("axpby_d")(dwrk[0], j, dwrk[1], k, i);
            break;
        }
        case kona::axpby_s: {
            // using state array set iwrk[0] = dwrk[0]*iwrk[1] + dwrk[1]*iwrk[2]
            int i = iwrk[0];
            int j = iwrk[1];
            int k = iwrk[2];
            assert((i >= 0) && (i < num_state_vec));
            assert(j < num_state_vec);
            assert(k < num_state_vec);
            solver_.attr("axpby_s")(dwrk[0], j, dwrk[1], k, i);
            break;
        }
        case kona::axpby_c: {
            // using dual array set iwrk[0] = dwrk[0]*iwrk[1] + dwrk[1]*iwrk[2]
            int i = iwrk[0];
            int j = iwrk[1];
            int k = iwrk[2];
            assert((i >= 0) && (i < num_ceq_vec));
            assert(j < num_ceq_vec);
            assert(k < num_ceq_vec);
            solver_.attr("axpby_ceq")(dwrk[0], j, dwrk[1], k, i);
            break;
        }
        case kona::innerprod_d: {
            // using design array set dwrk[0] = (iwrk[0])^{T} * iwrk[1]
            int i = iwrk[0];
            int j = iwrk[1];
            assert((i >= 0) && (i < num_design_vec));
            assert((j >= 0) && (j < num_design_vec));
            dwrk[0] = bp::extract<double>(solver_.attr("inner_prod_d")(i, j));
            break;
        } 
        case kona::innerprod_s: {
            // using state array set dwrk[0] = (iwrk[0])^{T} * iwrk[1]
            int i = iwrk[0];
            int j = iwrk[1];
            assert((i >= 0) && (i < num_state_vec));
            assert((j >= 0) && (j < num_state_vec));
            dwrk[0] = bp::extract<double>(solver_.attr("inner_prod_s")(i, j));
            break;
        }
        case kona::innerprod_c: {
            // using dual array set dwrk[0] = (iwrk[0])^{T} * iwrk[1]
            int i = iwrk[0];
            int j = iwrk[1];
            assert((i >= 0) && (i < num_ceq_vec));
            assert((j >= 0) && (j < num_ceq_vec));
            dwrk[0] = bp::extract<double>(solver_.attr("inner_prod_ceq")(i, j));
            break;
        }
        case kona::restrict_d: {// restrict design vector to a subspace
            int i = iwrk[0];
            int type = iwrk[1];
            solver_.attr("restrict_design")(type, i);
            break;
        }
        case kona::convert_d: { // convert dual vector to target vector subspace
            int i = iwrk[0];
            int j = iwrk[1];
            assert((i >= 0) && (i < num_design_vec));
            assert((j >= 0) && (j < num_ceq_vec));
            solver_.attr("copy_dual_to_targstate")(j, i);   
            break;
        }
        case kona::convert_c: { // convert design target subspace to dual
            int i = iwrk[0];
            int j = iwrk[1];
            assert((i >= 0) && (i < num_ceq_vec));
            assert((j >= 0) && (j < num_design_vec));
            solver_.attr("copy_targstate_to_dual")(j, i);
            break;
        }
        case kona::eval_obj: {
            // evaluate objective value
            int i = iwrk[0];
            int j = iwrk[1];
            assert((i >= 0) && (i < num_design_vec));
            assert((j >= -1) && (j < num_state_vec));
            const bp::tuple out = bp::extract<bp::tuple>(
                solver_.attr("eval_obj")(i, j));
            dwrk[0] = bp::extract<double>(out[0]); // save obj fun value
            iwrk[0] = bp::extract<int>(out[1]); // update precond counts
            break;
        }
        case kona::eval_pde: {
            // evaluate PDE at (design,state) = (iwrk[0],iwrk[1])
            int i = iwrk[0];
            int j = iwrk[1];
            int k = iwrk[2];
            assert((i >= 0) && (i < num_design_vec));
            assert((j >= 0) && (j < num_state_vec));
            assert((k >= 0) && (k < num_state_vec));
            solver_.attr("eval_residual")(i, j, k);
            break;
        }
        case kona::eval_ceq: {
            // evaluate the equality constraints
            int i = iwrk[0];
            int j = iwrk[1];
            int k = iwrk[2];
            assert((i >= 0) && (i < num_design_vec));
            assert((j >= 0) && (j < num_state_vec));
            assert((k >= 0) && (k < num_ceq_vec));
            solver_.attr("eval_ceq")(i, j, k);
            break;
        }      
        case kona::jacvec_d: {
            // apply design component of the Jacobian-vec
            int i = iwrk[0];
            int j = iwrk[1];
            int k = iwrk[2];
            int m = iwrk[3];
            assert((i >= 0) && (i < num_design_vec));
            assert((j >= 0) && (j < num_state_vec));
            assert((k >= 0) && (k < num_design_vec));
            assert((m >= 0) && (m < num_state_vec));
            solver_.attr("multiply_jac_d")(i, j, k, m);
            break;
        }
        case kona::jacvec_s: {
            // apply state component of the Jacobian-vector product to vector iwrk[2]
            int i = iwrk[0];
            int j = iwrk[1];
            int k = iwrk[2];
            int m = iwrk[3];
            assert((i >= 0) && (i < num_design_vec));
            assert((j >= 0) && (j < num_state_vec));
            assert((i >= 0) && (i < num_state_vec));
            assert((j >= 0) && (j < num_state_vec));
            solver_.attr("multiply_jac_s")(i, j, k, m);
            break;
        }
        case kona::tjacvec_d: {
            // apply design component of Jacobian to adj in iwrk[k]; return in iwrk[m]
            int i = iwrk[0];
            int j = iwrk[1];
            int k = iwrk[2];
            int m = iwrk[3];
            assert((i >= 0) && (i < num_design_vec));
            assert((j >= 0) && (j < num_state_vec));
            assert((k >= 0) && (k < num_state_vec));
            assert((m >= 0) && (m < num_design_vec));
            solver_.attr("multiply_tjac_d")(i, j, k, m);
            break;
        }
        case kona::tjacvec_s: {
            // apply state component of Jacobian to adj
            int i = iwrk[0];
            int j = iwrk[1];
            int k = iwrk[2];
            int m = iwrk[3];
            assert((i >= 0) && (i < num_design_vec));
            assert((j >= 0) && (j < num_state_vec));
            assert((k >= 0) && (k < num_state_vec));
            assert((m >= 0) && (m < num_state_vec));
            solver_.attr("multiply_tjac_s")(i, j, k, m);
            break;
        }
        case kona::eval_precond: {
            solver_.attr("build_precond")();
            break;
        } 
        case kona::precond_s: {
            // apply primal preconditioner to iwrk[2]
            int i = iwrk[0]; // design index
            int j = iwrk[1]; // state index
            int k = iwrk[2]; // input vector
            int m = iwrk[3]; // output vector
            assert((i >= 0) && (i < num_design_vec));
            assert((j >= 0) && (j < num_state_vec));
            assert((k >= 0) && (k < num_state_vec));
            assert((m >= 0) && (m < num_state_vec));
            iwrk[0] = bp::extract<int>(
                solver_.attr("multiply_precond")(i, j, k, m));
            break;
        }
        case kona::tprecond_s: {
            // apply adjoint preconditioner to iwrk[2]
            int i = iwrk[0]; // design index
            int j = iwrk[1]; // state index
            int k = iwrk[2]; // input vector
            int m = iwrk[3]; // output vector
            assert((i >= 0) && (i < num_design_vec));
            assert((j >= 0) && (j < num_state_vec));
            assert((k >= 0) && (k < num_state_vec));
            assert((m >= 0) && (m < num_state_vec));
            iwrk[0] = bp::extract<int>(
                solver_.attr("multiply_tprecond")(i, j, k, m));
            break;
        }
        case kona::ceqjac_d: {
            // design component of equality constraint Jacobian
            int i = iwrk[0];
            int j = iwrk[1];
            int k = iwrk[2];
            int m = iwrk[3];
            assert((i >= 0) && (i < num_design_vec));
            assert((j >= 0) && (j < num_state_vec));
            assert((k >= 0) && (k < num_design_vec));
            assert((m >= 0) && (m < num_ceq_vec));
            solver_.attr("multiply_ceqjac_d")(i, j, k, m);
            break;
        }
        case kona::ceqjac_s: {
            // state component of equality constraint Jacobian
            int i = iwrk[0];
            int j = iwrk[1];
            int k = iwrk[2];
            int m = iwrk[3];
            assert((i >= 0) && (i < num_design_vec));
            assert((j >= 0) && (j < num_state_vec));
            assert((k >= 0) && (k < num_state_vec));
            assert((m >= 0) && (m < num_ceq_vec));
            solver_.attr("multiply_ceqjac_s")(i, j, k, m);
            break;
        }
        case kona::tceqjac_d: {
            // apply design component of constraint Jac to dual
            int i = iwrk[0];
            int j = iwrk[1];
            int k = iwrk[2];
            int m = iwrk[3];
            assert((i >= 0) && (i < num_design_vec));
            assert((j >= 0) && (j < num_state_vec));
            assert((k >= 0) && (k < num_ceq_vec));
            assert((m >= 0) && (m < num_design_vec));
            solver_.attr("multiply_tceqjac_d")(i, j, k, m);
            break;
        }
        case kona::tceqjac_s: {
            // apply state component of constraint Jac to dual
            int i = iwrk[0];
            int j = iwrk[1];
            int k = iwrk[2];
            int m = iwrk[3];
            assert((i >= 0) && (i < num_design_vec));
            assert((j >= 0) && (j < num_state_vec));
            assert((k >= 0) && (k < num_ceq_vec));
            assert((m >= 0) && (m < num_state_vec));
            solver_.attr("multiply_tceqjac_s")(i, j, k, m);
            break;
        } 
        case kona::grad_d: {
            // design component of objective gradient
            int i = iwrk[0];
            int j = iwrk[1];
            int k = iwrk[2];
            assert((i >= 0) && (i < num_design_vec));
            assert((j >= 0) && (j < num_state_vec));
            assert((k >= 0) && (k < num_design_vec));
            solver_.attr("eval_grad_d")(i, j, k);
            break;
        }
        case kona::grad_s: {
            // state component of objective gradient
            int i = iwrk[0];
            int j = iwrk[1];
            int k = iwrk[2];
            assert((i >= 0) && (i < num_design_vec));
            assert((j >= 0) && (j < num_state_vec));
            assert((k >= 0) && (k < num_state_vec));
            solver_.attr("eval_grad_s")(i, j, k);
            break;
        }
        case kona::initdesign: {
            // intiailize the design variables
            int i = iwrk[0];
            assert((i >= 0) && (i < num_design_vec));
            solver_.attr("init_design")(i);
            break;
        }
        case kona::solve: {// solve the primal equations
            int i = iwrk[0];
            int j = iwrk[1];
            assert((i >= 0) && (i < num_design_vec));
            assert((j >= 0) && (j < num_state_vec));
            iwrk[0] = bp::extract<int>(solver_.attr("solve_system")(i, j));
            break;
        }
        case kona::linsolve: {// solve the linearized system equations
            int i = iwrk[0];
            int j = iwrk[1];
            int k = iwrk[2];
            int m = iwrk[3];
            double tol = dwrk[0];
            assert((i >= 0) && (i < num_design_vec));
            assert((j >= 0) && (j < num_state_vec));
            assert((k >= -1) && (k < num_state_vec));
            assert((m >= 0) && (k < num_state_vec));
            // pass to iwrk[0] # of preconditioner calls
            iwrk[0] = bp::extract<int>(
                solver_.attr("solve_linearsys")(i, j, k, tol, m));
            break;
        }
        case kona::adjsolve: {// solve the adjoint equations
            int i = iwrk[0];
            int j = iwrk[1];
            int k = iwrk[2];
            int m = iwrk[3];
            double tol = dwrk[0];
            assert((i >= 0) && (i < num_design_vec));
            assert((j >= 0) && (j < num_state_vec));
            assert((k >= -1) && (k < num_state_vec));
            assert((m >= 0) && (k < num_state_vec));
            iwrk[0] = bp::extract<int>(
                solver_.attr("solve_adjoint")(i, j, k, tol, m));
            // pass to iwrk[0] # of preconditioner calls
            if (iwrk[2] != -1) iwrk[1] = -10; // Temporary during transition
            break;
        }
        case kona::info: {
            // This supplies information to the user
            // current design is in iwrk[0]
            // current pde solution is in iwrk[1]
            // current adjoint solution is in iwrk[2]
            // dual index is in iwrk[3]
            // number of iterations are in iwrk[4]
            int i = iwrk[0];
            int j = iwrk[1];
            int k = iwrk[2];
            int m = iwrk[3];
            int iter = iwrk[4];
            assert((i >= 0) && (i < num_design_vec));
            if (num_state_vec > 0) {
                assert((j >= 0) && (j < num_state_vec));
                assert((k >= 0) && (k < num_state_vec));
            }
            if (num_ceq_vec > 0) assert((m >= 0) && (m < num_ceq_vec));
            // you can display your current solution here if desired
            solver_.attr("user_info")(i, j, k, m, iter);
            break;
        }
        default: {
            cerr << "userFunc: "
               << "unrecognized request value: request = "
               << request << endl;
            throw(-1);
            break;
        }
    }
    return 0;
};

void PyKona::Configure(bp::object pyoptns)
{
    bp::dict pydict = bp::extract<bp::dict>(pyoptns);
    optns = pydict2strmap(pydict);
    map<string, string>::iterator pos;
};

void PyKona::Run(bp::object solver)
{
    solver_ = solver;
    KonaOptimize(Toolbox, optns);
};

