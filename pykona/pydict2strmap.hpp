#include <map>
#include <string>
#include <iostream>
#include <boost/python.hpp>

using std::string;
using std::map;
namespace bp = boost::python;

map<string, string> pydict2strmap(bp::dict& py_dict);