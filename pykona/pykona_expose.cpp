#include "pykona.hpp"

/*!
 * \brief exposes the PyKona optimization object to Python as a module
 */
BOOST_PYTHON_MODULE(libpykona)
{
    namespace bp = boost::python;
    bp::class_< PyKona, boost::shared_ptr<PyKona> >("Optimizer", bp::init<>())
        // Kona configurator function that takes in a Python dictionary
        .def("configure", &PyKona::Configure)
            .staticmethod("configure")
        // main optimization function call
        .def("run", &PyKona::Run)
            .staticmethod("run")
    ;
};