import pickle
import time

import numpy as np
import pykona

import pdb


def testOptimize(gx, gy, nlevel):

	kona_start = time.time()

	#------------------ Preparing ---------------
	nx = gx + 1
	ny = gy + 1
	caseid = str(gx) + "_" + str(gy) + "_" + str(nlevel) + "noP"    #  this time for no precondiitoner!! 
	info_file = "opt_" + caseid + ".dat"
	hist_file = "opt_" + caseid + "_hist.dat"


	x_control = np.linspace(0.0, 1.0, num = nx)[1:-1] 
	T_control = x_control * (1 - x_control)

	init_array = np.zeros(len(x_control))

	solver = pykona.examples.inversePoisson()
	solver.setup(nx, ny, nlevel, x_control, T_control, init_array)

	optns = {
	'opt_method' : 'trust_reduced',
	'des_tol' : '1.e-6',
	'krylov.solver' : 'cg',                        
	'reduced.precond': 'none',              
	'info_file' : info_file,
	'hist_file' : hist_file
	}

	#------------------- Solving ----------------
	kona = pykona.Optimizer()
	kona.configure(optns)
	kona.run(solver)

	#-------------- Post-Processing ------------------
	#-------------- Saving everything, including setup stage cost --------------

	kona_time = time.time() - kona_start

	whole = {'setup_precond': solver.setup_precond, 'setup_time': solver.setup_time, 
	'error_norm': solver.error_norm, 
	'precond_calls': solver.precond_calls_total, 
	'kona_time': kona_time, 
	'time_solve_state' : solver.time_solve_state,
	'solve_state_count' : solver.solve_calls, 'Kv_count' : solver.Kvcount,
	'time_solve_system' : solver.time_solve_system, 
	'time_solve_adjoint' : solver.time_solve_adjoint, 
	'time_solve_linsys' : solver.time_solve_linsys,
	'kona_time_precond' : solver.time_kona_precond,    # multiply_precond
	'kona_time_jacS' : solver.time_kona_jacS}		   # jacS	

	with open(caseid, 'wb') as f:
		pickle.dump(whole, f)
	f.close()

def main():

	nlevel = 4

	# gx = 64      # 64, 128, 256, 512, 1024
	# gy = gx
	# testOptimize( gx, gy, nlevel)

	# gx = 128    
	# gy = gx
	# testOptimize( gx, gy, nlevel)

	gx = 256    
	gy = gx
	testOptimize( gx, gy, nlevel)

	# gx = 512    
	# gy = gx
	# testOptimize( gx, gy, nlevel)

	# gx = 1024    
	# gy = gx
	# testOptimize( gx, gy, nlevel)

	# #-------- clear stuff -----------
	# this = sys.modules[__name__]
	# for n in dir():
	# 	if n[0]!='_': delattr(this, n)
	# #--------------------------------

if __name__ == '__main__':
	main()