import unittest
import numpy
import pykona

class MDO_MDFTests(unittest.TestCase):

    def testOptimize(self):
        alpha = 0.5
        mu = 0.25
        nState = 20
        nDesign = 10
        init_design = 5.*numpy.ones(nDesign)
        solver = pykona.examples.MDO_MDF()
        solver.configure(alpha, mu, nState, init_design)
        optns = {
            # Available optimization algorithms:
            # verify | quasi_newton | lnks | aug_lag | inexact
            # reduced | cnstr_reduced | trust_reduced
            'opt_method' : 'trust_reduced',
            'des_tol' : '1.e-4',
            'ceq_tol' : '1.e-4',
            'max_iter' : '100',
            'send_info_to_cout' : 'true',
            # verification run settings
            'verify.design_vec' : 'true',
            'verify.gradient' : 'true',
            'verify.pde_vec' : 'true',
            'verify.jacobian' : 'true',
            'verify.dual_vec' : 'false',
            'verify.cnstr_jac' : 'false',
            'verify.red_grad' : 'true',
            'verify.linear_solve' : 'true',
            'verify.hessian_prod' : 'true',
            # inner iterations
            'inner.max_iter' : '10',
            'inner.lambda_init' : '0.0',
            'inner.des_tol' : '0.1',
            # Available krylov system solvers (used to solve KKT system)
            # fitr | minres | cg | fisqp | ffom | ffom_smart | trisqp | fgmres
            'krylov.solver' : 'cg',
            'krylov.space_size' : '10',
            'krylov.tolerance' : '0.5',
            'krylov.check_res' : 'true',
            # quasi-newton algorithm settings
            'quasi_newton.type' : 'lbfgs',
            'quasi_newton.max_stored' : '10',
            # Available preconditioners for the reduced algorithms:
            # none | quasi_newton | nested_krylov | idf_schur
            'reduced.precond' : 'none',
            'reduced.product_fac' : '0.5',
            'reduced.krylov_size' : '20',
            # trust region settings (applicable to reduced algorithms)
            'trust.init_radius' : '10.0',
            'trust.max_radius' : '10.0',
            # augmented lagrangian settings
            'aug_lag.mu_init' : '10.0',
            'aug_lag.mu_pow' : '0.5',
            # inexact newton-krylov algorithm settings
            'inexact.mu' : '1e-8'
        }
        kona = pykona.Optimizer()
        kona.configure(optns)
        kona.run(solver)
        error_design = numpy.linalg.norm(solver.current_design)
        error_state = numpy.linalg.norm(solver.current_state)
        self.failUnless(error_design<=10**(-2) and error_state<=10**(-2))

def main():
    unittest.main()

if __name__ == '__main__':
    main()
