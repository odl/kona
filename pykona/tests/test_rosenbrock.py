import unittest
import numpy as np
import pykona

# Here's our "unit tests".
class IsOddTests(unittest.TestCase):

    def testObj(self):
        solver = pykona.examples.Rosenbrock(2)
        solver.x = np.array([1,1])
        solver.calc_obj()
        self.failUnless(abs(solver.obj)<=10**(-5))

    def testGrad(self):
        solver = pykona.examples.Rosenbrock(2)
        solver.x = np.array([1,1])
        solver.calc_grad()
        self.failUnless(np.linalg.norm(solver.grad_d)<=10**(-5))

    def testOptimize(self):
        solver = pykona.examples.Rosenbrock(2)
        kona = pykona.Optimizer()
        kona.run(solver)
        error = np.linalg.norm(solver.x - np.array([1,1]))
        self.failUnless(error<=10**(-3))

def main():
    unittest.main()

if __name__ == '__main__':
    main()
