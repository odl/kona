Objective Function Factory
==========================

PyKona's user interface is built on the programming concept of inheritance. The 
user_template.py module documented below offers a base class that is intended 
to be an inherited template for all specific optimization problems that the 
user needs to solve.

The intention is for PyKona to be scalable to the size and complexity of the 
problem. This base class includes basic linear algebra capabilities built on 
top of NumPy, allowing the user to construct analytical objective functions 
without the need to provide any storage or mathematical functionality beyond 
what PyKona already implements. Simultaneously, advanced useds can derive more 
involved solver objects that interface with their own PDE solvers and linear 
algebra packages. This allows Kona to perform optimization with parallelized 
solver objects without actually implementing any parallelization within itself.

UserTemplate
------------

.. automodule:: usertemplate

.. currentmodule:: usertemplate

.. autoclass:: UserTemplate
   :members:













