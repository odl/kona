.. PyKona documentation master file, created by
   sphinx-quickstart on Sat May 10 01:33:15 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyKona's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 2

   includeme

   obj_fun_factory

   examples

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

