Example Derived Classes
=======================

Rosenbrock Equation
-------------------

.. automodule:: rosenbrock

.. currentmodule:: rosenbrock

.. autoclass:: Rosenbrock
   :members:
   :show-inheritance:

Non-Linear System Dependent Objective Function
----------------------------------------------

The optimization problem is defined below:

.. math::
    :nowrap:

    \begin{gather*}
    \underset{x}{min} \quad \mathcal{J} = \begin{pmatrix}u^T & w^T\end{pmatrix} 
    D \begin{pmatrix}u \\ w\end{pmatrix} \\
    \text{subject to} \quad \begin{bmatrix}H & 0 \\ 0 & H\end{bmatrix}
    \begin{bmatrix}(1-\mu)A(u) & -\mu A(w) \\ \mu A(u) & (1-\mu)A(w)\end{bmatrix}
    \begin{bmatrix}H & 0 \\ 0 & H\end{bmatrix} \begin{pmatrix}u \\ w
    \end{pmatrix} = \begin{pmatrix}bx_1 \\ bx_2\end{pmatrix}
    \end{gather*}

where :math:`\mu \geq 0` is a parameter and

    - :math:`x \in \Re^2` is the design variable

    - :math:`u, w \in \Re^m` are the state variables, and their dimension, 
      :math:`m`, can be varied by a user input. **This size must be a power of 
      2**; necessitated by the Hadamard matrix :math:`H` described below.

    - The matrix :math:`H \in \Re^{m \times m}` is a Hadamard matrix. More 
      information on this matrix can be found at 
      (http://en.wikipedia.org/wiki/Hadamard_matrix). For :math:`m = 2^p`, 
      :math:`H` can be formed explicitly using Sylvester's construction.

    - The diagonal matrix :math:`A(v) \in \Re^{m \times m}` is a function of 
      its argument :math:`v \in \Re^m`, and is given by

    .. math::

        A(v) = D \left[ \left( 1 - \alpha \right) I + 
        \alpha \mathrm{diag}(\hat{v}_1^2, \hat{v}_2^2, \dots ,\hat{v}_m^2) 
        \right]

    where :math:`\hat{v} = Hv` and :math:`\alpha \geq 0` is a parameter.

    - The diagonal matrix :math:`D \in \Re^{m \times m}` is given by

    .. math::
        
        D = \mathrm{diag}(\lambda_1, \lambda_2, \dots , \lambda_m)

    where :math:`\lambda_i = 1/i, \forall i = 1, 2, \dots, m`
    
    - The diagonal matrix :math:`\Lambda \in \Re^{2m \times 2m}` weights 
      :math:`u` and :math:`w` to define the objective, and is given by

    .. math::

        \Lambda = \begin{bmatrix}D & 0 \\ 0 & D\end{bmatrix}

    and;

    - The entries in the vector :math:`b \in \Re^m` are all ones, i.e.:

    .. math:: b^T = \begin{pmatrix}1 & 1 & \dots & 1\end{pmatrix}.

This problem models a 2-discipline multi-disciplinary design optimization 
problem. The coupled state equation forms the equality constraint to the 
objective function. The parameter :math:`\mu` is used to control the strength 
of the coupling; if :math:`\mu = 0` the problem is decoupled, and if 
:math:`\mu = 1` the problem is strongly coupled. The parameter :math:`\alpha` 
is used to control the degree of nonlinearity; if :math:`\alpha = 0` the 
problem is linear, and if :math:`\alpha = 1` the problem is highly nonlinear.

We will implement this problem using both the **Multi-Disciplinary Feasible** 
(MDF) and the **Individual Discipline Feasible** (IDF) architectures.

The code comments and docstrings reference a further partitioned form of this
system as described below:

.. math::
    :nowrap:

    \begin{gather*}
    \mathbf{x} = \begin{pmatrix} x_1 \\ x_2 \end{pmatrix}
    \quad , \quad
    \mathbf{v} = \begin{pmatrix} u \\ v \end{pmatrix} \\
    C = \begin{bmatrix} H & 0 \\ 0 & H \end{bmatrix}
    \quad , \quad
    B(\mathbf{v}) = \begin{bmatrix} (1-\mu)A(u) & -\mu A(w) \\ 
    \mu A(u) & (1-\mu)A(w) \end{bmatrix}
    \quad , \quad
    \mathcal{F} = \begin{pmatrix} bx_1 \\ bx_2 \end{pmatrix} \\
    \mathrm{residual} = \mathcal{R} = CBC \mathbf{v} - \mathcal{F}
    \end{gather*}

The exact solution to this optimization problem is :math:`x = 0` regardless of 
the parameter values. The objective is convex with respect to :math:`u` and 
:math:`w`, and these state variables are zero only if :math:`x = 0`.

We start the optimizer at :math:`x_0^T = \begin{pmatrix}1 & 1\end{pmatrix}`.

.. automodule:: nonlinearMDF

.. currentmodule:: nonlinearMDF

.. autoclass:: NonLinearMDF
   :members:
   :show-inheritance: