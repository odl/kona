import os, sys
template_path = os.path.abspath('./../')
sys.path.append(template_path)

from ..usertemplate import UserTemplate
import numpy as np

class Rosenbrock(UserTemplate):
    """Objective function class for the multi-dimensional Rosenbrock function.

    Since this is a purely analytical problem with no non-linear system
    dependencies, there are no state variables or equality constraints.

    The only base methods that have been modified here are:

        - ``Rosenbrock.eval_obj()``

        - ``Rosenbrock.eval_grad_d()``

        - ``Rosenbrock.init_design()``

        - ``Rosenbrock.user_info()``

    This example represents the absolute bare minimum implementation necessary
    to optimize a simple problem with Kona's optimization tools.

    See source code for further details.
    """

    def eval_obj(self, at_design, at_state):
        # take the specified design point out of storage
        self.x = self.kona_design[at_design]
        # calculate the objective function
        self.calc_obj()
        # return the value
        return (self.obj, 0)

    def eval_grad_d(self, at_design, at_state, result):
        # take the specified design point out of storage
        self.x = self.kona_design[at_design]
        # calculate the gradient w.r.t. design variables at this point
        self.calc_grad()
        # store the gradient into the specified location
        self.kona_design[result] = self.grad_d

    def init_design(self, store):
        # pick a random point to start the optimization from
        self.x = 3.*np.random.rand(self.num_design)
        # store this initial design vector at the specified location
        self.kona_design[store] = self.x
        print self.x

    def user_info(self, curr_design, curr_state, curr_adj, num_iter):
        # print the current design vector
        # this function is called by Kona at every optimization iteration
        self.x = self.kona_design[curr_design]
        print self.x

    def calc_obj(self):
        self.obj = sum(100.0*(self.x[1:]-self.x[:-1]**2.0)**2.0 + 
            (1-self.x[:-1])**2.0)

    def calc_grad(self):
        xm = self.x[1:-1]
        xm_m1 = self.x[:-2]
        xm_p1 = self.x[2:]
        self.grad_d = np.zeros(self.num_design)
        self.grad_d[1:-1] = (200*(xm-xm_m1**2) - 400*(xm_p1 - xm**2)*xm 
            - 2*(1-xm))
        self.grad_d[0] = (-400*self.x[0]*(self.x[1]-self.x[0]**2) 
            - 2*(1-self.x[0]))
        self.grad_d[-1] = 200*(self.x[-1]-self.x[-2]**2)













