import numpy
import pykona
from mdoDiscipline import MDODiscipline
from scipy.sparse.linalg import LinearOperator
from pyamg.krylov._fgmres import fgmres as KrylovSolver

class MDO_MDF(pykona.UserTemplate):
    
    def configure(self, alpha, mu, nState, initDesign, cout = True):
        # check if number of design variables is even
        if len(initDesign)%2 != 0:
            raise ValueError('ERROR: Odd number of design variables!')
        # store the initial design point
        self.startFrom = initDesign
        # initialize the discipline solver
        self.solver = MDODiscipline(len(self.startFrom)/2, nState, alpha)
        # allocate optimization storage and sizing
        self.__init__(len(self.startFrom), 2*self.solver.nState)
        # store the coupling strength parameters
        self.mu = mu
        # preconditioner call counter for cost measurements
        self.precondCount = 0
        # info print flag
        self.cout = cout
        
    def dRdStateProd(self, state, vec):
        # split vectors into discipline components
        [u, w] = numpy.hsplit(state, 2)
        [Vu, Vw] = numpy.hsplit(vec, 2)
        # assemble the result
        outU = (1. - self.mu)*self.solver.dRdStateProd(u, Vu) \
                    - self.mu*self.solver.dRdStateProd(w, Vw)
        outW = self.mu*self.solver.dRdStateProd(u, Vu) \
                    +(1. - self.mu)*self.solver.dRdStateProd(w, Vw)
        # stack and return product
        return numpy.hstack((outU, outW))
        
    def dRdStateTransProd(self, state, vec):
        # split vectors into discipline components
        [u, w] = numpy.hsplit(state, 2)
        [Vu, Vw] = numpy.hsplit(vec, 2)
        # assemble the result
        outU = (1. - self.mu)*self.solver.dRdStateTransProd(u, Vu) \
                    + self.mu*self.solver.dRdStateTransProd(u, Vw)
        outW = - self.mu*self.solver.dRdStateTransProd(w, Vu) \
                    +(1. - self.mu)*self.solver.dRdStateTransProd(w, Vw)
        # stack and return product
        return numpy.hstack((outU, outW))
        
    def getResidual(self, design, state):
        # split state vector into discipline components
        [u, w] = numpy.hsplit(state, 2)
        [yu, yw] = numpy.hsplit(design, 2)
        # calculate the u component of the residual
        Ru = (1.-self.mu)*self.solver.resKernel(u) \
                - self.mu*self.solver.resKernel(w) \
                + self.solver.getB(yu)
        # calculate the w component of the residual
        Rw = self.mu*self.solver.resKernel(u) \
                +(1.-self.mu)*self.solver.resKernel(w) \
                + self.solver.getB(yw)
        # stack and return the result
        return numpy.hstack((Ru, Rw))
        
    def applyPrecond(self, state, vec):
        # split vectors into discipline components
        [u, w] = numpy.hsplit(state, 2)
        [Vu, Vw] = numpy.hsplit(vec, 2)
        # apply the SSOR preconditioner
        Vu_bar = self.solver.resDerivPrecondProd(u, 
            (1./(1.-self.mu))*Vu)
        b = Vw + self.mu*self.solver.dRdStateProd(w, Vu_bar)
        Vw_bar = self.solver.resDerivPrecondProd(w, 
            (1./(1.-self.mu))*b)
        b = Vu - self.mu*self.solver.dRdStateProd(u, Vw_bar)
        Vu_bar = self.solver.resDerivPrecondProd(u, 
            (1./(1.-self.mu))*b)
        # stack and return the result
        self.precondCount += 3
        return numpy.hstack((Vu_bar, Vw_bar))
        
    def applyTransPrecond(self, state, vec):
        # split vectors into discipline components
        [u, w] = numpy.hsplit(state, 2)
        [Vu, Vw] = numpy.hsplit(vec, 2)
        # apply the SSOR preconditioner
        Vu_bar = self.solver.resDerivTransPrecondProd(u, 
            (1./(1.-self.mu))*Vu)
        b = Vw + self.mu*self.solver.dRdStateTransProd(w, Vu_bar)
        Vw_bar = self.solver.resDerivTransPrecondProd(w, 
            (1./(1.-self.mu))*b)
        b = Vu - self.mu*self.solver.dRdStateTransProd(u, Vw_bar)
        Vu_bar = self.solver.resDerivTransPrecondProd(u, 
            (1./(1.-self.mu))*b)
        # stack and return the result
        self.precondCount += 3
        return numpy.hstack((Vu_bar, Vw_bar))
        
    def nonlinearSolve(self, design):
        # start with an initial guess
        cur_state = numpy.zeros(self.num_state)
        # calculate initial residual
        R = self.getResidual(design, cur_state)
        # print initial residual
        if self.cout: 
            print "iter = %i : L2 norm of residual = %e" % (0, numpy.linalg.norm(R))
        # mat-vec product for dR/dState
        dRdState = LinearOperator((self.num_state, self.num_state),
            matvec = lambda dState: self.dRdStateProd(cur_state, dState),
            dtype='float64')
        # mat-vec product for the ILU-based block-jacobi preconditioner
        precond = LinearOperator((self.num_state, self.num_state),
            matvec = lambda vec: self.applyPrecond(cur_state, vec),
            dtype='float64')
        # solution parameters
        max_iter = 100
        res_tol = 1.e-7
        i = 1
        converged = False
        # reset precond count for cost tracking
        self.precondCount = 0
        while i < max_iter:
            # solve linearized system
            dstate, info = KrylovSolver(dRdState, -R, M=precond)
            # update guess
            cur_state += dstate
            # update residual
            R = self.getResidual(design, cur_state)
            # print iteration information
            if self.cout: 
                print "iter = %i : L2 norm of residual = %e" % (i, numpy.linalg.norm(R))
            if info != 0:
                print "MDO_MDF.nonlinearSolve() >> GMRES failed!"
                break
            elif numpy.linalg.norm(R) <= res_tol: # check for convergence
                converged = True
                break
            else:
                i += 1
        # return the solution vector, flags for convergence and solution cost
        return cur_state, converged, self.precondCount
        
        
################################################################################
#####################            KONA FUNCTIONS            #####################
################################################################################

    def eval_obj(self, at_design, at_state):
        if at_state == -1:
            design = self.kona_design[at_design]
            state, converged, cost = self.nonlinearSolve(design)
            if not converged:
                cost = -cost
        else:
            # take the state vector out from storage
            state = self.kona_state[at_state]
            cost = 0
        # split the state vector into its discipline components
        [state_u, state_w] = numpy.hsplit(state, 2)
        # calculate the objective function
        obj_val = 0.5*numpy.trapz((state_u**2 + state_w**2), dx=self.solver.dx)       
        # return the value and cost of calculation as a tuple
        return (obj_val, cost)
        
    def eval_residual(self, at_design, at_state, result):
        # check the design and state vectors out of storage
        design = self.kona_design[at_design]
        state = self.kona_state[at_state]
        # store the result
        self.kona_state[result] = self.getResidual(design, state)
        
    def multiply_jac_d(self, at_design, at_state, vec, result):
        vec = self.kona_design[vec]
        [vecU, vecW] = numpy.hsplit(vec, 2)
        outU = self.solver.dBdDesignProd(vecU)
        outW = self.solver.dBdDesignProd(vecW)
        self.kona_state[result] = numpy.hstack((outU, outW))
        
    def multiply_jac_s(self, at_design, at_state, vec, result):
        # get the state vector from storage
        state = self.kona_state[at_state]
        # get the arbitrary vector
        vec = self.kona_state[vec]
        # store the result
        self.kona_state[result] = self.dRdStateProd(state, vec)
        
    def multiply_tjac_d(self, at_design, at_state, vec, result):
        vec = self.kona_state[vec]
        [Vu, Vw] = numpy.hsplit(vec, 2)
        outU = self.solver.dBdDesignTransProd(Vu)
        outW = self.solver.dBdDesignTransProd(Vw)
        self.kona_design[result] = numpy.hstack((outU, outW))
        
    def multiply_tjac_s(self, at_design, at_state, vec, result):
        # get the state vector from storage
        state = self.kona_state[at_state]
        # get the arbitrary vector
        vec = self.kona_state[vec]
        # store the result
        self.kona_state[result] = self.dRdStateTransProd(state, vec)
        
    def multiply_precond(self, at_design, at_state, vec, result):
        state = self.kona_state[at_state]
        vec = self.kona_state[vec]
        self.precondCount = 0
        self.kona_state[result] = self.applyPrecond(state, vec)
        return self.precondCount
        
    def multiply_tprecond(self, at_design, at_state, vec, result):
        state = self.kona_state[at_state]
        vec = self.kona_state[vec]
        self.precondCount = 0
        self.kona_state[result] = self.applyTransPrecond(state, vec)
        return self.precondCount
        
    def eval_grad_d(self, at_design, at_state, result):
        # for this problem, the gradient of the objective function w.r.t the 
        # design variables is zero
        self.kona_design[result] = numpy.zeros(self.num_design)
        
    def eval_grad_s(self, at_design, at_state, result):
        # take the state vector out from storage
        v = self.kona_state[at_state]   
        # split the state vector into its discipline components
        [u, w] = numpy.hsplit(v, 2)
        # calculate the two components of the gradient
        baseVec = numpy.ones(self.solver.nState)
        dJdu = self.solver.dx*u*baseVec
        dJdu[0] *= 0.5
        dJdu[-1] *= 0.5
        dJdw = self.solver.dx*w*baseVec
        dJdw[0] *= 0.5
        dJdw[-1] *= 0.5
        # merge the components and store the gradient at the specified index
        self.kona_state[result] = numpy.hstack((dJdu, dJdw))
        
    def init_design(self, store):
        self.kona_design[store] = self.startFrom
        
    def solve_system(self, at_design, result):
        # get the design vector from storage
        design = self.kona_design[at_design]
        # perform the non-linear solution
        state, converged, cost = self.nonlinearSolve(design)
        # check convergence
        if not converged:
            cost = -cost
        # store result and return solution cost
        self.kona_state[result] = state
        return cost
        
    def solve_linearsys(self, at_design, at_state, rhs, tol, result):
        # take the state vector out of storage
        state = self.kona_state[at_state]
        # get the RHS vector from storage
        rhs = self.kona_state[rhs]
        [rhsU, rhsW] = numpy.hsplit(rhs, 2)
        rhsU[0] = 0.
        rhsU[-1] = 0.
        rhsW[0] = 0.
        rhsW[-1] = 0.
        rhs = numpy.hstack((rhsU, rhsW))
        # mat-vec product for dR/dState
        dRdState = LinearOperator((self.num_state, self.num_state),
            matvec = lambda vec: self.dRdStateProd(state, vec),
            dtype='float64')
        # mat-vec product for the ILU-based block-jacobi preconditioner
        precond = LinearOperator((self.num_state, self.num_state),
            matvec = lambda vec: self.applyPrecond(state, vec),
            dtype='float64')
        # calculate tolerances
        abs_tol = 1.e-7
        rel_tol = abs_tol/numpy.linalg.norm(rhs)
        # calculate the solution and store them at the specified index
        self.precondCount = 0
        solution, info = KrylovSolver(dRdState, rhs, tol=rel_tol, M=precond)
        self.kona_state[result] = solution
        # check convergence and return cost
        if info == 0:
            return self.precondCount
        elif info < 0:
            raise TypeError("solve_linearsys() >> GMRES: Illegal input or breakdown!")
        else:
            print "solve_linearsys() >> GMRES: Failed with %i iterations" % info
            return -self.precondCount
    
    def solve_adjoint(self, at_design, at_state, rhs, tol, result):
        # take the design and state vectors out of storage
        state = self.kona_state[at_state]
        # if rhs index is negative, use -dJ/dState as the RHS vector
        if rhs < 0:
            # split the state vector into its discipline components
            [u, w] = numpy.hsplit(state, 2)
            # calculate the two components of the gradient
            baseVec = numpy.ones(self.solver.nState)
            dJdu = self.solver.dx*u*baseVec
            dJdu[0] *= 0.5
            dJdu[-1] *= 0.5
            dJdw = self.solver.dx*w*baseVec
            dJdw[0] *= 0.5
            dJdw[-1] *= 0.5
            # merge the components and store the gradient at the specified index
            rhs = -numpy.hstack((dJdu, dJdw))
        # otherwise use whatever rhs index is provided
        else:
            rhs = self.kona_state[rhs]
            [rhsU, rhsW] = numpy.hsplit(rhs, 2)
            rhsU[0] = 0.
            rhsU[-1] = 0.
            rhsW[0] = 0.
            rhsW[-1] = 0.
            rhs = numpy.hstack((rhsU, rhsW))
        # mat-vec product for dR/dState
        dRdStateTrans = LinearOperator((self.num_state, self.num_state),
            matvec = lambda vec: self.dRdStateTransProd(state, vec),
            dtype='float64')
        # mat-vec product for the ILU-based block-jacobi preconditioner
        precond = LinearOperator((self.num_state, self.num_state),
            matvec = lambda vec: self.applyTransPrecond(state, vec),
            dtype='float64')
        # calculate tolerances
        abs_tol = 1.e-7
        rel_tol = abs_tol/numpy.linalg.norm(rhs)
        # calculate the solution and store them at the specified index
        self.precondCount = 0
        solution, info = KrylovSolver(dRdStateTrans, rhs, tol=rel_tol, M=precond)
        self.kona_state[result] = solution
        # check convergence and return cost
        if info == 0:
            return self.precondCount
        elif info < 0:
            raise TypeError("solve_adjoint() >> GMRES: Illegal input or breakdown!")
        else:
            print "solve_adjoint() >> GMRES: Failed with %i iterations" % info
            return -self.precondCount
            
    def user_info(self, curr_design, curr_state, curr_adj, curr_dual, num_iter):
        self.current_design = self.kona_design[curr_design]
        self.current_state = self.kona_state[curr_state]
        if self.cout:
            print self.current_design
    
    def axpby_s(self, a, vec1, b, vec2, result):
        if vec1 == -1:
            if vec2 == -1: # if indexes for both vectors are -1
                # answer is a vector of ones scaled by a
                out = a*numpy.ones(self.num_state)
            else: # if only the index for vec1 is -1
                # answer is vec2 scaled by b
                if b == 0.:
                    out = numpy.zeros(self.num_state)
                else:
                    out = b*self.kona_state[vec2]
        elif vec2 == -1: # if only the index for vec2 is -1
            # answer is vec1 scaled by a
            if a == 0.:
                out = numpy.zeros(self.num_state)
            else:
                out = a*self.kona_state[vec1]
        else:
            # otherwise perform the full a*vec1 + b*vec2 operation
            if a == 0.:
                if b == 0.:
                    out = self.zeros(self.num_state)
                else:
                    out = b*self.kona_state[vec2]
            else:
                if b == 0.:
                    out = a*self.kona_state[vec1]
                else:
                    out = a*self.kona_state[vec1] + b*self.kona_state[vec2]
        # write the result into the designated location
        self.kona_state[result] = out
        
    def inner_prod_s(self, vec1, vec2):
        return numpy.inner(self.kona_state[vec1], self.kona_state[vec2])