import matplotlib
matplotlib.use('Agg')

import os, sys
template_path = os.path.abspath('..')
sys.path.append(template_path)

from usertemplate import UserTemplate

import numpy as np
import pickle
import time

from scipy.sparse import * 
from scipy.sparse.linalg import spsolve

from scipy.sparse.linalg import LinearOperator
from pyamg.krylov._cg import cg  
from pyamg import *
from pyamg.util.linalg import norm

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import pdb


class inversePoisson(UserTemplate):

	class level(object):

		def __init__(self, Nx, Ny):
			self.nx = Nx + 2
			self.ny = Ny + 2

			self.Nx = Nx    # number of interior points
			self.Ny = Ny

			self.hx = 1./(Nx+1)
			self.hy = 1./(Ny+1)

			Dh2 = 2*eye(self.Nx, self.Nx, dtype=np.int8) + \
				(-1)*eye(self.Nx, self.Nx, k=1, dtype=np.int8) + \
				(-1)*eye(self.Nx, self.Nx, k=-1, dtype=np.int8)

			I2 = eye(self.Ny, self.Ny, dtype=np.int8)
			I1 = eye(self.Nx, self.Nx, dtype=np.int8)

			self.K = 1./(self.hx**2) * kron(I2, Dh2) + 1./(self.hy**2) * kron(Dh2, I1)		

			self.L = tril(self.K).tocsr()
			self.U = triu(self.K, k=1).tocsr()


			#--------------------------------------#

			self.odd = (self.Ny + 1)/2                      # red
			self.even = (self.Ny - 1)/2                     # black 

			I_red_11 = 2*eye(self.odd, self.odd, dtype=np.int8)            # red - self.odd A11

			C_red_21 = (-1)*eye(self.even, self.odd, dtype=np.int8) + \
				(-1)*eye(self.even, self.odd, k=1, dtype=np.int8)            # A21      

			I_black_22 = 2*eye(self.even, self.even, dtype=np.int8)        # A22

			C_black_12 = (-1)*eye(self.odd, self.even, dtype=np.int8) + \
				(-1)*eye(self.odd, self.even, k=-1, dtype=np.int8)            # A12 

			K_2 = bmat([[I_red_11, C_black_12],[C_red_21, I_black_22]])

			# pdb.set_trace()

			self.K_RB = 1./(self.hx**2) * kron(I2, Dh2) + 1./(self.hy**2) * kron(K_2, I1)

			self.K_RB_11 = self.K_RB[:self.odd*self.Nx, :self.odd*self.Nx]
			self.K_RB_12 = self.K_RB[:self.odd*self.Nx, self.odd*self.Nx:]
			self.K_RB_21 = self.K_RB[self.odd*self.Nx:, :self.odd*self.Nx]
			self.K_RB_22 = self.K_RB[self.odd*self.Nx:,self.odd*self.Nx:]



	def setup(self, nx, ny, nlevel, x_control, T_control, init_array):
	
		""" 
		Attributes: 
		----------------
		nlevel : how many levels for the MG cycle 
		nx     : the total grid points on x axis, b.c. included
		ny     : the total grid points on y axis, b.c. included
		
		vi, mu, RB are fixed here using preliminarily selected optimal settings
		--------------------
		vi : 2, number of iterations for smoother 
		mu : mu = 1 being V cycle, mu = 2 being W cycle 
		RB : RB = 1 using Red-Black Zebra line alternative iteration 

		V(0,2)  :  V cycle, 0 presmoothing iterations, 2 postsmoothing iterations
				   postsmoothing using Red-Black Gauss-Seidel iteration			
		""" 

		#------------- fixed settings --------------%
		self.__init__(nx-2, (nx-2)*(ny-2)) 

		self.vi = 2
		self.mu = 1 
		self.RB = 1
		self.Nx = nx-2
		self.Ny = ny-2
		self.hx = 1./(self.Nx+1)
		self.hy = 1./(self.Ny+1)

		#---------- others ---------# 
		self.start_from = init_array
		self.des_info = True
		self.solve_info = True
		self.solve_calls = 0
		self.precond_calls = 0
		self.Kvcount = 0	
		self.precond_calls_total = 0

		self.time_solve_system = []
		self.time_solve_adjoint = []
		self.time_solve_linsys = []
		self.time_solve_state = []

		self.time_kona_precond = []
		self.time_kona_jacS = []


		#------------- Setting up Multi level info -----------%
		self.nlevel = nlevel      # 4 in this case

		self.levels = []
		cur_Nx = nx-2
		cur_Ny = ny-2

		self.levels.append(inversePoisson.level(cur_Nx, cur_Ny))       # the finest level

		for i in range(nlevel-1):

			print "i: ", i
			cur_Nx = (cur_Nx + 1)/2 - 1 
			cur_Ny = (cur_Ny + 1)/2 - 1 

			if ((cur_Nx <= 3) or (cur_Ny <= 3)):
				print "level at ", i+2, "is too coarse, will not be considered" 
				break

			self.levels.append(inversePoisson.level(cur_Nx, cur_Ny))

		#--------------------Storing target----------------
		setup_start = time.time()

		self.x_control = x_control
		self.T_control = T_control

		self.error_norm = []

		T, converged = self.solve_state(self.T_control)

		self.plot_field(nx, ny, T_control, T)

		# pdb.set_trace()
		self.setup_time = time.time() - setup_start

		if converged:
			self.T_target = T
			print "Number of Preconditioner Calls for Initial Target T field: ", self.precond_calls
			self.setup_precond = self.precond_calls

		else: 
			print "solve state not converged! setup"


	def add_solves(self):
		self.solve_calls += 1

	def add_precond(self):
		self.precond_calls += 1
		self.precond_calls_total += 1

	def reset_precond(self):
		self.precond_calls = 0

	def reset_solves(self):
		self.solve_calls = 0

	def init_design(self, store):

		# self.kona_design[store] = np.random.rand(self.num_design)
		self.kona_design[store] = self.start_from


	def apply_Kv(self, v):
		"""As K has already been built for each level, one can directly use 
		K.dot(v) -- capable of each level's 
		or still using matrix-free matrix vector product -- only for the finest level """

		# return self.levels[il].K.dot(v)
		##-------------------------------
		self.Kvcount += 1	

		v_mat = v.reshape((self.Ny, self.Nx))
		Dh1v = np.zeros((self.Ny, self.Nx))

		for k in np.arange(self.Ny):
			Dh1v[k,] = self.apply_Dh1_v(v_mat[k,])

		Dh1v = Dh1v.flatten() 

		Dh2v = np.zeros((self.Ny, self.Nx))
		Dh2v[0,] = 2*v_mat[0,] - v_mat[1,]  
		Dh2v[-1,] = -v_mat[-2,] + 2*v_mat[-1,]
		Dh2v[1:-1,] = -v_mat[:-2, ] + 2*v_mat[1:-1,] - v_mat[2:,]
		Dh2v = Dh2v.flatten()

		Kv = 1./(self.hx**2)*Dh1v + 1./(self.hy**2)*Dh2v
		return Kv

	def apply_Dh1_v(self, v):

		Dh1v_sub = np.zeros(len(v))

		Dh1v_sub[0] = 2*v[0] - v[1]
		Dh1v_sub[-1] = -v[-2] + 2*v[-1]
		Dh1v_sub[1:-1] = -v[:-2] + 2*v[1:-1] - v[2:]

		return Dh1v_sub

	def apply_Kdotv(self, v):
		pass

	def apply_KTv(self, v):

		self.apply_Kv(v)    # because K is symmetrical in this case


	def solve_state(self, design): 

		#----------------
		start_state = time.time()
		#----------------
		
		if len(design) == (self.Nx*self.Ny):
			b = design
		else: 
			b = np.zeros(self.Ny*self.Nx)
			b[:self.Nx] = (1./(self.hy)**2) * design

		self.reset_precond()

		Kv = LinearOperator((self.Nx*self.Ny, self.Nx*self.Ny), 
			matvec = lambda v: self.apply_Kv(v), dtype='float64')

		P = LinearOperator((self.Nx*self.Ny, self.Nx*self.Ny),
			matvec = lambda v: self.apply_precond(self.nlevel,v), dtype='float64')

		max_iter = self.Nx*self.Ny
		converged = False
		res_tol = 1.e-5
		
		(T, info) = cg(Kv, b, M=P, maxiter=max_iter, tol = 1.e-12)     # 
		self.add_solves()
		R = self.apply_Kv(T) - b

		#-----------------
		state_period = time.time() - start_state
		self.time_solve_state.append(state_period)
		#-----------------

		if info == 0:
			converged = True

			# if norm(R) <= res_tol: 
			# 	# print "Solving_state L2 norm of residual = %e" % (norm(R))
			# else: 
			# 	print "Reduce convergence condition!! L2 norm of residual = %e" % (norm(R))			
			if norm(R) > res_tol: 
				print "Reduce convergence condition!! L2 norm of residual = %e" % (norm(R))			
		else:
			raise TypeError("solve_state() >> CG failed!")

		return T, converged


	def apply_precond(self, ilevel, b):    
		# print "------ apply_precond ----- called!!! "
		self.add_precond()

		x = self.V(ilevel, b)
		return x

	def V(self, ilevel, b):

		if ( ilevel == 1 ):
			x = self.solve_state_direct(b, -1)
			return x
			
		else:	
		
			il = self.nlevel - ilevel
			# x = self.gauss(np.zeros(b.shape), b, il)
			# r = b - self.apply_Kv(x, il)

			r = b 
			r_c = self.restriction(r, il)     
			
			for i in range(self.mu):
				e_c = self.V(ilevel-1, r_c)   # The name here need change
	
			e = self.interpolation(e_c, il)	      
			x = self.gauss(e, b,  il)       
			return x
				

	def multiply_precond(self, at_design, at_state, vec, result):

		start_precond = time.time()


		x = self.kona_design[at_design]

		b_res = np.zeros(self.Ny * self.Nx)
		b_res[:self.Nx] =  (1./(self.hy)**2) * x 

		z = self.kona_state[vec]

		self.reset_precond()
		z_hat = self.apply_precond(self.nlevel, z)

		self.kona_state[result] = z_hat


		period = time.time()-start_precond
		self.time_kona_precond.append(period)

		return self.precond_calls

	def multiply_tprecond(self, at_design, at_state, vec, result):
		cost = self.multiply_precond(at_design, at_state, vec, result)
		return cost


	def eval_obj(self, at_design, at_state):
		
		if at_state == -1:
			x = self.kona_design[at_design]
			T, converged = self.solve_state(x)

			if not converged:
				precond = -self.precond_calls
			else: 
				precond = self.precond_calls

		else:
			T = self.kona_state[at_state]
			precond = 0

		diff_T = (T - self.T_target) * (self.hx * self.hy)
		self.obj = diff_T.dot(diff_T)

		print "current obj:", self.obj
		return (self.obj, precond)


	def eval_grad_d(self, at_design, at_state, result):
		self.kona_design[result] = np.zeros(self.Nx)


	def eval_grad_s(self, at_design, at_state, result):
		T = self.kona_state[at_state]
		diff_T = (T - self.T_target) * (self.hx * self.hy)

		self.kona_state[result] = 2*diff_T*(self.hx * self.hy)

	def solve_system(self, at_design, result):

		#----- tic -----
		startTime = time.time()

		#---------------
		x = self.kona_design[at_design]
		T, converged = self.solve_state(x)	
		self.kona_state[result] = T	

		#------ toc ------
		elapsedTime = time.time() - startTime 
		self.time_solve_system.append(elapsedTime)


		if converged:			
			if self.solve_info:
				print "System solve CONVERGED!"
			return self.precond_calls
		else:
			print "System solve FAILED!"
			return -self.precond_calls			
			
	def eval_residual(self, at_design, at_state, result):

		x = self.kona_design[at_design]
		T = self.kona_state[at_state]

		b_res = np.zeros(self.Ny * self.Nx)
		b_res[:self.Nx] =  (1./(self.hy)**2) * x 

		self.kona_state[result] = self.apply_Kv(T) - b_res


	def multiply_jac_d(self, at_design, at_state, vec, result):

		z = self.kona_design[vec]

		temp = np.zeros(self.Ny*self.Nx)
		temp[:self.Nx] = (-1.)/((self.hy)**2) * z

		self.kona_state[result] = temp 

	def multiply_jac_s(self, at_design, at_state, vec, result):

		start_jacs = time.time()


		z = self.kona_state[vec]
		self.kona_state[result] = self.apply_Kv(z)

		period = time.time() - start_jacs
		self.time_kona_jacS.append(period)

	def multiply_tjac_d(self, at_design, at_state, vec, result):
		
		z = self.kona_state[vec]
		self.kona_design[result] = (-1.)/((self.hy)**2) * z[:self.Nx]

	def multiply_tjac_s(self, at_design, at_state, vec, result):

		start_jacs = time.time()

		z = self.kona_state[vec]
		self.kona_state[result] = self.apply_Kv(z)

		period = time.time() - start_jacs
		self.time_kona_jacS.append(period)

	def solve_linearsys(self, at_design, at_state, rhs, tol, result):

		startLinsys = time.time()

		#-----------------
		b_rhs = self.kona_state[rhs]

		T, converged = self.solve_state(b_rhs)
		self.kona_state[result] = T

		#-------------
		linsys_period = time.time() - startLinsys
		self.time_solve_linsys.append(linsys_period)
		#-------------

		if converged:
			return self.precond_calls
		else:
			print "solve_state not converged, solve_linearsys!"
			return -self.precond_calls

	def solve_adjoint(self, at_design, at_state, rhs, tol, result):

		startAdjoint = time.time()

		if rhs == -1:
			T = self.kona_state[at_state]
			diff_T = (T - self.T_target) * (self.hx * self.hy)
			b_adj = ((-2.))*(diff_T) * (self.hx * self.hy)

		else:
			b_adj = self.kona_state[rhs]
		
		T, converged = self.solve_state(b_adj)
		self.kona_state[result] = T

		#---------------
		adjoint_period = time.time() - startAdjoint
		self.time_solve_adjoint.append(adjoint_period)
		#---------------

		if converged:
			return self.precond_calls			
		else:
			return -self.precond_calls

	def user_info(self, curr_design, curr_state, curr_adj, num_iter, xab):
		# print the current design and state vectors at each iteration
		self.x = self.kona_design[curr_design]
		self.T  = self.kona_state[curr_state]

		diff_T = (self.T - self.T_target) * (self.hx * self.hy)
		error_norm = diff_T.dot(diff_T)

		self.error_norm.append(error_norm)


	##### From here is the MultiGrid Preconditioners' aiding routines ####	
	def restriction(self, inV, il):
		res_bc = np.zeros((self.levels[il].ny, self.levels[il].nx))
		res_bc[1:-1, 1:-1] = inV.reshape(self.levels[il].Ny, self.levels[il].Nx)

		out_mat = np.zeros((self.levels[il+1].Ny, self.levels[il+1].Nx))

		for i in np.arange(self.levels[il+1].Ny)+1: 
			for j in np.arange(self.levels[il+1].Nx)+1: 
			
				out_mat[i-1, j-1] = ( res_bc[2*i-1, 2*j-1] + res_bc[2*i-1, 2*j+1] + \
					res_bc[2*i+1, 2*j-1] + res_bc[2*i+1, 2*j+1] + \
					2*(res_bc[2*i, 2*j-1] + res_bc[2*i, 2*j+1] + res_bc[2*i-1, 2*j] + res_bc[2*i+1, 2*j] ) + \
					4*res_bc[2*i, 2*j])/16 

		return out_mat.flatten()

	def interpolation(self, inV, il):

		in_mat = np.zeros((self.levels[il+1].Ny+2, self.levels[il+1].Nx+2))
		in_mat[1:-1, 1:-1] = inV.reshape((self.levels[il+1].Ny, self.levels[il+1].Nx))

		out_mat = np.zeros((self.levels[il].ny, self.levels[il].nx))

		for i in np.arange(self.levels[il+1].Ny+1):
			for j in np.arange(self.levels[il+1].Nx+1):
				# print "current i, j:", i, j
				out_mat[2*i, 2*j] = in_mat[i,j]
				out_mat[2*i+1, 2*j] = (in_mat[i,j] + in_mat[i+1,j])/2
				out_mat[2*i, 2*j+1] = (in_mat[i,j] + in_mat[i,j+1])/2
				out_mat[2*i+1, 2*j+1] = (in_mat[i,j] + in_mat[i+1,j] + in_mat[i,j+1] + in_mat[i+1,j+1])/4

		return out_mat[1:-1, 1:-1].flatten()

	def gauss(self, x, b, il):

		"""
		Attributes: 
		--------------- 
		RB  :  0  -  Gauss-Seidel   regular, (D+L)^-1 x_new = -U x_old + f
		       1  -  Gauss-Seidel   red-black, horizontal zebra line alternately 


		"""
		if (self.RB == 0): 
			for i in range(self.vi):
				# print "current Gauss i: ", i
				x = spsolve(self.levels[il].L, b - self.levels[il].U.dot(x))

		elif (self.RB == 1): 

			for i in range(self.vi):
				x_mat = x.reshape((self.levels[il].Ny, self.levels[il].Nx))
				b_mat = b.reshape((self.levels[il].Ny, self.levels[il].Nx))
				

				x_red = x_mat[::2].flatten()               # red_odd
				x_black = x_mat[1::2].flatten()            # black_self.even

				b_red = b_mat[::2].flatten()
				b_black = b_mat[1::2].flatten()

				for i in range(self.vi):

					x_red = spsolve(self.levels[il].K_RB_11, b_red-self.levels[il].K_RB_12.dot(x_black))
					x_black = spsolve(self.levels[il].K_RB_22, b_black-self.levels[il].K_RB_21.dot(x_red))

				x_red = x_red.reshape((self.levels[il].odd, self.levels[il].Nx))
				x_black = x_black.reshape((self.levels[il].even, self.levels[il].Nx))

				x_mat[::2] = x_red
				x_mat[1::2] = x_black

				x = x_mat.flatten()

		return x	 

	def solve_state_direct(self, b_rhs, il):		
		T = spsolve(self.levels[il].K, b_rhs)
		return T

	###################################################	
	def plot_field(self, nx, ny, T_control, T_field):

		Ny = ny - 2
		Nx = nx - 2 

		T = np.zeros((ny,nx))
		T[0, 1:-1] = T_control
		T[1:-1, 1:-1] = T_field.reshape((Ny, Nx))

		X = np.linspace(0.0, 1.0, num=nx)
		Y = np.linspace(0.0, 1.0, num=ny)  
		X, Y = np.meshgrid(X, Y)

		fig = plt.figure()
		ax1 = fig.add_subplot(1,1,1,projection='3d')
		surf1 = ax1.plot_surface(X, Y, T, rstride=1, cstride=1, cmap=cm.coolwarm,
			linewidth=0, antialiased=False)
		plt.xlabel('X')
		plt.ylabel('Y')
		# plt.title('Target T')
		ax1.set_zlim(0, 0.3)
		plt.colorbar(surf1, shrink=0.5, aspect=5)
		ax1.view_init(30,45)
		plt.savefig('target.eps')


	def axpby_s(self, a, vec1, b, vec2, result):
		"""See ``axpby_d``. Perform the same tasks for vectors of size
		``self.num_state``.
		"""
		if vec1 == -1:
		    if vec2 == -1: # if indexes for both vectors are -1
		        # answer is a vector of ones scaled by a
		        out = a*np.ones(self.num_state)
		    else: # if only the index for vec1 is -1
		        # answer is vec2 scaled by b
		        if b == 0.:
		            out = np.zeros(self.num_state)
		        else:
		            out = b*self.kona_state[vec2]
		elif vec2 == -1: # if only the index for vec2 is -1
		    # answer is vec1 scaled by a
		    if a == 0.:
		        out = np.zeros(self.num_state)
		    else:
		        out = a*self.kona_state[vec1]
		else:
		    # otherwise perform the full a*vec1 + b*vec2 operation
		    if a == 0.:
		        if b == 0.:
		            out = self.zeros(self.num_design)
		        else:
		            out = b*self.kona_state[vec2]
		    else:
		        if b == 0.:
		            out = a*self.kona_state[vec1]
		        else:
		            out = a*self.kona_state[vec1] + b*self.kona_state[vec2]
		# write the result into the designated location
		self.kona_state[result] = out

	def inner_prod_s(self, vec1, vec2):
		"""See ``inner_prod_d``. Must return ``0.0`` when not implemented.
		"""
		return np.inner(self.kona_state[vec1], self.kona_state[vec2])