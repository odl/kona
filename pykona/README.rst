:Author: Alp Dener <alp@dener.me>

Introduction
============

The Kona library implements a set of algorithms for PDE-constrained optimization
problems. It is being developed in C++ by Prof. Jason E. Hicken at Rensselaer
Polytechnic Institute.

PyKona hopes to provide a Python wrapper for the Kona library and present, through
analytic examples, a standardized way of performing optimization via Kona on
problems defined in Python instead of C++.

More information can be found in:

* `Online documentation <http://odl.bitbucket.org/PyKona>`_
* `PDF User Manual <https://bitbucket.org/odl/odl.bitbucket.org/src/bcea21a0f3389abff6383bdc3306c9a2ef91c95d/PyKona/user_manual.pdf?at=master>`_

Dependencies
------------ 

* Python 2.7 <https://www.python.org/download/releases/2.7/>
* NumPy <http://www.numpy.org/>
* NoseTest <https://nose.readthedocs.org/>
* Kona <https://bitbucket.org/odl/kona>
* Boost.Python <http://www.boost.org>

License
------- 

PyKona is free software: you can redistribute it and/or modify it under the terms
of the GNU Lesser General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

PyKona is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

A copy of the GNU Lesser General Public License can be found at
<http://www.gnu.org/licenses/>.

Tests
------------------
To run a specific test file: 

    python <name_of_test_file>.py

To run all tests, from the top level of the repository: 

    nosetests 
