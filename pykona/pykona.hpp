#include <Python.h>
#include <boost/python.hpp>
#include <boost/smart_ptr/shared_ptr.hpp>

#include "../src/user_memory.hpp"
#include "../src/kona.hpp"

#include <ostream>
#include <iostream>
#include <fstream>
#include <sstream>

namespace bp = boost::python;

/*!
 * \class PyKona
 * \brief Boost::Python extensible C++ object that bridges Kona into Python
 */
class PyKona 
{
public:

    /*!
     * \brief default constructor
     */
    PyKona() {};

    ~PyKona() { ///< class destructor
        solver_.~object();
    }; 

    /*!
     * \brief configure Kona
     * \param[in] optns -- Python dictionary containing Kona config options
     */
    static void Configure(bp::object pyoptns);

    /*!
     * \brief perform the optimization
     * \param[in] solver -- a Python instance of the user created problem class
     */
    static void Run(bp::object solver);
    
private:

    /*!
     * \brief Kona toolbox of optimization tasks
     * \param[in] request - operation request integer from Kona
     * \param[in] leniwrk - length of the Kona iwrk array
     * \param[in] iwrk - array of indices for UserVector memory
     * \param[in] lendwork - length of the Kona dwrk array
     * \param[in,out] dwrk - work array for passing and returning scalar values
     * \returns completion status of the requested operation
     */
    static int Toolbox(int request, int leniwrk, int *iwrk, int lendwrk, 
        double *dwrk);

    static bp::object solver_; ///< Python problem object
};